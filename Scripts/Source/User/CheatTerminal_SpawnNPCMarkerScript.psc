Scriptname CheatTerminal_SpawnNPCMarkerScript extends ObjectReference Conditional

Actor NPC

Event OnInit()
	if ( CheatTerminalGlobalFuncs.GetNPCSpawnActor() == none )
		CheatTerminal_SpawnNPC_MissingNPC.Show()
		return
	endIf
	NPC = Self.PlaceAtMe(CheatTerminalGlobalFuncs.GetNPCSpawnActor(), 1, false, true, false) as Actor
	NPC.RemoveFromAllFactions()
	CheatTerminalGlobalFuncs.SpawnNPCExplosion(NPC)
	if ( CheatTerminal_SpawnNPC_SetCompanion.GetValue() < 2 )
		NPC.SetRelationshipRank(Game.GetPlayer(), 4)
		NPC.SetEssential(true)
		NPC.AddToFaction(PlayerFaction)
		NPC.SetValue(Health, 1000000)
		NPC.SetPlayerTeammate(true, true, false)
		if ( CheatTerminal_SpawnNPC_SetCompanion.GetValue() == 1 )
			CheatTerminalGlobalFuncs.DismissCustomCompanion()
			NPC.AllowCompanion(true, true)
			NPC.AddKeyword(CheatTerminal_MarkCompanion)
		endIf
	else
		NPC.SetRelationshipRank(Game.GetPlayer(), -4)
	endIf
	NPC.Enable(true)
	NPC.SetAngle(0, 0, NPC.GetAngleZ()) ;We need to remove the rotation, if any, so they don't moonwalk.
	NPC.MoveTo(self)
	if ( CheatTerminal_SpawnNPC_SetCompanion.GetValue() < 2 )
		if ( CheatTerminal_SpawnNPC_SetCompanion.GetValue() == 0 )
			StartTimer(CheatTerminal_SpawnNPC_Duration.GetValue())
		endIf
		NPC.AddKeyword(PowerArmorPreventArmorDamageKeyword)
		AddInventoryEventFilter(ObjectTypeAmmo)
		AddInventoryEventFilter(AmmoFusionCore)
		AddInventoryEventFilter(AmmoGammaCell)
		AddInventoryEventFilter(AmmoAlienBlaster)
		RegisterForRemoteEvent(NPC, "OnItemRemoved")
		RegisterForRemoteEvent(NPC, "OnCombatStateChanged")
	else
		NPC.StartCombat(Game.GetPlayer(), true)
	endIf
EndEvent

Event Actor.OnCombatStateChanged(Actor akSender, Actor akTarget, int aeCombatState)
	if ( akSender == NPC && akTarget == Game.GetPlayer() )
		NPC.StopCombat()
		NPC.SetRelationshipRank(Game.GetPlayer(), 4)
		NPC.SetPlayerTeammate(true, true, false)
	endIf
EndEvent

Event ObjectReference.OnItemRemoved(ObjectReference akSource, Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akDestContainer)
	if ( !Utility.IsInMenuMode() )
		akSource.AddItem(akBaseItem, aiItemCount, true)
	endIf
EndEvent

Event OnTimer(int aiTimerID)
	CheatTerminalGlobalFuncs.SpawnLeaveNPCExplosion(NPC)
	UnregisterForRemoteEvent(NPC, "OnItemRemoved")
	UnregisterForRemoteEvent(NPC, "OnCombatStateChanged")
	NPC.Disable()
	NPC.Delete()
	self.Delete()
EndEvent

Keyword Property PowerArmorPreventArmorDamageKeyword Auto Const
Keyword Property CheatTerminal_MarkCompanion Auto Const
Keyword Property ObjectTypeAmmo Auto Const
Ammo Property AmmoFusionCore Auto Const
Ammo Property AmmoGammaCell Auto Const
Ammo Property AmmoAlienBlaster Auto Const
Message Property CheatTerminal_SpawnNPC_MissingNPC Auto Const
Faction Property PlayerFaction Auto Const
ActorValue Property Health Auto Const
GlobalVariable Property CheatTerminal_SpawnNPC_Duration Auto Const
GlobalVariable Property CheatTerminal_SpawnNPC_SetCompanion Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const