Scriptname UnlockNearestTerminalMagicEffect extends activemagiceffect

Event OnEffectStart(Actor akTarget, Actor akCaster)
	ObjectReference nearestTerminal = Game.FindClosestReferenceOfAnyTypeInListFromRef(CheatTerminal_AllTerminals, Game.GetPlayer(), 200)
	if ( nearestTerminal )
		nearestTerminal.Unlock()
	endIf
EndEvent

FormList Property CheatTerminal_AllTerminals Auto Const