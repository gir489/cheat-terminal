Scriptname CheatTerminalRaceMenuScript extends ActiveMagicEffect

Import ScriptObject

Event OnEffectStart(Actor akTarget, Actor akCaster)
	While (Utility.IsInMenuMode())
		Utility.Wait(0.5)
	EndWhile
	RegisterForMenuOpenCloseEvent("LooksMenu")
	Game.ForceThirdPerson()
	Utility.Wait(0.1)
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.IsWeaponDrawn() )
		Weapon curWeapon = playerActor.GetEquippedWeapon()
		playerActor.PlayIdle(RaiderSheath)
	endIf
	playerActor.PlayIdle(ElevatorFaceCamera)
	Game.ShowRaceMenu(uimode = 1)
EndEvent

Event OnMenuOpenCloseEvent(string asMenuName, bool abOpening)
	If (asMenuName == "LooksMenu" && abOpening == false )
		Game.ForceFirstPerson()
		Dispel()
	EndIf
EndEvent

Idle Property RaiderSheath Auto Const
Idle Property ElevatorFaceCamera Auto Const
Keyword Property WeaponTypeMelee1H Auto Const
Keyword Property WeaponTypeMelee2H Auto Const
