Scriptname InfiniteGrenadeQuest extends Quest

event ObjectReference.OnItemRemoved(ObjectReference akSource, Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)
	if ( !Utility.IsInMenuMode() )
		akSource.AddItem(akBaseItem, aiItemCount, true)
	endif
endEvent

Event OnQuestInit()
	AddInventoryEventFilter(WeaponTypeCryoGrenade)
	AddInventoryEventFilter(WeaponTypeGrenade)
	AddInventoryEventFilter(WeaponTypeNukaGrenade)
	AddInventoryEventFilter(WeaponTypePlasmaGrenade)
	AddInventoryEventFilter(WeaponTypePulseGrenade)
	AddInventoryEventFilter(WeaponTypeBottlecapMine)
	AddInventoryEventFilter(WeaponTypeCryoMine)
	AddInventoryEventFilter(WeaponTypeNukaMine)
	AddInventoryEventFilter(WeaponTypePlasmaMine)
	AddInventoryEventFilter(WeaponTypePulseMine)
	RegisterForRemoteEvent(Game.GetPlayer(), "OnItemRemoved")
EndEvent

Event OnQuestShutdown()
	RemoveAllInventoryEventFilters()
	UnregisterForAllRemoteEvents()
EndEvent

Keyword Property WeaponTypeCryoGrenade Auto Const
Keyword Property WeaponTypeGrenade Auto Const
Keyword Property WeaponTypeNukaGrenade Auto Const
Keyword Property WeaponTypePlasmaGrenade Auto Const
Keyword Property WeaponTypePulseGrenade Auto Const
Keyword Property WeaponTypeBottlecapMine Auto Const
Keyword Property WeaponTypeCryoMine Auto Const
Keyword Property WeaponTypeNukaMine Auto Const
Keyword Property WeaponTypePlasmaMine Auto Const
Keyword Property WeaponTypePulseMine Auto Const