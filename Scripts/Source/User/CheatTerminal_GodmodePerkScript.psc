Scriptname CheatTerminal_GodmodePerkScript extends Quest

Event OnQuestInit()
	RegisterForHitEvent(Player)
	Player.AddKeyword(PowerArmorPreventArmorDamageKeyword)
EndEvent

Event OnHit(ObjectReference akTarget, ObjectReference akAggressor, Form akSource, Projectile akProjectile, bool abPowerAttack, bool abSneakAttack, bool abBashAttack, bool abHitBlocked, string apMaterial)
	Game.GetPlayer().RestoreValue(Health, 999999)
	RegisterForHitEvent(Player)
EndEvent

Event OnQuestShutdown()
	UnRegisterForHitEvent(Player)
	Player.RemoveKeyword(PowerArmorPreventArmorDamageKeyword)
EndEvent

ActorValue Property Health Auto Const
Actor Property Player Auto Const
Keyword Property PowerArmorPreventArmorDamageKeyword Auto Const