Scriptname UnlockNearestDoorMagicEffect extends activemagiceffect

Event OnEffectStart(Actor akTarget, Actor akCaster)
	ObjectReference nearestDoor = Game.FindClosestReferenceOfAnyTypeInListFromRef(CheatTerminal_AllDoors, Game.GetPlayer(), 200)
	if ( nearestDoor )
		nearestDoor.Unlock()
		nearestDoor.Activate(Game.GetPlayer())
	endIf
EndEvent

FormList Property CheatTerminal_AllDoors Auto Const