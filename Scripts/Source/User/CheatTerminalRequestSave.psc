Scriptname CheatTerminalRequestSave extends ActiveMagicEffect

Event OnEffectStart(Actor akTarget, Actor akCaster)
	While (Utility.IsInMenuMode())
		Utility.Wait(0.1)
	EndWhile
	CheatTerminal_Saving.Show()
	Utility.Wait(0.5)
	Game.RequestSave()
EndEvent

Message Property CheatTerminal_Saving Auto Const