Scriptname TheMerchantEffectScript extends activemagiceffect

Event OnEffectStart(Actor akTarget, Actor akCaster)
	While (Utility.IsInMenuMode())
		Utility.Wait(0.5)
	EndWhile
	Utility.Wait(0.5)
	TheMerchant.ShowBarterMenu()
endEvent

Actor Property TheMerchant Auto Const