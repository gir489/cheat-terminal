Scriptname CheatTerminalPersistSurvivor extends Quest

Event OnQuestInit()
   RegisterForRemoteEvent(Game.GetPlayer(), "OnPlayerLoadGame")
   RegisterForRemoteEvent(Game.GetPlayer(), "OnDifficultyChanged")
EndEvent

Event OnQuestShutdown()
   UnRegisterForRemoteEvent(Game.GetPlayer(), "OnPlayerLoadGame")
   UnRegisterForRemoteEvent(Game.GetPlayer(), "OnDifficultyChanged")
EndEvent

Event Actor.OnDifficultyChanged(Actor akSource, int aOldDifficulty, int aNewDifficulty)
	if (aOldDifficulty == 6) && (aNewDifficulty != 6)
		self.Stop()
	EndIf
EndEvent

Event Actor.OnPlayerLoadGame(Actor akSource)
    GlobalVariableToPersist.SetValue(0)
EndEvent

GlobalVariable Property GlobalVariableToPersist Auto Const