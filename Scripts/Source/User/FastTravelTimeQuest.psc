Scriptname FastTravelTimeQuest extends Quest

float GameDayBackup
float GameDaysPassedBackup
float GameHourBackup
float GameMonthBackup
float GameYearBackup
bool bWasFastTravel

Event OnMenuOpenCloseEvent(string asMenuName, bool abOpening)
	if ( abOpening == true )
		BackupTime()
		bWasFastTravel = true
	else
		Utility.Wait(5.0)
		bWasFastTravel = false
	endIf
EndEvent

Event OnQuestInit()
	RegisterForMenuOpenCloseEvent("PipboyMenu")
	RegisterForPlayerTeleport()
EndEvent

Event OnQuestShutdown()
	UnregisterForMenuOpenCloseEvent("PipboyMenu")
	UnregisterForPlayerTeleport()
EndEvent

Event OnPlayerTeleport()
	if ( bWasFastTravel )
		RestoreTime()
	endIf
endEvent

Function BackupTime()
	GameDayBackup = GameDay.GetValue()
	GameDaysPassedBackup = GameDaysPassed.GetValue()
	GameHourBackup = GameHour.GetValue()
	GameMonthBackup = GameMonth.GetValue()
	GameYearBackup = GameYear.GetValue()
EndFunction

Function RestoreTime()
	GameDay.SetValue(GameDayBackup)
	GameDaysPassed.SetValue(GameDaysPassedBackup)
	GameHour.SetValue(GameHourBackup)
	GameMonth.SetValue(GameMonthBackup)
	GameYear.SetValue(GameYearBackup)
EndFunction

GlobalVariable Property GameDay Auto Const
GlobalVariable Property GameDaysPassed Auto Const
GlobalVariable Property GameHour Auto Const
GlobalVariable Property GameMonth Auto Const
GlobalVariable Property GameYear Auto Const