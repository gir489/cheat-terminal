Scriptname UnlockNearestContainerMagicEffect extends activemagiceffect

Event OnEffectStart(Actor akTarget, Actor akCaster)
	ObjectReference nearestContainer = Game.FindClosestReferenceOfAnyTypeInListFromRef(CheatTerminal_AllContainers, Game.GetPlayer(), 200)
	if ( nearestContainer )
		nearestContainer.Unlock()
	endIf
EndEvent

FormList Property CheatTerminal_AllContainers Auto Const