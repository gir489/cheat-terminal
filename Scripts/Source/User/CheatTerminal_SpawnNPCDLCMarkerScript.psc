Scriptname CheatTerminal_SpawnNPCDLCMarkerScript extends ObjectReference Conditional

Actor NPC

Event OnInit()
	ActorBase actorToSpawn = Game.GetFormFromFile(FormID, DLCName) as ActorBase
	NPC = Self.PlaceAtMe(actorToSpawn, 1, false, true, false) as Actor
	CheatTerminalGlobalFuncs.SpawnNPCExplosion(NPC)
	NPC.SetRelationshipRank(Game.GetPlayer(), 4)
	NPC.SetPlayerTeammate(true, true, false)
	NPC.SetEssential(true)
	NPC.Enable(true)
	NPC.SetAngle(0, 0, NPC.GetAngleZ()) ;We need to remove the rotation, if any, so they don't moonwalk.
	NPC.MoveTo(self)
	NPC.AddKeyword(PowerArmorPreventArmorDamageKeyword)
	NPC.RemoveFromAllFactions()
	NPC.AddToFaction(PlayerFaction)
	NPC.SetValue(Health, 1000000)
	AddInventoryEventFilter(ObjectTypeAmmo)
	AddInventoryEventFilter(AmmoFusionCore)
	AddInventoryEventFilter(AmmoGammaCell)
	AddInventoryEventFilter(AmmoAlienBlaster)
	RegisterForRemoteEvent(NPC, "OnItemRemoved")
	RegisterForRemoteEvent(NPC, "OnCombatStateChanged")
	StartTimer(CheatTerminal_SpawnNPC_Duration.GetValue())
EndEvent

Event Actor.OnCombatStateChanged(Actor akSender, Actor akTarget, int aeCombatState)
	if ( akSender == NPC && akTarget == Game.GetPlayer() )
		NPC.StopCombat()
		NPC.SetRelationshipRank(Game.GetPlayer(), 4)
		NPC.SetPlayerTeammate(true, true, false)
	endIf
EndEvent

Event ObjectReference.OnItemRemoved(ObjectReference akSource, Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akDestContainer)
	if ( !Utility.IsInMenuMode() )
		akSource.AddItem(akBaseItem, aiItemCount, true)
	endIf
EndEvent

Event OnTimer(int aiTimerID)
	CheatTerminalGlobalFuncs.SpawnLeaveNPCExplosion(NPC)
	UnregisterForRemoteEvent(NPC, "OnItemRemoved")
	UnregisterForRemoteEvent(NPC, "OnCombatStateChanged")
	NPC.Disable()
	NPC.Delete()
	self.Delete()
EndEvent

Int Property FormID Auto Const
String Property DLCName Auto Const
Faction Property PlayerFaction Auto Const
ActorValue Property Health Auto Const
Keyword Property PowerArmorPreventArmorDamageKeyword Auto Const
Keyword Property ObjectTypeAmmo Auto Const
Ammo Property AmmoFusionCore Auto Const
Ammo Property AmmoGammaCell Auto Const
Ammo Property AmmoAlienBlaster Auto Const
GlobalVariable Property CheatTerminal_SpawnNPC_Duration Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const