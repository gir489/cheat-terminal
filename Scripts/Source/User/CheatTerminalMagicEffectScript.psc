Scriptname CheatTerminalMagicEffectScript extends ActiveMagicEffect

ObjectReference Property ContainerToActivate Auto Const
Sound Property SoundToPlay Auto Const

Event OnEffectStart(Actor akTarget, Actor akCaster)
	While (Utility.IsInMenuMode())
		Utility.Wait(0.5)
	EndWhile
	Utility.Wait(0.5)
	SoundToPlay.Play(Game.GetPlayer())
	ContainerToActivate.Activate(Game.GetPlayer())
endEvent