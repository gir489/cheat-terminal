Scriptname CheatTerminalEasyTerminals extends Quest

Event Actor.OnSit(Actor akActor, ObjectReference akFurniture)
	if ( akFurniture.HasKeyword(FurnitureForce1stPerson) )
		if ( akFurniture.IsLocked() )
			akFurniture.Unlock()
			if ( CheatTerminal_EasyTerminalMode.GetValue() == 1.0 )
				UITerminalCharEnter.Play(akFurniture)
				UITerminalPasswordGood.Play(akFurniture)
				Game.GetPlayer().SayCustom(PlayerHackSuccessSubtype)
				akActor.MoveTo(akActor)
				akFurniture.Activate(akActor)
				Game.RewardPlayerXP(Game.GetGameSettingfloat("fHackingExperienceBase") as int)
				FollowersScript.SendAffinityEvent(self, CA_Event_HackComputer, None, None, True, 1.000000)
			endif
		endIf
	endIf
EndEvent

Event OnQuestInit()
	RegisterForRemoteEvent(Game.GetPlayer(), "OnSit")
EndEvent

Event OnQuestShutdown()
	UnregisterForAllRemoteEvents()
EndEvent

Keyword Property FurnitureForce1stPerson Auto Const
Keyword Property PlayerHackSuccessSubtype Auto Const
Keyword Property CA_Event_HackComputer Auto Const
GlobalVariable Property CheatTerminal_EasyTerminalMode Auto Const
Sound Property UITerminalPasswordGood Auto Const
Sound Property UITerminalCharEnter Auto Const