Scriptname ItemManipulation1Script extends ObjectReference

Event OnActivate(ObjectReference akActionRef)
	if ( akActionRef == self )
		int i = 0
		While ( i < ItemsAdded.Length )
			ObjectReference objRef = ItemsAdded[i]
			int iFound = CheatTerminal_PersistentQuest_ItemsRefCol.Find(objRef)
			if ( iFound >= 0 )
				CheatTerminal_PersistentQuest_ItemsRefCol.RemoveRef(objRef)
			endIf
			i += 1
		EndWhile
		ItemsAdded.Clear()
	else
		AddInventoryEventFilter(None)
		if ( ItemsAdded.Length == 0 )
			ItemsAdded = new ObjectReference[0]
		endIf
	endIf
EndEvent

Event OnItemAdded(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akDestContainer)
	CheatTerminal_ItemManipulation1_HolderRef.AddItem(akBaseItem, aiItemCount)
	if ( akItemReference != None )
		ItemsAdded.Add(akItemReference)
	endIf
EndEvent

Event OnItemRemoved(Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akDestContainer)
	CheatTerminal_ItemManipulation1_HolderRef.RemoveItem(akBaseItem, aiItemCount)
	if ( akItemReference != None )
		int iIndexToRemove = ItemsAdded.RFind(akItemReference)
		if ( iIndexToRemove >= 0 )
			ItemsAdded.Remove(iIndexToRemove)
		endIf
	endIf
EndEvent

ObjectReference Property CheatTerminal_ItemManipulation1_HolderRef Auto Const
ObjectReference[] ItemsAdded
RefCollectionAlias Property CheatTerminal_PersistentQuest_ItemsRefCol Auto Const