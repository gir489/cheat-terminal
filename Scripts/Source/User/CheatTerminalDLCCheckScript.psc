Scriptname CheatTerminalDLCCheckScript extends Quest

Event OnQuestInit()
   RegisterForRemoteEvent(Game.GetPlayer(), "OnPlayerLoadGame")
   CheatTerminalGlobalFuncs.CheckDLC()
EndEvent

Event Actor.OnPlayerLoadGame(actor akSource)
    CheatTerminalGlobalFuncs.CheckDLC()
EndEvent

CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const