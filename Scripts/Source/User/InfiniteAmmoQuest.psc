Scriptname InfiniteAmmoQuest extends Quest

event ObjectReference.OnItemRemoved(ObjectReference akSource, Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akDestContainer)
	if ( !Utility.IsInMenuMode() )
		akSource.AddItem(akBaseItem, aiItemCount, true)
	endif
endEvent

Event OnQuestInit()
	AddInventoryEventFilter(ObjectTypeAmmo)
	AddInventoryEventFilter(AmmoFusionCore)
	AddInventoryEventFilter(AmmoGammaGun)
	AddInventoryEventFilter(AmmoAlienBlaster)
	form AlienWeaponizedNukaColaQuantum = Game.GetFormFromFile(0x000054A7,"alienassaultrifle.esp")
	if ( AlienWeaponizedNukaColaQuantum != None )
		AddInventoryEventFilter(AlienWeaponizedNukaColaQuantum)
	endIf
	RegisterForRemoteEvent(Game.GetPlayer(), "OnItemRemoved")
EndEvent

Event OnQuestShutdown()
	RemoveAllInventoryEventFilters()
	UnregisterForAllRemoteEvents()
EndEvent

Keyword Property ObjectTypeAmmo Auto Const
Ammo Property AmmoFusionCore Auto Const
Ammo Property AmmoGammaGun Auto Const
Ammo Property AmmoAlienBlaster Auto Const