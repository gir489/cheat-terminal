Scriptname CheatTerminalInfiniteItems extends Quest

event ObjectReference.OnItemRemoved(ObjectReference akSource, Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akDestContainer)
	;Debug.Trace(akSource + " " + akBaseItem + " " + aiItemCount+ " " + akItemReference + " " + akDestContainer)
	if ( akItemReference || akDestContainer ) ;Check if the user is moving the item.
		;Debug.Trace("Item was moved.")
		return
	endIf
	if ( akBaseItem.HasKeyword(ObjectTypeAmmo) || akBaseItem == AmmoFusionCore || akBaseItem == AmmoGammaGun || akBaseItem == AmmoAlienBlaster )
		;Debug.Trace("Item was Ammo.")
		return
	endIf
	if ( akBaseItem.HasKeyword(isPowerArmorFrame) )
		;Debug.Trace("Item was Power Armor Frame.")
		return
	endIf
	int currentElement = 0
	while (currentElement < GrenadeKeywords.Length)
		Keyword iterationKeyword = GrenadeKeywords[currentElement]
		if ( akBaseItem.HasKeyword(iterationKeyword) )
			;Debug.Trace("Item was Grenade.")
			return
		endIf
		currentElement += 1
	endWhile
	if ( CheatTerminal_InfiniteItems_BannedItems.HasForm(akBaseItem) )
		return
	endIf
	if ( HC_Vendor_Antiboitic_ChanceNone.GetValue() == 0 )
		if ( CheatTerminal_HC_EffectsToIgnore.HasForm(akBaseItem) == true )
			;Debug.Trace("Item was Hardcore Parkour effect item.")
			return
		endIf
	endIf
	;Debug.Trace("Added " + akBaseItem)
	akSource.AddItem(akBaseItem, aiItemCount, true)
endEvent

Event OnQuestInit()
	AddInventoryEventFilter(None)
	RegisterForRemoteEvent(Game.GetPlayer(), "OnItemRemoved")
EndEvent

Event OnQuestShutdown()
	RemoveAllInventoryEventFilters()
	UnregisterForAllRemoteEvents()
EndEvent

Keyword Property ObjectTypeAmmo Auto Const
Keyword Property isPowerArmorFrame Auto Const
Ammo Property AmmoFusionCore Auto Const
Ammo Property AmmoGammaGun Auto Const
Ammo Property AmmoAlienBlaster Auto Const
Keyword[] Property GrenadeKeywords Auto Const
FormList Property CheatTerminal_InfiniteItems_BannedItems Auto Const
GlobalVariable Property HC_Vendor_Antiboitic_ChanceNone Auto Const
FormList Property CheatTerminal_HC_EffectsToIgnore Auto Const