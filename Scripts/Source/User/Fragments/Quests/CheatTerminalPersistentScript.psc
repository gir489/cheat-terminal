;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Quests:CheatTerminalPersistentScript Extends Quest Hidden Const

;BEGIN FRAGMENT Fragment_Stage_0000_Item_00
Function Fragment_Stage_0000_Item_00()
;BEGIN CODE
int i = 0
While(i < ContainersToAdd.Length)
	CheatTerminal_PersistentQuest_ContainersRefCol.AddRef(ContainersToAdd[i])
	i += 1
EndWhile
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0010_Item_00
Function Fragment_Stage_0010_Item_00()
;BEGIN CODE
ObjectReference objRef = Game.GetPlayerGrabbedRef()
if ( objRef )
	if ( CheatTerminal_PersistentQuest_ItemsRefCol.Find(objRef) < 0 )
			CheatTerminal_PersistentQuest_ItemsRefCol.AddRef(objRef)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0069_Item_00
Function Fragment_Stage_0069_Item_00()
;BEGIN CODE
CheatTerminal_PersistentQuest_ItemsRefCol.RemoveAll()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectReference[] Property ContainersToAdd Auto Const
RefCollectionAlias Property CheatTerminal_PersistentQuest_ContainersRefCol Auto Const
RefCollectionAlias Property CheatTerminal_PersistentQuest_ItemsRefCol Auto Const
