;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Quests:CheatTerminalPortableQuestFragment Extends Quest Hidden Const

;BEGIN FRAGMENT Fragment_Stage_0000_Item_00
Function Fragment_Stage_0000_Item_00()
;BEGIN AUTOCAST TYPE PortableCheatTerminalQuestScript
Quest __temp = self as Quest
PortableCheatTerminalQuestScript kmyQuest = __temp as PortableCheatTerminalQuestScript
;END AUTOCAST
;BEGIN CODE
kmyQuest.InitializeQuest()
While (Utility.IsInMenuMode())
	Utility.Wait(1.0)
EndWhile
Game.FadeOutGame(abFadingOut = false, abBlackFade = true, afSecsBeforeFade = 0.5, afFadeDuration = 0.1, abStayFaded = false)
CheatTerminal_Installed.Show()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Stage_0010_Item_00
Function Fragment_Stage_0010_Item_00()
;BEGIN CODE
ObjectReference myHolotape = Alias_Portable.GetRef()
PortableTerminal.ForceRefTo(myHolotape)
Game.GetPlayer().AddItem(myHolotape)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ReferenceAlias Property PortableTerminal Auto Const

ReferenceAlias Property Alias_Portable Auto Const

Quest Property MQ102 Auto Const

Message Property CheatTerminal_Installed Auto Const
