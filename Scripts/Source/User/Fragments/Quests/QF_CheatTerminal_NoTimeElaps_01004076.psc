;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Quests:QF_CheatTerminal_NoTimeElaps_01004076 Extends Quest Hidden Const

;BEGIN FRAGMENT Fragment_Stage_0010_Item_00
Function Fragment_Stage_0010_Item_00()
;BEGIN AUTOCAST TYPE FastTravelTimeQuest
Quest __temp = self as Quest
FastTravelTimeQuest kmyQuest = __temp as FastTravelTimeQuest
;END AUTOCAST
;BEGIN CODE
kmyQuest.OnMenuOpenCloseEvent("PipboyMenu", true)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
