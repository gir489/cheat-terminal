;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Mele_0100A8FC Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_Shishkebab_ExtraFlameJets)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_Shishkebab_ExtraFlameJets)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_melee_Shishkebab_ExtraFlameJets Auto Const
MiscObject Property miscmod_mod_melee_Shishkebab_ExtraFlameJets Auto Const
