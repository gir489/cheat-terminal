;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01007A86 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Scope_SightsIron)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Scope_SightsIron)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Scope_SightsGlow)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Scope_SightsGlow)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Scope_SightReflex)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Scope_SightReflex)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Scope_SightReflex_Circle)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Scope_SightReflex_Circle)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Scope_ScopeRecon)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Scope_ScopeRecon)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ObjectMod Property mod_10mm_Scope_SightsIron Auto Const
ObjectMod Property mod_10mm_Scope_SightsGlow Auto Const
ObjectMod Property mod_10mm_Scope_SightReflex Auto Const
ObjectMod Property mod_10mm_Scope_SightReflex_Circle Auto Const
ObjectMod Property mod_10mm_Scope_ScopeRecon Auto Const
MiscObject Property miscmod_mod_10mm_Scope_SightsIron Auto Const
MiscObject Property miscmod_mod_10mm_Scope_SightsGlow Auto Const
MiscObject Property miscmod_mod_10mm_Scope_SightReflex Auto Const
MiscObject Property miscmod_mod_10mm_Scope_SightReflex_Circle Auto Const
MiscObject Property miscmod_mod_10mm_Scope_ScopeRecon Auto Const
