;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01002ED4 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0001998A, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod,100)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0001998A, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod,1000)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0001998A, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod,10000)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0001998A, "DLCNukaWorld.esm")
if formFromMod
	int countBefore = Game.GetPlayer().GetItemCount(formFromMod)
	Game.GetPlayer().AddItem(formFromMod, 100000, False)
	int countToRemove = Game.GetPlayer().GetItemCount(formFromMod) - countBefore 
	Game.GetPlayer().RemoveItem(formFromMod, countToRemove, True, None)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 10000, True)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0001998A, "DLCNukaWorld.esm")
if formFromMod
	int countBefore = Game.GetPlayer().GetItemCount(formFromMod)
	Game.GetPlayer().AddItem(formFromMod, 1000000, False)
	int countToRemove = Game.GetPlayer().GetItemCount(formFromMod) - countBefore 
	Game.GetPlayer().RemoveItem(formFromMod, countToRemove, True, None)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 30000, True)
	Game.GetPlayer().AddItem(formFromMod, 10000, True)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
