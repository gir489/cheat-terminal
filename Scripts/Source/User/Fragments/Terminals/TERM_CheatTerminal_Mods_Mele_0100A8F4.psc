;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Mele_0100A8F4 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_Knife_Serrated)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_Knife_Serrated)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_Knife_SerratedStealth)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_Knife_SerratedStealth)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_melee_Knife_Serrated Auto Const
ObjectMod Property mod_melee_Knife_SerratedStealth Auto Const
MiscObject Property miscmod_mod_melee_Knife_Serrated Auto Const
MiscObject Property miscmod_mod_melee_Knife_SerratedStealth Auto Const
