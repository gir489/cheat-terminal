;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Armor_Met_01002E15 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Armor_Metal_ArmLeft_Light)
Game.GetPlayer().AddItem(LL_Armor_Metal_ArmRight_Light)
Game.GetPlayer().AddItem(LL_Armor_Metal_LegLeft_Light)
Game.GetPlayer().AddItem(LL_Armor_Metal_LegRight_Light)
Game.GetPlayer().AddItem(LL_Armor_Metal_Torso_Light)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Armor_Metal_ArmLeft_Medium)
Game.GetPlayer().AddItem(LL_Armor_Metal_ArmRight_Medium)
Game.GetPlayer().AddItem(LL_Armor_Metal_LegLeft_Medium)
Game.GetPlayer().AddItem(LL_Armor_Metal_LegRight_Medium)
Game.GetPlayer().AddItem(LL_Armor_Metal_Torso_Medium)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Armor_Metal_ArmLeft_Heavy)
Game.GetPlayer().AddItem(LL_Armor_Metal_ArmRight_Heavy)
Game.GetPlayer().AddItem(LL_Armor_Metal_LegLeft_Heavy)
Game.GetPlayer().AddItem(LL_Armor_Metal_LegRight_Heavy)
Game.GetPlayer().AddItem(LL_Armor_Metal_Torso_Heavy)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_Metal_Helmet)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

LeveledItem Property LL_Armor_Metal_ArmLeft_Light Auto Const
LeveledItem Property LL_Armor_Metal_ArmRight_Light Auto Const
LeveledItem Property LL_Armor_Metal_LegLeft_Light Auto Const
LeveledItem Property LL_Armor_Metal_LegRight_Light Auto Const
LeveledItem Property LL_Armor_Metal_Torso_Light Auto Const

LeveledItem Property LL_Armor_Metal_ArmLeft_Medium Auto Const
LeveledItem Property LL_Armor_Metal_ArmRight_Medium Auto Const
LeveledItem Property LL_Armor_Metal_LegLeft_Medium Auto Const
LeveledItem Property LL_Armor_Metal_LegRight_Medium Auto Const
LeveledItem Property LL_Armor_Metal_Torso_Medium Auto Const

LeveledItem Property LL_Armor_Metal_ArmLeft_Heavy Auto Const
LeveledItem Property LL_Armor_Metal_ArmRight_Heavy Auto Const
LeveledItem Property LL_Armor_Metal_LegLeft_Heavy Auto Const
LeveledItem Property LL_Armor_Metal_LegRight_Heavy Auto Const
LeveledItem Property LL_Armor_Metal_Torso_Heavy Auto Const

Armor Property Armor_Metal_Helmet Auto Const

GlobalVariable Property LL_EpicChance_Standard Auto Const
