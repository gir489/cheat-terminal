;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:ItemsMiscVaseOther Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageDirtyFlowers03e, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageDirtyFlowers03d, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageDirtyFlowers03b, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageDirtyFlowers03a, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageDirtyFlowers03c, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageDirtyFlowers02e, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageDirtyFlowers02d, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageDirtyFlowers02b, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageDirtyFlowers02a, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageDirtyFlowers02c, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Vase01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageDirtyFlowers01e, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageDirtyFlowers01d, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageDirtyFlowers01b, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageDirtyFlowers01a, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageDirtyFlowers01c, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
GlobalVariable Property CheatTerminal_MiscToGive Auto Const
MiscObject Property VaseVintageDirtyFlowers03e Auto Const
MiscObject Property VaseVintageDirtyFlowers03d Auto Const
MiscObject Property VaseVintageDirtyFlowers03b Auto Const
MiscObject Property VaseVintageDirtyFlowers03a Auto Const
MiscObject Property VaseVintageDirtyFlowers03c Auto Const
MiscObject Property VaseVintageDirtyFlowers02e Auto Const
MiscObject Property VaseVintageDirtyFlowers02d Auto Const
MiscObject Property VaseVintageDirtyFlowers02b Auto Const
MiscObject Property VaseVintageDirtyFlowers02a Auto Const
MiscObject Property VaseVintageDirtyFlowers02c Auto Const
MiscObject Property Vase01 Auto Const
MiscObject Property VaseVintageDirtyFlowers01e Auto Const
MiscObject Property VaseVintageDirtyFlowers01d Auto Const
MiscObject Property VaseVintageDirtyFlowers01b Auto Const
MiscObject Property VaseVintageDirtyFlowers01a Auto Const
MiscObject Property VaseVintageDirtyFlowers01c Auto Const
