;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01006B9F Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(DarkHollowPondMapMarker.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(DrumlinDinerMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(DunwichBorersMapMarker.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(EgretToursMarinaMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(FairlineHillsMapMarker.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(FederalRationStockPile.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(FiddlersGreenTrailerMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(FinchFarmMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(ForestGroveMarshMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(FortHagenMapMarker.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(FortHagenSateliteArrayMapMarker.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(FortStrongMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ObjectReference Property DarkHollowPondMapMarker Auto Const

ObjectReference Property DrumlinDinerMapMarkerRef Auto Const

ObjectReference Property DunwichBorersMapMarker Auto Const

ObjectReference Property EgretToursMarinaMapMarkerRef Auto Const

ObjectReference Property FairlineHillsMapMarker Auto Const

ObjectReference Property FederalRationStockPile Auto Const

ObjectReference Property FiddlersGreenTrailerMapMarkerRef Auto Const

ObjectReference Property FinchFarmMapMarkerRef Auto Const

ObjectReference Property ForestGroveMarshMapMarkerRef Auto Const

ObjectReference Property FortHagenMapMarker Auto Const

ObjectReference Property FortHagenSateliteArrayMapMarker Auto Const

ObjectReference Property FortStrongMapMarkerRef Auto Const
