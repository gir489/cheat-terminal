;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_MoveToLoc_01007B00 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, TaffingtonBoatHouseMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, TenPinesBluffMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, TheSlogMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, ThicketExcavationsMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, TuckerMemorialBridgeMapMarkerRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, UFORotateHelperFree, 1157.2159, -231.7632, 153.3152, 283)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, USAFSatelliteMapMarkerRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Vault111ExtMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Vault75MapMarker)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property TaffingtonBoatHouseMapMarker Auto Const
ObjectReference Property TenPinesBluffMapMarker Auto Const
ObjectReference Property TheSlogMapMarker Auto Const
ObjectReference Property ThicketExcavationsMapMarker Auto Const
ObjectReference Property TuckerMemorialBridgeMapMarkerRef Auto Const
ObjectReference Property UFORotateHelperFree Auto Const
ObjectReference Property USAFSatelliteMapMarkerRef Auto Const
ObjectReference Property Vault111ExtMapMarker Auto Const
ObjectReference Property Vault75MapMarker Auto Const
