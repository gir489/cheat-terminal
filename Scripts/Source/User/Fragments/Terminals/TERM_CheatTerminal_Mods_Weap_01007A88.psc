;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Weap_01007A88 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_44_Receiver_Standard)
else
	Game.GetPlayer().AddItem(miscmod_mod_44_Receiver_Standard)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_44_Receiver_MoreDamage1)
else
	Game.GetPlayer().AddItem(miscmod_mod_44_Receiver_MoreDamage1)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_44_Receiver_MoreDamage2)
else
	Game.GetPlayer().AddItem(miscmod_mod_44_Receiver_MoreDamage2)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_44_Receiver_MoreDamage3)
else
	Game.GetPlayer().AddItem(miscmod_mod_44_Receiver_MoreDamage3)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_44_Receiver_Standard Auto Const
ObjectMod Property mod_44_Receiver_MoreDamage1 Auto Const
ObjectMod Property mod_44_Receiver_MoreDamage2 Auto Const
ObjectMod Property mod_44_Receiver_MoreDamage3 Auto Const
MiscObject Property miscmod_mod_44_Receiver_Standard Auto Const
MiscObject Property miscmod_mod_44_Receiver_MoreDamage1 Auto Const
MiscObject Property miscmod_mod_44_Receiver_MoreDamage2 Auto Const
MiscObject Property miscmod_mod_44_Receiver_MoreDamage3 Auto Const
