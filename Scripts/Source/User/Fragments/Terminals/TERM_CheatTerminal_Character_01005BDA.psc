;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Character_01005BDA Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(GodmodePerk, false)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(GodmodePerk)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(StaminaPerk, false)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(StaminaPerk)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(CarryweightPerk, false)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(CarryweightPerk)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(InfiniteMinesPerk, false)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(InfiniteMinesPerk)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(CritPerk, false)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(CritPerk)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(InfiniteAmmoPerk, false)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(InfiniteAmmoPerk)
InfiniteAmmoQuestProperty.Stop()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(EasyLocksPerk, false)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(EasyLocksPerk)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(AlwaysLegend, false)
ChanceLegendary.SetValue(100.0)
LL_ChanceNone_PowerArmorMod.SetValue(0.0)
LL_EpicChance_Armor_Boss.SetValue(100.0)
LL_EpicChance_Standard.SetValue(100.0)
LL_EpicChance_VendorBase.SetValue(100.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(AlwaysLegend)
if game.GetPlayer().GetLevel() > 50
     ChanceLegendary.SetValue(20)
elseif game.GetPlayer().GetLevel() > 40
     ChanceLegendary.SetValue(18)
elseif game.GetPlayer().GetLevel() > 30
     ChanceLegendary.SetValue(15)
elseif game.GetPlayer().GetLevel() > 20
     ChanceLegendary.SetValue(12)
elseif game.GetPlayer().GetLevel() > 5
     ChanceLegendary.SetValue(10)
endif
LL_ChanceNone_PowerArmorMod.SetValue(50.0)
LL_EpicChance_Armor_Boss.SetValue(50.0)
LL_EpicChance_Standard.SetValue(10.0)
LL_EpicChance_VendorBase.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(PerfectVATS, false)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(PerfectVATS)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(InfiniteGremadesPerk, false)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(InfiniteGremadesPerk)
InfiniteGrenadeQuestProperty.Stop()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(OneShotKill, false)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_22
Function Fragment_Terminal_22(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(OneShotKill)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_23
Function Fragment_Terminal_23(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(VATSCrits, false)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_24
Function Fragment_Terminal_24(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(VATSCrits)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Perk Property GodmodePerk Auto Const

Perk Property StaminaPerk Auto Const

Perk Property CarryweightPerk Auto Const

Perk Property CritPerk Auto Const

Perk Property InfiniteMinesPerk Auto Const

Perk Property InfiniteAmmoPerk Auto Const

Perk Property EasyLocksPerk Auto Const

Perk Property AlwaysLegend Auto Const

GlobalVariable Property ChanceLegendary Auto Const

GlobalVariable Property LL_ChanceNone_PowerArmorMod Auto Const

GlobalVariable Property LL_EpicChance_Armor_Boss Auto Const

GlobalVariable Property LL_EpicChance_Standard Auto Const

GlobalVariable Property LL_EpicChance_VendorBase Auto Const

Perk Property PerfectVATS Auto Const

Keyword Property ObjectTypeAmmo Auto Const

Ammo Property AmmoFusionCore Auto Const

Quest Property InfiniteAmmoQuestProperty Auto Const

Perk Property InfiniteGremadesPerk Auto Const

Quest Property InfiniteGrenadeQuestProperty Auto Const

Perk Property OneShotKill Auto Const

Perk Property VATSCrits Auto Const
