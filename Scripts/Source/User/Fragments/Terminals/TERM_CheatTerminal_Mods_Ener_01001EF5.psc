;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Ener_01001EF5 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_LaserReceiver_BetterCriticals1)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Receiver_BetterCriticals1)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_LaserReceiver_Standard)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Receiver_Standard)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_LaserReceiver_ArmorPiercing1)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Receiver_ArmorPiercing1)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_LaserReceiver_MoreDamage1)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Receiver_MoreDamage1)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_LaserReceiver_BetterCriticals2)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Receiver_BetterCriticals2)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_LaserReceiver_ArmorPiercing2)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Receiver_ArmorPiercing2)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_LaserReceiver_MoreDamage2)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Receiver_MoreDamage2)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_LaserReceiver_MoreDamage1_BetterCriticals2)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Receiver_MoreDamage1_BetterCriticals2)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_LaserReceiver_MoreDamage1_ArmorPiercing2)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Receiver_MoreDamage1_ArmorPiercing2)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_LaserReceiver_MoreDamage3)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Receiver_MoreDamage3)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_LaserGun_LaserReceiver_Standard Auto Const
ObjectMod Property mod_LaserGun_LaserReceiver_BetterCriticals1 Auto Const
ObjectMod Property mod_LaserGun_LaserReceiver_ArmorPiercing1 Auto Const
ObjectMod Property mod_LaserGun_LaserReceiver_MoreDamage1 Auto Const
ObjectMod Property mod_LaserGun_LaserReceiver_BetterCriticals2 Auto Const
ObjectMod Property mod_LaserGun_LaserReceiver_ArmorPiercing2 Auto Const
ObjectMod Property mod_LaserGun_LaserReceiver_MoreDamage2 Auto Const
ObjectMod Property mod_LaserGun_LaserReceiver_MoreDamage1_BetterCriticals2 Auto Const
ObjectMod Property mod_LaserGun_LaserReceiver_MoreDamage1_ArmorPiercing2 Auto Const
ObjectMod Property mod_LaserGun_LaserReceiver_MoreDamage3 Auto Const
MiscObject Property miscmod_mod_LaserGun_Receiver_Standard Auto Const
MiscObject Property miscmod_mod_LaserGun_Receiver_BetterCriticals1 Auto Const
MiscObject Property miscmod_mod_LaserGun_Receiver_ArmorPiercing1 Auto Const
MiscObject Property miscmod_mod_LaserGun_Receiver_MoreDamage1 Auto Const
MiscObject Property miscmod_mod_LaserGun_Receiver_BetterCriticals2 Auto Const
MiscObject Property miscmod_mod_LaserGun_Receiver_ArmorPiercing2 Auto Const
MiscObject Property miscmod_mod_LaserGun_Receiver_MoreDamage2 Auto Const
MiscObject Property miscmod_mod_LaserGun_Receiver_MoreDamage1_BetterCriticals2 Auto Const
MiscObject Property miscmod_mod_LaserGun_Receiver_MoreDamage1_ArmorPiercing2 Auto Const
MiscObject Property miscmod_mod_LaserGun_Receiver_MoreDamage3 Auto Const
