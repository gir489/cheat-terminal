;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAltWorldManipMerchReg Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, RegTraderRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, RufusRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, ScarlettREF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, ShengKawolskiREF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, SlimRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, REMerchant01REF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, SolomonREF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, SprocketRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, StashRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, SupervisorGreene01Ref)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, RowdyRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_22
Function Fragment_Terminal_22(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, RonnieShawREF)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property RegTraderRef Auto Const
ObjectReference Property RonnieShawREF Auto Const
ObjectReference Property RowdyRef Auto Const
ObjectReference Property RufusRef Auto Const
ObjectReference Property ScarlettREF Auto Const
ObjectReference Property ShengKawolskiREF Auto Const
ObjectReference Property SlimRef Auto Const
ObjectReference Property REMerchant01REF Auto Const
ObjectReference Property SolomonREF Auto Const
ObjectReference Property SprocketRef Auto Const
ObjectReference Property StashRef Auto Const
ObjectReference Property SupervisorGreene01Ref Auto Const
ObjectReference Property InstituteSynthFoodVendorREF Auto Const
ObjectReference Property InstituteSynthMiscVendorRef Auto Const
ObjectReference Property TakahashiREF Auto Const
ObjectReference Property TinkerTomREF Auto Const
ObjectReference Property CarlaREF Auto Const
ObjectReference Property TrudyREF Auto Const
ObjectReference Property VadimBobrovREF Auto Const
ObjectReference Property WaitronRef Auto Const
ObjectReference Property WellinghamREF Auto Const
ObjectReference Property WhitechapelCharlieREF Auto Const
ObjectReference Property WolfgangREF Auto Const
