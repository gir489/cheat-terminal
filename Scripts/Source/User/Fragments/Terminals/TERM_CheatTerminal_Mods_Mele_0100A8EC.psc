;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Mele_0100A8EC Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_BaseballBat_Spikes)
else
	Game.GetPlayer().AddItem(miscmod_mod_baseballBat_Spikes)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_BaseballBat_SpikesSmall)
else
	Game.GetPlayer().AddItem(miscmod_mod_baseballBat_SpikesSmall)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_BaseballBat_Blades)
else
	Game.GetPlayer().AddItem(miscmod_mod_baseballBat_Blades)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_BaseballBat_Chains)
else
	Game.GetPlayer().AddItem(miscmod_mod_baseballBat_Chains)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_BaseballBat_BladesLarge)
else
	Game.GetPlayer().AddItem(miscmod_mod_baseballBat_BladesLarge)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_melee_BaseballBat_SpikesSmall Auto Const
ObjectMod Property mod_melee_BaseballBat_Blades Auto Const
ObjectMod Property mod_melee_BaseballBat_Spikes Auto Const
ObjectMod Property mod_melee_BaseballBat_Chains Auto Const
ObjectMod Property mod_melee_BaseballBat_BladesLarge Auto Const
MiscObject Property miscmod_mod_baseballBat_SpikesSmall Auto Const
MiscObject Property miscmod_mod_baseballBat_Blades Auto Const
MiscObject Property miscmod_mod_baseballBat_Spikes Auto Const
MiscObject Property miscmod_mod_baseballBat_Chains Auto Const
MiscObject Property miscmod_mod_baseballBat_BladesLarge Auto Const
