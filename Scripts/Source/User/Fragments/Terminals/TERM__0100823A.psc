;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__0100823A Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AssaultRifle_Barrel_Short)
else
	Game.GetPlayer().AddItem(miscmod_mod_AssaultRifle_Barrel_Short)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AssaultRifle_Barrel_Long)
else
	Game.GetPlayer().AddItem(miscmod_mod_AssaultRifle_Barrel_Long)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AssaultRifle_Barrel_PortedShort)
else
	Game.GetPlayer().AddItem(miscmod_mod_AssaultRifle_Barrel_PortedShort)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AssaultRifle_Barrel_PortedLong)
else
	Game.GetPlayer().AddItem(miscmod_mod_AssaultRifle_Barrel_PortedLong)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_AssaultRifle_Barrel_Short Auto Const
ObjectMod Property mod_AssaultRifle_Barrel_Long Auto Const
ObjectMod Property mod_AssaultRifle_Barrel_PortedShort Auto Const
ObjectMod Property mod_AssaultRifle_Barrel_PortedLong Auto Const
MiscObject Property miscmod_mod_AssaultRifle_Barrel_Short Auto Const
MiscObject Property miscmod_mod_AssaultRifle_Barrel_Long Auto Const
MiscObject Property miscmod_mod_AssaultRifle_Barrel_PortedShort Auto Const
MiscObject Property miscmod_mod_AssaultRifle_Barrel_PortedLong Auto Const
