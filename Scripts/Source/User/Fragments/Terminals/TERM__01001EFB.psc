;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01001EFB Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_InstituteLaserGun_BarrelLaser_Auto_A)
else
	Game.GetPlayer().AddItem(miscmod_mod_InstituteLaserGun_BarrelLaser_Auto_A)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_InstituteLaserGun_BarrelLaser_Short_A)
else
	Game.GetPlayer().AddItem(miscmod_mod_InstituteLaserGun_BarrelLaser_Short_A)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_InstituteLaserGun_BarrelLaser_Long_A)
else
	Game.GetPlayer().AddItem(miscmod_mod_InstituteLaserGun_BarrelLaser_Long_A)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_InstituteLaserGun_BarrelLaser_Short_B)
else
	Game.GetPlayer().AddItem(miscmod_mod_InstituteLaserGun_BarrelLaser_Short_B)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_InstituteLaserGun_BarrelLaser_Long_B)
else
	Game.GetPlayer().AddItem(miscmod_mod_InstituteLaserGun_BarrelLaser_Long_B)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_InstituteLaserGun_BarrelLaser_Auto_B)
else
	Game.GetPlayer().AddItem(miscmod_mod_InstituteLaserGun_BarrelLaser_Auto_B)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_InstituteLaserGun_BarrelLaser_Short_A Auto Const
ObjectMod Property mod_InstituteLaserGun_BarrelLaser_Long_A Auto Const
ObjectMod Property mod_InstituteLaserGun_BarrelLaser_Auto_A Auto Const
ObjectMod Property mod_InstituteLaserGun_BarrelLaser_Short_B Auto Const
ObjectMod Property mod_InstituteLaserGun_BarrelLaser_Long_B Auto Const
ObjectMod Property mod_InstituteLaserGun_BarrelLaser_Auto_B Auto Const
MiscObject Property miscmod_mod_InstituteLaserGun_BarrelLaser_Short_A Auto Const
MiscObject Property miscmod_mod_InstituteLaserGun_BarrelLaser_Long_A Auto Const
MiscObject Property miscmod_mod_InstituteLaserGun_BarrelLaser_Auto_A Auto Const
MiscObject Property miscmod_mod_InstituteLaserGun_BarrelLaser_Short_B Auto Const
MiscObject Property miscmod_mod_InstituteLaserGun_BarrelLaser_Long_B Auto Const
MiscObject Property miscmod_mod_InstituteLaserGun_BarrelLaser_Auto_B Auto Const
