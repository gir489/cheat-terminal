;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__0100451F Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0004FA88, "DLCRobot.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0004885A, "DLCRobot.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0004FA7D, "DLCRobot.esm")
if objectToSpawn
	Actor playerActor = Game.GetPlayer()
	ObjectReference objRef = playerActor.PlaceAtMe(objectToSpawn)
	objRef.AttachMod(Game.GetFormFromFile(0x0005D371, "DLCRobot.esm") as ObjectMod)
	Game.GetPlayer().AddItem(objRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x00045678, "DLCRobot.esm")
if objectToSpawn
	Actor playerActor = Game.GetPlayer()
	ObjectReference objRef = playerActor.PlaceAtMe(objectToSpawn)
	objRef.AttachMod(Game.GetFormFromFile(0x00043225, "DLCRobot.esm") as ObjectMod)
	Game.GetPlayer().AddItem(objRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x00045677, "DLCRobot.esm")
if objectToSpawn
	Actor playerActor = Game.GetPlayer()
	ObjectReference objRef = playerActor.PlaceAtMe(objectToSpawn)
	objRef.AttachMod(Game.GetFormFromFile(0x00010BB5, "DLCRobot.esm") as ObjectMod)
	Game.GetPlayer().AddItem(objRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
