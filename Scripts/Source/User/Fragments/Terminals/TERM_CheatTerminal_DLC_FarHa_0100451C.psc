;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_FarHa_0100451C Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x00026FA2, "DLCCoast.esm")
if objectToSpawn
	Actor playerActor = Game.GetPlayer()
	ObjectReference spawnedObjectedRef = playerActor.PlaceAtMe(objectToSpawn, abForcePersist = true, abDeleteWhenAble = false)
	spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x00043C80, "DLCCoast.esm")
if objectToSpawn
	Actor playerActor = Game.GetPlayer()
	ObjectReference spawnedObjectedRef = playerActor.PlaceAtMe(objectToSpawn, abForcePersist = true, abDeleteWhenAble = false)
	spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
