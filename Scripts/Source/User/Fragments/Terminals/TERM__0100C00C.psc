;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__0100C00C Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_RailwayRifle_Muzzle_BayonetLarge)
else
	Game.GetPlayer().AddItem(miscmod_mod_RailwayRifle_Muzzle_BayonetLarge)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_RailwayRifle_Muzzle_BayonetLarge Auto Const
MiscObject Property miscmod_mod_RailwayRifle_Muzzle_BayonetLarge Auto Const
