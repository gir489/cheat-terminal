;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Othe_0100BFFD Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Flamer_HoseMuzzle_Vaporizer)
else
	Game.GetPlayer().AddItem(miscmod_mod_Flamer_HoseMuzzle_Vaporizer)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Flamer_HoseMuzzle_Standard)
else
	Game.GetPlayer().AddItem(miscmod_mod_Flamer_HoseMuzzle_Standard)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Flamer_HoseMuzzle_Compression)
else
	Game.GetPlayer().AddItem(miscmod_mod_Flamer_HoseMuzzle_Compression)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_Flamer_HoseMuzzle_Standard Auto Const
ObjectMod Property mod_Flamer_HoseMuzzle_Compression Auto Const
ObjectMod Property mod_Flamer_HoseMuzzle_Vaporizer Auto Const
MiscObject Property miscmod_mod_Flamer_HoseMuzzle_Standard Auto Const
MiscObject Property miscmod_mod_Flamer_HoseMuzzle_Compression Auto Const
MiscObject Property miscmod_mod_Flamer_HoseMuzzle_Vaporizer Auto Const
