;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:QuestsFixMinutemen Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Min03.SetStage(470)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Min03.SetStage(480)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(ArtillerySmokeFlare)
ArtilarySchmeatic.GetReference().Activate(Game.GetPlayer())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
WorkshopObjectScript tower = Game.GetFormFromFile(0x0009FF48, "Fallout4.esm") as WorkshopObjectScript
tower.bAllowPlayerAssignment = true
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference Z114 = Game.GetFormFromFile(0x0009FF48, "Fallout4.esm") as ObjectReference
Z114.Disable()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
COMDeacon.SetStage(1000)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CheatTerminal_TakeOrLeaveShaun.Show() == 0 )
	MQ302Min.SetStage(1720)
else
	MQ302Min.SetStage(1730)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Quest Property Min03 Auto Const
ReferenceAlias Property ArtilarySchmeatic Auto Const
Weapon Property ArtillerySmokeFlare Auto Const
Quest Property COMDeacon Auto Const
Quest Property MQ302Min Auto Const
Message Property CheatTerminal_TakeOrLeaveShaun Auto Const