;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01005C05 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
if ( FollowerAlias.GetRef() == None)
Actor NPC = Game.GetPlayer().PlaceActorAtMe(EncDeathclaw06Legendary, 4, None)
NPC.SetRelationshipRank(Game.GetPlayer(), 4)
NPC.SetPlayerTeammate(1 as bool, True, False)
NPC.AllowCompanion(1 as bool, 1 as bool)
NPC.AddKeyword(CheatTerminal_MarkCompanion)
NPC.FollowerFollow()
NPC.SetEssential(0 as bool)
else
dismissmessage.show()
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
if ( FollowerAlias.GetRef() == None)
Actor NPC = Game.GetPlayer().PlaceActorAtMe(PEncSMBehemoth03, 4, None)
NPC.SetRelationshipRank(Game.GetPlayer(), 4)
NPC.SetPlayerTeammate(1 as bool, True, False)
NPC.AllowCompanion(1 as bool, 1 as bool)
NPC.AddKeyword(CheatTerminal_MarkCompanion)
NPC.FollowerFollow()
NPC.SetEssential(0 as bool)
else
dismissmessage.show()
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
if ( FollowerAlias.GetRef() != None)
      Actor followerActor = FollowerAlias.GetRef() as Actor
	if ( followerActor.HasKeyword(CheatTerminal_MarkCompanion) )
	      followerActor.DisallowCompanion(true)
	      FollowerAlias.Clear()
	endIf
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	PowerArmorMessage.Show()
else
	playerActor.MoveTo(playerActor)
	playerActor.EquipItem(RacePotion, false, true)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference objRef = Game.GetPlayer().PlaceAtMe(WeaponsBench)
objRef.SetAngle(0,0,0)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ActorBase Property PEncSMBehemoth03 Auto Const

ActorBase Property EncDeathclaw06Legendary Auto Const

ReferenceAlias Property FollowerAlias Auto Const

Message Property DismissMessage Auto Const

Potion Property RacePotion Auto Const

Message Property PowerArmorMessage Auto Const

Keyword Property CheatTerminal_MarkCompanion Auto Const

Furniture Property WeaponsBench Auto Const
