;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAltWorldManipMerchLevel4 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, Game.GetFormFromFile(0x00031FB4, "Fallout4.esm") as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, Game.GetFormFromFile(0x0002A82C, "Fallout4.esm") as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, Game.GetFormFromFile(0x0003EFF3, "Fallout4.esm") as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, Game.GetFormFromFile(0x00036D72, "Fallout4.esm") as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference merchantRef = Game.GetFormFromFile(0x0010D56D, "Fallout4.esm") as ObjectReference;
if ( merchantRef.GetParentCell() == REHoldingCell && CheatTerminal_MerchantStillInHoldingCell.Show() == 1 )
	return
endIf
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, merchantRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference merchantRef = Game.GetFormFromFile(0x00192357, "Fallout4.esm") as ObjectReference;
if ( merchantRef.GetParentCell() == REHoldingCell && CheatTerminal_MerchantStillInHoldingCell.Show() == 1 )
	return
endIf
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, merchantRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference merchantRef = Game.GetFormFromFile(0x00190047, "Fallout4.esm") as ObjectReference;
if ( merchantRef.GetParentCell() == REHoldingCell && CheatTerminal_MerchantStillInHoldingCell.Show() == 1 )
	return
endIf
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, merchantRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference merchantRef = Game.GetFormFromFile(0x0002F2A7, "Fallout4.esm") as ObjectReference;
if ( merchantRef.GetParentCell() == REHoldingCell && CheatTerminal_MerchantStillInHoldingCell.Show() == 1 )
	return
endIf
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, merchantRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
Message Property CheatTerminal_MerchantStillInHoldingCell Auto Const
Cell Property REHoldingCell Auto Const
