;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:CheatTerminalCheatsInventory Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(CheatTerminal_InfiniteItems, false)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(CheatTerminal_InfiniteItems)
CheatTerminal_InfiniteItemsQuest.Stop()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
if (Game.GetPlayer().HasPerk(CheatTerminal_FortifyCarryWeight))
	Game.GetPlayer().RemovePerk(CheatTerminal_FortifyCarryWeight)
endIf
Game.GetPlayer().AddPerk(CarryweightPerk, false)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(CarryweightPerk)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(InfiniteMinesPerk, false)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(InfiniteMinesPerk)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CheatTerminal_ReadMeEnable.GetValueInt() == 1 && CheatTerminal_InfiniteAmmoQuery.Show() == 0 )
	Game.GetPlayer().AddPerk(InfiniteAmmoPerk, false)
EndIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(InfiniteAmmoPerk)
InfiniteAmmoQuestProperty.Stop()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(AlwaysLegend, false)
ChanceLegendary.SetValue(100.0)
LL_ChanceNone_PowerArmorMod.SetValue(0.0)
LL_EpicChance_Armor_Boss.SetValue(100.0)
LL_EpicChance_Standard.SetValue(100.0)
LL_EpicChance_VendorBase.SetValue(100.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(AlwaysLegend)
if game.GetPlayer().GetLevel() > 50
     ChanceLegendary.SetValue(20)
elseif game.GetPlayer().GetLevel() > 40
     ChanceLegendary.SetValue(18)
elseif game.GetPlayer().GetLevel() > 30
     ChanceLegendary.SetValue(15)
elseif game.GetPlayer().GetLevel() > 20
     ChanceLegendary.SetValue(12)
elseif game.GetPlayer().GetLevel() > 5
     ChanceLegendary.SetValue(10)
endif
LL_ChanceNone_PowerArmorMod.SetValue(50.0)
LL_EpicChance_Armor_Boss.SetValue(50.0)
LL_EpicChance_Standard.SetValue(10.0)
LL_EpicChance_VendorBase.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(InfiniteGremadesPerk, false)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(InfiniteGremadesPerk)
InfiniteGrenadeQuestProperty.Stop()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Perk Property CarryweightPerk Auto Const

Perk Property InfiniteMinesPerk Auto Const

Perk Property InfiniteAmmoPerk Auto Const

Perk Property AlwaysLegend Auto Const

GlobalVariable Property ChanceLegendary Auto Const

GlobalVariable Property LL_ChanceNone_PowerArmorMod Auto Const

GlobalVariable Property LL_EpicChance_Armor_Boss Auto Const

GlobalVariable Property LL_EpicChance_Standard Auto Const

GlobalVariable Property LL_EpicChance_VendorBase Auto Const

Quest Property InfiniteAmmoQuestProperty Auto Const

Perk Property InfiniteGremadesPerk Auto Const

Quest Property InfiniteGrenadeQuestProperty Auto Const

Perk Property CheatTerminal_InfiniteItems Auto Const

Quest Property CheatTerminal_InfiniteItemsQuest Auto Const

Perk Property CheatTerminal_FortifyCarryWeight Auto Const

Message Property CheatTerminal_InfiniteAmmoQuery Auto Const

GlobalVariable Property CheatTerminal_ReadMeEnable Auto Const
