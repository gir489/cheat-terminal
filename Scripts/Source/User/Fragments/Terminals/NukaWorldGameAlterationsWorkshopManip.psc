;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:NukaWorldGameAlterationsWorkshopManip Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004D8E6, "DLCNukaWorld.esm")
If ( formFromMod )
	CheatTerminalGlobalFuncs.SpawnWorkshopItem(formFromMod, true)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002D44C, "DLCNukaWorld.esm")
If ( formFromMod )
	CheatTerminalGlobalFuncs.SpawnWorkshopItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002D44C, "DLCNukaWorld.esm")
If ( formFromMod )
	WorkshopScript workshopRef = CheatTerminalGlobalFuncs.FindClosestWorkshop()
	if ( workshopRef )
		ObjectReference[] linkedContainers = workshopRef.FindAllReferencesOfType(formFromMod, 4000.0)
		int i = 0
		While ( i < linkedContainers.Length)
			ObjectReference objFromArray = linkedContainers[i]
			DLC04:TributeChestScript scriptedContainer = objFromArray as DLC04:TributeChestScript
			scriptedContainer.SpawnItemsIfNeeded(scriptedContainer.DaysBetweenItemSpawns)
			i += 1
		EndWhile
	else
		CheatTerminal_GameAlterations_MissingWorkshop.Show()
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
Message Property CheatTerminal_GameAlterations_MissingWorkshop Auto Const
