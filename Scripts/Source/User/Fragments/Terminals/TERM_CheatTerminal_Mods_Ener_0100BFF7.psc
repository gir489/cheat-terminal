;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Ener_0100BFF7 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GatlingLaser_Scope_SightsIron)
else
	Game.GetPlayer().AddItem(miscmod_mod_GatlingLaser_Scope_SightsIron)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GatlingLaser_Scope_SightsReflex)
else
	Game.GetPlayer().AddItem(miscmod_mod_GatlingLaser_Scope_SightsReflex)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_GatlingLaser_Scope_SightsIron Auto Const
ObjectMod Property mod_GatlingLaser_Scope_SightsReflex Auto Const
MiscObject Property miscmod_mod_GatlingLaser_Scope_SightsIron Auto Const
MiscObject Property miscmod_mod_GatlingLaser_Scope_SightsReflex Auto Const
