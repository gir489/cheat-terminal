;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:FarHarborLocationsHP Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_25
Function Fragment_Terminal_25(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x000258E3, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_26
Function Fragment_Terminal_26(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004FD24, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_27
Function Fragment_Terminal_27(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x000347D1, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_28
Function Fragment_Terminal_28(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00049D0F, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_29
Function Fragment_Terminal_29(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x000247BF, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_30
Function Fragment_Terminal_30(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00009351, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_31
Function Fragment_Terminal_31(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0001046E, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_32
Function Fragment_Terminal_32(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00024F71, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_33
Function Fragment_Terminal_33(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004FFA6, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_34
Function Fragment_Terminal_34(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004FFA8, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_35
Function Fragment_Terminal_35(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x000258E7, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_36
Function Fragment_Terminal_36(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00037DD7, "DLCCoast.esm") ;DLC03MQ06a_TapeObjMarker
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
