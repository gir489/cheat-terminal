;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Weapons_E_01002E0B Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BaseballGrenade, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CryoGrenade, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(fragGrenade, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(DN102_FrenzyGrenade, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(SynthTeleportGrenade, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(PulseGrenadeInstitute, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MolotovCocktail, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(nukaGrenade, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(PlasmaGrenade, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(PulseGrenade, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MS02HomingBeacon, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(ArtillerySmokeFlare, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BoSVertibirdGrenade, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Weapon Property BaseballGrenade Auto Const
Weapon Property CryoGrenade Auto Const
Weapon Property fragGrenade Auto Const
Weapon Property DN102_FrenzyGrenade Auto Const
Weapon Property SynthTeleportGrenade Auto Const
Weapon Property PulseGrenadeInstitute Auto Const
Weapon Property MolotovCocktail Auto Const
Weapon Property nukaGrenade Auto Const
Weapon Property PlasmaGrenade Auto Const
Weapon Property PulseGrenade Auto Const
GlobalVariable Property CheatTerminal_ThrowablesToGive Auto Const
Weapon Property MS02HomingBeacon Auto Const
Weapon Property ArtillerySmokeFlare Auto Const
Weapon Property BoSVertibirdGrenade Auto Const
