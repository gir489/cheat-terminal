;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:AUGMoveToLocation Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x0009B571, "Fallout4.esm") as ObjectReference, 3892.9597, 1509.7491, 512.0000, 0.0, CheatTerminal_NakatomiLookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x000BF95E, "Fallout4.esm") as ObjectReference, -1576.7009, -6.3812, -6.3278, 0.0, CheatTerminal_F88LookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x000585B9, "Fallout4.esm") as ObjectReference, 757.0102, 422.2244, 945.117, 0.0, CheatTerminal_STGLookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x000B42E5, "Fallout4.esm") as ObjectReference, -298.4797, 414.8561, 261.5708, 0.0,CheatTerminal_DonutLookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property CheatTerminal_STGLookAtRef Auto Const
ObjectReference Property CheatTerminal_F88LookAtRef Auto Const
ObjectReference Property CheatTerminal_NakatomiLookAtRef Auto Const
ObjectReference Property CheatTerminal_DonutLookAtRef Auto Const
