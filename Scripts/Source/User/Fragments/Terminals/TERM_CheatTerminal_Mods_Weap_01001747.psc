;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Weap_01001747 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Receiver_Standard)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Receiver_Standard)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Receiver_BetterCriticals)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Receiver_BetterCriticals)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Receiver_MoreDamage1)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Receiver_MoreDamage1)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Receiver_MoreDamage2_and_BetterCriticals)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Receiver_MoreDamage2_and_BetterCriticals)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Receiver_Lighter)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Receiver_Lighter)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Receiver_MoreDamage2)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Receiver_MoreDamage2)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Receiver_AmmoConversion)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Receiver_AmmoConversion)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Receiver_MoreDamage3_AmmoConversion)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Receiver_MoreDamage3_AmmoConversion)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Receiver_Heavier)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Receiver_Heavier)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Receiver_FastAction)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Receiver_FastAction)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_PipeRevolver_Receiver_Standard Auto Const
ObjectMod Property mod_PipeRevolver_Receiver_Lighter Auto Const
ObjectMod Property mod_PipeRevolver_Receiver_Heavier Auto Const
ObjectMod Property mod_PipeRevolver_Receiver_FastAction Auto Const
ObjectMod Property mod_PipeRevolver_Receiver_BetterCriticals Auto Const
ObjectMod Property mod_PipeRevolver_Receiver_MoreDamage1 Auto Const
ObjectMod Property mod_PipeRevolver_Receiver_MoreDamage2_and_BetterCriticals Auto Const
ObjectMod Property mod_PipeRevolver_Receiver_MoreDamage2 Auto Const
ObjectMod Property mod_PipeRevolver_Receiver_AmmoConversion Auto Const
ObjectMod Property mod_PipeRevolver_Receiver_MoreDamage3_AmmoConversion Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Receiver_Standard Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Receiver_Lighter Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Receiver_Heavier Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Receiver_FastAction Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Receiver_BetterCriticals Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Receiver_MoreDamage1 Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Receiver_MoreDamage2_and_BetterCriticals Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Receiver_MoreDamage2 Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Receiver_AmmoConversion Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Receiver_MoreDamage3_AmmoConversion Auto Const
