;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Weapons_M_010044DF Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_ChineseOfficerSword)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(Aspiration_Weapon_Drumlin_OfficerSword)
weaponRef.AttachMod(mod_Legendary_Weapon_DamageVsRobots)
weaponRef.AttachMod(mod_melee_ChineseOfficerSword_Serrated)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(MS02_LL_ChineseOfficerSword)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Knife)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_DN101PickmansBlade,CustomItemMods_DN101PickmansBlade)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_GrognaksAxe, CustomItemMods_GrognaksAxe)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Machete)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(DN033_LL_Machete_Sacrificial)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_RevolutionarySword_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(LL_RevolutionarySword_ShemDrowne)
weaponRef.AttachMod(mod_Custom_RevolutionarySword_ShemDrowne)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Ripper_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(Aspiration_Weapon_Work_Ripper)
weaponRef.AttachMod(mod_Legendary_Weapon_LessDamageStandStill)
weaponRef.AttachMod(mod_Aspiration_Weapon_IncreasedCost_10)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Shishkebab_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Switchblade_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
LeveledItem Property LL_ChineseOfficerSword Auto Const
LeveledItem Property MS02_LL_ChineseOfficerSword Auto Const
LeveledItem Property Aspiration_Weapon_Drumlin_OfficerSword Auto Const
ObjectMod Property mod_Legendary_Weapon_DamageVsRobots Auto Const
ObjectMod Property mod_melee_ChineseOfficerSword_Serrated Auto Const
LeveledItem Property LL_Knife Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_DN101PickmansBlade Auto Const
FormList Property CustomItemMods_DN101PickmansBlade Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_GrognaksAxe Auto Const
FormList Property CustomItemMods_GrognaksAxe Auto Const
LeveledItem Property LL_Machete Auto Const
LeveledItem Property DN033_LL_Machete_Sacrificial Auto Const
LeveledItem Property LL_RevolutionarySword_Simple Auto Const
LeveledItem Property LL_RevolutionarySword_ShemDrowne Auto Const
ObjectMod Property mod_Custom_RevolutionarySword_ShemDrowne Auto Const
LeveledItem Property LL_Ripper_Simple Auto Const
LeveledItem Property Aspiration_Weapon_Work_Ripper Auto Const
ObjectMod Property mod_Legendary_Weapon_LessDamageStandStill Auto Const
ObjectMod Property mod_Aspiration_Weapon_IncreasedCost_10 Auto Const
LeveledItem Property LL_Shishkebab_Simple Auto Const
LeveledItem Property LL_Switchblade_Simple Auto Const
GlobalVariable Property LL_EpicChance_Standard Auto Const
