;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:DeagleWeapons Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00000F9C, "DesertEagleNV.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00006B90, "DesertEagleNV.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00006B91, "DesertEagleNV.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00006B92, "DesertEagleNV.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x0000732F, "DesertEagleNV.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
CheatTerminal_DEagle_gir489.Revert()
CheatTerminal_DEagle_gir489.AddForm(Game.GetFormFromFile(0x00006B91, "DesertEagleNV.esp"), 1, 1)
ObjectReference weaponRef = playerActor.PlaceAtMe(CheatTerminal_DEagle_gir489)
weaponRef.AttachMod(Game.GetFormFromFile(0x00006B68,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00002E1E,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00006B5F,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00006B77,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00002E1F,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x000035CB,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00005C13,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00004CBF,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00003D76,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(CheatTerminal_NoRecoilRapidFire)
weaponRef.AttachMod(mod_Legendary_Weapon_DamageLimbRecoil)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
CheatTerminal_DEagleS_gir489.Revert()
CheatTerminal_DEagleS_gir489.AddForm(Game.GetFormFromFile(0x00006B92, "DesertEagleNV.esp"), 1, 1)
ObjectReference weaponRef = playerActor.PlaceAtMe(CheatTerminal_DEagleS_gir489)
weaponRef.AttachMod(Game.GetFormFromFile(0x00006B68,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00002E1E,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00006B5F,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00006B77,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00002E22,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x000035CB,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00005C13,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00004CBF,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00003D74,"DesertEagleNV.esp") as ObjectMod)
weaponRef.AttachMod(CheatTerminal_NoRecoilRapidFire)
weaponRef.AttachMod(mod_Legendary_Weapon_DamageFirstBlood)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
LeveledItem Property CheatTerminal_DEagle_gir489 Auto Const
LeveledItem Property CheatTerminal_DEagleS_gir489 Auto Const
ObjectMod Property CheatTerminal_NoRecoilRapidFire Auto Const
ObjectMod Property mod_Legendary_Weapon_DamageLimbRecoil Auto Const
ObjectMod Property mod_Legendary_Weapon_DamageFirstBlood Auto Const
