;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Weap_01007A8F Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AssaultRifle_Mag_ExtraLarge)
else
	Game.GetPlayer().AddItem(miscmod_mod_AssaultRifle_Mag_ExtraLarge)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AssaultRifle_Mag_Medium)
else
	Game.GetPlayer().AddItem(miscmod_mod_AssaultRifle_Mag_Medium)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AssaultRifle_Mag_MediumQuick)
else
	Game.GetPlayer().AddItem(miscmod_mod_AssaultRifle_Mag_MediumQuick)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AssaultRifle_Mag_ExtraLargeQuick)
else
	Game.GetPlayer().AddItem(miscmod_mod_AssaultRifle_Mag_ExtraLargeQuick)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_AssaultRifle_Mag_Medium Auto Const
ObjectMod Property mod_AssaultRifle_Mag_ExtraLarge Auto Const
ObjectMod Property mod_AssaultRifle_Mag_MediumQuick Auto Const
ObjectMod Property mod_AssaultRifle_Mag_ExtraLargeQuick Auto Const
MiscObject Property miscmod_mod_AssaultRifle_Mag_Medium Auto Const
MiscObject Property miscmod_mod_AssaultRifle_Mag_ExtraLarge Auto Const
MiscObject Property miscmod_mod_AssaultRifle_Mag_MediumQuick Auto Const
MiscObject Property miscmod_mod_AssaultRifle_Mag_ExtraLargeQuick Auto Const

