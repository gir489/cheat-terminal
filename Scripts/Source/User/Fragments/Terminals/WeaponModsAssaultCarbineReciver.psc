;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:WeaponModsAssaultCarbineReciver Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000F9B,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x0000179F,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FEF,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x0000179B,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FED,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x0000179A,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FE9,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00001798,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FF1,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x0000179C,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FDD,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00001791,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FDF,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00001792,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FF5,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x0000365C,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FEB,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00001799,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FF3,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x0000179D,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FE1,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00001793,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00005C92,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00005C93,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00002706,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x0000367E,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FE7,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00001796,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FE3,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00001795,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FF7,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x0000179E,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00005C91,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00005C90,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FE5,"DPAssaultCarbine.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x0000367C,"DPAssaultCarbine.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00003A76,"DPAssaultCarbineNukaWorld.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00003A77,"DPAssaultCarbineNukaWorld.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00003A79,"DPAssaultCarbineNukaWorld.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00003A78,"DPAssaultCarbineNukaWorld.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
