;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__0100A90A Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_44_Scope_SightsIron)
else
	Game.GetPlayer().AddItem(miscmod_mod_44_Scope_SightsIron)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_44_Scope_ScopeShort)
else
	Game.GetPlayer().AddItem(miscmod_mod_44_Scope_ScopeShort)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_44_Scope_SightReflex)
else
	Game.GetPlayer().AddItem(miscmod_mod_44_Scope_SightReflex)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_44_Scope_ScopeShort_Recon)
else
	Game.GetPlayer().AddItem(miscmod_mod_44_Scope_ScopeShort_Recon)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_44_Scope_SightsIron Auto Const
ObjectMod Property mod_44_Scope_ScopeShort Auto Const
ObjectMod Property mod_44_Scope_SightReflex Auto Const
ObjectMod Property mod_44_Scope_ScopeShort_Recon Auto Const
MiscObject Property miscmod_mod_44_Scope_SightsIron Auto Const
MiscObject Property miscmod_mod_44_Scope_ScopeShort Auto Const
MiscObject Property miscmod_mod_44_Scope_SightReflex Auto Const
MiscObject Property miscmod_mod_44_Scope_ScopeShort_Recon Auto Const
