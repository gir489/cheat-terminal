;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsWorldManipDays Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
float currentDay = GameDay.GetValue()
float currentMonth = GameMonth.GetValue()
float currentMonthMaxDays = MaxDays[currentMonth as int]
if ( currentMonth == 2 && IsLeapYear() )
	currentMonthMaxDays += 1
endIf
currentDay += 1
if ( currentDay > currentMonthMaxDays )
	currentDay = 1
endIf
GameDay.SetValue(currentDay)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
float currentDay = GameDay.GetValue()
float currentMonth = GameMonth.GetValue()
float currentMonthMaxDays = MaxDays[currentMonth as int]
if ( currentMonth == 2 && IsLeapYear() )
	currentMonthMaxDays += 1
endIf
currentDay -= 1
if ( currentMonthMaxDays <= 0)
	currentDay = currentMonthMaxDays
endIf
GameDay.SetValue(currentDay)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_34
Function Fragment_Terminal_34(ObjectReference akTerminalRef)
;BEGIN CODE
GameDay.SetValue(1)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_35
Function Fragment_Terminal_35(ObjectReference akTerminalRef)
;BEGIN CODE
GameDay.SetValue(15)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_36
Function Fragment_Terminal_36(ObjectReference akTerminalRef)
;BEGIN CODE
GameDay.SetValue(28)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
bool Function IsLeapYear()
	int year = 2000 + GameYear.GetValueInt()
	if (((year % 4) == 0) && ((year % 100) != 0))
		return true
	elseIf (((year % 4) == 0) && ((year % 100) == 0) && ((year % 400) == 0))
		return true
	else
		return false
	endIf
endFunction
GlobalVariable Property GameDay Auto Const
GlobalVariable Property GameMonth Auto Const
GlobalVariable Property GameYear Auto Const
Float[] Property MaxDays Auto Const
