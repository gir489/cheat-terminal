;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:NukaWorldWorkshopManipRaider Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
GlobalVariable glov = Game.GetFormFromFile(0x0000CCC1, "DLCNukaWorld.esm") as GlobalVariable
if ( glov != None )
	WorkshopScript workshopRef = CheatTerminalGlobalFuncs.FindClosestWorkshop()
	if ( workshopRef == None )
		CheatTerminal_GameAlterations_MissingWorkshop.Show()
		return
	endIf
	glov.SetValue(0)
	workshopRef.myLocation.SetKeywordData(WorkshopParent.WorkshopType02, 1.0)
	Perk perkFromForm = Game.GetFormFromFile(0x000277D0, "DLCNukaWorld.esm") as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
		CheatTerminal_NukaWorld_HasWW1.SetValue(1)
	endIf
	perkFromForm = Game.GetFormFromFile(0x000277D1, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
		CheatTerminal_NukaWorld_HasWW2.SetValue(1)
	endIf
	perkFromForm = Game.GetFormFromFile(0x000277D2, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
		CheatTerminal_NukaWorld_HasWW3.SetValue(1)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
GlobalVariable glov = Game.GetFormFromFile(0x0000CCC1, "DLCNukaWorld.esm") as GlobalVariable
if ( glov != None )
	glov.SetValue(0)
	int index = 0
	while index < WorkshopParent.WorkshopsCollection.GetCount()
		WorkshopScript workshopRef = WorkshopParent.WorkshopsCollection.GetAt(index) as WorkshopScript
		if ( workshopRef )
			workshopRef.myLocation.SetKeywordData(WorkshopParent.WorkshopType02, 1.0)
		endIf
		index += 1
	endWhile
	Perk perkFromForm = Game.GetFormFromFile(0x000277D0, "DLCNukaWorld.esm") as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
		CheatTerminal_NukaWorld_HasWW1.SetValue(1)
	endIf
	perkFromForm = Game.GetFormFromFile(0x000277D1, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
		CheatTerminal_NukaWorld_HasWW2.SetValue(1)
	endIf
	perkFromForm = Game.GetFormFromFile(0x000277D2, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
		CheatTerminal_NukaWorld_HasWW3.SetValue(1)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
WorkshopParentScript Property WorkshopParent Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasWW1 Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasWW2 Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasWW3 Auto Const
Message Property CheatTerminal_GameAlterations_MissingWorkshop Auto Const
