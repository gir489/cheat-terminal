;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_FarHa_0100266C Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0005158F, "DLCCoast.esm")
if ( formFromMod)
	LeveledItem modLeveledItem = formFromMod as LeveledItem
	FormList modFormList = Game.GetFormFromFile(0x00051591, "DLCCoast.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(modLeveledItem, modFormList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0005158E, "DLCCoast.esm")
if ( formFromMod)
	LeveledItem modLeveledItem = formFromMod as LeveledItem
	FormList modFormList = Game.GetFormFromFile(0x00051598, "DLCCoast.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(modLeveledItem, modFormList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00045675, "DLCCoast.esm")
if ( formFromMod)
	LeveledItem modLeveledItem = formFromMod as LeveledItem
	FormList modFormList = Game.GetFormFromFile(0x00045676, "DLCCoast.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(modLeveledItem, modFormList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00045673, "DLCCoast.esm")
if ( formFromMod)
	LeveledItem modLeveledItem = formFromMod as LeveledItem
	FormList modFormList = Game.GetFormFromFile(0x00045674, "DLCCoast.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(modLeveledItem, modFormList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00014459, "DLCCoast.esm")
if objectToGive
	Actor playerActor = Game.GetPlayer()
	ObjectReference objectRef = playerActor.PlaceAtMe(objectToGive)
	ObjectMod attachment = Game.GetFormFromFile(0x00014457, "DLCCoast.esm") as ObjectMod
	objectRef.AttachMod(attachment)
	playerActor.AddItem(objectRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
