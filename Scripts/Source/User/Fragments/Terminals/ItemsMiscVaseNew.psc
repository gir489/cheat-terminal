;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:ItemsMiscVaseNew Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageCleanFlowers03e, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageCleanFlowers03d, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageCleanFlowers03b, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageCleanFlowers03a, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageCleanFlowers03c, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageCleanFlowers01e, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageCleanFlowers01d, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageCleanFlowers01b, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageCleanFlowers01a, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageCleanFlowers01c, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageCleanFlowers02e, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageCleanFlowers02d, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageCleanFlowers02b, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageCleanFlowers02a, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(VaseVintageCleanFlowers02c, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
GlobalVariable Property CheatTerminal_MiscToGive Auto Const
MiscObject Property VaseVintageCleanFlowers03e Auto Const
MiscObject Property VaseVintageCleanFlowers03d Auto Const
MiscObject Property VaseVintageCleanFlowers03b Auto Const
MiscObject Property VaseVintageCleanFlowers03a Auto Const
MiscObject Property VaseVintageCleanFlowers03c Auto Const
MiscObject Property VaseVintageCleanFlowers01e Auto Const
MiscObject Property VaseVintageCleanFlowers01d Auto Const
MiscObject Property VaseVintageCleanFlowers01b Auto Const
MiscObject Property VaseVintageCleanFlowers01a Auto Const
MiscObject Property VaseVintageCleanFlowers01c Auto Const
MiscObject Property VaseVintageCleanFlowers02e Auto Const
MiscObject Property VaseVintageCleanFlowers02d Auto Const
MiscObject Property VaseVintageCleanFlowers02b Auto Const
MiscObject Property VaseVintageCleanFlowers02a Auto Const
MiscObject Property VaseVintageCleanFlowers02c Auto Const
