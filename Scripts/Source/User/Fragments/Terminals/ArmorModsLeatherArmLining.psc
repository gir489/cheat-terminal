;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:ArmorModsLeatherArmLining Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_armor_Lining_Null)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_armor_Leather_Lining_LimbArm_BetterBlocking)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_armor_Leather_Lining_LimbArm_Brawler)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_armor_Leather_Lining_LimbArm_IgnoreArmor)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_armor_Leather_Lining_LimbArm_ReducedPowerAttack)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_armor_Leather_Lining_LimbArm_Stabilized)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_armor_Leather_Lining_Limb_ImprovedCarryCapacity)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_armor_Leather_Lining_Limb_ImprovedCarryCapacity2)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_armor_Leather_Lining_Limb_Lighter)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_armor_Leather_Lining_Limb_Lighter2)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_armor_Leather_Lining_Limb_ReducedDetection)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_armor_Leather_Lining_Limb_ResistanceLimbDamage)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_armor_Leather_Lining_Limb_ResistanceRadiation)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_armor_Lining_Null Auto Const
ObjectMod Property mod_armor_Leather_Lining_LimbArm_BetterBlocking Auto Const
ObjectMod Property mod_armor_Leather_Lining_LimbArm_Brawler Auto Const
ObjectMod Property mod_armor_Leather_Lining_LimbArm_IgnoreArmor Auto Const
ObjectMod Property mod_armor_Leather_Lining_LimbArm_ReducedPowerAttack Auto Const
ObjectMod Property mod_armor_Leather_Lining_LimbArm_Stabilized Auto Const
ObjectMod Property mod_armor_Leather_Lining_Limb_ImprovedCarryCapacity Auto Const
ObjectMod Property mod_armor_Leather_Lining_Limb_ImprovedCarryCapacity2 Auto Const
ObjectMod Property mod_armor_Leather_Lining_Limb_Lighter Auto Const
ObjectMod Property mod_armor_Leather_Lining_Limb_Lighter2 Auto Const
ObjectMod Property mod_armor_Leather_Lining_Limb_ReducedDetection Auto Const
ObjectMod Property mod_armor_Leather_Lining_Limb_ResistanceLimbDamage Auto Const
ObjectMod Property mod_armor_Leather_Lining_Limb_ResistanceRadiation Auto Const
