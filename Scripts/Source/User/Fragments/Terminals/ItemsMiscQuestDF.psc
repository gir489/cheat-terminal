;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:ItemsMiscQuestDF Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MS02DampeningCoil, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(DN070_VaseVintageDirty03c, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BoS101DeepRangeTransmitter, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(DN070_Newspaper, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(DN070_ToyCar, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Holotape00BADTFL.GetRef())
Game.GetPlayer().AddItem(Holotape02Malden.GetRef())
Game.GetPlayer().AddItem(Holotape03Quincy.GetRef())
Game.GetPlayer().AddItem(Holotape04Natick.GetRef())
Game.GetPlayer().AddItem(Holotape05Nahant.GetRef())
Game.GetPlayer().AddItem(Holotape06EastBoston.GetRef())
Game.GetPlayer().AddItem(Holotape07SouthBoston.GetRef())
Game.GetPlayer().AddItem(Holotape08Fens.GetRef())
Game.GetPlayer().AddItem(Holotape09CoastGuard.GetRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BoSM01_PulserVarham)
Game.GetPlayer().AddItem(BoSM01_PulserFaris)
Game.GetPlayer().AddItem(BoSM01_PulserAstlin)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(RR201TomsHolotape, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(DN053_VirgilsSerum, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(RR303TinkExplosives, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MS11TurbopumpPlaced, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(DN070_Newspaper01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
GlobalVariable Property CheatTerminal_MiscToGive Auto Const
MiscObject Property MS02DampeningCoil Auto Const
MiscObject Property DN070_VaseVintageDirty03c Auto Const
MiscObject Property BoS101DeepRangeTransmitter Auto Const
MiscObject Property DN070_Newspaper Auto Const
MiscObject Property DN070_ToyCar Auto Const
MiscObject Property BoS301DistressPulser Auto Const
MiscObject Property BoSM01_DistressPulser Auto Const
MiscObject Property RR201TomsHolotape Auto Const
MiscObject Property DN053_VirgilsSerum Auto Const
MiscObject Property RR303TinkExplosives Auto Const
;MiscObject Property MS11Turbopump Auto Const
MiscObject Property DN070_Newspaper01 Auto Const
ObjectReference Property MS11TurbopumpPlaced Auto Const
ObjectReference Property BoSM01_PulserVarham Auto Const
ObjectReference Property BoSM01_PulserFaris Auto Const
ObjectReference Property BoSM01_PulserAstlin Auto Const
ReferenceAlias Property Holotape00BADTFL Auto Const
ReferenceAlias Property Holotape02Malden Auto Const
ReferenceAlias Property Holotape03Quincy Auto Const
ReferenceAlias Property Holotape04Natick Auto Const
ReferenceAlias Property Holotape05Nahant Auto Const
ReferenceAlias Property Holotape06EastBoston Auto Const
ReferenceAlias Property Holotape07SouthBoston Auto Const
ReferenceAlias Property Holotape08Fens Auto Const
ReferenceAlias Property Holotape09CoastGuard Auto Const
