;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__0100451B Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00056F80, "DLCCoast.esm")
if objectToGive
	Actor playerActor = Game.GetPlayer()
	LL_EpicChance_Standard.SetValue(0.0)
	playerActor.AddItem(objectToGive)
	playerActor.AddItem(Game.GetFormFromFile(0x00056F7F, "DLCCoast.esm"))
	playerActor.AddItem(Game.GetFormFromFile(0x00056F7E, "DLCCoast.esm"))
	playerActor.AddItem(Game.GetFormFromFile(0x00056F7C, "DLCCoast.esm"))
	playerActor.AddItem(Game.GetFormFromFile(0x00056F7B, "DLCCoast.esm"))
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00056F79, "DLCCoast.esm")
if objectToGive
	Actor playerActor = Game.GetPlayer()
	LL_EpicChance_Standard.SetValue(0.0)
	playerActor.AddItem(objectToGive)
	playerActor.AddItem(Game.GetFormFromFile(0x00056F78, "DLCCoast.esm"))
	playerActor.AddItem(Game.GetFormFromFile(0x00056F77, "DLCCoast.esm"))
	playerActor.AddItem(Game.GetFormFromFile(0x00056F75, "DLCCoast.esm"))
	playerActor.AddItem(Game.GetFormFromFile(0x00056F74, "DLCCoast.esm"))
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00051588, "DLCCoast.esm")
if objectToGive
	Actor playerActor = Game.GetPlayer()
	LL_EpicChance_Standard.SetValue(0.0)
	ObjectReference objRef = playerActor.PlaceAtMe(objectToGive)
	objRef.AttachMod(mod_Legendary_Armor_Agility_and_Perception)
	playerActor.AddItem(objRef)
	objRef = playerActor.PlaceAtMe(Game.GetFormFromFile(0x00051587, "DLCCoast.esm"))
	objRef.AttachMod(mod_Legendary_Armor_LessDamageStandStill)
	playerActor.AddItem(objRef)
	objRef = playerActor.PlaceAtMe(Game.GetFormFromFile(0x0005158A, "DLCCoast.esm"))
	objRef.AttachMod(mod_Legendary_Armor_Speed)
	playerActor.AddItem(objRef)
	objRef = playerActor.PlaceAtMe(Game.GetFormFromFile(0x00051589, "DLCCoast.esm"))
	objRef.AttachMod(mod_Legendary_Armor_LowHealthSlowTime)
	playerActor.AddItem(objRef)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00056F7D, "DLCCoast.esm")
if objectToGive
	LL_EpicChance_Standard.SetValue(0.0)
	Game.GetPlayer().AddItem(objectToGive)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x0003A557, "DLCCoast.esm")
if objectToGive
	LL_EpicChance_Standard.SetValue(0.0)
	Game.GetPlayer().AddItem(objectToGive)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ObjectMod Property mod_Legendary_Armor_Agility_and_Perception Auto Const
ObjectMod Property mod_Legendary_Armor_LessDamageStandStill Auto Const
ObjectMod Property mod_Legendary_Armor_Speed Auto Const
ObjectMod Property mod_Legendary_Armor_LowHealthSlowTime Auto Const

GlobalVariable Property LL_EpicChance_Standard Auto Const
