;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__010035C5 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x0003A2D4, "DLCCoast.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00019E16, "DLCCoast.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00019E17, "DLCCoast.esm"))
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00019E18, "DLCCoast.esm"))
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00040DF0, "DLCCoast.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x000365EE, "DLCCoast.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x0003A2D1, "DLCCoast.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00045E97, "DLCCoast.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00045E99, "DLCCoast.esm"))
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00045E9B, "DLCCoast.esm"))
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00045E9D, "DLCCoast.esm"))
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00045EA6, "DLCCoast.esm"))
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00045EA8, "DLCCoast.esm"))
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00045EAA, "DLCCoast.esm"))
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
