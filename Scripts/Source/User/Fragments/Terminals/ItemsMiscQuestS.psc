;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:ItemsMiscQuestS Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MS11TurbopumpSabotaged, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MQ106_Cigar, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MQ105KelloggCigar, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Min03_ShapedCharge, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MS07ShemAlloy, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MS07b_ShemDrowneSkull, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MQ206SignalInterceptorPlans, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MS07SilverBar, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(DN070_Hairbrush_02_Silver, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MS04CallingCard, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MS04_CollectiblePropGun, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(DN124_BuzzBitesRecipe, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(InsUVSweeper01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
GlobalVariable Property CheatTerminal_MiscToGive Auto Const
MiscObject Property MS11TurbopumpSabotaged Auto Const
MiscObject Property MQ106_Cigar Auto Const
MiscObject Property MQ105KelloggCigar Auto Const
MiscObject Property Min03_ShapedCharge Auto Const
MiscObject Property MS07ShemAlloy Auto Const
MiscObject Property MS07b_ShemDrowneSkull Auto Const
MiscObject Property MQ206SignalInterceptorPlans Auto Const
MiscObject Property MS07SilverBar Auto Const
MiscObject Property DN070_Hairbrush_02_Silver Auto Const
MiscObject Property MS04CallingCard Auto Const
MiscObject Property MS04_CollectiblePropGun Auto Const
MiscObject Property DN124_BuzzBitesRecipe Auto Const
MiscObject Property InsUVSweeper01 Auto Const
