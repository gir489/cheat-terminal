;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Othe_0100BFFA Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Flamer_BarrelHose_Short)
else
	Game.GetPlayer().AddItem(miscmod_mod_Flamer_BarrelHose_Short)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Flamer_BarrelHose_Long)
else
	Game.GetPlayer().AddItem(miscmod_mod_Flamer_BarrelHose_Long)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_Flamer_BarrelHose_Short Auto Const
ObjectMod Property mod_Flamer_BarrelHose_Long Auto Const
MiscObject Property miscmod_mod_Flamer_BarrelHose_Short Auto Const
MiscObject Property miscmod_mod_Flamer_BarrelHose_Long Auto Const
