;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01007A8F Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Mag_Large)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Mag_Large)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Mag_Small)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Mag_Small)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Mag_SmallQuick)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Mag_SmallQuick)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Mag_LargeQuick)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Mag_LargeQuick)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_10mm_Mag_Small Auto Const
ObjectMod Property mod_10mm_Mag_Large Auto Const
ObjectMod Property mod_10mm_Mag_SmallQuick Auto Const
ObjectMod Property mod_10mm_Mag_LargeQuick Auto Const
MiscObject Property miscmod_mod_10mm_Mag_Small Auto Const
MiscObject Property miscmod_mod_10mm_Mag_Large Auto Const
MiscObject Property miscmod_mod_10mm_Mag_SmallQuick Auto Const
MiscObject Property miscmod_mod_10mm_Mag_LargeQuick Auto Const
