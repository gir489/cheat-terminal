;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:AutomatronGameAlterations Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference mapMarker = Game.GetFormFromFile(0x00004314, "DLCRobot.esm") as ObjectReference
If ( mapMarker != None )
	mapMarker.AddToMap(true)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
GlobalVariable glov = Game.GetFormFromFile(0x0000416D, "DLCRobot.esm") as GlobalVariable
if ( glov != None )
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x0000416E, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00004451, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00004450, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x0000B179, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00004452, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00004453, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x0000445C, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00004458, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00004455, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00004457, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x0000445D, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x0000445A, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x0000DE32, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00002A74, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00004454, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x0000DE33, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00002A75, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x0000DE34, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00002A76, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00004456, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x0000DE35, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00002A77, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x0000445E, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x0000DE36, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x0000445B, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00004459, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x000109BF, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x000109C0, "DLCRobot.esm") as GlobalVariable
	glov.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0000FF12, "DLCRobot.esm")
if ( formFromMod )
	ObjectReference adaRef = formFromMod as ObjectReference
	CheatTerminalGlobalFuncs.DoCompanionTeleport(akTerminalRef, adaRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference mapMarker = Game.GetFormFromFile(0x00004314, "DLCRobot.esm") as ObjectReference
If ( mapMarker != None )
	mapMarker.AddToMap(false)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, Game.GetFormFromFile(0x0000097C, "DLCRobot.esm") as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
