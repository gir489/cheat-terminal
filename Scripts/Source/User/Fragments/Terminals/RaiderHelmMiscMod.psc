;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:RaiderHelmMiscMod Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(GetArmorPiece()) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(GetArmorPiece()) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(GetArmorPiece(), PA_Misc_Null)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(GetArmorPiece()) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(GetArmorPiece()) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(GetArmorPiece(), PA_Raider_Helmet_Misc_DetectLife)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(GetArmorPiece()) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(GetArmorPiece()) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(GetArmorPiece(), PA_Raider_Helmet_Misc_Int)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(GetArmorPiece()) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(GetArmorPiece()) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(GetArmorPiece(), PA_Raider_Helmet_Misc_RadScrubbers)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(GetArmorPiece()) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(GetArmorPiece()) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(GetArmorPiece(), PA_Raider_Helmet_Misc_Sensor)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Form Function GetArmorPiece()
	return Armor_Power_Raider_Helm
EndFunction
Message Property CheatTerminal_PowerArmorModMessage Auto Const
Message Property CheatTerminal_PowerArmorTooMany Auto Const
Message Property CheatTerminal_PowerArmorNotEnough Auto Const
Armor Property Armor_Power_Raider_Helm Auto Const
ObjectMod Property PA_Misc_Null Auto Const
ObjectMod Property PA_Raider_Helmet_Misc_DetectLife Auto Const
ObjectMod Property PA_Raider_Helmet_Misc_Int Auto Const
ObjectMod Property PA_Raider_Helmet_Misc_RadScrubbers Auto Const
ObjectMod Property PA_Raider_Helmet_Misc_Sensor Auto Const
