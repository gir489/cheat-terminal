;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsWorldManipYears Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
GameYear.Mod(1)
CheatTerminal_GameAlterations_SetYear.Show(2000 + GameYear.GetValue())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
if ( GameYear.GetValue() > 287 )
	GameYear.Mod(-1)
endIf
CheatTerminal_GameAlterations_SetYear.Show(2000 + GameYear.GetValue())
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Message Property CheatTerminal_GameAlterations_SetYear Auto Const

GlobalVariable Property GameYear Auto Const
