;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsSurvivalManipCell Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
HC_HoursToRespawnCellMult.SetValue(0.5)
CheatTerminal_HC_HoursToRespawnCellMult.SetValue(0.5)
if ( CheatTerminal_PersistHoursToRespawnCellMult.IsRunning() == false )
	CheatTerminal_PersistHoursToRespawnCellMult.Start()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
HC_HoursToRespawnCellMult.SetValue(0)
CheatTerminal_HC_HoursToRespawnCellMult.SetValue(0)
if ( CheatTerminal_PersistHoursToRespawnCellMult.IsRunning() == false )
	CheatTerminal_PersistHoursToRespawnCellMult.Start()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
HC_HoursToRespawnCellMult.SetValue(1)
CheatTerminal_HC_HoursToRespawnCellMult.SetValue(1)
if ( CheatTerminal_PersistHoursToRespawnCellMult.IsRunning() == false )
	CheatTerminal_PersistHoursToRespawnCellMult.Start()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
HC_HoursToRespawnCellMult.SetValue(2)
CheatTerminal_HC_HoursToRespawnCellMult.SetValue(2)
if ( CheatTerminal_PersistHoursToRespawnCellMult.IsRunning() == false )
	CheatTerminal_PersistHoursToRespawnCellMult.Start()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
HC_HoursToRespawnCellMult.SetValue(4)
CheatTerminal_HC_HoursToRespawnCellMult.SetValue(4)
if ( CheatTerminal_PersistHoursToRespawnCellMult.IsRunning() == true )
	CheatTerminal_PersistHoursToRespawnCellMult.Stop()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
HC_HoursToRespawnCellMult.SetValue(5)
CheatTerminal_HC_HoursToRespawnCellMult.SetValue(5)
if ( CheatTerminal_PersistHoursToRespawnCellMult.IsRunning() == true )
	CheatTerminal_PersistHoursToRespawnCellMult.Stop()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
HC_HoursToRespawnCellMult.SetValue(10)
CheatTerminal_HC_HoursToRespawnCellMult.SetValue(10)
if ( CheatTerminal_PersistHoursToRespawnCellMult.IsRunning() == false )
	CheatTerminal_PersistHoursToRespawnCellMult.Start()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
HC_HoursToRespawnCellMult.SetValue(15)
CheatTerminal_HC_HoursToRespawnCellMult.SetValue(15)
if ( CheatTerminal_PersistHoursToRespawnCellMult.IsRunning() == false )
	CheatTerminal_PersistHoursToRespawnCellMult.Start()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
HC_HoursToRespawnCellMult.SetValue(24)
CheatTerminal_HC_HoursToRespawnCellMult.SetValue(24)
if ( CheatTerminal_PersistHoursToRespawnCellMult.IsRunning() == false )
	CheatTerminal_PersistHoursToRespawnCellMult.Start()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
HC_HoursToRespawnCellMult.SetValue(48)
CheatTerminal_HC_HoursToRespawnCellMult.SetValue(48)
if ( CheatTerminal_PersistHoursToRespawnCellMult.IsRunning() == false )
	CheatTerminal_PersistHoursToRespawnCellMult.Start()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
HC_HoursToRespawnCellMult.SetValue(72)
CheatTerminal_HC_HoursToRespawnCellMult.SetValue(72)
if ( CheatTerminal_PersistHoursToRespawnCellMult.IsRunning() == false )
	CheatTerminal_PersistHoursToRespawnCellMult.Start()
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
GlobalVariable Property HC_HoursToRespawnCellMult Auto Const
GlobalVariable Property CheatTerminal_HC_HoursToRespawnCellMult Auto Const
Quest Property CheatTerminal_PersistHoursToRespawnCellMult Auto Const
