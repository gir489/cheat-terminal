;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Ener_01001EF9 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Muzzle_Camera_A)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Muzzle_Camera_A)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Muzzle_Splitter_A)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Muzzle_Splitter_A)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Muzzle_Focuser_A)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Muzzle_Focuser_A)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Muzzle_Splitter_B)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Muzzle_Splitter_B)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Muzzle_Focuser_B)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Muzzle_Focuser_B)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Muzzle_Camera_B)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Muzzle_Camera_B)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Null_Muzzle as ObjectMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_LaserGun_Muzzle_Splitter_A Auto Const
ObjectMod Property mod_LaserGun_Muzzle_Focuser_A Auto Const
ObjectMod Property mod_LaserGun_Muzzle_Camera_A Auto Const
ObjectMod Property mod_LaserGun_Muzzle_Splitter_B Auto Const
ObjectMod Property mod_LaserGun_Muzzle_Focuser_B Auto Const
ObjectMod Property mod_LaserGun_Muzzle_Camera_B Auto Const
MiscObject Property miscmod_mod_LaserGun_Muzzle_Splitter_A Auto Const
MiscObject Property miscmod_mod_LaserGun_Muzzle_Focuser_A Auto Const
MiscObject Property miscmod_mod_LaserGun_Muzzle_Camera_A Auto Const
MiscObject Property miscmod_mod_LaserGun_Muzzle_Splitter_B Auto Const
MiscObject Property miscmod_mod_LaserGun_Muzzle_Focuser_B Auto Const
MiscObject Property miscmod_mod_LaserGun_Muzzle_Camera_B Auto Const

ObjectMod Property mod_Null_Muzzle Auto Const
