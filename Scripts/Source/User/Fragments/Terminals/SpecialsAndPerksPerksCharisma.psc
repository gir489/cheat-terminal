;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:SpecialsAndPerksPerksCharisma Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(CapCollector01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(CapCollector02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(CapCollector03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(CapCollector01)
Game.GetPlayer().RemovePerk(CapCollector02)
Game.GetPlayer().RemovePerk(CapCollector03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(LadyKiller01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(LadyKiller02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(LadyKiller03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(LadyKiller01)
Game.GetPlayer().RemovePerk(LadyKiller02)
Game.GetPlayer().RemovePerk(LadyKiller03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(BlackWidow01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(BlackWidow02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(BlackWidow03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(BlackWidow01)
Game.GetPlayer().RemovePerk(BlackWidow02)
Game.GetPlayer().RemovePerk(BlackWidow03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(LoneWanderer01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(LoneWanderer02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(LoneWanderer03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(LoneWanderer01)
Game.GetPlayer().RemovePerk(LoneWanderer02)
Game.GetPlayer().RemovePerk(LoneWanderer03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(AttackDog01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(AttackDog02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(AttackDog03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(AttackDog01)
Game.GetPlayer().RemovePerk(AttackDog02)
Game.GetPlayer().RemovePerk(AttackDog03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(AnimalFriend01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_22
Function Fragment_Terminal_22(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(AnimalFriend02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_23
Function Fragment_Terminal_23(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(AnimalFriend03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_24
Function Fragment_Terminal_24(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(AnimalFriend01)
Game.GetPlayer().RemovePerk(AnimalFriend02)
Game.GetPlayer().RemovePerk(AnimalFriend03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_25
Function Fragment_Terminal_25(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(LocalLeader01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_26
Function Fragment_Terminal_26(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(LocalLeader02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_27
Function Fragment_Terminal_27(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(LocalLeader01)
Game.GetPlayer().RemovePerk(LocalLeader02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_28
Function Fragment_Terminal_28(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(PartyBoy01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_29
Function Fragment_Terminal_29(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(PartyBoy02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_30
Function Fragment_Terminal_30(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(PartyBoy03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_31
Function Fragment_Terminal_31(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(PartyBoy01)
Game.GetPlayer().RemovePerk(PartyBoy02)
Game.GetPlayer().RemovePerk(PartyBoy03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_32
Function Fragment_Terminal_32(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(PartyGirl01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_33
Function Fragment_Terminal_33(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(PartyGirl02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_34
Function Fragment_Terminal_34(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(PartyGirl03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_35
Function Fragment_Terminal_35(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(PartyGirl01)
Game.GetPlayer().RemovePerk(PartyGirl02)
Game.GetPlayer().RemovePerk(PartyGirl03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_36
Function Fragment_Terminal_36(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Inspirational01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_37
Function Fragment_Terminal_37(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Inspirational02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_38
Function Fragment_Terminal_38(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Inspirational03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_39
Function Fragment_Terminal_39(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Inspirational01)
Game.GetPlayer().RemovePerk(Inspirational02)
Game.GetPlayer().RemovePerk(Inspirational03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_40
Function Fragment_Terminal_40(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(WastelandWhisperer01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_41
Function Fragment_Terminal_41(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(WastelandWhisperer02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_42
Function Fragment_Terminal_42(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(WastelandWhisperer03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_43
Function Fragment_Terminal_43(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(WastelandWhisperer01)
Game.GetPlayer().RemovePerk(WastelandWhisperer02)
Game.GetPlayer().RemovePerk(WastelandWhisperer03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_44
Function Fragment_Terminal_44(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Intimidation01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_45
Function Fragment_Terminal_45(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Intimidation02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_46
Function Fragment_Terminal_46(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Intimidation03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_47
Function Fragment_Terminal_47(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Intimidation01)
Game.GetPlayer().RemovePerk(Intimidation02)
Game.GetPlayer().RemovePerk(Intimidation03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_48
Function Fragment_Terminal_48(ObjectReference akTerminalRef)
;BEGIN CODE
ActorBase PlayerBase = Game.GetPlayer().GetBaseObject() as ActorBase
bool bIsMale = PlayerBase.GetSex() == 0
givePerk(CapCollector01)
givePerk(CapCollector02)
givePerk(CapCollector03)
if ( bIsMale )
givePerk(LadyKiller01)
givePerk(LadyKiller02)
givePerk(LadyKiller03)
else
givePerk(BlackWidow01)
givePerk(BlackWidow02)
givePerk(BlackWidow03)
endif
givePerk(LoneWanderer01)
givePerk(LoneWanderer02)
givePerk(LoneWanderer03)
givePerk(AttackDog01)
givePerk(AttackDog02)
givePerk(AttackDog03)
givePerk(AnimalFriend01)
givePerk(AnimalFriend02)
givePerk(AnimalFriend03)
givePerk(LocalLeader01)
givePerk(LocalLeader02)
if ( bIsMale )
givePerk(PartyBoy01)
givePerk(PartyBoy02)
givePerk(PartyBoy03)
else
givePerk(PartyGirl01)
givePerk(PartyGirl02)
givePerk(PartyGirl03)
endif
givePerk(Inspirational01)
givePerk(Inspirational02)
givePerk(Inspirational03)
givePerk(WastelandWhisperer01)
givePerk(WastelandWhisperer02)
givePerk(WastelandWhisperer03)
givePerk(Intimidation01)
givePerk(Intimidation02)
givePerk(Intimidation03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_49
Function Fragment_Terminal_49(ObjectReference akTerminalRef)
;BEGIN CODE
ActorBase PlayerBase = Game.GetPlayer().GetBaseObject() as ActorBase
bool bIsMale = PlayerBase.GetSex() == 0
removePerk(CapCollector01)
removePerk(CapCollector02)
removePerk(CapCollector03)
if ( bIsMale )
removePerk(LadyKiller01)
removePerk(LadyKiller02)
removePerk(LadyKiller03)
else
removePerk(BlackWidow01)
removePerk(BlackWidow02)
removePerk(BlackWidow03)
endif
removePerk(LoneWanderer01)
removePerk(LoneWanderer02)
removePerk(LoneWanderer03)
removePerk(AttackDog01)
removePerk(AttackDog02)
removePerk(AttackDog03)
removePerk(AnimalFriend01)
removePerk(AnimalFriend02)
removePerk(AnimalFriend03)
removePerk(LocalLeader01)
removePerk(LocalLeader02)
if ( bIsMale )
removePerk(PartyBoy01)
removePerk(PartyBoy02)
removePerk(PartyBoy03)
else
removePerk(PartyGirl01)
removePerk(PartyGirl02)
removePerk(PartyGirl03)
endif
removePerk(Inspirational01)
removePerk(Inspirational02)
removePerk(Inspirational03)
removePerk(WastelandWhisperer01)
removePerk(WastelandWhisperer02)
removePerk(WastelandWhisperer03)
removePerk(Intimidation01)
removePerk(Intimidation02)
removePerk(Intimidation03)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Function givePerk(Perk perkToGive)
	Actor playerActor = Game.GetPlayer()
	if (playerActor.HasPerk(perkToGive) == false)
		playerActor.AddPerk(perkToGive)
	endIf
endFunction
Function removePerk(Perk perkToGive)
	Actor playerActor = Game.GetPlayer()
	if (playerActor.HasPerk(perkToGive))
		playerActor.RemovePerk(perkToGive)
	endIf
endFunction
Perk Property CapCollector01 Auto Const
Perk Property CapCollector02 Auto Const
Perk Property CapCollector03 Auto Const
Perk Property LadyKiller01 Auto Const
Perk Property LadyKiller02 Auto Const
Perk Property LadyKiller03 Auto Const
Perk Property BlackWidow01 Auto Const
Perk Property BlackWidow02 Auto Const
Perk Property BlackWidow03 Auto Const
Perk Property LoneWanderer01 Auto Const
Perk Property LoneWanderer02 Auto Const
Perk Property LoneWanderer03 Auto Const
Perk Property AttackDog01 Auto Const
Perk Property AttackDog02 Auto Const
Perk Property AttackDog03 Auto Const
Perk Property AnimalFriend01 Auto Const
Perk Property AnimalFriend02 Auto Const
Perk Property AnimalFriend03 Auto Const
Perk Property LocalLeader01 Auto Const
Perk Property LocalLeader02 Auto Const
Perk Property PartyBoy01 Auto Const
Perk Property PartyBoy02 Auto Const
Perk Property PartyBoy03 Auto Const
Perk Property PartyGirl01 Auto Const
Perk Property PartyGirl02 Auto Const
Perk Property PartyGirl03 Auto Const
Perk Property Inspirational01 Auto Const
Perk Property Inspirational02 Auto Const
Perk Property Inspirational03 Auto Const
Perk Property WastelandWhisperer01 Auto Const
Perk Property WastelandWhisperer02 Auto Const
Perk Property WastelandWhisperer03 Auto Const
Perk Property Intimidation01 Auto Const
Perk Property Intimidation02 Auto Const
Perk Property Intimidation03 Auto Const
