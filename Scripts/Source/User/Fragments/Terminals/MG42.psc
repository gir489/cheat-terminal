;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:MG42 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00000FB4, "MG42.esp") as Form)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x0000ECB6, "MG42.esp") as Form)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00010467, "MG42.esp") as Form, CheatTerminal_AmmoToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
CheatTerminal_MG42_gir489.Revert()
CheatTerminal_MG42_gir489.AddForm(Game.GetFormFromFile(0x00000FB4, "MG42.esp"), 1, 1)
ObjectReference weaponRef = playerActor.PlaceAtMe(CheatTerminal_MG42_gir489)
weaponRef.AttachMod(Game.GetFormFromFile(0x00000F9D,"MG42.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00000F9F,"MG42.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000CE0B,"MG42.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000CE08,"MG42.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x000035CA,"MG42.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000E4F3,"MG42.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00001869,"MG42.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00001856,"MG42.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000ECA1,"MG42.esp") as ObjectMod)
weaponRef.AttachMod(CheatTerminal_NoRecoil)
weaponRef.AttachMod(mod_Legendary_Weapon_DamageLimbRecoil)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
GlobalVariable Property CheatTerminal_AmmoToGive Auto Const
LeveledItem Property CheatTerminal_MG42_gir489 Auto Const
ObjectMod Property CheatTerminal_NoRecoil Auto Const
ObjectMod Property mod_Legendary_Weapon_DamageLimbRecoil Auto Const
