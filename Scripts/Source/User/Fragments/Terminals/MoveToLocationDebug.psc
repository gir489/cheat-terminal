;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:MoveToLocationDebug Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, zUnusedCaveCOCMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, QASmokeCOCMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, DebugCompanionsCOCMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, DebugJPSACOCMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, DebugRangeCOCMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.ReturnPlayerFromDebug(akTerminalRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, zUnusedCellar02COCMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, zUnusedFalloutShelter02COCMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, zUnusedFinancial23COCMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, zUnusedGrenadesCOCMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, zUnusedSubwayTerminalShowcaseCOCMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, zUnusedTheater46ACOCMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, zUnusedTheaterLaundromatCOCMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, CTESTCOCMarker)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property QASmokeCOCMarker Auto Const
ObjectReference Property DebugCompanionsCOCMarker Auto Const
ObjectReference Property DebugRangeCOCMarker Auto Const
ObjectReference Property DebugJPSACOCMarker Auto Const
ObjectReference Property SancHillsRef Auto Const
ObjectReference Property SancHillsPreWarRef Auto Const
ObjectReference Property zUnusedCaveCOCMarker Auto Const
ObjectReference Property zUnusedCellar02COCMarker Auto Const
ObjectReference Property zUnusedFalloutShelter02COCMarker Auto Const
ObjectReference Property zUnusedFinancial23COCMarker Auto Const
ObjectReference Property zUnusedGrenadesCOCMarker Auto Const
ObjectReference Property zUnusedSubwayTerminalShowcaseCOCMarker Auto Const
ObjectReference Property zUnusedTheater46ACOCMarker Auto Const
ObjectReference Property zUnusedTheaterLaundromatCOCMarker Auto Const
ObjectReference Property CTESTCOCMarker Auto Const
