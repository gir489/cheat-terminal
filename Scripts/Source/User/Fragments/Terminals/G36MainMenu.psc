;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:G36MainMenu Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00000F9E, "G36Complex.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x00048B23, "Fallout4.esm") as ObjectReference, -67.575, 363.0188, 3.8642, 0.0, CheatTermianlG36LookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
CheatTerminal_G36_gir489.Revert()
CheatTerminal_G36_gir489.AddForm(Game.GetFormFromFile(0x00000F9E, "G36Complex.esp"), 1, 1)
ObjectReference weaponRef = playerActor.PlaceAtMe(CheatTerminal_G36_gir489)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000173C,"G36Complex.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000AAED,"G36Complex.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00011D24,"G36Complex.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00001F39,"G36Complex.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x000026D5,"G36Complex.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00006B75,"G36Complex.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00002E7C,"G36Complex.esp") as ObjectMod)
weaponRef.AttachMod(CheatTerminal_NoRecoil)
weaponRef.AttachMod(mod_Legendary_Weapon_DamageLimbRecoil)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property CheatTermianlG36LookAtRef Auto Const
LeveledItem Property CheatTerminal_G36_gir489 Auto Const
ObjectMod Property CheatTerminal_NoRecoil Auto Const
ObjectMod Property mod_Legendary_Weapon_DamageLimbRecoil Auto Const
