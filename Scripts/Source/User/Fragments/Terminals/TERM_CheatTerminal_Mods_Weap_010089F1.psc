;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Weap_010089F1 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_CombatShotgun_Mag_ExtraLarge)
else
	Game.GetPlayer().AddItem(miscmod_mod_CombatShotgun_Mag_Large)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_CombatShotgun_Mag_Small)
else
	Game.GetPlayer().AddItem(miscmod_mod_CombatShotgun_Mag_Small)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_CombatShotgun_Mag_SmallQuick)
else
	Game.GetPlayer().AddItem(miscmod_mod_CombatShotgun_Mag_SmallQuick)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_CombatShotgun_Mag_Medium)
else
	Game.GetPlayer().AddItem(miscmod_mod_CombatShotgun_Mag_Medium)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_CombatShotgun_Mag_MediumQuick)
else
	Game.GetPlayer().AddItem(miscmod_mod_CombatShotgun_Mag_MediumQuick)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_CombatShotgun_Mag_QuickExtraLarge)
else
	Game.GetPlayer().AddItem(miscmod_mod_CombatShotgun_Mag_LargeQuick)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_CombatShotgun_Mag_Small Auto Const
ObjectMod Property mod_CombatShotgun_Mag_Medium Auto Const
ObjectMod Property mod_CombatShotgun_Mag_ExtraLarge Auto Const
ObjectMod Property mod_CombatShotgun_Mag_SmallQuick Auto Const
ObjectMod Property mod_CombatShotgun_Mag_MediumQuick Auto Const
ObjectMod Property mod_CombatShotgun_Mag_QuickExtraLarge Auto Const
MiscObject Property miscmod_mod_CombatShotgun_Mag_Small Auto Const
MiscObject Property miscmod_mod_CombatShotgun_Mag_Medium Auto Const
MiscObject Property miscmod_mod_CombatShotgun_Mag_Large Auto Const
MiscObject Property miscmod_mod_CombatShotgun_Mag_SmallQuick Auto Const
MiscObject Property miscmod_mod_CombatShotgun_Mag_MediumQuick Auto Const
MiscObject Property miscmod_mod_CombatShotgun_Mag_LargeQuick Auto Const
