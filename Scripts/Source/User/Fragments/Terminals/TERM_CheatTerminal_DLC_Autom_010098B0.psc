;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_Autom_010098B0 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x0000864A, "DLCRobot.esm")
if objectToGive
	LL_EpicChance_Standard.SetValue(0.0)
	Game.GetPlayer().AddItem(objectToGive)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x0000864C, "DLCRobot.esm")
if objectToGive
	LL_EpicChance_Standard.SetValue(0.0)
	Game.GetPlayer().AddItem(objectToGive)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00008BC1, "DLCRobot.esm")
if objectToGive
	LL_EpicChance_Standard.SetValue(0.0)
	Game.GetPlayer().AddItem(objectToGive)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00008BC3, "DLCRobot.esm")
if objectToGive
	LL_EpicChance_Standard.SetValue(0.0)
	Game.GetPlayer().AddItem(objectToGive)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form chestPiece = Game.GetFormFromFile(0x000102C5, "DLCRobot.esm")
if chestPiece
	LL_EpicChance_Standard.SetValue(0.0)
	Form rightLeg = Game.GetFormFromFile(0x000102C1, "DLCRobot.esm")
	Form leftLeg = Game.GetFormFromFile(0x000102BD, "DLCRobot.esm")
	Form rightArm = Game.GetFormFromFile(0x000102B8, "DLCRobot.esm")
	Form leftArm = Game.GetFormFromFile(0x000102B3, "DLCRobot.esm")
	Actor playerActor = Game.GetPlayer()
	playerActor.AddItem(chestPiece)
	playerActor.AddItem(rightLeg)
	playerActor.AddItem(leftLeg)
	playerActor.AddItem(rightArm)
	playerActor.AddItem(leftArm)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x0000864E, "DLCRobot.esm")
if objectToGive
	LL_EpicChance_Standard.SetValue(0.0)
	Game.GetPlayer().AddItem(objectToGive)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0000C579, "DLCRobot.esm")
if objectToSpawn
	LL_EpicChance_Standard.SetValue(0.0)
	Actor playerActor = Game.GetPlayer()
	ObjectReference spawnedObjectedRef = playerActor.PlaceAtMe(objectToSpawn, abForcePersist = true, abDeleteWhenAble = false)
	spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form chestPiece = Game.GetFormFromFile(0x000102C6, "DLCRobot.esm")
if chestPiece
	LL_EpicChance_Standard.SetValue(0.0)
	Form rightLeg = Game.GetFormFromFile(0x000102C2, "DLCRobot.esm")
	Form leftLeg = Game.GetFormFromFile(0x000102BE, "DLCRobot.esm")
	Form rightArm = Game.GetFormFromFile(0x000102B9, "DLCRobot.esm")
	Form leftArm = Game.GetFormFromFile(0x000102B4, "DLCRobot.esm")
	Actor playerActor = Game.GetPlayer()
	playerActor.AddItem(chestPiece)
	playerActor.AddItem(rightLeg)
	playerActor.AddItem(leftLeg)
	playerActor.AddItem(rightArm)
	playerActor.AddItem(leftArm)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form chestPiece = Game.GetFormFromFile(0x000102C7, "DLCRobot.esm")
if chestPiece
	LL_EpicChance_Standard.SetValue(0.0)
	Form rightLeg = Game.GetFormFromFile(0x000102C3, "DLCRobot.esm")
	Form leftLeg = Game.GetFormFromFile(0x000102BF, "DLCRobot.esm")
	Form rightArm = Game.GetFormFromFile(0x000102BA, "DLCRobot.esm")
	Form leftArm = Game.GetFormFromFile(0x000102B5, "DLCRobot.esm")
	Actor playerActor = Game.GetPlayer()
	playerActor.AddItem(chestPiece)
	playerActor.AddItem(rightLeg)
	playerActor.AddItem(leftLeg)
	playerActor.AddItem(rightArm)
	playerActor.AddItem(leftArm)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

GlobalVariable Property LL_EpicChance_Standard Auto Const
