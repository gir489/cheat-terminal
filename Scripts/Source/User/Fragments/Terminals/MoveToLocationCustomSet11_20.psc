;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:MoveToLocationCustomSet11_20 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Marker15Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Marker16Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Marker17Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Marker18Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Marker11Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Marker12Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Marker13Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Marker14Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Marker19Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Marker20Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectReference Property Marker11Ref Auto Const
ObjectReference Property Marker12Ref Auto Const
ObjectReference Property Marker13Ref Auto Const
ObjectReference Property Marker14Ref Auto Const
ObjectReference Property Marker15Ref Auto Const
ObjectReference Property Marker16Ref Auto Const
ObjectReference Property Marker17Ref Auto Const
ObjectReference Property Marker18Ref Auto Const
ObjectReference Property Marker19Ref Auto Const
ObjectReference Property Marker20Ref Auto Const
