;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterSurvivalManipulation Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().EquipItem(CheatTerminal_RequestSavePotion, false, true)
CheatTerminal_ClosePipboy.Show()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().EquipItem(CheatTerminal_RequestAutoSavePotion, false, true)
CheatTerminal_ClosePipboy.Show()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
HC_Rule_NoFastTravel.SetValue(0)
CheatTerminal_PersistFastTravelSurvival.Start()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
HC_Rule_NoFastTravel.SetValue(1)
CheatTerminal_PersistFastTravelSurvival.Stop()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
HC_Rule_NoLimbConditionHeal.SetValue(1)
CheatTerminal_PersistStimpacksSurvival.Stop()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
HC_Rule_NoLimbConditionHeal.SetValue(0)
CheatTerminal_PersistStimpacksSurvival.Start()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
HC_Rule_CompanionNoHeal.SetValue(0)
CheatTerminal_PersistHealCompanionSurvival.Start()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
HC_Rule_CompanionNoHeal.SetValue(1)
CheatTerminal_PersistHealCompanionSurvival.Stop()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
HC_Rule_DamageWhenEncumbered.SetValue(0)
CheatTerminal_PersistDamageWhenEncumberedSurvival.Start()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
HC_Rule_DamageWhenEncumbered.SetValue(1)
CheatTerminal_PersistDamageWhenEncumberedSurvival.Stop()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminal_SaveOnTeleportOrDoorForSurvival.Start()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminal_SaveOnTeleportOrDoorForSurvival.Stop()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Message Property CheatTerminal_ClosePipboy Auto Const
Potion Property CheatTerminal_RequestSavePotion Auto Const
Potion Property CheatTerminal_RequestAutoSavePotion Auto Const
GlobalVariable Property HC_Rule_NoFastTravel Auto Const
GlobalVariable Property HC_Rule_NoLimbConditionHeal Auto Const
GlobalVariable Property HC_Rule_CompanionNoHeal Auto Const
GlobalVariable Property HC_Rule_DamageWhenEncumbered Auto Const
GlobalVariable Property HC_Rule_ScaleDamage Auto Const
Quest Property CheatTerminal_PersistFastTravelSurvival Auto Const
Quest Property CheatTerminal_PersistStimpacksSurvival Auto Const
Quest Property CheatTerminal_PersistHealCompanionSurvival Auto Const
Quest Property CheatTerminal_PersistDamageWhenEncumberedSurvival Auto Const
Quest Property CheatTerminal_SaveOnTeleportOrDoorForSurvival Auto Const
