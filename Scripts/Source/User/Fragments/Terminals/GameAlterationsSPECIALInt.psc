;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsSPECIALInt Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
IncrementSpecial()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
DecrementSpecial()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
SetLevel(10)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
SetLevel(5)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
SetLevel(1)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Function DecrementSpecial()
	ObjectReference companionActor = CompanionRef.GetRef()
	int iCurrentLevel = companionActor.GetBaseValue(ActorValueToSet) as int
	iCurrentLevel -= 1
	CheatTerminal_Companion_SPECIALHolder.SetValueInt(iCurrentLevel)
	companionActor.SetValue(ActorValueToSet, iCurrentLevel)
endFunction
Function IncrementSpecial()
	ObjectReference companionActor = CompanionRef.GetRef()
	int iCurrentLevel = companionActor.GetBaseValue(ActorValueToSet) as int
	iCurrentLevel += 1
	CheatTerminal_Companion_SPECIALHolder.SetValueInt(iCurrentLevel)
	companionActor.SetValue(ActorValueToSet, iCurrentLevel)
endFunction
Function SetLevel(int levelToSet)
	CheatTerminal_Companion_SPECIALHolder.SetValueInt(levelToSet)
	CompanionRef.GetRef().SetValue(ActorValueToSet, levelToSet)
endFunction
GlobalVariable Property CheatTerminal_Companion_SPECIALHolder Auto Const
ActorValue Property ActorValueToSet Auto Const
ReferenceAlias Property CompanionRef Auto Const
