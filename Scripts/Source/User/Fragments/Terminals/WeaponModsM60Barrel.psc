;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:WeaponModsM60Barrel Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FD1,"M60.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00000FB5,"M60.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod )
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FD2,"M60.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00000FB6,"M60.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod )
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FD3,"M60.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00000FB7,"M60.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod )
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00000FD4,"M60.esp")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00000FB8,"M60.esp")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod )
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
