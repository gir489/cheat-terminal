;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAltWorldManipMerchDanny Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, JuneWarwickRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, KatRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, KayRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, KleoREF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, DannyRobotRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, DebRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, DocWeathersREF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, DoctorCarringtonREF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, DoctorPatriciaREF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, DoctorSunRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, EleanorRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, FredAllenREF)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property DannyRobotRef Auto Const
ObjectReference Property DebRef Auto Const
ObjectReference Property DocWeathersREF Auto Const
ObjectReference Property DoctorCarringtonREF Auto Const
ObjectReference Property DoctorPatriciaREF Auto Const
ObjectReference Property DoctorSunRef Auto Const
ObjectReference Property EleanorRef Auto Const
ObjectReference Property FredAllenREF Auto Const
ObjectReference Property JuneWarwickRef Auto Const
ObjectReference Property KatRef Auto Const
ObjectReference Property KayRef Auto Const
ObjectReference Property KleoREF Auto Const
