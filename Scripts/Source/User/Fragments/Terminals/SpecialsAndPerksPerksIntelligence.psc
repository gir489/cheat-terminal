;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:SpecialsAndPerksPerksIntelligence Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(VANS)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemoveSpell(VansSpell)
Game.GetPlayer().RemovePerk(VANS)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Medic01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Medic02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Medic03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Medic04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Medic01)
Game.GetPlayer().RemovePerk(Medic02)
Game.GetPlayer().RemovePerk(Medic03)
Game.GetPlayer().RemovePerk(Medic04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(GunNut01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(GunNut02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(GunNut03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(GunNut04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(GunNut01)
Game.GetPlayer().RemovePerk(GunNut02)
Game.GetPlayer().RemovePerk(GunNut03)
Game.GetPlayer().RemovePerk(GunNut04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Hacker01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Hacker02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Hacker03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Hacker04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Hacker01)
Game.GetPlayer().RemovePerk(Hacker02)
Game.GetPlayer().RemovePerk(Hacker03)
Game.GetPlayer().RemovePerk(Hacker04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Scrapper01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Scrapper02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Scrapper01)
Game.GetPlayer().RemovePerk(Scrapper02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Science01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_22
Function Fragment_Terminal_22(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Science02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_23
Function Fragment_Terminal_23(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Science03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_24
Function Fragment_Terminal_24(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Science04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_25
Function Fragment_Terminal_25(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Science01)
Game.GetPlayer().RemovePerk(Science02)
Game.GetPlayer().RemovePerk(Science03)
Game.GetPlayer().RemovePerk(Science04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_26
Function Fragment_Terminal_26(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Chemist01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_27
Function Fragment_Terminal_27(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Chemist02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_28
Function Fragment_Terminal_28(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Chemist03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_29
Function Fragment_Terminal_29(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Chemist04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_30
Function Fragment_Terminal_30(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Chemist01)
Game.GetPlayer().RemovePerk(Chemist02)
Game.GetPlayer().RemovePerk(Chemist03)
Game.GetPlayer().RemovePerk(Chemist04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_31
Function Fragment_Terminal_31(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(RoboticsExpert01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_32
Function Fragment_Terminal_32(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(RoboticsExpert02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_33
Function Fragment_Terminal_33(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(RoboticsExpert03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_34
Function Fragment_Terminal_34(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(RoboticsExpert01)
Game.GetPlayer().RemovePerk(RoboticsExpert02)
Game.GetPlayer().RemovePerk(RoboticsExpert03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_35
Function Fragment_Terminal_35(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(NuclearPhysicist01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_36
Function Fragment_Terminal_36(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(NuclearPhysicist02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_37
Function Fragment_Terminal_37(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(NuclearPhysicist03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_38
Function Fragment_Terminal_38(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(NuclearPhysicist01)
Game.GetPlayer().RemovePerk(NuclearPhysicist02)
Game.GetPlayer().RemovePerk(NuclearPhysicist03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_39
Function Fragment_Terminal_39(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(NerdRage01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_40
Function Fragment_Terminal_40(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(NerdRage02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_41
Function Fragment_Terminal_41(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(NerdRage03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_42
Function Fragment_Terminal_42(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(NerdRage01)
Game.GetPlayer().RemovePerk(NerdRage02)
Game.GetPlayer().RemovePerk(NerdRage03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_43
Function Fragment_Terminal_43(ObjectReference akTerminalRef)
;BEGIN CODE
givePerk(VANS)
givePerk(Medic01)
givePerk(Medic02)
givePerk(Medic03)
givePerk(Medic04)
givePerk(GunNut01)
givePerk(GunNut02)
givePerk(GunNut03)
givePerk(GunNut04)
givePerk(Hacker01)
givePerk(Hacker02)
givePerk(Hacker03)
givePerk(Hacker04)
givePerk(Scrapper01)
givePerk(Scrapper02)
givePerk(Science01)
givePerk(Science02)
givePerk(Science03)
givePerk(Science04)
givePerk(Chemist01)
givePerk(Chemist02)
givePerk(Chemist03)
givePerk(Chemist04)
givePerk(RoboticsExpert01)
givePerk(RoboticsExpert02)
givePerk(RoboticsExpert03)
givePerk(NuclearPhysicist01)
givePerk(NuclearPhysicist02)
givePerk(NuclearPhysicist03)
givePerk(NerdRage01)
givePerk(NerdRage02)
givePerk(NerdRage03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_44
Function Fragment_Terminal_44(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemoveSpell(VansSpell)
removePerk(VANS)
removePerk(Medic01)
removePerk(Medic02)
removePerk(Medic03)
removePerk(Medic04)
removePerk(GunNut01)
removePerk(GunNut02)
removePerk(GunNut03)
removePerk(GunNut04)
removePerk(Hacker01)
removePerk(Hacker02)
removePerk(Hacker03)
removePerk(Hacker04)
removePerk(Scrapper01)
removePerk(Scrapper02)
removePerk(Science01)
removePerk(Science02)
removePerk(Science03)
removePerk(Science04)
removePerk(Chemist01)
removePerk(Chemist02)
removePerk(Chemist03)
removePerk(Chemist04)
removePerk(RoboticsExpert01)
removePerk(RoboticsExpert02)
removePerk(RoboticsExpert03)
removePerk(NuclearPhysicist01)
removePerk(NuclearPhysicist02)
removePerk(NuclearPhysicist03)
removePerk(NerdRage01)
removePerk(NerdRage02)
removePerk(NerdRage03)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Function givePerk(Perk perkToGive)
	Actor playerActor = Game.GetPlayer()
	if (playerActor.HasPerk(perkToGive) == false)
		playerActor.AddPerk(perkToGive)
	endIf
endFunction
Function removePerk(Perk perkToGive)
	Actor playerActor = Game.GetPlayer()
	if (playerActor.HasPerk(perkToGive))
		playerActor.RemovePerk(perkToGive)
	endIf
endFunction
Perk Property VANS Auto Const
Perk Property Medic01 Auto Const
Perk Property Medic02 Auto Const
Perk Property Medic03 Auto Const
Perk Property Medic04 Auto Const
Perk Property GunNut01 Auto Const
Perk Property GunNut02 Auto Const
Perk Property GunNut03 Auto Const
Perk Property GunNut04 Auto Const
Perk Property Hacker01 Auto Const
Perk Property Hacker02 Auto Const
Perk Property Hacker03 Auto Const
Perk Property Hacker04 Auto Const
Perk Property Scrapper01 Auto Const
Perk Property Scrapper02 Auto Const
Perk Property Science01 Auto Const
Perk Property Science02 Auto Const
Perk Property Science03 Auto Const
Perk Property Science04 Auto Const
Perk Property Chemist01 Auto Const
Perk Property Chemist02 Auto Const
Perk Property Chemist03 Auto Const
Perk Property Chemist04 Auto Const
Perk Property RoboticsExpert01 Auto Const
Perk Property RoboticsExpert02 Auto Const
Perk Property RoboticsExpert03 Auto Const
Perk Property NuclearPhysicist01 Auto Const
Perk Property NuclearPhysicist02 Auto Const
Perk Property NuclearPhysicist03 Auto Const
Perk Property NerdRage01 Auto Const
Perk Property NerdRage02 Auto Const
Perk Property NerdRage03 Auto Const
Spell Property VansSpell Auto Const
