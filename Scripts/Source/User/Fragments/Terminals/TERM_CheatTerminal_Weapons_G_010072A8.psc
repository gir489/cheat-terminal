;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Weapons_G_010072A8 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_HuntingRifle_Sniper)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_HuntingRifle_Rifle)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_HuntingRifle_ShortRifle)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

LeveledItem Property LL_HuntingRifle_Rifle Auto Const

LeveledItem Property LL_HuntingRifle_ShortRifle Auto Const

LeveledItem Property LL_HuntingRifle_Sniper Auto Const

GlobalVariable Property LL_EpicChance_Standard Auto Const
