;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:NukaWorldMoveToLocationBE Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004CD96, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004CD95, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004DF89, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004CBC7, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0000B94B, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004C5ED, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004190A, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004C5EA, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004C5EB, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004C7A5, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004B06E, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
