;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__0100A911 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_JunkJet_SpecialMuzzle_Electricity)
else
	Game.GetPlayer().AddItem(miscmod_mod_JunkJet_Barrel_Electro)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_JunkJet_SpecialMuzzle_Fire)
else
	Game.GetPlayer().AddItem(miscmod_mod_JunkJet_Barrel_Fire)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_JunkJet_SpecialMuzzle_Electricity Auto Const
ObjectMod Property mod_JunkJet_SpecialMuzzle_Fire Auto Const
MiscObject Property miscmod_mod_JunkJet_Barrel_Electro Auto Const
MiscObject Property miscmod_mod_JunkJet_Barrel_Fire Auto Const
