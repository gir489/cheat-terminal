;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:FarHarborLocationsDebug Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0000291C, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, markerRef, -516.5039, -456.2782, 0.0, 255.1484)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.ReturnPlayerFromDebug(akTerminalRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00000807, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, markerRef, 128.0, -64.0, 0.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00034E07, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, markerRef, 419.5416, 795.0218, 0.0, 0.01)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x000575D8, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00040D6A, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, markerRef, 223.29, 1042.2832, 0.0001, 193.2691)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004FBFB, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x000018E8, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, markerRef, 216.7079, -1916.9552, -329.8013)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0005090A, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const