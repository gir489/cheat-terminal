;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01006B9E Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(AbernathyFarmMarker.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(ArcjetSystemsMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(AtomCatGarageMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(BeantownBreweryMapMarker.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(BedfordStationMapMarkerHeadingRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(BigJohnsSalvageMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(CoastGuardPierMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(CoastalCottageMapMarkerREF.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(ConcordMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(CountyCrossingMapMarker.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(CovenantMapMarker.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(CutlerBendMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ObjectReference Property AbernathyFarmMarker Auto Const

ObjectReference Property ArcjetSystemsMapMarkerRef Auto Const

ObjectReference Property AtomCatGarageMapMarkerRef Auto Const

ObjectReference Property BeantownBreweryMapMarker Auto Const

ObjectReference Property BedfordStationMapMarkerHeadingRef Auto Const

ObjectReference Property BigJohnsSalvageMapMarkerRef Auto Const

ObjectReference Property CoastGuardPierMapMarkerRef Auto Const

ObjectReference Property CoastalCottageMapMarkerREF Auto Const

ObjectReference Property CountyCrossingMapMarker Auto Const

ObjectReference Property CovenantMapMarker Auto Const

ObjectReference Property CutlerBendMapMarkerRef Auto Const

ObjectReference Property ConcordMapMarkerRef Auto Const
