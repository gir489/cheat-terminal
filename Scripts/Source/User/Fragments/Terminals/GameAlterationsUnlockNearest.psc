;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsUnlockNearest Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference nearestDoor = Game.FindClosestReferenceOfAnyTypeInListFromRef(CheatTerminal_AllDoors, Game.GetPlayer(), 200)
if ( nearestDoor )
	nearestDoor.Unlock()
	nearestDoor.Activate(Game.GetPlayer())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference nearestTerminal = Game.FindClosestReferenceOfAnyTypeInListFromRef(CheatTerminal_AllTerminals, Game.GetPlayer(), 200)
if ( nearestTerminal )
	nearestTerminal.Unlock()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference nearestContainer = Game.FindClosestReferenceOfAnyTypeInListFromRef(CheatTerminal_AllContainers, Game.GetPlayer(), 200)
if ( nearestContainer )
	nearestContainer.Unlock()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_UnlockNearestDoorPotion, CheatTerminal_AidToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_UnlockNearestTerminalPotion, CheatTerminal_AidToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_UnlockNearestContainerPotion, CheatTerminal_AidToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
FormList Property CheatTerminal_AllDoors Auto Const
FormList Property CheatTerminal_AllTerminals Auto Const
FormList Property CheatTerminal_AllContainers Auto Const
Potion Property CheatTerminal_UnlockNearestContainerPotion Auto Const
Potion Property CheatTerminal_UnlockNearestDoorPotion Auto Const
Potion Property CheatTerminal_UnlockNearestTerminalPotion Auto Const
GlobalVariable Property CheatTerminal_AidToGive Auto Const
