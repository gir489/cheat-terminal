;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:DLCFarHarborItemsQuest Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00001B42, "DLCCoast.esm")
if formFromMod
	Quest DLC03MQ04 = formFromMod as Quest
	if ( DLC03MQ04.GetStage() < 1000 && DLC03MQ04.IsCompleted() == false )
		if ( DLC03MQ04.GetStageDone(50) == false )
			DLC03MQ04.SetStage(50)
		endIf
		DLC03MQ04.SetStage(100)
		DLC03MQ04.SetStage(150)
		DLC03MQ04.SetStage(160)
		DLC03MQ04.SetStage(250)
		DLC03MQ04.SetStage(260)
		DLC03MQ04.SetStage(350)
		DLC03MQ04.SetStage(360)
		DLC03MQ04.SetStage(450)
		DLC03MQ04.SetStage(550)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004E094, "DLCCoast.esm")
if ( formFromMod )
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0001B0AF, "DLCCoast.esm")
if ( formFromMod )
	Quest questRef = formFromMod as Quest
	questRef.SetStage(210)
	questRef.SetStage(220)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00023ACA, "DLCCoast.esm")
if ( formFromMod )
	Quest questRef = formFromMod as Quest
	ReferenceAlias  powerModule = questRef.GetAlias(2) as ReferenceAlias
	Game.GetPlayer().AddItem(powerModule.GetReference())
	powerModule = questRef.GetAlias(4) as ReferenceAlias
	Game.GetPlayer().AddItem(powerModule.GetReference())
	powerModule = questRef.GetAlias(5) as ReferenceAlias
	Game.GetPlayer().AddItem(powerModule.GetReference())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00020569, "DLCCoast.esm")
if ( formFromMod )
	Quest questRef = Game.GetFormFromFile(0x0002039A, "DLCCoast.esm") as Quest
	if (!questRef.IsRunning()  && !questRef.IsCompleted())
		questRef.SetStage(100)
	endIf
	if (questRef.IsRunning() && questRef.GetStage() < 200 && !questRef.IsCompleted())
		Game.GetPlayer().AddItem(formFromMod, 3)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004607D, "DLCCoast.esm")
if ( formFromMod )
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
	formFromMod = Game.GetFormFromFile(0x0004609B, "DLCCoast.esm")
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0001536F, "DLCCoast.esm")
if ( formFromMod )
	Quest questRef = formFromMod as Quest
	questRef.SetStage(420) ;Blaze it
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002C8B2, "DLCCoast.esm")
if ( formFromMod )
	Quest questRef = formFromMod as Quest
	ReferenceAlias  pumpAlias = questRef.GetAlias(2) as ReferenceAlias
	Game.GetPlayer().AddItem(pumpAlias.GetReference())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0000AFF7, "DLCCoast.esm")
if ( formFromMod )
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0001C703, "DLCCoast.esm")
if ( formFromMod )
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0001C6FF, "DLCCoast.esm")
if ( formFromMod )
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
