;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsItemManipulation Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoPotion(CheatTerminal_ItemManipulation1Potion)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoPotion(CheatTerminal_ItemManipulation2Potion)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CheatTerminal_ItemManipulation1Question.Show() == 0 )
	CheatTerminal_ItemManipulation1Ref.Activate(CheatTerminal_ItemManipulation1Ref)
	CheatTerminal_ItemManipulation1Ref.Reset()
	CheatTerminal_ItemManipulation1_HolderRef.Reset()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CheatTerminal_ItemManipulation2Question.Show() == 0 )
	CheatTerminal_ItemManipulation2Ref.Reset()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminal_ItemManipulation1_HolderRef.RemoveAllItems(CheatTerminal_ItemManipulation2Ref, false)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_AddHeldItemToPersistentQuest, CheatTerminal_AidToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminal_ItemManipulation1Ref.Activate(CheatTerminal_ItemManipulation1Ref)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CheatTerminal_PersistentQuest_RemoveAllMessage.Show() == 0 )
	CheatTerminal_PersistentQuest.SetStage(69)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectReference Property CheatTerminal_ItemManipulation1Ref Auto Const
ObjectReference Property CheatTerminal_ItemManipulation1_HolderRef Auto Const
ObjectReference Property CheatTerminal_ItemManipulation2Ref Auto Const
Potion Property CheatTerminal_ItemManipulation1Potion Auto Const
Potion Property CheatTerminal_ItemManipulation2Potion Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
Message Property CheatTerminal_ItemManipulation1Question Auto Const
Message Property CheatTerminal_ItemManipulation2Question Auto Const
Quest Property CheatTerminal_PersistentQuest Auto Const
Potion Property CheatTerminal_AddHeldItemToPersistentQuest Auto Const
GlobalVariable Property CheatTerminal_AidToGive Auto Const
Message Property CheatTerminal_PersistentQuest_RemoveAllMessage Auto Const
