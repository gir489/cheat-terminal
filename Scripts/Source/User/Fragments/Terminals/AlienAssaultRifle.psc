;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:AlienAssaultRifle Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00000800,"alienassaultrifle.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x000054A7,"alienassaultrifle.esp"), CheatTerminal_AmmoToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
NukaColaLookAtRef.MoveTo(RelayTower05_Receiver03, 36.2008, -24.5971, 56.0801, false)
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, RelayTower05_Receiver03, -35.9292, -92.3171, -4.5399, 0.0, NukaColaLookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
GlobalVariable Property CheatTerminal_AmmoToGive Auto Const
ObjectReference Property RelayTower05_Receiver03 Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property NukaColaLookAtRef Auto Const
