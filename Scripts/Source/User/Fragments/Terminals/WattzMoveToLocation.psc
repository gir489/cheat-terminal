;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:WattzMoveToLocation Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x001A8DBE, "Fallout4.esm") as ObjectReference, -509.756, 51.096, -0.1, 0.0, Wattz3000LookToRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x00035C57, "Fallout4.esm") as ObjectReference, 2951.4972, -233.3847, 87.4889, 0.0, WattzHaruMarkerRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x001DE522, "Fallout4.esm") as ObjectReference, -112.3096, 183.8377, -2.9956, 0.0, WattzI06LookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x00144ED9, "Fallout4.esm") as ObjectReference, 160.2, -449.39, -14.9, 0.0, WattzLiberatorLookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, WattzDisntegratorLookAtRef, objectToLookAtParam = WattzDisntegratorLookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x000BA072, "Fallout4.esm") as ObjectReference, 226.9983, 86.2984, 0.66, 0.0, WattzAdVictoriumLookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property WattzHaruMarkerRef Auto Const
ObjectReference Property Wattz3000LookToRef Auto Const
ObjectReference Property WattzI06LookAtRef Auto Const
ObjectReference Property WattzLiberatorLookAtRef Auto Const
ObjectReference Property WattzDisntegratorLookAtRef Auto Const
ObjectReference Property WattzAdVictoriumLookAtRef Auto Const