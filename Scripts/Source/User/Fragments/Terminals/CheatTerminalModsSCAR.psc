;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:CheatTerminalModsSCAR Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00002E1F, "SCAR-L.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x0000272E, "SCAR-L.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
NukaWorldCappyLookAtRef.MoveTo(ConcordMuseumBalconyDoorREF, -1233.5047, -1589.0074, 58.045776, false)
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, ConcordMuseumBalconyDoorREF, -1132.9447, -1590.1974, 0.0, 0.0, NukaWorldCappyLookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectReference Property ConcordMuseumBalconyDoorREF Auto Const
ObjectReference Property NukaWorldCappyLookAtRef Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
