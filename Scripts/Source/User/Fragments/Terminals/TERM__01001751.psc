;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01001751 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Barrel_VeryShort)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Barrel_Null)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Barrel_Short)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Barrel_Short)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Barrel_LightShort)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Barrel_LightShort)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Barrel_Long)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Barrel_Long)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Barrel_LightLong)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Barrel_LightLong)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Barrel_PortedShort)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Barrel_PortedShort)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Barrel_PortedLong)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Barrel_PortedLong)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeRevolver_Barrel_FinnedLong)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeRevolver_Barrel_FinnedLong)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_PipeRevolver_Barrel_VeryShort Auto Const
ObjectMod Property mod_PipeRevolver_Barrel_Short Auto Const
ObjectMod Property mod_PipeRevolver_Barrel_LightShort Auto Const
ObjectMod Property mod_PipeRevolver_Barrel_Long Auto Const
ObjectMod Property mod_PipeRevolver_Barrel_LightLong Auto Const
ObjectMod Property mod_PipeRevolver_Barrel_PortedShort Auto Const
ObjectMod Property mod_PipeRevolver_Barrel_PortedLong Auto Const
ObjectMod Property mod_PipeRevolver_Barrel_FinnedLong Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Barrel_Null Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Barrel_Short Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Barrel_LightShort Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Barrel_Long Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Barrel_LightLong Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Barrel_PortedShort Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Barrel_PortedLong Auto Const
MiscObject Property miscmod_mod_PipeRevolver_Barrel_FinnedLong Auto Const
