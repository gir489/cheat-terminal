;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:X01RightLegMiscMod Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(Armor_Power_X01_LegRight) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(Armor_Power_X01_LegRight) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(Armor_Power_X01_LegRight, PA_Misc_Null)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(Armor_Power_X01_LegRight) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(Armor_Power_X01_LegRight) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(Armor_Power_X01_LegRight, PA_X01_Leg_Misc_Carry)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(Armor_Power_X01_LegRight) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(Armor_Power_X01_LegRight) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(Armor_Power_X01_LegRight, PA_X01_Leg_Misc_OptimizedServos)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(Armor_Power_X01_LegRight) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(Armor_Power_X01_LegRight) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(Armor_Power_X01_LegRight, PA_X01_Leg_Misc_APRegen)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(Armor_Power_X01_LegRight) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(Armor_Power_X01_LegRight) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(Armor_Power_X01_LegRight, PA_X01_Leg_Misc_ExplVent)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(Armor_Power_X01_LegRight) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(Armor_Power_X01_LegRight) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(Armor_Power_X01_LegRight, PA_X01_Leg_Misc_SprintBoost)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Message Property CheatTerminal_PowerArmorModMessage Auto Const
Message Property CheatTerminal_PowerArmorTooMany Auto Const
Message Property CheatTerminal_PowerArmorNotEnough Auto Const
Armor Property Armor_Power_X01_LegRight Auto Const
ObjectMod Property PA_Misc_Null Auto Const
ObjectMod Property PA_X01_Leg_Misc_APRegen Auto Const
ObjectMod Property PA_X01_Leg_Misc_Carry Auto Const
ObjectMod Property PA_X01_Leg_Misc_ExplVent Auto Const
ObjectMod Property PA_X01_Leg_Misc_OptimizedServos Auto Const
ObjectMod Property PA_X01_Leg_Misc_SprintBoost Auto Const
