;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Othe_0100BFFF Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeSyringer_Barrel_VeryShort)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeSyringer_Barrel_Short)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeSyringer_Barrel_Short)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeSyringer_Barrel_Medium)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PipeSyringer_Barrel_Long)
else
	Game.GetPlayer().AddItem(miscmod_mod_PipeSyringer_Barrel_Long)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_PipeSyringer_Barrel_VeryShort Auto Const
ObjectMod Property mod_PipeSyringer_Barrel_Short Auto Const
ObjectMod Property mod_PipeSyringer_Barrel_Long Auto Const
MiscObject Property miscmod_mod_PipeSyringer_Barrel_Short Auto Const
MiscObject Property miscmod_mod_PipeSyringer_Barrel_Medium Auto Const
MiscObject Property miscmod_mod_PipeSyringer_Barrel_Long Auto Const
