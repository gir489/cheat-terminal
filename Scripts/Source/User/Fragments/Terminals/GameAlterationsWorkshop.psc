;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsWorkshop Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
PA_Global_Material_AtomCat.SetValue(1)
PA_Global_Material_Institute.SetValue(1)
PA_Global_Material_Minuteman.SetValue(1)
PA_Global_Material_Railroad.SetValue(1)
MinArtilleryCanBuild.SetValue(1)
BoSPaintJobKnight.SetValue(1)
BoSPaintJobPaladin.SetValue(1)
BoSPaintJobSentinel.SetValue(1)
RailroadClothingArmorModAvailable.SetValue(1)
PerksQuest.SetStage(1400)
PerksQuest.SetStage(1410)
PerksQuest.SetStage(1420)
PerksQuest.SetStage(1430)
PerksQuest.SetStage(1440)
PerksQuest.SetStage(1500)
PerksQuest.SetStage(1510)
PerksQuest.SetStage(1520)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnWorkshopItem(WorkshopCaravanHitchingPost)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
Quest Property PerksQuest Auto Const
GlobalVariable Property PA_Global_Material_AtomCat Auto Const
GlobalVariable Property PA_Global_Material_Institute Auto Const
GlobalVariable Property PA_Global_Material_Minuteman Auto Const
GlobalVariable Property PA_Global_Material_Railroad Auto Const
GlobalVariable Property BoSPaintJobKnight Auto Const
GlobalVariable Property BoSPaintJobPaladin Auto Const
GlobalVariable Property BoSPaintJobSentinel Auto Const
GlobalVariable Property RailroadClothingArmorModAvailable Auto Const
Furniture Property WorkshopCaravanHitchingPost Auto Const

GlobalVariable Property MinArtilleryCanBuild Auto Const
