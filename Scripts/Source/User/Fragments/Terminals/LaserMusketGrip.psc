;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:LaserMusketGrip Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserMusket_Grip_StockFull)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserMusket_Grip_StockLong)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserMusket_Grip_StockShort)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserMusket_Grip_StockShort)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_LaserMusket_Grip_StockFull Auto Const
ObjectMod Property mod_LaserMusket_Grip_StockShort Auto Const
MiscObject Property miscmod_mod_LaserMusket_Grip_StockLong Auto Const
MiscObject Property miscmod_mod_LaserMusket_Grip_StockShort Auto Const
