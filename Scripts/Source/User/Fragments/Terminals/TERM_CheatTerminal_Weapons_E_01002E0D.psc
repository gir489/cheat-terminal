;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Weapons_E_01002E0D Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Fatman_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_MissileLauncher)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(Aspiration_Weapon_DC_Fatman)
weaponRef.AttachMod(mod_Legendary_Weapon_TwoShot)
weaponRef.AttachMod(mod_Aspiration_Weapon_IncreasedCostLesser_5)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Fatman_MIRV)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(Aspiration_Weapon_Goodneighbor_MissileLauncher)
weaponRef.AttachMod(mod_MissileLauncher_Scope_ScopeLong_NV)
weaponRef.AttachMod(mod_MissileLauncher_TubeMuzzle_Stabilizer)
weaponRef.AttachMod(mod_Legendary_Weapon_DamageVsHumans)
weaponRef.AttachMod(mod_Aspiration_Weapon_IncreasedCost_10)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_BoSMissileLauncher, CustomItemMods_BoSMissileLauncher)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
LeveledItem Property LL_Fatman_Simple Auto Const
LeveledItem Property Aspiration_Weapon_DC_Fatman Auto Const
ObjectMod Property mod_Legendary_Weapon_TwoShot Auto Const
ObjectMod Property mod_Aspiration_Weapon_IncreasedCostLesser_5 Auto Const
LeveledItem Property LL_Fatman_MIRV Auto Const
LeveledItem Property LL_MissileLauncher Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_BoSMissileLauncher Auto Const
ObjectMod Property mod_MissileLauncher_Scope_ScopeLong_NV Auto Const
ObjectMod Property mod_MissileLauncher_TubeMuzzle_Stabilizer Auto Const
ObjectMod Property mod_Legendary_Weapon_DamageVsHumans Auto Const
ObjectMod Property mod_Aspiration_Weapon_IncreasedCost_10 Auto Const
LeveledItem Property Aspiration_Weapon_Goodneighbor_MissileLauncher Auto Const
FormList Property CustomItemMods_BoSMissileLauncher Auto Const
GlobalVariable Property LL_EpicChance_Standard Auto Const
