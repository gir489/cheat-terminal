;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:NukaWorldItemsAidIceColdBC Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00030EFF, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AidToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00030EFD, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AidToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0003B8B4, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AidToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00030F02, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AidToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00030EF6, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AidToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00024536, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AidToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002453A, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AidToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00024538, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AidToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00024544, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AidToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002453D, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AidToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00030EFA, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AidToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
GlobalVariable Property CheatTerminal_AidToGive Auto Const
