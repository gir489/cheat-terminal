;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Armor_Com_01002E16 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Armor_Combat_ArmLeft_Light)
Game.GetPlayer().AddItem(LL_Armor_Combat_ArmRight_Light)
Game.GetPlayer().AddItem(LL_Armor_Combat_LegLeft_Light)
Game.GetPlayer().AddItem(LL_Armor_Combat_LegRight_Light)
Game.GetPlayer().AddItem(LL_Armor_Combat_Torso_Light)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Armor_Combat_ArmLeft_Medium)
Game.GetPlayer().AddItem(LL_Armor_Combat_ArmRight_Medium)
Game.GetPlayer().AddItem(LL_Armor_Combat_LegLeft_Medium)
Game.GetPlayer().AddItem(LL_Armor_Combat_LegRight_Medium)
Game.GetPlayer().AddItem(LL_Armor_Combat_Torso_Medium)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Armor_Combat_ArmLeft_Heavy)
Game.GetPlayer().AddItem(LL_Armor_Combat_ArmRight_Heavy)
Game.GetPlayer().AddItem(LL_Armor_Combat_LegLeft_Heavy)
Game.GetPlayer().AddItem(LL_Armor_Combat_LegRight_Heavy)
Game.GetPlayer().AddItem(LL_Armor_Combat_Torso_Heavy)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_Combat_Helmet)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

LeveledItem Property LL_Armor_Combat_ArmLeft_Light Auto Const
LeveledItem Property LL_Armor_Combat_ArmRight_Light Auto Const
LeveledItem Property LL_Armor_Combat_LegLeft_Light Auto Const
LeveledItem Property LL_Armor_Combat_LegRight_Light Auto Const
LeveledItem Property LL_Armor_Combat_Torso_Light Auto Const

LeveledItem Property LL_Armor_Combat_ArmLeft_Medium Auto Const
LeveledItem Property LL_Armor_Combat_ArmRight_Medium Auto Const
LeveledItem Property LL_Armor_Combat_LegLeft_Medium Auto Const
LeveledItem Property LL_Armor_Combat_LegRight_Medium Auto Const
LeveledItem Property LL_Armor_Combat_Torso_Medium Auto Const

LeveledItem Property LL_Armor_Combat_ArmLeft_Heavy Auto Const
LeveledItem Property LL_Armor_Combat_ArmRight_Heavy Auto Const
LeveledItem Property LL_Armor_Combat_LegLeft_Heavy Auto Const
LeveledItem Property LL_Armor_Combat_LegRight_Heavy Auto Const
LeveledItem Property LL_Armor_Combat_Torso_Heavy Auto Const

Armor Property Armor_Combat_Helmet Auto Const

GlobalVariable Property LL_EpicChance_Standard Auto Const
