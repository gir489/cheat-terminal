;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:ItemsMiscBobbleheads Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Agility)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Barter)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_BigGuns)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Charisma)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Endurance)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_EnergyWeapons)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Explosives)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Intelligence)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Lockpicking)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Luck)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Medicine)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Melee)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Perception)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Repair)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Science)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_SmallGuns)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Sneak)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Speech)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Strength)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Unarmed)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
GetWorldItem(BobbleHead_Agility)
GetWorldItem(BobbleHead_Barter)
GetWorldItem(BobbleHead_BigGuns)
GetWorldItem(BobbleHead_Charisma)
GetWorldItem(BobbleHead_Endurance)
GetWorldItem(BobbleHead_EnergyWeapons)
GetWorldItem(BobbleHead_Explosives)
GetWorldItem(BobbleHead_Intelligence)
GetWorldItem(BobbleHead_Lockpicking)
GetWorldItem(BobbleHead_Luck)
GetWorldItem(BobbleHead_Medicine)
GetWorldItem(BobbleHead_Melee)
GetWorldItem(BobbleHead_Perception)
GetWorldItem(BobbleHead_Repair)
GetWorldItem(BobbleHead_Science)
GetWorldItem(BobbleHead_SmallGuns)
GetWorldItem(BobbleHead_Sneak)
GetWorldItem(BobbleHead_Speech)
GetWorldItem(BobbleHead_Strength)
GetWorldItem(BobbleHead_Unarmed)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Function GetWorldItem(ObjectReference worldItem)
	if ( worldItem.IsDeleted() == false )
		worldItem.Activate(Game.GetPlayer())
	endIf
EndFunction
ObjectReference Property BobbleHead_Strength Auto Const
ObjectReference Property BobbleHead_Perception Auto Const
ObjectReference Property BobbleHead_Endurance Auto Const
ObjectReference Property BobbleHead_Charisma Auto Const
ObjectReference Property BobbleHead_Intelligence Auto Const
ObjectReference Property BobbleHead_Agility Auto Const
ObjectReference Property BobbleHead_Luck Auto Const
ObjectReference Property BobbleHead_Repair Auto Const
ObjectReference Property BobbleHead_EnergyWeapons Auto Const
ObjectReference Property BobbleHead_LockPicking Auto Const
ObjectReference Property BobbleHead_BigGuns Auto Const
ObjectReference Property BobbleHead_Speech Auto Const
ObjectReference Property BobbleHead_Melee Auto Const
ObjectReference Property BobbleHead_Explosives Auto Const
ObjectReference Property BobbleHead_Unarmed Auto Const
ObjectReference Property BobbleHead_Barter Auto Const
ObjectReference Property BobbleHead_Science Auto Const
ObjectReference Property BobbleHead_Sneak Auto Const
ObjectReference Property BobbleHead_Medicine Auto Const
ObjectReference Property BobbleHead_SmallGuns Auto Const
