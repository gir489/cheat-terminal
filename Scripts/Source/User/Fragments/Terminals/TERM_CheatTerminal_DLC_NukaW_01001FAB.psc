;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_NukaW_01001FAB Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x0004D94C, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x0004D94E, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x0004D943, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x0004D944, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x0004D941, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x0004D945, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x0004D93B, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x0004D946, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
