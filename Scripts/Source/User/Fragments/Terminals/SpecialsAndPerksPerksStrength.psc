;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:SpecialsAndPerksPerksStrength Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(IronFist01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(IronFist02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(IronFist03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(IronFist04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(IronFist05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(IronFist01)
Game.GetPlayer().RemovePerk(IronFist02)
Game.GetPlayer().RemovePerk(IronFist03)
Game.GetPlayer().RemovePerk(IronFist04)
Game.GetPlayer().RemovePerk(IronFist05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(BigLeagues01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(BigLeagues02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(BigLeagues03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(BigLeagues04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(BigLeagues05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(BigLeagues01)
Game.GetPlayer().RemovePerk(BigLeagues02)
Game.GetPlayer().RemovePerk(BigLeagues03)
Game.GetPlayer().RemovePerk(BigLeagues04)
Game.GetPlayer().RemovePerk(BigLeagues05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Armorer01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Armorer02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Armorer03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Armorer04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Armorer01)
Game.GetPlayer().RemovePerk(Armorer02)
Game.GetPlayer().RemovePerk(Armorer03)
Game.GetPlayer().RemovePerk(Armorer04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Blacksmith01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Blacksmith02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Blacksmith03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Blacksmith01)
Game.GetPlayer().RemovePerk(Blacksmith02)
Game.GetPlayer().RemovePerk(Blacksmith03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_22
Function Fragment_Terminal_22(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(HeavyGunner01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_23
Function Fragment_Terminal_23(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(HeavyGunner02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_24
Function Fragment_Terminal_24(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(HeavyGunner03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_25
Function Fragment_Terminal_25(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(HeavyGunner04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_26
Function Fragment_Terminal_26(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(HeavyGunner05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_27
Function Fragment_Terminal_27(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(HeavyGunner01)
Game.GetPlayer().RemovePerk(HeavyGunner02)
Game.GetPlayer().RemovePerk(HeavyGunner03)
Game.GetPlayer().RemovePerk(HeavyGunner04)
Game.GetPlayer().RemovePerk(HeavyGunner05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_28
Function Fragment_Terminal_28(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(StrongBack01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_29
Function Fragment_Terminal_29(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(StrongBack02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_30
Function Fragment_Terminal_30(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(StrongBack03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_31
Function Fragment_Terminal_31(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(StrongBack04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_32
Function Fragment_Terminal_32(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(StrongBack01)
Game.GetPlayer().RemovePerk(StrongBack02)
Game.GetPlayer().RemovePerk(StrongBack03)
Game.GetPlayer().RemovePerk(StrongBack04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_33
Function Fragment_Terminal_33(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(SteadyAim01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_34
Function Fragment_Terminal_34(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(SteadyAim02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_35
Function Fragment_Terminal_35(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(SteadyAim01)
Game.GetPlayer().RemovePerk(SteadyAim02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_36
Function Fragment_Terminal_36(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Basher01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_37
Function Fragment_Terminal_37(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Basher02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_38
Function Fragment_Terminal_38(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Basher03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_39
Function Fragment_Terminal_39(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Basher04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_40
Function Fragment_Terminal_40(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Basher01)
Game.GetPlayer().RemovePerk(Basher02)
Game.GetPlayer().RemovePerk(Basher03)
Game.GetPlayer().RemovePerk(Basher04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_41
Function Fragment_Terminal_41(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Rooted01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_42
Function Fragment_Terminal_42(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Rooted02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_43
Function Fragment_Terminal_43(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Rooted03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_44
Function Fragment_Terminal_44(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Rooted01)
Game.GetPlayer().RemovePerk(Rooted02)
Game.GetPlayer().RemovePerk(Rooted03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_45
Function Fragment_Terminal_45(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(PainTrain01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_46
Function Fragment_Terminal_46(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(PainTrain02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_47
Function Fragment_Terminal_47(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(PainTrain03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_48
Function Fragment_Terminal_48(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(PainTrain01)
Game.GetPlayer().RemovePerk(PainTrain02)
Game.GetPlayer().RemovePerk(PainTrain03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_49
Function Fragment_Terminal_49(ObjectReference akTerminalRef)
;BEGIN CODE
givePerk(IronFist01)
givePerk(IronFist02)
givePerk(IronFist03)
givePerk(IronFist04)
givePerk(IronFist05)
givePerk(BigLeagues01)
givePerk(BigLeagues02)
givePerk(BigLeagues03)
givePerk(BigLeagues04)
givePerk(BigLeagues05)
givePerk(Armorer01)
givePerk(Armorer02)
givePerk(Armorer03)
givePerk(Armorer04)
givePerk(Blacksmith01)
givePerk(Blacksmith02)
givePerk(Blacksmith03)
givePerk(HeavyGunner01)
givePerk(HeavyGunner02)
givePerk(HeavyGunner03)
givePerk(HeavyGunner04)
givePerk(HeavyGunner05)
givePerk(StrongBack01)
givePerk(StrongBack02)
givePerk(StrongBack03)
givePerk(StrongBack04)
givePerk(SteadyAim01)
givePerk(SteadyAim02)
givePerk(Basher01)
givePerk(Basher02)
givePerk(Basher03)
givePerk(Basher04)
givePerk(Rooted01)
givePerk(Rooted02)
givePerk(Rooted03)
givePerk(PainTrain01)
givePerk(PainTrain02)
givePerk(PainTrain03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_50
Function Fragment_Terminal_50(ObjectReference akTerminalRef)
;BEGIN CODE
removePerk(IronFist01)
removePerk(IronFist02)
removePerk(IronFist03)
removePerk(IronFist04)
removePerk(IronFist05)
removePerk(BigLeagues01)
removePerk(BigLeagues02)
removePerk(BigLeagues03)
removePerk(BigLeagues04)
removePerk(BigLeagues05)
removePerk(Armorer01)
removePerk(Armorer02)
removePerk(Armorer03)
removePerk(Armorer04)
removePerk(Blacksmith01)
removePerk(Blacksmith02)
removePerk(Blacksmith03)
removePerk(HeavyGunner01)
removePerk(HeavyGunner02)
removePerk(HeavyGunner03)
removePerk(HeavyGunner04)
removePerk(HeavyGunner05)
removePerk(StrongBack01)
removePerk(StrongBack02)
removePerk(StrongBack03)
removePerk(StrongBack04)
removePerk(SteadyAim01)
removePerk(SteadyAim02)
removePerk(Basher01)
removePerk(Basher02)
removePerk(Basher03)
removePerk(Basher04)
removePerk(Rooted01)
removePerk(Rooted02)
removePerk(Rooted03)
removePerk(PainTrain01)
removePerk(PainTrain02)
removePerk(PainTrain03)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Function givePerk(Perk perkToGive)
	Actor playerActor = Game.GetPlayer()
	if (playerActor.HasPerk(perkToGive) == false)
		playerActor.AddPerk(perkToGive)
	endIf
endFunction
Function removePerk(Perk perkToGive)
	Actor playerActor = Game.GetPlayer()
	if (playerActor.HasPerk(perkToGive))
		playerActor.RemovePerk(perkToGive)
	endIf
endFunction
Perk Property IronFist01 Auto Const
Perk Property IronFist02 Auto Const
Perk Property IronFist03 Auto Const
Perk Property IronFist04 Auto Const
Perk Property IronFist05 Auto Const
Perk Property BigLeagues01 Auto Const
Perk Property BigLeagues02 Auto Const
Perk Property BigLeagues03 Auto Const
Perk Property BigLeagues04 Auto Const
Perk Property BigLeagues05 Auto Const
Perk Property Armorer01 Auto Const
Perk Property Armorer02 Auto Const
Perk Property Armorer03 Auto Const
Perk Property Armorer04 Auto Const
Perk Property Blacksmith01 Auto Const
Perk Property Blacksmith02 Auto Const
Perk Property Blacksmith03 Auto Const
Perk Property HeavyGunner01 Auto Const
Perk Property HeavyGunner02 Auto Const
Perk Property HeavyGunner03 Auto Const
Perk Property HeavyGunner04 Auto Const
Perk Property HeavyGunner05 Auto Const
Perk Property StrongBack01 Auto Const
Perk Property StrongBack02 Auto Const
Perk Property StrongBack03 Auto Const
Perk Property StrongBack04 Auto Const
Perk Property SteadyAim01 Auto Const
Perk Property SteadyAim02 Auto Const
Perk Property Basher01 Auto Const
Perk Property Basher02 Auto Const
Perk Property Basher03 Auto Const
Perk Property Basher04 Auto Const
Perk Property Rooted01 Auto Const
Perk Property Rooted02 Auto Const
Perk Property Rooted03 Auto Const
Perk Property PainTrain01 Auto Const
Perk Property PainTrain02 Auto Const
Perk Property PainTrain03 Auto Const
