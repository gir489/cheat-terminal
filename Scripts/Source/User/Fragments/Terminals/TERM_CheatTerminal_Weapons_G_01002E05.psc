;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Weapons_G_01002E05 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_GaussRifle)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(Aspiration_Weapon_V81_CombatRifle)
weaponRef.AttachMod(mod_Legendary_Weapon_TwoShot)
weaponRef.AttachMod(mod_CombatRifle_Receiver_Automatic1_and_ArmorPiercing)
weaponRef.AttachMod(mod_CombatRifle_Grip_StockRecoil)
weaponRef.AttachMod(mod_CombatRifle_Scope_ScopeShort_NV)
weaponRef.AttachMod(mod_Aspiration_Weapon_IncreasedCost_10)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_RailwayRifle)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_DN083_Rifle_Reba)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_DN083_Rifle_Reba2, CustomItemMods_DN083_Rifle_Reba2)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_SubmachineGun_SilverShroud)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(Aspiration_Weapon_Caravan_SMG)
weaponRef.AttachMod(mod_Legendary_Weapon_ExplosiveBullets)
weaponRef.AttachMod(mod_SubmachineGun_Receiver_Automatic1_and_MoreDamage2)
weaponRef.AttachMod(mod_SubmachineGun_Barrel_Short)
weaponRef.AttachMod(mod_SubmachineGun_Grip_StockRecoil)
weaponRef.AttachMod(mod_SubmachineGun_Mag_LargeQuick)
weaponRef.AttachMod(mod_SubmachineGun_Muzzle_Suppressor)
weaponRef.AttachMod(mod_Aspiration_Weapon_IncreasedCost_10)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_SubmachineGun)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_PipeSyringer)
Game.GetPlayer().AddItem(LL_Ammo_PipeSyringer, 1)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_GaussRifle_Sniper)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(Aspiration_Weapon_Railroad_SniperRifle)
weaponRef.AttachMod(mod_Legendary_Weapon_AccuracyNotInCombat)
weaponRef.AttachMod(mod_HuntingRifle_Receiver_MoreDamage2_and_BetterCriticals)
weaponRef.AttachMod(mod_HuntingRifle_Barrel_Long)
weaponRef.AttachMod(mod_HuntingRifle_Grip_StockFull)
weaponRef.AttachMod(mod_HuntingRifle_Scope_ScopeLong)
weaponRef.AttachMod(mod_HuntingRifle_Muzzle_Suppressor)
weaponRef.AttachMod(mod_Aspiration_Weapon_IncreasedCost_10)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(Aspiration_Weapon_Work_GaussRifle)
weaponRef.AttachMod(mod_Legendary_Weapon_DamageLimb)
weaponRef.AttachMod(mod_GaussRifle_GaussBarrel_Long_and_MoreDamage1)
weaponRef.AttachMod(mod_GaussRifle_Grip_StockFull)
weaponRef.AttachMod(mod_GaussRifle_GaussMag_ExtraLarge_and_MoreDamage2)
weaponRef.AttachMod(mod_GaussRifle_Scope_ScopeLong_NV)
weaponRef.AttachMod(mod_GaussRifle_Muzzle_Compensator)
weaponRef.AttachMod(mod_Aspiration_Weapon_IncreasedCost_10)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
LeveledItem Property Aspiration_Weapon_V81_CombatRifle Auto Const
ObjectMod Property mod_Legendary_Weapon_TwoShot Auto Const
LeveledItem Property LL_DN083_Rifle_Reba Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_DN083_Rifle_Reba2 Auto Const
FormList Property CustomItemMods_DN083_Rifle_Reba2 Auto Const
LeveledItem Property Aspiration_Weapon_Railroad_SniperRifle Auto Const
ObjectMod Property mod_Legendary_Weapon_AccuracyNotInCombat Auto Const
ObjectMod Property mod_HuntingRifle_Receiver_MoreDamage2_and_BetterCriticals Auto Const
ObjectMod Property mod_HuntingRifle_Barrel_Long Auto Const
ObjectMod Property mod_HuntingRifle_Grip_StockFull Auto Const
ObjectMod Property mod_HuntingRifle_Scope_ScopeLong Auto Const
ObjectMod Property mod_HuntingRifle_Muzzle_Suppressor Auto Const
ObjectMod Property mod_Aspiration_Weapon_IncreasedCost_10 Auto Const
LeveledItem Property LL_RailwayRifle Auto Const
LeveledItem Property LL_SubmachineGun Auto Const
LeveledItem Property LL_GaussRifle Auto Const
LeveledItem Property LL_GaussRifle_Sniper Auto Const
LeveledItem Property LL_SubmachineGun_SilverShroud Auto Const
LeveledItem Property Aspiration_Weapon_Caravan_SMG Auto Const
ObjectMod Property mod_Legendary_Weapon_ExplosiveBullets Auto Const
ObjectMod Property mod_SubmachineGun_Receiver_Automatic1_and_MoreDamage2 Auto Const
ObjectMod Property mod_SubmachineGun_Barrel_Short Auto Const
ObjectMod Property mod_SubmachineGun_Grip_StockRecoil Auto Const
ObjectMod Property mod_SubmachineGun_Mag_LargeQuick Auto Const
ObjectMod Property mod_SubmachineGun_Scope_SightsIron Auto Const
ObjectMod Property mod_SubmachineGun_Muzzle_Suppressor Auto Const
LeveledItem Property Aspiration_Weapon_Work_GaussRifle Auto Const
ObjectMod Property mod_GaussRifle_GaussBarrel_Long_and_MoreDamage1 Auto Const
ObjectMod Property mod_GaussRifle_Grip_StockFull Auto Const
ObjectMod Property mod_GaussRifle_GaussMag_ExtraLarge_and_MoreDamage2 Auto Const
ObjectMod Property mod_GaussRifle_Scope_ScopeLong_NV Auto Const
ObjectMod Property mod_GaussRifle_Muzzle_Compensator Auto Const
ObjectMod Property mod_Legendary_Weapon_DamageLimb Auto Const
LeveledItem Property LL_PipeSyringer Auto Const
LeveledItem Property LL_Ammo_PipeSyringer Auto Const
GlobalVariable Property LL_EpicChance_Standard Auto Const
ObjectMod Property mod_CombatRifle_Receiver_Automatic1_and_ArmorPiercing Auto Const
ObjectMod Property mod_CombatRifle_Grip_StockRecoil Auto Const
ObjectMod Property mod_CombatRifle_Scope_ScopeShort_NV Auto Const
