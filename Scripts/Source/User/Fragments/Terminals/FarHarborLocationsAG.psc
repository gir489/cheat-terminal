;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:FarHarborLocationsAG Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0005689A, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0000AFE3, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00016E3A, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0000ECE5, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0001F97F, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004F8E2, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00006128, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00016E37, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004FE1B, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004F140, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0004FE1D, "DLCCoast.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
