;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Othe_0100C008 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_RailwayRifle_Receiver_Automatic1)
else
	Game.GetPlayer().AddItem(miscmod_mod_RailwayRifle_Receiver_Automatic1)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_RailwayRifle_Receiver_Standard)
else
	Game.GetPlayer().AddItem(miscmod_mod_RailwayRifle_Receiver_Standard)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_RailwayRifle_Receiver_Standard Auto Const
ObjectMod Property mod_RailwayRifle_Receiver_Automatic1 Auto Const
MiscObject Property miscmod_mod_RailwayRifle_Receiver_Standard Auto Const
MiscObject Property miscmod_mod_RailwayRifle_Receiver_Automatic1 Auto Const
