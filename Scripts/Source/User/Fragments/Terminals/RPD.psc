;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:RPD Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00001066, "RPD.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x0000B0E3, "RPD.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
CheatTerminal_RPD_gir489.Revert()
CheatTerminal_RPD_gir489.AddForm(Game.GetFormFromFile(0x00001066, "RPD.esp"), 1, 1)
ObjectReference weaponRef = playerActor.PlaceAtMe(CheatTerminal_RPD_gir489)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000B99B,"RPD.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000A866,"RPD.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00001069,"RPD.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000A7F1,"RPD.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000B9D0,"RPD.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000A82C,"RPD.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000A853,"RPD.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000A7F8,"RPD.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000B977,"RPD.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000A844,"RPD.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000A846,"RPD.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000A864,"RPD.esp") as ObjectMod)
weaponRef.AttachMod(CheatTerminal_NoRecoil)
weaponRef.AttachMod(mod_Legendary_Weapon_DamageLimbRecoil)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property CheatTerminal_NoRecoil Auto Const
ObjectMod Property mod_Legendary_Weapon_DamageLimbRecoil Auto Const
LeveledItem Property CheatTerminal_RPD_gir489 Auto Const
