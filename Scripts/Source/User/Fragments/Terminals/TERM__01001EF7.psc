;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01001EF7 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GaussRifle_Grip_StockFull)
else
	Game.GetPlayer().AddItem(miscmod_mod_GaussRifle_Grip_WoodenStock)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GaussRifle_Grip_StockRecoil)
else
	Game.GetPlayer().AddItem(miscmod_mod_GaussRifle_Grip_RecoilStock)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_GaussRifle_Grip_StockFull Auto Const
ObjectMod Property mod_GaussRifle_Grip_StockRecoil Auto Const
MiscObject Property miscmod_mod_GaussRifle_Grip_WoodenStock Auto Const
MiscObject Property miscmod_mod_GaussRifle_Grip_RecoilStock Auto Const
