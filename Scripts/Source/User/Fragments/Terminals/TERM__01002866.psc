;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01002866 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004EF8B, "DLCCoast.esm")
if formFromMod
	Game.GetPlayer().PlaceAtMe(formFromMod).Activate(Game.GetPlayer())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004EF8D, "DLCCoast.esm")
if formFromMod
	Game.GetPlayer().PlaceAtMe(formFromMod).Activate(Game.GetPlayer())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004EF8F, "DLCCoast.esm")
if formFromMod
	Game.GetPlayer().PlaceAtMe(formFromMod).Activate(Game.GetPlayer())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004EF92, "DLCCoast.esm")
if formFromMod
	Game.GetPlayer().PlaceAtMe(formFromMod).Activate(Game.GetPlayer())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004EF94, "DLCCoast.esm")
if formFromMod
	Game.GetPlayer().PlaceAtMe(formFromMod).Activate(Game.GetPlayer())
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
