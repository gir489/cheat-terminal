;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:VaultTecWorkshopApparel Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00001AFD, "DLCworkshop03.esm")
if ( formFromMod )
	ObjectReference objRef = Game.GetPlayer().PlaceAtMe(formFromMod)
	objRef.AttachMod(mod_Legendary_Armor_LessDMGGhouls)
	Game.GetPlayer().AddItem(objRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000042AE, "DLCworkshop03.esm")
if ( formFromMod )
	Game.GetPlayer().AddItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000042B2, "DLCworkshop03.esm")
if ( formFromMod )
	Game.GetPlayer().AddItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00005264, "DLCworkshop03.esm")
if ( formFromMod )
	Game.GetPlayer().AddItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00005266, "DLCworkshop03.esm")
if ( formFromMod )
	Game.GetPlayer().AddItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ObjectMod Property mod_Legendary_Armor_LessDMGGhouls Auto Const
