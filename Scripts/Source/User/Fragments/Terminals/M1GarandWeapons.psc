;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:M1GarandWeapons Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00000F99, "m1garand.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00001745, "m1garand.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x0000638B, "m1garand.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00006B41, "m1garand.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
CheatTerminal_M1Garand_gir489.Revert()
CheatTerminal_M1Garand_gir489.AddForm(Game.GetFormFromFile(0x00006B41, "M1Garand.esp"), 1, 1)
ObjectReference weaponRef = playerActor.PlaceAtMe(CheatTerminal_M1Garand_gir489)
weaponRef.AttachMod(Game.GetFormFromFile(0x000063A1, "M1Garand.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000E51F, "M1Garand.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00007A1A, "M1Garand.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000C6A1, "M1Garand.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000638C, "M1Garand.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00003DAE, "M1Garand.esp") as ObjectMod)
weaponRef.AttachMod(mod_Legendary_Weapon_DamageFirstBlood)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
LeveledItem Property CheatTerminal_M1Garand_gir489 Auto Const
ObjectMod Property mod_Legendary_Weapon_DamageFirstBlood Auto Const