;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_NukaW_01001FA8 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x0004D953, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x0004D957, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00044DB1, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x0004D956, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B70, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x0004D958, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B6F, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x0004D959, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x0003788C, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x0004D95B, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x0003788D, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x0004D95A, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
