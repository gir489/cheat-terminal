;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:CheatTerminalSpecialEndurance Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
IncrementSpecial()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
DecrementSpecial()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
SetLevel(10)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
SetLevel(5)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
SetLevel(1)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
SetLevel(50)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_22
Function Fragment_Terminal_22(ObjectReference akTerminalRef)
;BEGIN CODE
SetLevel(99)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Function DecrementSpecial()
	Actor playerActor = Game.GetPlayer()
	int iCurrentLevel = playerActor.GetBaseValue(ActorValueToSet) as int
	if ( iCurrentLevel >= 2 && iCurrentLevel <= 10 )
		Perk perkToRemove = PerkArray[iCurrentLevel]
		if ( playerActor.HasPerk(perkToRemove) )
			playerActor.RemovePerk(perkToRemove)
		endIf
	endIf
	playerActor.SetValue(ActorValueToSet, iCurrentLevel - 1)
endFunction
Function IncrementSpecial()
	Actor playerActor = Game.GetPlayer()
	int iCurrentLevel = playerActor.GetBaseValue(ActorValueToSet) as int
	if ( iCurrentLevel >= 10 )
		playerActor.SetValue(ActorValueToSet, iCurrentLevel + 1)
		return
	endIf
	iCurrentLevel += 1
	Perk perkToAdd = PerkArray[iCurrentLevel]
	if ( playerActor.HasPerk(perkToAdd) == false )
		playerActor.AddPerk(perkToAdd)
		Utility.WaitMenuMode(0.01)
	else
		playerActor.SetValue(ActorValueToSet, iCurrentLevel)
	endIf
endFunction
Function SetLevel(int levelToSet)
	Actor playerActor = Game.GetPlayer()
	int iCurrentLevel = playerActor.GetBaseValue(ActorValueToSet) as int
	int delta = (iCurrentLevel - levelToSet)
	if ( delta > 0 )
		While ( levelToSet != iCurrentLevel )
			DecrementSpecial()
			iCurrentLevel = playerActor.GetBaseValue(ActorValueToSet) as int
		EndWhile
	elseif ( delta < 0 )
		While ( levelToSet != iCurrentLevel )
			IncrementSpecial()
			iCurrentLevel = playerActor.GetBaseValue(ActorValueToSet) as int
		EndWhile
	endIf
endFunction
ActorValue Property ActorValueToSet Auto Const
Perk[] Property PerkArray Auto Const
