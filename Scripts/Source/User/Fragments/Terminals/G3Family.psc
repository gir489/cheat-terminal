;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:G3Family Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00000F99, "G3Family.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x0000FC49, "G3Family.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
CheatTerminal_G3Family_gir489.Revert()
CheatTerminal_G3Family_gir489.AddForm(Game.GetFormFromFile(0x00000F99, "G3Family.esp"), 1, 1)
ObjectReference weaponRef = playerActor.PlaceAtMe(CheatTerminal_G3Family_gir489)
weaponRef.AttachMod(Game.GetFormFromFile(0x00002EA2, "G3Family.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00000FDF, "G3Family.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00000FD8, "G3Family.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000825A, "G3Family.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00006B88, "G3Family.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00001000, "G3Family.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0004F21D, "G3Family.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x000017AC, "G3Family.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x000017B7, "G3Family.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000A0D7, "G3Family.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000ED06, "G3Family.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00000FE9, "G3Family.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000F4A5, "G3Family.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000457A, "G3Family.esp") as ObjectMod)
weaponRef.AttachMod(CheatTerminal_NoRecoilRapidFire)
weaponRef.AttachMod(mod_Legendary_Weapon_DamageFirstBlood)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x000ADD8B, "Fallout4.esm") as ObjectReference, -66696.6803, -8563.9081, 6538.4055, 0.0, CheatTerminalSalvationLookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property CheatTerminal_NoRecoilRapidFire Auto Const
ObjectMod Property mod_Legendary_Weapon_DamageFirstBlood Auto Const
LeveledItem Property CheatTerminal_G3Family_gir489 Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property CheatTerminalSalvationLookAtRef Auto Const
