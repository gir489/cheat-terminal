;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:DeagleMoveToLocation Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x0004E74B, "Fallout4.esm") as ObjectReference, -831.1158,-6353.3278, 1727.25, 0.0, CheatTerminal_NightHawkLookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x000BF95E, "Fallout4.esm") as ObjectReference, -1754.1709,-166.6812, -5.0778, 0.0, CheatTerminal_DeagleOP4Ref)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x001871CE, "Fallout4.esm") as ObjectReference, 1190.2809,-1583.2678, -313.9098, 0.0, CheatTerminal_YuriLookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property CheatTerminal_DeagleOP4Ref Auto Const
ObjectReference Property CheatTerminal_NightHawkLookAtRef Auto Const
ObjectReference Property CheatTerminal_YuriLookAtRef Auto Const
