;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Perk_Maga_01009186 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagAstoundinglyAwesomeTales01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagAstoundinglyAwesomeTales02).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagAstoundinglyAwesomeTales03).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagAstoundinglyAwesomeTales04).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagAstoundinglyAwesomeTales05).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagAstoundinglyAwesomeTales06).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagAstoundinglyAwesomeTales07).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagAstoundinglyAwesomeTales08).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagAstoundinglyAwesomeTales09).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagAstoundinglyAwesomeTales10).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagAstoundinglyAwesomeTales11).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagAstoundinglyAwesomeTales12).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagAstoundinglyAwesomeTales13).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagAstoundinglyAwesomeTales14).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagGrognakTheBarbarian01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGrognakTheBarbarian02).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGrognakTheBarbarian03).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGrognakTheBarbarian04).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGrognakTheBarbarian05).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGrognakTheBarbarian06).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGrognakTheBarbarian07).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGrognakTheBarbarian08).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGrognakTheBarbarian09).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGrognakTheBarbarian10).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagGunsAndBullets01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGunsAndBullets02).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGunsAndBullets03).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGunsAndBullets04).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGunsAndBullets05).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGunsAndBullets06).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGunsAndBullets07).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGunsAndBullets08).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGunsAndBullets09).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagGunsAndBullets10).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagLiveAndLove01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagLiveAndLove02).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagLiveAndLove03).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagLiveAndLove04).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagLiveAndLove05).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagLiveAndLove06).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagLiveAndLove07).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagLiveAndLove08).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagLiveAndLove09).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagMassSurgicalJournal01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagMassSurgicalJournal02).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagMassSurgicalJournal03).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagMassSurgicalJournal04).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagMassSurgicalJournal05).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagMassSurgicalJournal06).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagMassSurgicalJournal07).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagMassSurgicalJournal08).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagMassSurgicalJournal09).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagJunktownJerkyVendor01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagJunktownJerkyVendor02).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagJunktownJerkyVendor03).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagJunktownJerkyVendor04).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagJunktownJerkyVendor05).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagJunktownJerkyVendor06).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagJunktownJerkyVendor07).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagJunktownJerkyVendor08).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagNikolaTeslaAndYou01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagNikolaTeslaAndYou02).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagNikolaTeslaAndYou03).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagNikolaTeslaAndYou04).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagNikolaTeslaAndYou05).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagNikolaTeslaAndYou06).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagNikolaTeslaAndYou07).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagNikolaTeslaAndYou08).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagNikolaTeslaAndYou09).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagTumblersToday01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagTumblersToday02).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagTumblersToday03).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagTumblersToday04).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagTumblersToday05).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagUSCovertOperationsManual01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagUSCovertOperationsManual02).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagUSCovertOperationsManual03).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagUSCovertOperationsManual04).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagUSCovertOperationsManual05).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagUSCovertOperationsManual06).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagUSCovertOperationsManual07).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagUSCovertOperationsManual08).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagUSCovertOperationsManual09).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagUSCovertOperationsManual10).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagUnstoppables01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagUnstoppables02).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagUnstoppables03).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagUnstoppables04).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagUnstoppables05).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagWastelandSurvivalGuide01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagWastelandSurvivalGuide02).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagWastelandSurvivalGuide03).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagWastelandSurvivalGuide04).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagWastelandSurvivalGuide05).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagWastelandSurvivalGuide06).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagWastelandSurvivalGuide07).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagWastelandSurvivalGuide08).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagWastelandSurvivalGuide09).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Book Property PerkMagAstoundinglyAwesomeTales01 Auto Const
Book Property PerkMagAstoundinglyAwesomeTales02 Auto Const
Book Property PerkMagAstoundinglyAwesomeTales03 Auto Const
Book Property PerkMagAstoundinglyAwesomeTales04 Auto Const
Book Property PerkMagAstoundinglyAwesomeTales05 Auto Const
Book Property PerkMagAstoundinglyAwesomeTales06 Auto Const
Book Property PerkMagAstoundinglyAwesomeTales07 Auto Const
Book Property PerkMagAstoundinglyAwesomeTales08 Auto Const
Book Property PerkMagAstoundinglyAwesomeTales09 Auto Const
Book Property PerkMagAstoundinglyAwesomeTales10 Auto Const
Book Property PerkMagAstoundinglyAwesomeTales11 Auto Const
Book Property PerkMagAstoundinglyAwesomeTales12 Auto Const
Book Property PerkMagAstoundinglyAwesomeTales13 Auto Const
Book Property PerkMagAstoundinglyAwesomeTales14 Auto Const

Book Property PerkMagGrognakTheBarbarian01 Auto Const
Book Property PerkMagGrognakTheBarbarian02 Auto Const
Book Property PerkMagGrognakTheBarbarian03 Auto Const
Book Property PerkMagGrognakTheBarbarian04 Auto Const
Book Property PerkMagGrognakTheBarbarian05 Auto Const
Book Property PerkMagGrognakTheBarbarian06 Auto Const
Book Property PerkMagGrognakTheBarbarian07 Auto Const
Book Property PerkMagGrognakTheBarbarian08 Auto Const
Book Property PerkMagGrognakTheBarbarian09 Auto Const
Book Property PerkMagGrognakTheBarbarian10 Auto Const

Book Property PerkMagGunsAndBullets01 Auto Const
Book Property PerkMagGunsAndBullets02 Auto Const
Book Property PerkMagGunsAndBullets03 Auto Const
Book Property PerkMagGunsAndBullets04 Auto Const
Book Property PerkMagGunsAndBullets05 Auto Const
Book Property PerkMagGunsAndBullets06 Auto Const
Book Property PerkMagGunsAndBullets07 Auto Const
Book Property PerkMagGunsAndBullets08 Auto Const
Book Property PerkMagGunsAndBullets09 Auto Const
Book Property PerkMagGunsAndBullets10 Auto Const

Book Property PerkMagLiveAndLove01 Auto Const
Book Property PerkMagLiveAndLove02 Auto Const
Book Property PerkMagLiveAndLove03 Auto Const
Book Property PerkMagLiveAndLove04 Auto Const
Book Property PerkMagLiveAndLove05 Auto Const
Book Property PerkMagLiveAndLove06 Auto Const
Book Property PerkMagLiveAndLove07 Auto Const
Book Property PerkMagLiveAndLove08 Auto Const
Book Property PerkMagLiveAndLove09 Auto Const

Book Property PerkMagMassSurgicalJournal01 Auto Const
Book Property PerkMagMassSurgicalJournal02 Auto Const
Book Property PerkMagMassSurgicalJournal03 Auto Const
Book Property PerkMagMassSurgicalJournal04 Auto Const
Book Property PerkMagMassSurgicalJournal05 Auto Const
Book Property PerkMagMassSurgicalJournal06 Auto Const
Book Property PerkMagMassSurgicalJournal07 Auto Const
Book Property PerkMagMassSurgicalJournal08 Auto Const
Book Property PerkMagMassSurgicalJournal09 Auto Const

Book Property PerkMagJunktownJerkyVendor01 Auto Const
Book Property PerkMagJunktownJerkyVendor02 Auto Const
Book Property PerkMagJunktownJerkyVendor03 Auto Const
Book Property PerkMagJunktownJerkyVendor04 Auto Const
Book Property PerkMagJunktownJerkyVendor05 Auto Const
Book Property PerkMagJunktownJerkyVendor06 Auto Const
Book Property PerkMagJunktownJerkyVendor07 Auto Const
Book Property PerkMagJunktownJerkyVendor08 Auto Const

Book Property PerkMagNikolaTeslaAndYou01 Auto Const
Book Property PerkMagNikolaTeslaAndYou02 Auto Const
Book Property PerkMagNikolaTeslaAndYou03 Auto Const
Book Property PerkMagNikolaTeslaAndYou04 Auto Const
Book Property PerkMagNikolaTeslaAndYou05 Auto Const
Book Property PerkMagNikolaTeslaAndYou06 Auto Const
Book Property PerkMagNikolaTeslaAndYou07 Auto Const
Book Property PerkMagNikolaTeslaAndYou08 Auto Const
Book Property PerkMagNikolaTeslaAndYou09 Auto Const

Book Property PerkMagTumblersToday01 Auto Const
Book Property PerkMagTumblersToday02 Auto Const
Book Property PerkMagTumblersToday03 Auto Const
Book Property PerkMagTumblersToday04 Auto Const
Book Property PerkMagTumblersToday05 Auto Const

Book Property PerkMagUSCovertOperationsManual01 Auto Const
Book Property PerkMagUSCovertOperationsManual02 Auto Const
Book Property PerkMagUSCovertOperationsManual03 Auto Const
Book Property PerkMagUSCovertOperationsManual04 Auto Const
Book Property PerkMagUSCovertOperationsManual05 Auto Const
Book Property PerkMagUSCovertOperationsManual06 Auto Const
Book Property PerkMagUSCovertOperationsManual07 Auto Const
Book Property PerkMagUSCovertOperationsManual08 Auto Const
Book Property PerkMagUSCovertOperationsManual09 Auto Const
Book Property PerkMagUSCovertOperationsManual10 Auto Const

Book Property PerkMagUnstoppables01 Auto Const
Book Property PerkMagUnstoppables02 Auto Const
Book Property PerkMagUnstoppables03 Auto Const
Book Property PerkMagUnstoppables04 Auto Const
Book Property PerkMagUnstoppables05 Auto Const

Book Property PerkMagWastelandSurvivalGuide01 Auto Const
Book Property PerkMagWastelandSurvivalGuide02 Auto Const
Book Property PerkMagWastelandSurvivalGuide03 Auto Const
Book Property PerkMagWastelandSurvivalGuide04 Auto Const
Book Property PerkMagWastelandSurvivalGuide05 Auto Const
Book Property PerkMagWastelandSurvivalGuide06 Auto Const
Book Property PerkMagWastelandSurvivalGuide07 Auto Const
Book Property PerkMagWastelandSurvivalGuide08 Auto Const
Book Property PerkMagWastelandSurvivalGuide09 Auto Const
