;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:SpawnNPCSetNPCSynths Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActor(CheatTerminal_LvlSynth)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActor(CheatTerminal_LvlSynth_Institute_Boss)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActor(CheatTerminal_LvlSynth_Institute_AllArmor_Auto)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActor(CheatTerminal_LvlSynthCourser)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ActorBase Property CheatTerminal_LvlSynth Auto Const
ActorBase Property CheatTerminal_LvlSynth_Institute_Boss Auto Const
ActorBase Property CheatTerminal_LvlSynth_Institute_AllArmor_Auto Auto Const
ActorBase Property CheatTerminal_LvlSynthCourser Auto Const
