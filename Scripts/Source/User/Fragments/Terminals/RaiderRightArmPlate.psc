;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:RaiderRightArmPlate Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(GetArmorPiece()) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(GetArmorPiece()) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(GetArmorPiece(), PA_Raider_Arm_Lining_A)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(GetArmorPiece()) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(GetArmorPiece()) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(GetArmorPiece(), PA_Raider_Arm_Lining_B)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Form Function GetArmorPiece()
	return Armor_Power_Raider_ArmRight
EndFunction
Message Property CheatTerminal_PowerArmorModMessage Auto Const
Message Property CheatTerminal_PowerArmorTooMany Auto Const
Message Property CheatTerminal_PowerArmorNotEnough Auto Const
Armor Property Armor_Power_Raider_ArmRight Auto Const
ObjectMod Property PA_Raider_Arm_Lining_A Auto Const
ObjectMod Property PA_Raider_Arm_Lining_B Auto Const
