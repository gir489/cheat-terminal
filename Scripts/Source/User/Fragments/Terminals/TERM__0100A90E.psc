;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__0100A90E Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_CombatRifle_Muzzle_BayonetLarge)
else
	Game.GetPlayer().AddItem(miscmod_mod_CombatRifle_Muzzle_BayonetLarge)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_CombatRifle_Muzzle_Compensator)
else
	Game.GetPlayer().AddItem(miscmod_mod_CombatRifle_Muzzle_Compensator)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_CombatRifle_Muzzle_MuzzleBrake)
else
	Game.GetPlayer().AddItem(miscmod_mod_CombatRifle_Muzzle_MuzzleBrake)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_CombatRifle_Muzzle_Suppressor)
else
	Game.GetPlayer().AddItem(miscmod_mod_CombatRifle_Muzzle_Suppressor)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_CombatRifle_Muzzle_BayonetLarge Auto Const
ObjectMod Property mod_CombatRifle_Muzzle_Compensator Auto Const
ObjectMod Property mod_CombatRifle_Muzzle_MuzzleBrake Auto Const
ObjectMod Property mod_CombatRifle_Muzzle_Suppressor Auto Const
MiscObject Property miscmod_mod_CombatRifle_Muzzle_BayonetLarge Auto Const
MiscObject Property miscmod_mod_CombatRifle_Muzzle_Compensator Auto Const
MiscObject Property miscmod_mod_CombatRifle_Muzzle_MuzzleBrake Auto Const
MiscObject Property miscmod_mod_CombatRifle_Muzzle_Suppressor Auto Const
