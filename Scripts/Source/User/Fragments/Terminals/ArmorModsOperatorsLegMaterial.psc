;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:ArmorModsOperatorsLegMaterial Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00028789,"DLCNukaWorld.esm")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x0002876B,"DLCNukaWorld.esm")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x0002878A,"DLCNukaWorld.esm")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x0002876C,"DLCNukaWorld.esm")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
