;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_NukaW_01001F68 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00023E60, "DLCNukaWorld.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive, CheatTerminal_ThrowablesToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00025B0A, "DLCNukaWorld.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive, CheatTerminal_ThrowablesToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x0002618B, "DLCNukaWorld.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive, CheatTerminal_ThrowablesToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00033905, "DLCNukaWorld.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive, CheatTerminal_ThrowablesToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x000346FC, "DLCNukaWorld.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive, CheatTerminal_ThrowablesToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00040CDD, "DLCNukaWorld.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive, CheatTerminal_ThrowablesToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00040CDE, "DLCNukaWorld.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive, CheatTerminal_ThrowablesToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

GlobalVariable Property CheatTerminal_ThrowablesToGive Auto Const
