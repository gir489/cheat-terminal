;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:X02ArmMisc Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x000008D9,"ccbgsfo4115-x02.esl")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00000896,"ccbgsfo4115-x02.esl")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod )
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x000008DA,"ccbgsfo4115-x02.esl")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00000897,"ccbgsfo4115-x02.esl")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod )
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x000008DB,"ccbgsfo4115-x02.esl")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00000898,"ccbgsfo4115-x02.esl")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod )
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x000008DC,"ccbgsfo4115-x02.esl")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00000899,"ccbgsfo4115-x02.esl")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod )
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
