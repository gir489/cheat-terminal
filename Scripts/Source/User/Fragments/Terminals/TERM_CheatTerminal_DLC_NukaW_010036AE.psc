;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_NukaW_010036AE Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00027415, "DLCNukaWorld.esm")
if objectToGive
	LL_EpicChance_Standard.SetValue(0.0)
	Game.GetPlayer().AddItem(objectToGive)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00027417, "DLCNukaWorld.esm")
if objectToGive
	LL_EpicChance_Standard.SetValue(0.0)
	Game.GetPlayer().AddItem(objectToGive)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x0002741A, "DLCNukaWorld.esm")
if objectToGive
	LL_EpicChance_Standard.SetValue(0.0)
	Game.GetPlayer().AddItem(objectToGive)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x0002741C, "DLCNukaWorld.esm")
if objectToGive
	LL_EpicChance_Standard.SetValue(0.0)
	Game.GetPlayer().AddItem(objectToGive)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x0002741E, "DLCNukaWorld.esm")
if objectToGive
	LL_EpicChance_Standard.SetValue(0.0)
	Game.GetPlayer().AddItem(objectToGive)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

GlobalVariable Property LL_EpicChance_Standard Auto Const
