;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Mele_0100A906 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_Machete_Serrated)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_Machete_Serrated)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_Machete_Sacrificial)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_Machete_Sacrificial)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_melee_Machete_Serrated Auto Const
ObjectMod Property mod_melee_Machete_Sacrificial Auto Const
MiscObject Property miscmod_mod_melee_Machete_Serrated Auto Const
MiscObject Property miscmod_mod_melee_Machete_Sacrificial Auto Const
