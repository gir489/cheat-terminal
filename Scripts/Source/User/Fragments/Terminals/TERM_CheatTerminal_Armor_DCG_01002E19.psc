;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Armor_DCG_01002E19 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_DCGuard_Helmet02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_DCGuard_TorsoArmor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_DCGuard_LArm)
Game.GetPlayer().AddItem(Armor_DCGuard_RArm)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_DCGuard_Helmet01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_DCGuard_LArmUpper)
Game.GetPlayer().AddItem(Armor_DCGuard_RArmUpper)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_DCGuard_LArmLower)
Game.GetPlayer().AddItem(Armor_DCGuard_RArmLower)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Armor Property Armor_DCGuard_Helmet01 Auto Const

Armor Property Armor_DCGuard_Helmet02 Auto Const

Armor Property Armor_DCGuard_TorsoArmor Auto Const

Armor Property Armor_DCGuard_LArm Auto Const
Armor Property Armor_DCGuard_RArm Auto Const

Armor Property Armor_DCGuard_LArmUpper Auto Const
Armor Property Armor_DCGuard_RArmUpper Auto Const

Armor Property Armor_DCGuard_LArmLower Auto Const
Armor Property Armor_DCGuard_RArmLower Auto Const
