;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:AssaultCarbineMoveToLocation Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x00193B4D, "Fallout4.esm") as ObjectReference, 27.783437, 299.47375, -81.590273, 0.0, AssaultRifleAllAmericanLookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x0018F176, "Fallout4.esm") as ObjectReference, 983.227461, 865.055165, 145.13208, 0.0, AssaultRifleSurvivalistsLookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x001CB693, "Fallout4.esm") as ObjectReference, -3037.3381, 1483.4912, -493.0592, 0.0, AssaultRiflePatriotLookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property AssaultRifleSurvivalistsLookAtRef Auto Const
ObjectReference Property AssaultRifleAllAmericanLookAtRef Auto Const
ObjectReference Property AssaultRiflePatriotLookAtRef Auto Const
