;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:DLCAutomatronItems Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00004F12, "DLCRobot.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive, CheatTerminal_AidToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00007B20, "DLCRobot.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00001D29, "DLCRobot.esm")
KeyStorageRef.Reset()
KeyStorageRef.AddItem(objectToGive)
objectToGive = Game.GetFormFromFile(0x00008B66, "DLCRobot.esm")
KeyStorageRef.AddItem(objectToGive)
objectToGive = Game.GetFormFromFile(0x0000D5E0, "DLCRobot.esm")
KeyStorageRef.AddItem(objectToGive)
objectToGive = Game.GetFormFromFile(0x0000FEAC, "DLCRobot.esm")
KeyStorageRef.AddItem(objectToGive)
CheatTerminalGlobalFuncs.DoPotion(KeyPotion)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Potion Property KeyPotion Auto Const
ObjectReference Property KeyStorageRef Auto Const
GlobalVariable Property CheatTerminal_AidToGive Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
