;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_NukaW_01001F62 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002689C, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002689D, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002689E, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002689F, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000268A0, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000268A1, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002A180, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002A181, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002A182, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002A183, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002A184, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002A185, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002BFEF, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

GlobalVariable Property CheatTerminal_MiscToGive Auto Const
