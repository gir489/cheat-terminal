;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01001760 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_Armor_Power_X01_ArmLeft_A)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_Armor_Power_X01_ArmRight_A)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_Armor_Power_X01_LegLeft_A)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_Armor_Power_X01_Torso_A)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_Armor_Power_X01_LegRight_A)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_Armor_Power_X01_Helmet_A)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureX01, abForcePersist = true, abDeleteWhenAble = false)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

LeveledItem Property LL_Armor_Power_X01_Helmet_A Auto Const
LeveledItem Property LL_Armor_Power_X01_ArmLeft_A Auto Const
LeveledItem Property LL_Armor_Power_X01_ArmRight_A Auto Const
LeveledItem Property LL_Armor_Power_X01_Torso_A Auto Const
LeveledItem Property LL_Armor_Power_X01_LegLeft_A Auto Const
LeveledItem Property LL_Armor_Power_X01_LegRight_A Auto Const

Furniture Property PowerArmorFurnitureX01 Auto Const
