;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_MoveToLoc_01007AFE Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, RelayTower01extMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, RangerCabinMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, ReconBunkerThetaMapMarkerRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, RedRocketTruckStopMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, RoadsidePinesMapMarkerRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, ReebMarinaMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, RelayTower04ExtMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, RelayTower02ExtMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, RelayTower03ExtMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, RelayTower05ExtMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, RevereBeachStationMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, RevereSatelliteArrayMapMarkerRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, RoboticsPioneerParkMapMarkerRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, RoboticsDisposalGroundMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, RockyNarrowsParkMapMarkerRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, BarneysBunkerMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, RottenLandfillMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property RelayTower01extMapMarker Auto Const
ObjectReference Property RangerCabinMarker Auto Const
ObjectReference Property ReconBunkerThetaMapMarkerRef Auto Const
ObjectReference Property RedRocketTruckStopMapMarker Auto Const
ObjectReference Property ReebMarinaMapMarker Auto Const
ObjectReference Property RelayTower04ExtMapMarker Auto Const
ObjectReference Property RelayTower02ExtMapMarker Auto Const
ObjectReference Property RelayTower03ExtMapMarker Auto Const
ObjectReference Property RelayTower05ExtMapMarker Auto Const
ObjectReference Property RevereBeachStationMapMarker Auto Const
ObjectReference Property RevereSatelliteArrayMapMarkerRef Auto Const
ObjectReference Property RoadsidePinesMapMarkerRef Auto Const
ObjectReference Property RoboticsPioneerParkMapMarkerRef Auto Const
ObjectReference Property RoboticsDisposalGroundMapMarker Auto Const
ObjectReference Property RockyNarrowsParkMapMarkerRef Auto Const
ObjectReference Property BarneysBunkerMapMarker Auto Const
ObjectReference Property RottenLandfillMapMarker Auto Const
