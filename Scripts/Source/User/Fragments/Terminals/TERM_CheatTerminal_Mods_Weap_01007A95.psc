;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Weap_01007A95 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_SubmachineGun_Receiver_Automatic1)
else
	Game.GetPlayer().AddItem(miscmod_mod_SubmachineGun_Receiver_Automatic1)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_SubmachineGun_Receiver_Automatic1_and_ArmorPiercing)
else
	Game.GetPlayer().AddItem(miscmod_mod_SubmachineGun_Receiver_Automatic1_and_ArmorPiercing)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_SubmachineGun_Receiver_Automatic1_and_MoreDamage1)
else
	Game.GetPlayer().AddItem(miscmod_mod_SubmachineGun_Receiver_Automatic1_and_MoreDamage1)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_SubmachineGun_Receiver_Automatic2)
else
	Game.GetPlayer().AddItem(miscmod_mod_SubmachineGun_Receiver_Automatic2)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_SubmachineGun_Receiver_Automatic1_and_MoreDamage1_and_ArmorPiercing)
else
	Game.GetPlayer().AddItem(miscmod_mod_SubmachineGun_Receiver_Automatic1_and_MoreDamage1_and_ArmorPiercing)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_SubmachineGun_Receiver_Automatic1_and_MoreDamage2)
else
	Game.GetPlayer().AddItem(miscmod_mod_SubmachineGun_Receiver_Automatic1_and_MoreDamage2)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_SubmachineGun_Receiver_Automatic1 Auto Const
ObjectMod Property mod_SubmachineGun_Receiver_Automatic1_and_ArmorPiercing Auto Const
ObjectMod Property mod_SubmachineGun_Receiver_Automatic1_and_MoreDamage1 Auto Const
ObjectMod Property mod_SubmachineGun_Receiver_Automatic2 Auto Const
ObjectMod Property mod_SubmachineGun_Receiver_Automatic1_and_MoreDamage1_and_ArmorPiercing Auto Const
ObjectMod Property mod_SubmachineGun_Receiver_Automatic1_and_MoreDamage2 Auto Const
MiscObject Property miscmod_mod_SubmachineGun_Receiver_Automatic1 Auto Const
MiscObject Property miscmod_mod_SubmachineGun_Receiver_Automatic1_and_ArmorPiercing Auto Const
MiscObject Property miscmod_mod_SubmachineGun_Receiver_Automatic1_and_MoreDamage1 Auto Const
MiscObject Property miscmod_mod_SubmachineGun_Receiver_Automatic2 Auto Const
MiscObject Property miscmod_mod_SubmachineGun_Receiver_Automatic1_and_MoreDamage1_and_ArmorPiercing Auto Const
MiscObject Property miscmod_mod_SubmachineGun_Receiver_Automatic1_and_MoreDamage2 Auto Const
