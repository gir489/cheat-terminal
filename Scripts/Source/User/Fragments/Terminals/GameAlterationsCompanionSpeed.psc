;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsCompanionSpeed Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
FollowerAlias.GetRef().SetValue(SpeedMult, 100)
CheatTerminal_Companion_Speed.SetValue(100)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
FollowerAlias.GetRef().SetValue(SpeedMult, 500)
CheatTerminal_Companion_Speed.SetValue(500)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
FollowerAlias.GetRef().SetValue(SpeedMult, 1000)
CheatTerminal_Companion_Speed.SetValue(1000)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
FollowerAlias.GetRef().SetValue(SpeedMult, 300)
CheatTerminal_Companion_Speed.SetValue(300)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
FollowerAlias.GetRef().SetValue(SpeedMult, 200)
CheatTerminal_Companion_Speed.SetValue(200)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
FollowerAlias.GetRef().SetValue(SpeedMult, 50)
CheatTerminal_Companion_Speed.SetValue(50)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
FollowerAlias.GetRef().SetValue(SpeedMult, 25)
CheatTerminal_Companion_Speed.SetValue(25)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
FollowerAlias.GetRef().SetValue(SpeedMult, 75)
CheatTerminal_Companion_Speed.SetValue(75)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ReferenceAlias Property FollowerAlias Auto Const
ActorValue Property SpeedMult Auto Const

GlobalVariable Property CheatTerminal_Companion_Speed Auto Const
