;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01008240 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_assaultRifle_Scope_SightsIron)
else
	Game.GetPlayer().AddItem(miscmod_mod_assaultRifle_Scope_SightsIron)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AssaultRifle_Scope_ScopeShort)
else
	Game.GetPlayer().AddItem(miscmod_mod_AssaultRifle_Scope_ScopeShort)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_assaultRifle_Scope_SightReflex)
else
	Game.GetPlayer().AddItem(miscmod_mod_assaultRifle_Scope_SightReflex)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_assaultRifle_Scope_SightReflexCircle)
else
	Game.GetPlayer().AddItem(miscmod_mod_assaultRifle_Scope_SightReflexCircle)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AssaultRifle_Scope_ScopeMedium)
else
	Game.GetPlayer().AddItem(miscmod_mod_AssaultRifle_Scope_ScopeMedium)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AssaultRifle_Scope_ScopeLong)
else
	Game.GetPlayer().AddItem(miscmod_mod_AssaultRifle_Scope_ScopeLong)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AssaultRifle_Scope_ScopeShort_NV)
else
	Game.GetPlayer().AddItem(miscmod_mod_AssaultRifle_Scope_ScopeShort_NV)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AssaultRifle_Scope_ScopeMedium_NV)
else
	Game.GetPlayer().AddItem(miscmod_mod_AssaultRifle_Scope_ScopeMedium_NV)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AssaultRifle_Scope_ScopeLong_NV)
else
	Game.GetPlayer().AddItem(miscmod_mod_AssaultRifle_Scope_ScopeLong_NV)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AssaultRifle_Scope_ScopeShort_Recon)
else
	Game.GetPlayer().AddItem(miscmod_mod_AssaultRifle_Scope_ScopeShort_Recon)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AssaultRifle_Scope_ScopeLong_Recon)
else
	Game.GetPlayer().AddItem(miscmod_mod_AssaultRifle_Scope_ScopeLong_Recon)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_assaultRifle_Scope_SightsIron Auto Const
ObjectMod Property mod_AssaultRifle_Scope_ScopeShort Auto Const
ObjectMod Property mod_assaultRifle_Scope_SightReflexCircle Auto Const
ObjectMod Property mod_assaultRifle_Scope_SightReflex Auto Const
ObjectMod Property mod_AssaultRifle_Scope_ScopeMedium Auto Const
ObjectMod Property mod_AssaultRifle_Scope_ScopeLong Auto Const
ObjectMod Property mod_AssaultRifle_Scope_ScopeShort_NV Auto Const
ObjectMod Property mod_AssaultRifle_Scope_ScopeMedium_NV Auto Const
ObjectMod Property mod_AssaultRifle_Scope_ScopeLong_NV Auto Const
ObjectMod Property mod_AssaultRifle_Scope_ScopeShort_Recon Auto Const
ObjectMod Property mod_AssaultRifle_Scope_ScopeLong_Recon Auto Const
MiscObject Property miscmod_mod_assaultRifle_Scope_SightsIron Auto Const
MiscObject Property miscmod_mod_AssaultRifle_Scope_ScopeShort Auto Const
MiscObject Property miscmod_mod_assaultRifle_Scope_SightReflexCircle Auto Const
MiscObject Property miscmod_mod_assaultRifle_Scope_SightReflex Auto Const
MiscObject Property miscmod_mod_AssaultRifle_Scope_ScopeMedium Auto Const
MiscObject Property miscmod_mod_AssaultRifle_Scope_ScopeLong Auto Const
MiscObject Property miscmod_mod_AssaultRifle_Scope_ScopeShort_NV Auto Const
MiscObject Property miscmod_mod_AssaultRifle_Scope_ScopeMedium_NV Auto Const
MiscObject Property miscmod_mod_AssaultRifle_Scope_ScopeLong_NV Auto Const
MiscObject Property miscmod_mod_AssaultRifle_Scope_ScopeShort_Recon Auto Const
MiscObject Property miscmod_mod_AssaultRifle_Scope_ScopeLong_Recon Auto Const
