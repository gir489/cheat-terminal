;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:FarHarbor Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00034E7F, "DLCCoast.esm")
if ( formFromMod )
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_FarHarbor_HasActionBoy03, formFromMod as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_FarHarbor_HasActionGirl03, Game.GetFormFromFile(0x00034E80, "DLCCoast.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_FarHarbor_HasFortifyXPBonusPerk, Game.GetFormFromFile(0x0004FA84, "DLCCoast.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_FarHarbor_HasCriticalBanker04, Game.GetFormFromFile(0x00034E81, "DLCCoast.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_FarHarbor_HasCrusaderOfAtomPerk, Game.GetFormFromFile(0x00023B36, "DLCCoast.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_FarHarbor_HasDestroyerAcadiaPerk, Game.GetFormFromFile(0x0002C9B4, "DLCCoast.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_FarHarbor_HasFarHarborSurvivalistPerk, Game.GetFormFromFile(0x0002C9B5, "DLCCoast.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_FarHarbor_HasHuntersWisdom, Game.GetFormFromFile(0x00018621, "DLCCoast.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_FarHarbor_HasInquisitorOfAtomPerk, Game.GetFormFromFile(0x0002C9B2, "DLCCoast.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_FarHarbor_HasLoneWanderer04, Game.GetFormFromFile(0x000365F8, "DLCCoast.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_FarHarbor_HasNightPerson03, Game.GetFormFromFile(0x00043222, "DLCCoast.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_FarHarbor_HasRadResistant04, Game.GetFormFromFile(0x000423A4, "DLCCoast.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_FarHarbor_HasScrapper03, Game.GetFormFromFile(0x000423A5, "DLCCoast.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_FarHarbor_HasStrongBack05, Game.GetFormFromFile(0x000423A3, "DLCCoast.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_FarHarbor_HasProtectorOfArcadia, Game.GetFormFromFile(0x0002C9B3, "DLCCoast.esm") as Perk)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0001054D, "DLCCoast.esm")
if ( formFromMod )
	CheatTerminalGlobalFuncs.SpawnSettler(formFromMod as ActorBase)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ActorBase Property WorkshopNPC Auto Const
ActorBase Property WorkshopNPCMale Auto Const
ActorBase Property WorkshopNPCFemale Auto Const
ActorBase Property WorkshopNPCGuard Auto Const
ActorBase Property LvlMinutemanWorkshop Auto Const
ActorBase Property LvlMinutemanWorkshopMale Auto Const
ActorBase Property LvlMinutemanWorkshopFemale Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasActionBoy03 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasActionGirl03 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasFortifyXPBonusPerk Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasCriticalBanker04 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasCrusaderOfAtomPerk Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasDestroyerAcadiaPerk Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasFarHarborSurvivalistPerk Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasHuntersWisdom Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasInquisitorOfAtomPerk Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasLoneWanderer04 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasNightPerson03 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasRadResistant04 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasScrapper03 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasStrongBack05 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasProtectorOfArcadia Auto Const
