;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:ItemsModsWeaponEnergyGaussBarrel Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GaussRifle_GaussBarrel_Long)
else
	Game.GetPlayer().AddItem(miscmod_mod_GaussRifle_Barrel_BarrelHalf)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GaussRifle_GaussBarrel_Long_and_MoreDamage1)
else
	Game.GetPlayer().AddItem(miscmod_mod_GaussRifle_Barrel_BarrelFull)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_GaussRifle_GaussBarrel_Long Auto Const
ObjectMod Property mod_GaussRifle_GaussBarrel_Long_and_MoreDamage1 Auto Const
MiscObject Property miscmod_mod_GaussRifle_Barrel_BarrelHalf Auto Const
MiscObject Property miscmod_mod_GaussRifle_Barrel_BarrelFull Auto Const
