;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Mele_0100A905 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_Ripper_Null)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_Ripper_Standard)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_Ripper_BladesLarge)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_Ripper_BladesLarge)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_Ripper_Hook)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_Ripper_Hook)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_melee_Ripper_Null Auto Const
ObjectMod Property mod_melee_Ripper_BladesLarge Auto Const
ObjectMod Property mod_melee_Ripper_Hook Auto Const
MiscObject Property miscmod_mod_melee_Ripper_Standard Auto Const
MiscObject Property miscmod_mod_melee_Ripper_BladesLarge Auto Const
MiscObject Property miscmod_mod_melee_Ripper_Hook Auto Const
