;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:M1GarandLocations Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, BlueBettyLookToRef, -6.747, 72.3572, -105.68, 0.0, BlueBettyLookToRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, ThisMachineLookToRef, -59.82, -103.0594, -29.9989, 0.0, ThisMachineLookToRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, M1GarandFraternalLookToRef, -120.77, 11.42, -75.1, 0.0, M1GarandFraternalLookToRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, M1GarandNationalGuardLookToRef, 51.99, -94.54, -54, 0.0, M1GarandNationalGuardLookToRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, M1GarandCoastGuardLookToRef, -164.57, -34.87, -59.21, 0.0, M1GarandCoastGuardLookToRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, M1GarandBostonPDLookToRef, -140.34, 1.61, -7.32, 0.0, M1GarandBostonPDLookToRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, M1GarandRedRocketLookToRef, -30.8, -134.48, -129.4, 0.0, M1GarandRedRocketLookToRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property ThisMachineLookToRef Auto Const
ObjectReference Property BlueBettyLookToRef Auto Const
ObjectReference Property M1GarandFraternalLookToRef Auto Const
ObjectReference Property M1GarandNationalGuardLookToRef Auto Const
ObjectReference Property M1GarandCoastGuardLookToRef Auto Const
ObjectReference Property M1GarandBostonPDLookToRef Auto Const
ObjectReference Property M1GarandRedRocketLookToRef Auto Const
