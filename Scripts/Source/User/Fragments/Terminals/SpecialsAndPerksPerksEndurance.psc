;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:SpecialsAndPerksPerksEndurance Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Toughness01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Toughness02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Toughness03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Toughness04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Toughness05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Toughness01)
Game.GetPlayer().RemovePerk(Toughness02)
Game.GetPlayer().RemovePerk(Toughness03)
Game.GetPlayer().RemovePerk(Toughness04)
Game.GetPlayer().RemovePerk(Toughness05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(LeadBelly01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(LeadBelly02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(LeadBelly03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(LeadBelly01)
Game.GetPlayer().RemovePerk(LeadBelly02)
Game.GetPlayer().RemovePerk(LeadBelly03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(LifeGiver01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(LifeGiver02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(LifeGiver03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(LifeGiver01)
Game.GetPlayer().RemovePerk(LifeGiver02)
Game.GetPlayer().RemovePerk(LifeGiver03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(ChemResistant01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(ChemResistant02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(ChemResistant01)
Game.GetPlayer().RemovePerk(ChemResistant02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
ActorBase PlayerBase = Game.GetPlayer().GetBaseObject() as ActorBase
bool bIsMale = PlayerBase.GetSex() == 0
givePerk(Toughness01)
givePerk(Toughness02)
givePerk(Toughness03)
givePerk(Toughness04)
givePerk(Toughness05)
givePerk(LeadBelly01)
givePerk(LeadBelly02)
givePerk(LeadBelly03)
givePerk(LifeGiver01)
givePerk(LifeGiver02)
givePerk(LifeGiver03)
givePerk(ChemResistant01)
givePerk(ChemResistant02)
if ( bIsMale )
givePerk(Aquaboy01)
givePerk(Aquaboy02)
else
givePerk(Aquagirl01)
givePerk(Aquagirl02)
endif
givePerk(RadResistant01)
givePerk(RadResistant02)
givePerk(RadResistant03)
givePerk(AdamantiumSkeleton01)
givePerk(AdamantiumSkeleton02)
givePerk(AdamantiumSkeleton03)
givePerk(Cannibal01)
givePerk(Cannibal02)
givePerk(Cannibal03)
givePerk(Ghoulish01)
givePerk(Ghoulish02)
givePerk(Ghoulish03)
givePerk(SolarPowered01)
givePerk(SolarPowered02)
givePerk(SolarPowered03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Aquaboy01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Aquaboy02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Aquagirl01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_22
Function Fragment_Terminal_22(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Aquagirl02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_23
Function Fragment_Terminal_23(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Aquagirl01)
Game.GetPlayer().RemovePerk(Aquagirl02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_24
Function Fragment_Terminal_24(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Aquaboy01)
Game.GetPlayer().RemovePerk(Aquaboy02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_25
Function Fragment_Terminal_25(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(RadResistant01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_26
Function Fragment_Terminal_26(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(RadResistant02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_27
Function Fragment_Terminal_27(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(RadResistant03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_28
Function Fragment_Terminal_28(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(RadResistant01)
Game.GetPlayer().RemovePerk(RadResistant02)
Game.GetPlayer().RemovePerk(RadResistant03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_29
Function Fragment_Terminal_29(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(AdamantiumSkeleton01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_30
Function Fragment_Terminal_30(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(AdamantiumSkeleton02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_31
Function Fragment_Terminal_31(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(AdamantiumSkeleton03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_32
Function Fragment_Terminal_32(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(AdamantiumSkeleton01)
Game.GetPlayer().RemovePerk(AdamantiumSkeleton02)
Game.GetPlayer().RemovePerk(AdamantiumSkeleton03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_33
Function Fragment_Terminal_33(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Cannibal01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_34
Function Fragment_Terminal_34(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Cannibal02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_35
Function Fragment_Terminal_35(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Cannibal03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_36
Function Fragment_Terminal_36(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Cannibal01)
Game.GetPlayer().RemovePerk(Cannibal02)
Game.GetPlayer().RemovePerk(Cannibal03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_37
Function Fragment_Terminal_37(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Ghoulish01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_38
Function Fragment_Terminal_38(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Ghoulish02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_39
Function Fragment_Terminal_39(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Ghoulish03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_40
Function Fragment_Terminal_40(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Ghoulish01)
Game.GetPlayer().RemovePerk(Ghoulish02)
Game.GetPlayer().RemovePerk(Ghoulish03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_41
Function Fragment_Terminal_41(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(SolarPowered01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_42
Function Fragment_Terminal_42(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(SolarPowered02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_43
Function Fragment_Terminal_43(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(SolarPowered03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_44
Function Fragment_Terminal_44(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(SolarPowered01)
Game.GetPlayer().RemovePerk(SolarPowered02)
Game.GetPlayer().RemovePerk(SolarPowered03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_45
Function Fragment_Terminal_45(ObjectReference akTerminalRef)
;BEGIN CODE
ActorBase PlayerBase = Game.GetPlayer().GetBaseObject() as ActorBase
bool bIsMale = PlayerBase.GetSex() == 0
removePerk(Toughness01)
removePerk(Toughness02)
removePerk(Toughness03)
removePerk(Toughness04)
removePerk(Toughness05)
removePerk(LeadBelly01)
removePerk(LeadBelly02)
removePerk(LeadBelly03)
removePerk(LifeGiver01)
removePerk(LifeGiver02)
removePerk(LifeGiver03)
removePerk(ChemResistant01)
removePerk(ChemResistant02)
if ( bIsMale )
removePerk(Aquaboy01)
removePerk(Aquaboy02)
else
removePerk(Aquagirl01)
removePerk(Aquagirl02)
endif
removePerk(RadResistant01)
removePerk(RadResistant02)
removePerk(RadResistant03)
removePerk(AdamantiumSkeleton01)
removePerk(AdamantiumSkeleton02)
removePerk(AdamantiumSkeleton03)
removePerk(Cannibal01)
removePerk(Cannibal02)
removePerk(Cannibal03)
removePerk(Ghoulish01)
removePerk(Ghoulish02)
removePerk(Ghoulish03)
removePerk(SolarPowered01)
removePerk(SolarPowered02)
removePerk(SolarPowered03)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Function givePerk(Perk perkToGive)
	Actor playerActor = Game.GetPlayer()
	if (playerActor.HasPerk(perkToGive) == false)
		playerActor.AddPerk(perkToGive)
	endIf
endFunction
Function removePerk(Perk perkToGive)
	Actor playerActor = Game.GetPlayer()
	if (playerActor.HasPerk(perkToGive))
		playerActor.RemovePerk(perkToGive)
	endIf
endFunction
Perk Property Toughness01 Auto Const
Perk Property Toughness02 Auto Const
Perk Property Toughness03 Auto Const
Perk Property Toughness04 Auto Const
Perk Property Toughness05 Auto Const
Perk Property LeadBelly01 Auto Const
Perk Property LeadBelly02 Auto Const
Perk Property LeadBelly03 Auto Const
Perk Property LifeGiver01 Auto Const
Perk Property LifeGiver02 Auto Const
Perk Property LifeGiver03 Auto Const
Perk Property ChemResistant01 Auto Const
Perk Property ChemResistant02 Auto Const
Perk Property Aquaboy01 Auto Const
Perk Property Aquaboy02 Auto Const
Perk Property Aquagirl01 Auto Const
Perk Property Aquagirl02 Auto Const
Perk Property RadResistant01 Auto Const
Perk Property RadResistant02 Auto Const
Perk Property RadResistant03 Auto Const
Perk Property AdamantiumSkeleton01 Auto Const
Perk Property AdamantiumSkeleton02 Auto Const
Perk Property AdamantiumSkeleton03 Auto Const
Perk Property Cannibal01 Auto Const
Perk Property Cannibal02 Auto Const
Perk Property Cannibal03 Auto Const
Perk Property Ghoulish01 Auto Const
Perk Property Ghoulish02 Auto Const
Perk Property Ghoulish03 Auto Const
Perk Property SolarPowered01 Auto Const
Perk Property SolarPowered02 Auto Const
Perk Property SolarPowered03 Auto Const
