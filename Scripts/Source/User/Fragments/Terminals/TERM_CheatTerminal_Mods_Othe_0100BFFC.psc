;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Othe_0100BFFC Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Flamer_MagTank_ExtraLarge)
else
	Game.GetPlayer().AddItem(miscmod_mod_Flamer_MagTank_ExtraLarge)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Flamer_MagTank_Standard)
else
	Game.GetPlayer().AddItem(miscmod_mod_Flamer_MagTank_Standard)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Flamer_MagTank_Large)
else
	Game.GetPlayer().AddItem(miscmod_mod_Flamer_MagTank_Large)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_Flamer_MagTank_Standard Auto Const
ObjectMod Property mod_Flamer_MagTank_Large Auto Const
ObjectMod Property mod_Flamer_MagTank_ExtraLarge Auto Const
MiscObject Property miscmod_mod_Flamer_MagTank_Standard Auto Const
MiscObject Property miscmod_mod_Flamer_MagTank_Large Auto Const
MiscObject Property miscmod_mod_Flamer_MagTank_ExtraLarge Auto Const
