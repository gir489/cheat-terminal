;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsCompanion Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Actor followerActor = FollowerAlias.GetRef() as Actor
if ( followerActor )
	followerActor.StopCombat()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Actor followerActor = FollowerAlias.GetRef() as Actor
followerActor.SwitchToPowerArmor(None)
CheatTerminal_Companion_HasPowerArmor.SetValue(0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
if ( FollowerAlias.GetRef() != None)
	Actor followerActor = FollowerAlias.GetRef() as Actor
	followerActor.SetRelationshipRank(Game.GetPlayer(), 4)
	float affinity = followerActor.GetValue(CA_Affinity)
	if (affinity < 250.0 )
			followerActor.SetValue(CA_Affinity, 250.0)
	elseif (affinity < 500.0 )
			followerActor.SetValue(CA_Affinity, 500.0)
	elseif (affinity < 750.0 )
			followerActor.SetValue(CA_Affinity, 750.0)
	elseif (affinity < 1100.0 )
			followerActor.SetValue(CA_Affinity, 1100.0)
	endIf
	FollowersScript.SendAffinityEvent(self, CA_CustomEvent_CaitLoves, ShouldSuppressComment = false, IsDialogueBump = true, CheckCompanionProximity = true)
	FollowersScript.SendAffinityEvent(self, CA_CustomEvent_CodsworthLoves, ShouldSuppressComment = false, IsDialogueBump = true, CheckCompanionProximity = true)
	FollowersScript.SendAffinityEvent(self, CA_CustomEvent_CurieLoves, ShouldSuppressComment = false, IsDialogueBump = true, CheckCompanionProximity = true)
	FollowersScript.SendAffinityEvent(self, CA_CustomEvent_DanseLoves, ShouldSuppressComment = false, IsDialogueBump = true, CheckCompanionProximity = true)
	FollowersScript.SendAffinityEvent(self, CA_CustomEvent_DeaconLoves, ShouldSuppressComment = false, IsDialogueBump = true, CheckCompanionProximity = true)
	FollowersScript.SendAffinityEvent(self, CA_CustomEvent_HancockLoves, ShouldSuppressComment = false, IsDialogueBump = true, CheckCompanionProximity = true)
	FollowersScript.SendAffinityEvent(self, CA_CustomEvent_MacCreadyLoves, ShouldSuppressComment = false, IsDialogueBump = true, CheckCompanionProximity = true)
	FollowersScript.SendAffinityEvent(self, CA_CustomEvent_PiperLoves, ShouldSuppressComment = false, IsDialogueBump = true, CheckCompanionProximity = true)
	FollowersScript.SendAffinityEvent(self, CA_CustomEvent_PrestonLoves, ShouldSuppressComment = false, IsDialogueBump = true, CheckCompanionProximity = true)
	FollowersScript.SendAffinityEvent(self, CA_CustomEvent_StrongLoves, ShouldSuppressComment = false, IsDialogueBump = true, CheckCompanionProximity = true)
	FollowersScript.SendAffinityEvent(self, CA_CustomEvent_ValentineLoves, ShouldSuppressComment = false, IsDialogueBump = true, CheckCompanionProximity = true)
	FollowersScript.SendAffinityEvent(self, CA_CustomEvent_X688Loves, ShouldSuppressComment = false, IsDialogueBump = true, CheckCompanionProximity = true)
	Form longfellowLoves = Game.GetFormFromFile(0x00006E60, "DLCCoast.esm")
	if ( longfellowLoves )
		Keyword CA_CustomEvent_OldLongfellowLoves = longfellowLoves as Keyword
		FollowersScript.SendAffinityEvent(self, CA_CustomEvent_OldLongfellowLoves, ShouldSuppressComment = false, IsDialogueBump = true, CheckCompanionProximity = true)
	endIf
	Form gageLoves = Game.GetFormFromFile(0x00044B3F, "DLCNukaWorld.esm")
	if ( gageLoves )
		Keyword CA_CustomEvent_GageLoves = gageLoves as Keyword
		FollowersScript.SendAffinityEvent(self, CA_CustomEvent_GageLoves, ShouldSuppressComment = false, IsDialogueBump = true, CheckCompanionProximity = true)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Actor followerActor = FollowerAlias.GetRef() as Actor
followerActor.Disable()
followerActor.Enable()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
if ( FollowerAlias.GetRef() != None)
	CheatTerminal_Companion_Height.SetValue(FollowerAlias.GetRef().GetScale())
else
	CheatTerminal_Companion_Height.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
if ( FollowerAlias.GetRef() != None)
	CheatTerminal_Companion_Speed.SetValue(FollowerAlias.GetRef().GetBaseValue(SpeedMult))
else
	CheatTerminal_Companion_Speed.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Actor companionRef = FollowerAlias.GetRef() as Actor
CheatTerminal_DoesCompanionHaveInfiniteAmmo.SetValue(companionRef.HasKeyword(TeammateDontUseAmmoKeyword) as Float)
CheatTerminal_DoesCompanionHaveInfiniteCarryWeight.SetValue(companionRef.HasPerk(CheatTerminal_Companion_InfiniteCarryWeight) as Float)
CheatTerminal_DoesCompanionHavePowerArmorPerk.SetValue(companionRef.HasKeyword(PowerArmorPreventArmorDamageKeyword) as Float)
CheatTerminal_DoesCompanionHaveEssential.SetValue(companionRef.IsEssential() as Float)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Actor followerActor = FollowerAlias.GetRef() as Actor
int actorFormId = followerActor.GetFormID()
bool isDanse = (actorFormId == 384589)
bool doesDanseHavePowerArmorOn = (followerActor.GetItemCount(Armor_Power_T60_LegRight_Danse) == 1) || (followerActor.GetItemCount(Armor_Power_X01_LegRight_Danse) == 1)
if ( isDanse )
	if ( doesDanseHavePowerArmorOn  && CheatTerminal_DanseIsInPowerArmor.Show() == 1 )
		return
	endIf
else
	if ( followerActor.HasKeyword(AttachSlot2) && CheatTerminal_CompanionExitPowerArmor.Show() == 1 )
		return
	endIf
endIf
CheatTerminalGlobalFuncs.DoPotion(CheatTerminal_CompanionRaceMenuPotion)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ReferenceAlias Property FollowerAlias Auto Const
Message Property CheatTerminal_CompanionExitPowerArmor Auto Const
Message Property CheatTerminal_CompanionHumanRaceOnly Auto Const
Keyword Property CA_CustomEvent_CaitLoves Auto Const
Keyword Property CA_CustomEvent_CodsworthLoves Auto Const
Keyword Property CA_CustomEvent_CurieLoves Auto Const
Keyword Property CA_CustomEvent_DanseLoves Auto Const
Keyword Property CA_CustomEvent_DeaconLoves Auto Const
Keyword Property CA_CustomEvent_HancockLoves Auto Const
Keyword Property CA_CustomEvent_MacCreadyLoves Auto Const
Keyword Property CA_CustomEvent_PiperLoves Auto Const
Keyword Property CA_CustomEvent_PrestonLoves Auto Const
Keyword Property CA_CustomEvent_StrongLoves Auto Const
Keyword Property CA_CustomEvent_ValentineLoves Auto Const
Keyword Property CA_CustomEvent_X688Loves Auto Const
ActorValue Property CA_Affinity Auto Const
GlobalVariable Property CheatTerminal_Companion_Height Auto Const
GlobalVariable Property CheatTerminal_Companion_Speed Auto Const
GlobalVariable Property CheatTerminal_Companion_HasPowerArmor Auto Const
ActorValue Property SpeedMult Auto Const
Potion Property CheatTerminal_CompanionRaceMenuPotion Auto Const
Race Property HumanRace Auto Const
Keyword Property AttachSlot2 Auto Const
Armor Property Armor_Power_T60_LegRight_Danse Auto Const
Armor Property Armor_Power_X01_LegRight_Danse Auto Const
Keyword Property TeammateDontUseAmmoKeyword Auto Const
Message Property CheatTerminal_DanseIsInPowerArmor Auto Const
Keyword Property PowerArmorPreventArmorDamageKeyword Auto Const
Perk Property CheatTerminal_Companion_InfiniteCarryWeight Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
GlobalVariable Property CheatTerminal_DoesCompanionHaveInfiniteAmmo Auto Const
GlobalVariable Property CheatTerminal_DoesCompanionHaveInfiniteCarryWeight Auto Const
GlobalVariable Property CheatTerminal_DoesCompanionHavePowerArmorPerk Auto Const
GlobalVariable Property CheatTerminal_DoesCompanionHaveEssential Auto Const

