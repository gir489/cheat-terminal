;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Weap_01007A97 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_SubmachineGun_Grip_StockRecoil)
else
	Game.GetPlayer().AddItem(miscmod_mod_SubmachineGun_Grip_StockRecoil)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_SubmachineGun_Grip_StockShort)
else
	Game.GetPlayer().AddItem(miscmod_mod_SubmachineGun_Grip_StockShort)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_SubmachineGun_Grip_StockFull)
else
	Game.GetPlayer().AddItem(miscmod_mod_SubmachineGun_Grip_Stock)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_SubmachineGun_Grip_StockShort Auto Const
ObjectMod Property mod_SubmachineGun_Grip_StockFull Auto Const
ObjectMod Property mod_SubmachineGun_Grip_StockRecoil Auto Const
MiscObject Property miscmod_mod_SubmachineGun_Grip_StockShort Auto Const
MiscObject Property miscmod_mod_SubmachineGun_Grip_Stock Auto Const
MiscObject Property miscmod_mod_SubmachineGun_Grip_StockRecoil Auto Const
