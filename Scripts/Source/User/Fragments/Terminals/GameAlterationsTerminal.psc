;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsTerminal Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
;Automatron
Form formFromMod = Game.GetFormFromFile(0x00001D28, "DLCRobot.esm")
if ( formFromMod )
	if ( CheatTerminal_AllTerminals.HasForm(formFromMod) == false )
		CheatTerminal_AllTerminals.AddForm(formFromMod)
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x00001F04, "DLCRobot.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x00008B16, "DLCRobot.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x00008B5F, "DLCRobot.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0000D5DF, "DLCRobot.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0000F621, "DLCRobot.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x00010752, "DLCRobot.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x00010756, "DLCRobot.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x00010C62, "DLCRobot.esm"))
	endIf
endIf
;Far Harbor
formFromMod = Game.GetFormFromFile(0x00003A59, "DLCCoast.esm")
if ( formFromMod )
	if ( CheatTerminal_AllDoors.HasForm(formFromMod) == false )
		CheatTerminal_AllDoors.AddForm(formFromMod)
		CheatTerminal_AllDoors.AddForm(Game.GetFormFromFile(0x00003A5B, "DLCCoast.esm"))
		CheatTerminal_AllDoors.AddForm(Game.GetFormFromFile(0x00017E79, "DLCCoast.esm"))
		CheatTerminal_AllDoors.AddForm(Game.GetFormFromFile(0x0001FC89, "DLCCoast.esm"))
		CheatTerminal_AllDoors.AddForm(Game.GetFormFromFile(0x0002880A, "DLCCoast.esm"))
		CheatTerminal_AllDoors.AddForm(Game.GetFormFromFile(0x00053D20, "DLCCoast.esm"))
	endIf
	formFromMod = Game.GetFormFromFile(0x0002C901, "DLCCoast.esm")
	if ( CheatTerminal_AllContainers.HasForm(formFromMod) == false )
		CheatTerminal_AllContainers.AddForm(formFromMod)
		CheatTerminal_AllContainers.AddForm(Game.GetFormFromFile(0x00032C58, "DLCCoast.esm"))
		CheatTerminal_AllContainers.AddForm(Game.GetFormFromFile(0x0004B222, "DLCCoast.esm"))
		CheatTerminal_AllContainers.AddForm(Game.GetFormFromFile(0x0004F4A7, "DLCCoast.esm"))
		CheatTerminal_AllContainers.AddForm(Game.GetFormFromFile(0x00050045, "DLCCoast.esm"))
	endIf
	formFromMod = Game.GetFormFromFile(0x00007520, "DLCCoast.esm")
	if ( CheatTerminal_AllTerminals.HasForm(formFromMod) == false )
		CheatTerminal_AllTerminals.AddForm(formFromMod)
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0000B012, "DLCCoast.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x00027013, "DLCCoast.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x00027014, "DLCCoast.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x00027050, "DLCCoast.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x00027052, "DLCCoast.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0002705C, "DLCCoast.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0002958F, "DLCCoast.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0002F123, "DLCCoast.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0004FD0D, "DLCCoast.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0004FD0E, "DLCCoast.esm"))
	endIf
endIf
;Nuka World
formFromMod = Game.GetFormFromFile(0x000138D2, "DLCNukaWorld.esm")
if ( formFromMod )
	if ( CheatTerminal_AllDoors.HasForm(formFromMod) == false )
		CheatTerminal_AllDoors.AddForm(formFromMod)
		CheatTerminal_AllDoors.AddForm(Game.GetFormFromFile(0x00032776, "DLCNukaWorld.esm"))
		CheatTerminal_AllDoors.AddForm(Game.GetFormFromFile(0x00040B0C, "DLCNukaWorld.esm"))
		CheatTerminal_AllDoors.AddForm(Game.GetFormFromFile(0x00044991, "DLCNukaWorld.esm"))
		CheatTerminal_AllDoors.AddForm(Game.GetFormFromFile(0x00054164, "DLCNukaWorld.esm"))
	endIf
	formFromMod = Game.GetFormFromFile(0x0003F834, "DLCNukaWorld.esm")
	if ( CheatTerminal_AllContainers.HasForm(formFromMod) == false )
		CheatTerminal_AllContainers.AddForm(formFromMod)
		CheatTerminal_AllContainers.AddForm(Game.GetFormFromFile(0x0003F837, "DLCNukaWorld.esm"))
		CheatTerminal_AllContainers.AddForm(Game.GetFormFromFile(0x00046907, "DLCNukaWorld.esm"))
	endIf
	formFromMod = Game.GetFormFromFile(0x00022DCC, "DLCNukaWorld.esm")
	if ( CheatTerminal_AllTerminals.HasForm(formFromMod) == false )
		CheatTerminal_AllTerminals.AddForm(formFromMod)
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x000295CC, "DLCNukaWorld.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0002E1AB, "DLCNukaWorld.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0003B9E4, "DLCNukaWorld.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x00040787, "DLCNukaWorld.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x00042D55, "DLCNukaWorld.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0004863F, "DLCNukaWorld.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0004A092, "DLCNukaWorld.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0004CB4F, "DLCNukaWorld.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0004D3D0, "DLCNukaWorld.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0004D3F7, "DLCNukaWorld.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0004D54F, "DLCNukaWorld.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0004DF8B, "DLCNukaWorld.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0004E419, "DLCNukaWorld.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x0004F537, "DLCNukaWorld.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x000503B9, "DLCNukaWorld.esm"))
		CheatTerminal_AllTerminals.AddForm(Game.GetFormFromFile(0x00050606, "DLCNukaWorld.esm"))
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CompanionRef.GetRef() != None)
	Actor followerActor = CompanionRef.GetRef() as Actor
	if ( followerActor.HasKeyword(PowerArmorPreventArmorDamageKeyword) )
			CheatTerminal_DoesCompanionHavePowerArmorPerk.SetValue(1)
	else
			CheatTerminal_DoesCompanionHavePowerArmorPerk.SetValue(0)
	endIf
	if ( followerActor.IsEssential() )
		CheatTerminal_DoesCompanionHaveEssential.SetValue(1)
	else
		CheatTerminal_DoesCompanionHaveEssential.SetValue(0)
	endIf
	if ( followerActor.HasKeyword(AttachSlot1) )
		CheatTerminal_Companion_HasPowerArmor.SetValue(1)	
	else
		CheatTerminal_Companion_HasPowerArmor.SetValue(0)
	endIf
else
	CheatTerminal_DoesCompanionHavePowerArmorPerk.SetValue(0)
	CheatTerminal_DoesCompanionHaveEssential.SetValue(0)	
	CheatTerminal_Companion_HasPowerArmor.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.IncrementStat("Survival Denied", -1)
CheatTerminal_SurvivalModeDenied.SetValue(0)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ReferenceAlias Property CompanionRef Auto Const
Keyword Property PowerArmorPreventArmorDamageKeyword Auto Const
GlobalVariable Property CheatTerminal_DoesCompanionHavePowerArmorPerk Auto Const
GlobalVariable Property CheatTerminal_DoesCompanionHaveEssential Auto Const
FormList Property CheatTerminal_AllDoors Auto Const
FormList Property CheatTerminal_AllContainers Auto Const
FormList Property CheatTerminal_AllTerminals Auto Const
GlobalVariable Property CheatTerminal_Companion_HasPowerArmor Auto Const
Keyword Property AttachSlot1 Auto Const
GlobalVariable Property CheatTerminal_SurvivalModeDenied Auto Const
