;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Weapons_E_01002E0F Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_DN053VirgilsRifle, CustomItemMods_DN053VirgilsRifle)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(Aspiration_Weapon_DC_Lasergun)
weaponRef.AttachMod(Instigating)
weaponRef.AttachMod(mod_LaserGun_LaserReceiver_BetterCriticals2)
weaponRef.AttachMod(mod_LaserGun_BarrelLaser_Short_B)
weaponRef.AttachMod(mod_Aspiration_Weapon_IncreasedCost_10)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_BoSM01BrandisGun, CustomItemMods_BoSM01BrandisGun)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_DN109_ClintsGun, CustomItemMods_DN109_ClintsGun)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_UniversityPointLaserGun, CustomItemMods_UniversityPointLaserGun)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_BoS101LaserRifle, CustomItemMods_BoS101LaserRifle)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_WazerWifle, CustomItemMods_WazerWifle)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(GatlingLaser)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_BoSElderMaxonsGatling, CustomItemMods_BoSElderMaxonsGatling)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_DN053VirgilsRifle Auto Const Mandatory
FormList Property CustomItemMods_DN053VirgilsRifle Auto Const
LeveledItem Property Aspiration_Weapon_DC_Lasergun Auto Const
ObjectMod Property Instigating Auto Const
ObjectMod Property mod_LaserGun_BarrelLaser_Short_B Auto Const
ObjectMod Property mod_LaserGun_LaserReceiver_BetterCriticals2 Auto Const
ObjectMod Property mod_Aspiration_Weapon_IncreasedCost_10 Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_BoSM01BrandisGun Auto Const
FormList Property CustomItemMods_BoSM01BrandisGun Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_DN109_ClintsGun Auto Const
FormList Property CustomItemMods_DN109_ClintsGun Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_UniversityPointLaserGun Auto Const
FormList Property CustomItemMods_UniversityPointLaserGun Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_BoS101LaserRifle Auto Const
FormList Property CustomItemMods_BoS101LaserRifle Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_WazerWifle Auto Const
FormList Property CustomItemMods_WazerWifle Auto Const
Weapon Property GatlingLaser Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_BoSElderMaxonsGatling Auto Const
FormList Property CustomItemMods_BoSElderMaxonsGatling Auto Const
