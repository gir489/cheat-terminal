;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Ener_0100A148 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Gammagun_Grip_Standard)
else
	Game.GetPlayer().AddItem(miscmod_mod_Gammagun_Grip_Standard)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Gammagun_Grip_Better2)
else
	Game.GetPlayer().AddItem(miscmod_mod_Gammagun_Grip_Better2)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_Gammagun_Grip_Standard Auto Const
ObjectMod Property mod_Gammagun_Grip_Better2 Auto Const
MiscObject Property miscmod_mod_Gammagun_Grip_Standard Auto Const
MiscObject Property miscmod_mod_Gammagun_Grip_Better2 Auto Const
