;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Ener_0100BFF6 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GatlingLaser_BarrelMinigunLaser_Standard)
else
	Game.GetPlayer().AddItem(miscmod_mod_GatlingLaser_BarrelMingunLaser_Standard)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GatlingLaser_BarrelMinigunLaser_Super)
else
	Game.GetPlayer().AddItem(miscmod_mod_GatlingLaser_BarrelMingunLaser_Super)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_GatlingLaser_BarrelMinigunLaser_Standard Auto Const
ObjectMod Property mod_GatlingLaser_BarrelMinigunLaser_Super Auto Const
MiscObject Property miscmod_mod_GatlingLaser_BarrelMingunLaser_Standard Auto Const
MiscObject Property miscmod_mod_GatlingLaser_BarrelMingunLaser_Super Auto Const
