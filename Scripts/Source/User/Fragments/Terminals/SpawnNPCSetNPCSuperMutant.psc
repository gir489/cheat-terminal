;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:SpawnNPCSetNPCSuperMutant Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActor(CheatTerminal_LvlSupermutantMixed)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActor(CheatTerminal_LvlSupermutantSuicider)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActor(CheatTerminal_LvlSMBehemoth)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActor(CheatTerminal_DN131_LvlSMBehemothSwan)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ActorBase Property CheatTerminal_LvlSupermutantMixed Auto Const
ActorBase Property CheatTerminal_LvlSupermutantSuicider Auto Const
ActorBase Property CheatTerminal_LvlSMBehemoth Auto Const
ActorBase Property CheatTerminal_DN131_LvlSMBehemothSwan Auto Const
