;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAltWorldManipMerchSynth Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, WolfgangREF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, InstituteSynthFoodVendorREF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, InstituteSynthMiscVendorRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, TakahashiREF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, TinkerTomREF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, CarlaREF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, TrudyREF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, VadimBobrovREF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, WaitronRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, WellinghamREF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_22
Function Fragment_Terminal_22(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, WhitechapelCharlieREF)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property InstituteSynthFoodVendorREF Auto Const
ObjectReference Property InstituteSynthMiscVendorRef Auto Const
ObjectReference Property TakahashiREF Auto Const
ObjectReference Property TinkerTomREF Auto Const
ObjectReference Property CarlaREF Auto Const
ObjectReference Property TrudyREF Auto Const
ObjectReference Property VadimBobrovREF Auto Const
ObjectReference Property WaitronRef Auto Const
ObjectReference Property WellinghamREF Auto Const
ObjectReference Property WhitechapelCharlieREF Auto Const
ObjectReference Property WolfgangREF Auto Const
