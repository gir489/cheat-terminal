;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:SpecialsAndPerksPerksPerception Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Pickpocket01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Pickpocket02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Pickpocket03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Pickpocket01)
Game.GetPlayer().RemovePerk(Pickpocket02)
Game.GetPlayer().RemovePerk(Pickpocket03)
Game.GetPlayer().RemovePerk(Pickpocket04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Rifleman01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Rifleman02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Rifleman03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Rifleman04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Rifleman05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Rifleman01)
Game.GetPlayer().RemovePerk(Rifleman02)
Game.GetPlayer().RemovePerk(Rifleman03)
Game.GetPlayer().RemovePerk(Rifleman04)
Game.GetPlayer().RemovePerk(Rifleman05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Awareness)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Awareness)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Locksmith01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Locksmith02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Locksmith03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Locksmith04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Pickpocket04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Locksmith01)
Game.GetPlayer().RemovePerk(Locksmith02)
Game.GetPlayer().RemovePerk(Locksmith03)
Game.GetPlayer().RemovePerk(Locksmith04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(DemolitionExpert01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(DemolitionExpert02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(DemolitionExpert03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_22
Function Fragment_Terminal_22(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(DemolitionExpert04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_23
Function Fragment_Terminal_23(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(DemolitionExpert01)
Game.GetPlayer().RemovePerk(DemolitionExpert02)
Game.GetPlayer().RemovePerk(DemolitionExpert03)
Game.GetPlayer().RemovePerk(DemolitionExpert04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_24
Function Fragment_Terminal_24(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(NightPerson01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_25
Function Fragment_Terminal_25(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(NightPerson02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_26
Function Fragment_Terminal_26(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Refractor01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_27
Function Fragment_Terminal_27(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Refractor02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_28
Function Fragment_Terminal_28(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Refractor03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_29
Function Fragment_Terminal_29(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Refractor04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_30
Function Fragment_Terminal_30(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Refractor05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_31
Function Fragment_Terminal_31(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Refractor01)
Game.GetPlayer().RemovePerk(Refractor02)
Game.GetPlayer().RemovePerk(Refractor03)
Game.GetPlayer().RemovePerk(Refractor04)
Game.GetPlayer().RemovePerk(Refractor05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_32
Function Fragment_Terminal_32(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Sniper01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_33
Function Fragment_Terminal_33(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Sniper02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_34
Function Fragment_Terminal_34(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Sniper03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_35
Function Fragment_Terminal_35(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Sniper01)
Game.GetPlayer().RemovePerk(Sniper02)
Game.GetPlayer().RemovePerk(Sniper03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_36
Function Fragment_Terminal_36(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Penetrator01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_37
Function Fragment_Terminal_37(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Penetrator02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_38
Function Fragment_Terminal_38(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Penetrator01)
Game.GetPlayer().RemovePerk(Penetrator02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_39
Function Fragment_Terminal_39(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(ConcentratedFire01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_40
Function Fragment_Terminal_40(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(ConcentratedFire02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_41
Function Fragment_Terminal_41(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(ConcentratedFire03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_42
Function Fragment_Terminal_42(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(ConcentratedFire01)
Game.GetPlayer().RemovePerk(ConcentratedFire02)
Game.GetPlayer().RemovePerk(ConcentratedFire03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_43
Function Fragment_Terminal_43(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(NightPerson01)
Game.GetPlayer().RemovePerk(NightPerson02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_44
Function Fragment_Terminal_44(ObjectReference akTerminalRef)
;BEGIN CODE
givePerk(Pickpocket01)
givePerk(Pickpocket02)
givePerk(Pickpocket03)
givePerk(Pickpocket04)
givePerk(Rifleman01)
givePerk(Rifleman02)
givePerk(Rifleman03)
givePerk(Rifleman04)
givePerk(Rifleman05)
givePerk(Awareness)
givePerk(Locksmith01)
givePerk(Locksmith02)
givePerk(Locksmith03)
givePerk(Locksmith04)
givePerk(DemolitionExpert01)
givePerk(DemolitionExpert02)
givePerk(DemolitionExpert03)
givePerk(DemolitionExpert04)
givePerk(NightPerson01)
givePerk(NightPerson02)
givePerk(Refractor01)
givePerk(Refractor02)
givePerk(Refractor03)
givePerk(Refractor04)
givePerk(Refractor05)
givePerk(Sniper01)
givePerk(Sniper02)
givePerk(Sniper03)
givePerk(Penetrator01)
givePerk(Penetrator02)
givePerk(ConcentratedFire01)
givePerk(ConcentratedFire02)
givePerk(ConcentratedFire03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_45
Function Fragment_Terminal_45(ObjectReference akTerminalRef)
;BEGIN CODE
removePerk(Pickpocket01)
removePerk(Pickpocket02)
removePerk(Pickpocket03)
removePerk(Pickpocket04)
removePerk(Rifleman01)
removePerk(Rifleman02)
removePerk(Rifleman03)
removePerk(Rifleman04)
removePerk(Rifleman05)
removePerk(Awareness)
removePerk(Locksmith01)
removePerk(Locksmith02)
removePerk(Locksmith03)
removePerk(Locksmith04)
removePerk(DemolitionExpert01)
removePerk(DemolitionExpert02)
removePerk(DemolitionExpert03)
removePerk(DemolitionExpert04)
removePerk(NightPerson01)
removePerk(NightPerson02)
removePerk(Refractor01)
removePerk(Refractor02)
removePerk(Refractor03)
removePerk(Refractor04)
removePerk(Refractor05)
removePerk(Sniper01)
removePerk(Sniper02)
removePerk(Sniper03)
removePerk(Penetrator01)
removePerk(Penetrator02)
removePerk(ConcentratedFire01)
removePerk(ConcentratedFire02)
removePerk(ConcentratedFire03)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Function givePerk(Perk perkToGive)
	Actor playerActor = Game.GetPlayer()
	if (playerActor.HasPerk(perkToGive) == false)
		playerActor.AddPerk(perkToGive)
	endIf
endFunction
Function removePerk(Perk perkToGive)
	Actor playerActor = Game.GetPlayer()
	if (playerActor.HasPerk(perkToGive))
		playerActor.RemovePerk(perkToGive)
	endIf
endFunction
Perk Property Pickpocket01 Auto Const
Perk Property Pickpocket02 Auto Const
Perk Property Pickpocket03 Auto Const
Perk Property Pickpocket04 Auto Const
Perk Property Rifleman01 Auto Const
Perk Property Rifleman02 Auto Const
Perk Property Rifleman03 Auto Const
Perk Property Rifleman04 Auto Const
Perk Property Rifleman05 Auto Const
Perk Property Awareness Auto Const
Perk Property Locksmith01 Auto Const
Perk Property Locksmith02 Auto Const
Perk Property Locksmith03 Auto Const
Perk Property Locksmith04 Auto Const
Perk Property DemolitionExpert01 Auto Const
Perk Property DemolitionExpert02 Auto Const
Perk Property DemolitionExpert03 Auto Const
Perk Property DemolitionExpert04 Auto Const
Perk Property NightPerson01 Auto Const
Perk Property NightPerson02 Auto Const
Perk Property Refractor01 Auto Const
Perk Property Refractor02 Auto Const
Perk Property Refractor03 Auto Const
Perk Property Refractor04 Auto Const
Perk Property Refractor05 Auto Const
Perk Property Sniper01 Auto Const
Perk Property Sniper02 Auto Const
Perk Property Sniper03 Auto Const
Perk Property Penetrator01 Auto Const
Perk Property Penetrator02 Auto Const
Perk Property ConcentratedFire01 Auto Const
Perk Property ConcentratedFire02 Auto Const
Perk Property ConcentratedFire03 Auto Const
