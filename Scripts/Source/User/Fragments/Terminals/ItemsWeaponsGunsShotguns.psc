;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:ItemsWeaponsGunsShotguns Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_CombatShotgun_Rifle_SemiAuto)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_CombatShotgun_Rifle_Auto)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(Aspiration_Weapon_Covenant_Shotgun)
weaponRef.AttachMod(mod_CombatShotgun_Receiver_MoreDamage2)
weaponRef.AttachMod(mod_CombatShotgun_GripShotgun_StockMarksmans)
weaponRef.AttachMod(mod_CombatShotgun_Muzzle_Compensator)
weaponRef.AttachMod(mod_Legendary_Weapon_ChanceToStagger)
weaponRef.AttachMod(mod_Aspiration_Weapon_IncreasedCost_10)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_TerribleShotgun,CustomItemMods_TerribleShotgun)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_DoubleBarrelShotgun)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_CombatShotgun_ShortRifle_SemiAuto)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
LeveledItem Property LL_CombatShotgun_Rifle_Auto Auto Const
LeveledItem Property LL_CombatShotgun_Rifle_SemiAuto Auto Const
LeveledItem Property LL_CombatShotgun_ShortRifle_SemiAuto Auto Const
LeveledItem Property LL_DoubleBarrelShotgun Auto Const
LeveledItem Property Aspiration_Weapon_Covenant_Shotgun Auto Const
ObjectMod Property mod_CombatShotgun_Receiver_MoreDamage2 Auto Const
ObjectMod Property mod_CombatShotgun_GripShotgun_StockMarksmans Auto Const
ObjectMod Property mod_CombatShotgun_Muzzle_Compensator Auto Const
ObjectMod Property mod_Legendary_Weapon_ChanceToStagger Auto Const
ObjectMod Property mod_Aspiration_Weapon_IncreasedCost_10 Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_TerribleShotgun Auto Const
FormList Property CustomItemMods_TerribleShotgun Auto Const
GlobalVariable Property LL_EpicChance_Standard Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
