;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_FarHa_0100266F Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00054072, "DLCCoast.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive, CheatTerminal_ThrowablesToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x000540ED, "DLCCoast.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive, CheatTerminal_ThrowablesToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x000540F7, "DLCCoast.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive, CheatTerminal_ThrowablesToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x0005406E, "DLCCoast.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive, CheatTerminal_ThrowablesToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

GlobalVariable Property CheatTerminal_ThrowablesToGive Auto Const
