;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:QuestsFixMinutemenRadiant Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
WorkshopSynthInfiltrator02.CompleteAllObjectives()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
WorkShopSynthInfiltrator01.CompleteQuest()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Actor nearestSettler = Game.FindClosestReferenceOfTypeFromRef(WorkshopNPC, Game.GetPlayer(), 400) as Actor
Actor nearestFaction = Game.FindClosestReferenceOfTypeFromRef(WorkshopAttackDialogueFaction, Game.GetPlayer(), 400) as Actor
if ( nearestSettler != None && nearestSettler.IsInFaction(WorkshopAttackDialogueFaction) )
	nearestSettler.RemoveFromFaction(WorkshopAttackDialogueFaction)
elseif (nearestFaction != None )
	nearestFaction.RemoveFromFaction(WorkshopAttackDialogueFaction)
else
	CheatTerminal_FailedToFindRandomSettler.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
MinRecruit04.SetStage(200)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
MinRecruit04.SetStage(100)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference roger = Game.GetFormFromFile(0x00048BA9, "Fallout4.esm") as ObjectReference
roger.Disable()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
MinRecruit01.SetStage(100)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
MinRecruit01.SetStage(200)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
MinRadiantOwned02.SetStage(200)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
MinRecruit06.SetStage(450)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Quest Property MinRecruit07 Auto Const
Quest Property MinRecruit04 Auto Const
Quest Property MinRecruit01 Auto Const
Quest Property MinRadiantOwned02 Auto Const
Quest Property MinRecruit06 Auto Const
Quest Property WorkshopSynthInfiltrator02 Auto Const
Quest Property WorkshopSynthInfiltrator01 Auto Const
ActorBase Property WorkshopNPC Auto Const
Faction Property WorkshopAttackDialogueFaction Auto Const
Message Property CheatTerminal_FailedToFindRandomSettler Auto Const
