;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01001739 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_Armor_Power_T60_ArmLeft_A)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureJetpack, abForcePersist = true, abDeleteWhenAble = false)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_Armor_Power_T60_ArmRight_A)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_Armor_Power_T60_LegLeft_A)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_Armor_Power_T60_Torso_A)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_Armor_Power_T60_LegRight_A)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_Armor_Power_T60_Helmet_A)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureT60, abForcePersist = true, abDeleteWhenAble = false)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureT60_Military, abForcePersist = true, abDeleteWhenAble = false)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

LeveledItem Property LL_Armor_Power_T60_Helmet_A Auto Const
LeveledItem Property LL_Armor_Power_T60_ArmLeft_A Auto Const
LeveledItem Property LL_Armor_Power_T60_ArmRight_A Auto Const
LeveledItem Property LL_Armor_Power_T60_Torso_A Auto Const
LeveledItem Property LL_Armor_Power_T60_LegLeft_A Auto Const
LeveledItem Property LL_Armor_Power_T60_LegRight_A Auto Const

Furniture Property PowerArmorFurnitureT60 Auto Const

Furniture Property PowerArmorFurnitureJetpack Auto Const

Furniture Property PowerArmorFurnitureT60_Military Auto Const
