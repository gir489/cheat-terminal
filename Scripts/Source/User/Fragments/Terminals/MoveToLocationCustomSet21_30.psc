;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:MoveToLocationCustomSet21_30 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Marker25Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Marker26Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Marker27Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Marker28Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Marker21Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Marker22Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Marker23Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Marker24Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Marker29Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Marker30Ref.MoveTo(Game.GetPlayer() as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectReference Property Marker21Ref Auto Const
ObjectReference Property Marker22Ref Auto Const
ObjectReference Property Marker23Ref Auto Const
ObjectReference Property Marker24Ref Auto Const
ObjectReference Property Marker25Ref Auto Const
ObjectReference Property Marker26Ref Auto Const
ObjectReference Property Marker27Ref Auto Const
ObjectReference Property Marker28Ref Auto Const
ObjectReference Property Marker29Ref Auto Const
ObjectReference Property Marker30Ref Auto Const
