;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_9mm_W_010028A8 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0000823E, "9mmpistol.esp")
if ( formFromMod )
	Game.GetPlayer().AddItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0000823F, "9mmpistol.esp")
if ( formFromMod )
	Game.GetPlayer().AddItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000F99, "9mmpistol.esp")
if ( formFromMod )
	Game.GetPlayer().AddItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
CheatTerminal_9mm_gir489.Revert()
CheatTerminal_9mm_gir489.AddForm(Game.GetFormFromFile(0x00000F99, "9mmPistol.esp"), 1, 1)
ObjectReference weaponRef = playerActor.PlaceAtMe(CheatTerminal_9mm_gir489)
weaponRef.AttachMod(Game.GetFormFromFile(0x00006383,"9mmPistol.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x000035B8,"9mmPistol.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000636E,"9mmPistol.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000636C,"9mmPistol.esp") as ObjectMod)
weaponRef.AttachMod(mod_Null_Muzzle)
weaponRef.AttachMod(Game.GetFormFromFile(0x000089E8,"9mmPistol.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x000044FE,"9mmPistol.esp") as ObjectMod)
weaponRef.AttachMod(CheatTerminal_NoRecoilRapidFire)
weaponRef.AttachMod(mod_Legendary_Weapon_DamageLimbRecoil)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00008240, "9mmpistol.esp")
if ( formFromMod )
	Game.GetPlayer().AddItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00008241, "9mmpistol.esp")
if ( formFromMod )
	Game.GetPlayer().AddItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00008244, "9mmpistol.esp")
if ( formFromMod )
	Game.GetPlayer().AddItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

GlobalVariable Property LL_EpicChance_Standard Auto Const
LeveledItem Property CheatTerminal_9mm_gir489 Auto Const
ObjectMod Property mod_Null_Muzzle Auto Const
ObjectMod Property CheatTerminal_NoRecoilRapidFire Auto Const
ObjectMod Property mod_Legendary_Weapon_DamageLimbRecoil Auto Const
