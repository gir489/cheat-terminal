;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Mele_0100A8FB Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_RollingPin_Material_Wood)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_RollingPin_Material_Wood)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_RollingPin_Material_Aluminium)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_RollingPin_Material_Aluminium)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_RollingPin_Blades)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_RollingPin_Blades)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_RollingPin_Spikes)
else
	Game.GetPlayer().AddItem(miscmod_mod_RollingPin_Spikes)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_melee_RollingPin_Material_Wood Auto Const
ObjectMod Property mod_melee_RollingPin_Material_Aluminium Auto Const
ObjectMod Property mod_melee_RollingPin_Blades Auto Const
ObjectMod Property mod_melee_RollingPin_Spikes Auto Const
MiscObject Property miscmod_mod_melee_RollingPin_Material_Wood Auto Const
MiscObject Property miscmod_mod_melee_RollingPin_Material_Aluminium Auto Const
MiscObject Property miscmod_mod_melee_RollingPin_Blades Auto Const
MiscObject Property miscmod_mod_RollingPin_Spikes Auto Const
