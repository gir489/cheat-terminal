;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:NukaWorldItemsMiscQuest Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0001E34C, "DLCNukaWorld.esm")
if formFromMod
	Quest StarCoreQuest = formFromMod as Quest
	StarCoreQuest.SetStage(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0001B167, "DLCNukaWorld.esm")
if formFromMod
	ObjectReference formula = formFromMod as ObjectReference
	Game.GetPlayer().AddItem(formula)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00043B93, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00056CF9, "DLCNukaWorld.esm")
if formFromMod
	ObjectReference schematicsRef = Game.GetPlayer().PlaceAtMe(formFromMod)
	schematicsRef.Activate(Game.GetPlayer())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0003AC61, "DLCNukaWorld.esm")
if formFromMod
	Quest medalionQuest = formFromMod as Quest
	if (medalionQuest.IsStageDone(110) == false )
		medalionQuest.SetStage(110)
	endIf
	if (medalionQuest.IsStageDone(120) == false )
		medalionQuest.SetStage(120)
	endIf
	if (medalionQuest.IsStageDone(130) == false )
		medalionQuest.SetStage(130)
	endIf
	if (medalionQuest.IsStageDone(140) == false )
		medalionQuest.SetStage(140)
	endIf
	if (medalionQuest.IsStageDone(150) == false )
		medalionQuest.SetStage(150)
	endIf
	if (medalionQuest.IsStageDone(160) == false )
		medalionQuest.SetStage(160)
	endIf
	if (medalionQuest.IsStageDone(170) == false )
		medalionQuest.SetStage(170)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00022DC0, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00056CFE, "DLCNukaWorld.esm")
if formFromMod
	ObjectReference schematicsRef = Game.GetPlayer().PlaceAtMe(formFromMod)
	schematicsRef.Activate(Game.GetPlayer())
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
