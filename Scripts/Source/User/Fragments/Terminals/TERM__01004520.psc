;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01004520 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x00046024, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0004262B, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0004B9B3, "DLCCoast.esm")
if objectToSpawn
	Actor playerActor = Game.GetPlayer()
	ObjectReference objRef = playerActor.PlaceAtMe(objectToSpawn)
	objRef.AttachMod(mod_Legendary_Armor_Speed)
	Game.GetPlayer().AddItem(objRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x00045677, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0000914C, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0005159C, "DLCCoast.esm")
if objectToSpawn
	Actor playerActor = Game.GetPlayer()
	ObjectReference objRef = playerActor.PlaceAtMe(objectToSpawn)
	objRef.AttachMod(mod_Legendary_Armor_LessDMGRobots)
	Game.GetPlayer().AddItem(objRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x000391E8, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x000540FC, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x000247C5, "DLCCoast.esm")
if objectToSpawn
	Actor playerActor = Game.GetPlayer()
	ObjectReference objRef = playerActor.PlaceAtMe(objectToSpawn)
	objRef.AttachMod(Game.GetFormFromFile(0x0005D372, "DLCCoast.esm") as ObjectMod)
	Game.GetPlayer().AddItem(objRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0004FA89, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00056F7D, "DLCCoast.esm")
if objectToGive
	Game.GetPlayer().AddItem(objectToGive)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ObjectMod Property mod_Legendary_Armor_Speed Auto Const

ObjectMod Property mod_Legendary_Armor_LessDMGRobots Auto Const
