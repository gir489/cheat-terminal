;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:M60 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00001F2D, "M60.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00002E86, "M60.esp"), CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000088DC, "M60.esp")
If ( formFromMod )
	CheatTerminalGlobalFuncs.SpawnWorkshopItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
CheatTerminal_M60_gir489.Revert()
CheatTerminal_M60_gir489.AddForm(Game.GetFormFromFile(0x00001F2D, "M60.esp"), 1, 1)
ObjectReference weaponRef = playerActor.PlaceAtMe(CheatTerminal_M60_gir489)
weaponRef.AttachMod(Game.GetFormFromFile(0x00000FE7,"M60.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00000FD2,"M60.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0000C5BB,"M60.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00006A5F,"M60.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00000FEC,"M60.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00000FE1,"M60.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00000FDC,"M60.esp") as ObjectMod)
weaponRef.AttachMod(CheatTerminal_NoRecoil)
weaponRef.AttachMod(mod_Legendary_Weapon_DamageLimbRecoil)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const

GlobalVariable Property CheatTerminal_ThrowablesToGive Auto Const

ObjectMod Property CheatTerminal_NoRecoil Auto Const

ObjectMod Property mod_Legendary_Weapon_DamageLimbRecoil Auto Const

LeveledItem Property CheatTerminal_M60_gir489 Auto Const
