;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Weapons_E_010044DD Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(Aspiration_Weapon_Institute_AutoPlasmaGun)
weaponRef.AttachMod(Rapid)
weaponRef.AttachMod(mod_PlasmaGun_PlasmaReceiver_MoreDamage1_and_BetterCriticals2)
weaponRef.AttachMod(mod_PlasmaGun_BarrelPlasma_Spin_B)
weaponRef.AttachMod(mod_PlasmaGun_Grip_Stock)
weaponRef.AttachMod(mod_Aspiration_Weapon_IncreasedCost_10)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_BoSPlasmaRifle, CustomItemMods_BoSPlasmaRifle)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference oItem = Game.GetPlayer().PlaceAtMe(Aspiration_Weapon_BoS_PlasmaGun)
oItem.AttachMod(mod_Legendary_Weapon_DamageVsSupermutants)
oItem.AttachMod(mod_PlasmaGun_PlasmaReceiver_MoreDamage2)
oItem.AttachMod(mod_PlasmaGun_BarrelPlasma_Sniper_B)
oItem.AttachMod(mod_PlasmaGun_Scope_ScopeLong)
oItem.AttachMod(mod_Aspiration_Weapon_IncreasedCost_10)
Game.GetPlayer().AddItem(oItem)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_BoSPlasmaRifle Auto Const
FormList Property CustomItemMods_BoSPlasmaRifle Auto Const
LeveledItem Property Aspiration_Weapon_Institute_AutoPlasmaGun Auto Const
ObjectMod Property Rapid Auto Const
ObjectMod Property mod_PlasmaGun_PlasmaReceiver_MoreDamage1_and_BetterCriticals2 Auto Const
ObjectMod Property mod_PlasmaGun_BarrelPlasma_Spin_B Auto Const
ObjectMod Property mod_PlasmaGun_Grip_Stock Auto Const
LeveledItem Property Aspiration_Weapon_BoS_PlasmaGun Auto Const
ObjectMod Property mod_Legendary_Weapon_DamageVsSupermutants Auto Const
ObjectMod Property mod_PlasmaGun_PlasmaReceiver_MoreDamage2 Auto Const
ObjectMod Property mod_PlasmaGun_BarrelPlasma_Sniper_B Auto Const
ObjectMod Property mod_PlasmaGun_Scope_ScopeLong Auto Const
ObjectMod Property mod_Aspiration_Weapon_IncreasedCost_10 Auto Const
