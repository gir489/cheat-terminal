;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Weapons_G_01002E04 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_44_Pistol_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_10mm_Pistol_SemiAuto)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Deliverer)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_MS07cEddiesPeace, CustomItemMods_MS07cEddiesPeace)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(FlareGun)
Game.GetPlayer().AddItem(LL_Ammo_Flare)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_KelloggsGun, CustomItemMods_KelloggsGun)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_10mm_Pistol_Auto)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_TheGainer, CustomItemMods_TheGainer)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(Aspiration_Weapon_BunkerHill_10mm)
weaponRef.AttachMod(mod_Legendary_Weapon_DamageLimb)
weaponRef.AttachMod(mod_10mm_Receiver_MoreDamage2_and_BetterCriticals)
weaponRef.AttachMod(mod_10mm_Grip_Better2)
weaponRef.AttachMod(mod_10mm_Mag_Large)
weaponRef.AttachMod(mod_10mm_Scope_ScopeRecon)
weaponRef.AttachMod(mod_Aspiration_Weapon_IncreasedCost_10)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
LeveledItem Property LL_44_Pistol_Simple Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_MS07cEddiesPeace Auto Const
FormList Property CustomItemMods_MS07cEddiesPeace Auto Const
ObjectMod Property mod_Legendary_Weapon_DamageLimb Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_KelloggsGun Auto Const
FormList Property CustomItemMods_KelloggsGun Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_TheGainer Auto Const
FormList Property CustomItemMods_TheGainer Auto Const
LeveledItem Property LL_10mm_Pistol_SemiAuto Auto Const
LeveledItem Property LL_10mm_Pistol_Auto Auto Const
LeveledItem Property Aspiration_Weapon_BunkerHill_10mm Auto Const
ObjectMod Property mod_10mm_Receiver_MoreDamage2_and_BetterCriticals Auto Const
ObjectMod Property mod_10mm_Grip_Better2 Auto Const
ObjectMod Property mod_10mm_Mag_Large Auto Const
ObjectMod Property mod_10mm_Scope_ScopeRecon Auto Const
ObjectMod Property mod_Aspiration_Weapon_IncreasedCost_10 Auto Const
LeveledItem Property LL_Deliverer Auto Const
Weapon Property FlareGun Auto Const
LeveledItem Property LL_Ammo_Flare Auto Const
GlobalVariable Property LL_EpicChance_Standard Auto Const
