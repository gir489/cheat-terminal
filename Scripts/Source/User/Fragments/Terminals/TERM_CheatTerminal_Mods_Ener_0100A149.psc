;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Ener_0100A149 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GammaGun_BarrelDish_Long)
else
	Game.GetPlayer().AddItem(miscmod_mod_GammaGun_BarrelDish_Long)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GammaGun_BarrelDish_MS09JackRadiation)
else
	Game.GetPlayer().AddItem(miscmod_mod_GammaGun_BarrelDish_MS09JackRadiation)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GammaGun_BarrelDish_Short)
else
	Game.GetPlayer().AddItem(miscmod_mod_GammaGun_BarrelDish_Short)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GammaGun_BarrelDish_MS09Push)
else
	Game.GetPlayer().AddItem(miscmod_mod_GammaGun_BarrelDish_MS09Push)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_GammaGun_BarrelDish_MS09JackRadiation Auto Const
ObjectMod Property mod_GammaGun_BarrelDish_Short Auto Const
ObjectMod Property mod_GammaGun_BarrelDish_Long Auto Const
ObjectMod Property mod_GammaGun_BarrelDish_MS09Push Auto Const
MiscObject Property miscmod_mod_GammaGun_BarrelDish_MS09JackRadiation Auto Const
MiscObject Property miscmod_mod_GammaGun_BarrelDish_Short Auto Const
MiscObject Property miscmod_mod_GammaGun_BarrelDish_Long Auto Const
MiscObject Property miscmod_mod_GammaGun_BarrelDish_MS09Push Auto Const
