;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Config_Ch_010089EA Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ChemVar.SetValue(10)
If ( Game.GetPlayer().HasPerk(ChemPerk) )
	Game.GetPlayer().RemovePerk(ChemPerk)
	Game.GetPlayer().AddPerk(ChemPerk, false)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ChemVar.SetValue(15)
If ( Game.GetPlayer().HasPerk(ChemPerk) )
	Game.GetPlayer().RemovePerk(ChemPerk)
	Game.GetPlayer().AddPerk(ChemPerk, false)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ChemVar.SetValue(100)
If ( Game.GetPlayer().HasPerk(ChemPerk) )
	Game.GetPlayer().RemovePerk(ChemPerk)
	Game.GetPlayer().AddPerk(ChemPerk, false)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ChemVar.SetValue(20)
If ( Game.GetPlayer().HasPerk(ChemPerk) )
	Game.GetPlayer().RemovePerk(ChemPerk)
	Game.GetPlayer().AddPerk(ChemPerk, false)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ChemVar.SetValue(50)
If ( Game.GetPlayer().HasPerk(ChemPerk) )
	Game.GetPlayer().RemovePerk(ChemPerk)
	Game.GetPlayer().AddPerk(ChemPerk, false)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ChemVar.SetValue(5)
If ( Game.GetPlayer().HasPerk(ChemPerk) )
	Game.GetPlayer().RemovePerk(ChemPerk)
	Game.GetPlayer().AddPerk(ChemPerk, false)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Perk Property ChemPerk Auto Const

GlobalVariable Property ChemVar Auto Const
