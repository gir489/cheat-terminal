;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01007A8D Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Barrel_VeryShort)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Barrel_Standard)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Barrel_Short)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Barrel_Short)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Barrel_LightShort)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Barrel_LightLong)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Barrel_PortedShort)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Barrel_PortedLong)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Barrel_LightPortedShort)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Barrel_LightLongPorted)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ObjectMod Property mod_10mm_Barrel_VeryShort Auto Const
ObjectMod Property mod_10mm_Barrel_Short Auto Const
ObjectMod Property mod_10mm_Barrel_LightShort Auto Const
ObjectMod Property mod_10mm_Barrel_PortedShort Auto Const
ObjectMod Property mod_10mm_Barrel_LightPortedShort Auto Const
MiscObject Property miscmod_mod_10mm_Barrel_Standard Auto Const
MiscObject Property miscmod_mod_10mm_Barrel_Short Auto Const
MiscObject Property miscmod_mod_10mm_Barrel_LightLong Auto Const
MiscObject Property miscmod_mod_10mm_Barrel_PortedLong Auto Const
MiscObject Property miscmod_mod_10mm_Barrel_LightLongPorted Auto Const
