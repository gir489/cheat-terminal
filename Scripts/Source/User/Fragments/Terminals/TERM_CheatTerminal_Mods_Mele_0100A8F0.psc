;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Mele_0100A8F0 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_Board_BladesLarge)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_Board_BladesLarge)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_Board_Spikes)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_Board_Spikes)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_Board_SpikesLarge)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_Board_SpikesLarge)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_melee_Board_Spikes Auto Const
ObjectMod Property mod_melee_Board_SpikesLarge Auto Const
ObjectMod Property mod_melee_Board_BladesLarge Auto Const
MiscObject Property miscmod_mod_melee_Board_Spikes Auto Const
MiscObject Property miscmod_mod_melee_Board_SpikesLarge Auto Const
MiscObject Property miscmod_mod_melee_Board_BladesLarge Auto Const
