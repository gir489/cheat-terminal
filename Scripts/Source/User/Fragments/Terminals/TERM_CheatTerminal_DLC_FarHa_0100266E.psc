;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_FarHa_0100266E Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00020C4C, "DLCCoast.esm")
if objectToGive
	LL_EpicChance_Standard.SetValue(0.0)
	Game.GetPlayer().AddItem(objectToGive)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00031702, "DLCCoast.esm")
if objectToGive
	Actor playerActor = Game.GetPlayer()
	ObjectReference objectRef = playerActor.PlaceAtMe(objectToGive)
	ObjectMod attachment = Game.GetFormFromFile(0x0002740C, "DLCCoast.esm") as ObjectMod
	ObjectMod attachment2 = Game.GetFormFromFile(0x00056F2A, "DLCCoast.esm") as ObjectMod
	objectRef.AttachMod(attachment)
	objectRef.AttachMod(attachment2)
	playerActor.AddItem(objectRef)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x0002740E, "DLCCoast.esm"), 4)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004E752, "DLCCoast.esm")
if ( formFromMod)
	LeveledItem modLeveledItem = formFromMod as LeveledItem
	FormList modFormList = Game.GetFormFromFile(0x0004E753, "DLCCoast.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(modLeveledItem, modFormList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00051599, "DLCCoast.esm")
if ( formFromMod)
	LeveledItem modLeveledItem = formFromMod as LeveledItem
	FormList modFormList = Game.GetFormFromFile(0x0005159A, "DLCCoast.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(modLeveledItem, modFormList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0005158B, "DLCCoast.esm")
if ( formFromMod)
	LeveledItem modLeveledItem = formFromMod as LeveledItem
	FormList modFormList = Game.GetFormFromFile(0x00051590, "DLCCoast.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(modLeveledItem, modFormList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00032507, "DLCCoast.esm")
if ( formFromMod)
	LeveledItem modLeveledItem = formFromMod as LeveledItem
	FormList modFormList = Game.GetFormFromFile(0x00032508, "DLCCoast.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(modLeveledItem, modFormList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
GlobalVariable Property LL_EpicChance_Standard Auto Const
