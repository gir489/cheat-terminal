;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Weap_0100BFF2 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Null_Muzzle as ObjectMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_DoubleBarrelShotgun_Muzzle_MuzzleBrake)
else
	Game.GetPlayer().AddItem(miscmod_mod_DoubleBarrelShotgun_Muzzle_MuzzleBrake)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_DoubleBarrelShotgun_Muzzle_MuzzleBrakeSpike)
else
	Game.GetPlayer().AddItem(miscmod_mod_DoubleBarrelShotgun_Muzzle_MuzzleBrakeSpike)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_DoubleBarrelShotgun_Muzzle_MuzzleBrake Auto Const
ObjectMod Property mod_DoubleBarrelShotgun_Muzzle_MuzzleBrakeSpike Auto Const
MiscObject Property miscmod_mod_DoubleBarrelShotgun_Muzzle_MuzzleBrake Auto Const
MiscObject Property miscmod_mod_DoubleBarrelShotgun_Muzzle_MuzzleBrakeSpike Auto Const

ObjectMod Property mod_Null_Muzzle Auto Const
