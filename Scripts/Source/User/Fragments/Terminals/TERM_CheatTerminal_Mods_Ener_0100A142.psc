;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Ener_0100A142 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PlasmaGun_BarrelPlasma_Spin_A)
else
	Game.GetPlayer().AddItem(miscmod_mod_PlasmaGun_Barrel_Spin_A)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PlasmaGun_BarrelPlasma_Short_A)
else
	Game.GetPlayer().AddItem(miscmod_mod_PlasmaGun_Barrel_Short_A)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PlasmaGun_BarrelPlasma_Shotgun_A)
else
	Game.GetPlayer().AddItem(miscmod_mod_PlasmaGun_Barrel_Shotgun_A)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PlasmaGun_BarrelPlasma_Sniper_A)
else
	Game.GetPlayer().AddItem(miscmod_mod_PlasmaGun_Barrel_Sniper_A)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PlasmaGun_BarrelPlasma_Short_B)
else
	Game.GetPlayer().AddItem(miscmod_mod_PlasmaGun_Barrel_Short_B)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PlasmaGun_BarrelPlasma_Flamer)
else
	Game.GetPlayer().AddItem(miscmod_mod_PlasmaGun_BarrelPlasma_Flamer)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PlasmaGun_BarrelPlasma_Spin_B)
else
	Game.GetPlayer().AddItem(miscmod_mod_PlasmaGun_Barrel_Spin_B)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PlasmaGun_BarrelPlasma_Sniper_B)
else
	Game.GetPlayer().AddItem(miscmod_mod_PlasmaGun_Barrel_Sniper_B)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_PlasmaGun_BarrelPlasma_Shotgun_B)
else
	Game.GetPlayer().AddItem(miscmod_mod_PlasmaGun_Barrel_Shotgun_B)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_PlasmaGun_BarrelPlasma_Short_A Auto Const
ObjectMod Property mod_PlasmaGun_BarrelPlasma_Shotgun_A Auto Const
ObjectMod Property mod_PlasmaGun_BarrelPlasma_Spin_A Auto Const
ObjectMod Property mod_PlasmaGun_BarrelPlasma_Sniper_A Auto Const
ObjectMod Property mod_PlasmaGun_BarrelPlasma_Flamer Auto Const
ObjectMod Property mod_PlasmaGun_BarrelPlasma_Short_B Auto Const
ObjectMod Property mod_PlasmaGun_BarrelPlasma_Shotgun_B Auto Const
ObjectMod Property mod_PlasmaGun_BarrelPlasma_Spin_B Auto Const
ObjectMod Property mod_PlasmaGun_BarrelPlasma_Sniper_B Auto Const
MiscObject Property miscmod_mod_PlasmaGun_Barrel_Short_A Auto Const
MiscObject Property miscmod_mod_PlasmaGun_Barrel_Shotgun_A Auto Const
MiscObject Property miscmod_mod_PlasmaGun_Barrel_Spin_A Auto Const
MiscObject Property miscmod_mod_PlasmaGun_Barrel_Sniper_A Auto Const
MiscObject Property miscmod_mod_PlasmaGun_BarrelPlasma_Flamer Auto Const
MiscObject Property miscmod_mod_PlasmaGun_Barrel_Short_B Auto Const
MiscObject Property miscmod_mod_PlasmaGun_Barrel_Shotgun_B Auto Const
MiscObject Property miscmod_mod_PlasmaGun_Barrel_Spin_B Auto Const
MiscObject Property miscmod_mod_PlasmaGun_Barrel_Sniper_B Auto Const
