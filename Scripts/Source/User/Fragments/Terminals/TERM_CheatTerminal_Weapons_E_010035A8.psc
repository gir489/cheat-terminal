;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Weapons_E_010035A8 Extends Terminal Hidden Const

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

LeveledItem Property LL_LaserGun_Shotgun_Rifle_SemiAuto Auto Const

LeveledItem Property LL_LaserGun_Rifle_SemiAuto Auto Const

LeveledItem Property LL_LaserGun_Rifle_Auto Auto Const

LeveledItem Property LL_LaserGun_Pistol_Auto Auto Const

LeveledItem Property LL_LaserGun_SniperRifle Auto Const
