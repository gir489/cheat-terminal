;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__0100C7A8 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureT60)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureMaxson)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureT60_Military)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureRaider)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureGavil)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureBrandis)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureT51)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureT45)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureT60_AtomCat)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureJetpack)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(BoSPowerArmorFurnitureKnightPlayer)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(BoSPowerArmorFurniturePaladin)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(BoSPowerArmorFurnitureElder)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(BoSPowerArmorFurnitureSentinel)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(BoSPowerArmorFurnitureKnightSgt)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(BoSPowerArmorFurnitureKnightCaptain)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureX01)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Furniture Property PowerArmorFurnitureT45 Auto Const

Furniture Property PowerArmorFurnitureT51 Auto Const
Furniture Property PowerArmorFurnitureT60 Auto Const
Furniture Property PowerArmorFurnitureT60_AtomCat Auto Const
Furniture Property PowerArmorFurnitureT60_Military Auto Const
Furniture Property PowerArmorFurnitureJetpack Auto Const
Furniture Property BoSPowerArmorFurnitureKnightPlayer Auto Const
Furniture Property BoSPowerArmorFurnitureKnightSgt Auto Const
Furniture Property BoSPowerArmorFurnitureKnightCaptain Auto Const
Furniture Property BoSPowerArmorFurniturePaladin Auto Const
Furniture Property BoSPowerArmorFurnitureSentinel Auto Const
Furniture Property BoSPowerArmorFurnitureElder Auto Const
Furniture Property PowerArmorFurnitureGavil Auto Const
Furniture Property PowerArmorFurnitureBrandis Auto Const
Furniture Property PowerArmorFurnitureMaxson Auto Const

GlobalVariable Property LL_EpicChance_Standard Auto Const

Furniture Property PowerArmorFurnitureRaider Auto Const

Furniture Property PowerArmorFurnitureX01 Auto Const
