;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsPlayerManipCarryWeight Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
if (Game.GetPlayer().HasPerk(CheatTerminal_FortifyCarryWeight))
	Game.GetPlayer().RemovePerk(CheatTerminal_FortifyCarryWeight)
endIf
CheatTerminal_GameAlterations_AddCarryWeightAmount.SetValue(0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminal_GameAlterations_AddCarryWeightAmount.SetValue(50)
if (Game.GetPlayer().HasPerk(CheatTerminal_FortifyCarryWeight) == false)
	Game.GetPlayer().AddPerk(CheatTerminal_FortifyCarryWeight)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminal_GameAlterations_AddCarryWeightAmount.SetValue(100)
if (Game.GetPlayer().HasPerk(CheatTerminal_FortifyCarryWeight) == false)
	Game.GetPlayer().AddPerk(CheatTerminal_FortifyCarryWeight)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminal_GameAlterations_AddCarryWeightAmount.SetValue(250)
if (Game.GetPlayer().HasPerk(CheatTerminal_FortifyCarryWeight) == false)
	Game.GetPlayer().AddPerk(CheatTerminal_FortifyCarryWeight)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminal_GameAlterations_AddCarryWeightAmount.SetValue(500)
if (Game.GetPlayer().HasPerk(CheatTerminal_FortifyCarryWeight) == false)
	Game.GetPlayer().AddPerk(CheatTerminal_FortifyCarryWeight)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminal_GameAlterations_AddCarryWeightAmount.SetValue(1000)
if (Game.GetPlayer().HasPerk(CheatTerminal_FortifyCarryWeight) == false)
	Game.GetPlayer().AddPerk(CheatTerminal_FortifyCarryWeight)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminal_GameAlterations_AddCarryWeightAmount.SetValue(1500)
if (Game.GetPlayer().HasPerk(CheatTerminal_FortifyCarryWeight) == false)
	Game.GetPlayer().AddPerk(CheatTerminal_FortifyCarryWeight)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminal_GameAlterations_AddCarryWeightAmount.SetValue(2000)
if (Game.GetPlayer().HasPerk(CheatTerminal_FortifyCarryWeight) == false)
	Game.GetPlayer().AddPerk(CheatTerminal_FortifyCarryWeight)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminal_GameAlterations_AddCarryWeightAmount.SetValue(3500)
if (Game.GetPlayer().HasPerk(CheatTerminal_FortifyCarryWeight) == false)
	Game.GetPlayer().AddPerk(CheatTerminal_FortifyCarryWeight)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminal_GameAlterations_AddCarryWeightAmount.SetValue(5000)
if (Game.GetPlayer().HasPerk(CheatTerminal_FortifyCarryWeight) == false)
	Game.GetPlayer().AddPerk(CheatTerminal_FortifyCarryWeight)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

GlobalVariable Property CheatTerminal_GameAlterations_AddCarryWeightAmount Auto Const

Perk Property CheatTerminal_FortifyCarryWeight Auto Const
