;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:AutomatronLocationsDebug Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0000112C, "DLCRobot.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, markerRef, -128, -1605.8362, -128)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.ReturnPlayerFromDebug(akTerminalRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x01002490, "DLCRobot.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, markerRef, -144.69, -338.32, -0.1, 0.01)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00008078, "DLCRobot.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayerToDebug(akTerminalRef, markerRef, -656.0542, 1295.1368, 0.0, 180)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
