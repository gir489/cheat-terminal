;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Config_Va_010089E8 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
VATSCrits.SetValue(10)
If ( Game.GetPlayer().HasPerk(VatsCritsPerk) )
	Game.GetPlayer().RemovePerk(VatsCritsPerk)
	Game.GetPlayer().AddPerk(VatsCritsPerk, false)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
VATSCrits.SetValue(15)
If ( Game.GetPlayer().HasPerk(VatsCritsPerk) )
	Game.GetPlayer().RemovePerk(VatsCritsPerk)
	Game.GetPlayer().AddPerk(VatsCritsPerk, false)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
VATSCrits.SetValue(9999)
If ( Game.GetPlayer().HasPerk(VatsCritsPerk) )
	Game.GetPlayer().RemovePerk(VatsCritsPerk)
	Game.GetPlayer().AddPerk(VatsCritsPerk, false)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
VATSCrits.SetValue(20)
If ( Game.GetPlayer().HasPerk(VatsCritsPerk) )
	Game.GetPlayer().RemovePerk(VatsCritsPerk)
	Game.GetPlayer().AddPerk(VatsCritsPerk, false)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
VATSCrits.SetValue(50)
If ( Game.GetPlayer().HasPerk(VatsCritsPerk) )
	Game.GetPlayer().RemovePerk(VatsCritsPerk)
	Game.GetPlayer().AddPerk(VatsCritsPerk, false)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
VATSCrits.SetValue(4)
If ( Game.GetPlayer().HasPerk(VatsCritsPerk) )
	Game.GetPlayer().RemovePerk(VatsCritsPerk)
	Game.GetPlayer().AddPerk(VatsCritsPerk, false)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

GlobalVariable Property VATSCrits Auto Const

Perk Property VatsCritsPerk Auto Const
