;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:SpecialsAndPerksPerksAgility Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Gunslinger01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Gunslinger02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Gunslinger03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Gunslinger04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Gunslinger05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Gunslinger01)
Game.GetPlayer().RemovePerk(Gunslinger02)
Game.GetPlayer().RemovePerk(Gunslinger03)
Game.GetPlayer().RemovePerk(Gunslinger04)
Game.GetPlayer().RemovePerk(Gunslinger05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Commando02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Commando01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Commando03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Commando04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Commando05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Commando01)
Game.GetPlayer().RemovePerk(Commando02)
Game.GetPlayer().RemovePerk(Commando03)
Game.GetPlayer().RemovePerk(Commando04)
Game.GetPlayer().RemovePerk(Commando05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Sneak01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Sneak02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Sneak03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Sneak04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Sneak05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Sneak01)
Game.GetPlayer().RemovePerk(Sneak02)
Game.GetPlayer().RemovePerk(Sneak03)
Game.GetPlayer().RemovePerk(Sneak04)
Game.GetPlayer().RemovePerk(Sneak05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(MisterSandman01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(MisterSandman02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(MisterSandman01)
Game.GetPlayer().RemovePerk(MisterSandman02)
Game.GetPlayer().RemovePerk(MisterSandman03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_22
Function Fragment_Terminal_22(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(MisterSandman03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_23
Function Fragment_Terminal_23(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(ActionBoy01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_24
Function Fragment_Terminal_24(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(ActionBoy02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_25
Function Fragment_Terminal_25(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(ActionBoy01)
Game.GetPlayer().RemovePerk(ActionBoy02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_26
Function Fragment_Terminal_26(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(ActionGirl01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_27
Function Fragment_Terminal_27(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(ActionGirl02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_28
Function Fragment_Terminal_28(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(ActionGirl01)
Game.GetPlayer().RemovePerk(ActionGirl02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_29
Function Fragment_Terminal_29(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(MovingTarget01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_30
Function Fragment_Terminal_30(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(MovingTarget02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_31
Function Fragment_Terminal_31(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(MovingTarget03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_32
Function Fragment_Terminal_32(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(MovingTarget01)
Game.GetPlayer().RemovePerk(MovingTarget02)
Game.GetPlayer().RemovePerk(MovingTarget03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_33
Function Fragment_Terminal_33(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Ninja01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_34
Function Fragment_Terminal_34(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Ninja02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_35
Function Fragment_Terminal_35(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Ninja03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_36
Function Fragment_Terminal_36(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Ninja01)
Game.GetPlayer().RemovePerk(Ninja02)
Game.GetPlayer().RemovePerk(Ninja03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_37
Function Fragment_Terminal_37(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(QuickHands01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_38
Function Fragment_Terminal_38(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(QuickHands02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_39
Function Fragment_Terminal_39(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(QuickHands01)
Game.GetPlayer().RemovePerk(QuickHands02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_40
Function Fragment_Terminal_40(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Blitz01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_41
Function Fragment_Terminal_41(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Blitz02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_42
Function Fragment_Terminal_42(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Blitz01)
Game.GetPlayer().RemovePerk(Blitz02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_43
Function Fragment_Terminal_43(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(GunFu01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_44
Function Fragment_Terminal_44(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(GunFu02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_45
Function Fragment_Terminal_45(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(GunFu03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_46
Function Fragment_Terminal_46(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(GunFu01)
Game.GetPlayer().RemovePerk(GunFu02)
Game.GetPlayer().RemovePerk(GunFu03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_47
Function Fragment_Terminal_47(ObjectReference akTerminalRef)
;BEGIN CODE
ActorBase PlayerBase = Game.GetPlayer().GetBaseObject() as ActorBase
bool bIsMale = PlayerBase.GetSex() == 0
givePerk(Gunslinger01)
givePerk(Gunslinger02)
givePerk(Gunslinger03)
givePerk(Gunslinger04)
givePerk(Gunslinger05)
givePerk(Commando01)
givePerk(Commando02)
givePerk(Commando03)
givePerk(Commando04)
givePerk(Commando05)
givePerk(Sneak01)
givePerk(Sneak02)
givePerk(Sneak03)
givePerk(Sneak04)
givePerk(Sneak05)
givePerk(MisterSandman01)
givePerk(MisterSandman02)
givePerk(MisterSandman03)
if ( bIsMale )
givePerk(ActionBoy01)
givePerk(ActionBoy02)
else
givePerk(ActionGirl01)
givePerk(ActionGirl02)
endif
givePerk(MovingTarget01)
givePerk(MovingTarget02)
givePerk(MovingTarget03)
givePerk(Ninja01)
givePerk(Ninja02)
givePerk(Ninja03)
givePerk(QuickHands01)
givePerk(QuickHands02)
givePerk(Blitz01)
givePerk(Blitz02)
givePerk(GunFu01)
givePerk(GunFu02)
givePerk(GunFu03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_48
Function Fragment_Terminal_48(ObjectReference akTerminalRef)
;BEGIN CODE
ActorBase PlayerBase = Game.GetPlayer().GetBaseObject() as ActorBase
bool bIsMale = PlayerBase.GetSex() == 0
removePerk(Gunslinger01)
removePerk(Gunslinger02)
removePerk(Gunslinger03)
removePerk(Gunslinger04)
removePerk(Gunslinger05)
removePerk(Commando01)
removePerk(Commando02)
removePerk(Commando03)
removePerk(Commando04)
removePerk(Commando05)
removePerk(Sneak01)
removePerk(Sneak02)
removePerk(Sneak03)
removePerk(Sneak04)
removePerk(Sneak05)
removePerk(MisterSandman01)
removePerk(MisterSandman02)
removePerk(MisterSandman03)
if ( bIsMale )
removePerk(ActionBoy01)
removePerk(ActionBoy02)
else
removePerk(ActionGirl01)
removePerk(ActionGirl02)
endif
removePerk(MovingTarget01)
removePerk(MovingTarget02)
removePerk(MovingTarget03)
removePerk(Ninja01)
removePerk(Ninja02)
removePerk(Ninja03)
removePerk(QuickHands01)
removePerk(QuickHands02)
removePerk(Blitz01)
removePerk(Blitz02)
removePerk(GunFu01)
removePerk(GunFu02)
removePerk(GunFu03)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Function givePerk(Perk perkToGive)
	Actor playerActor = Game.GetPlayer()
	if (playerActor.HasPerk(perkToGive) == false)
		playerActor.AddPerk(perkToGive)
	endIf
endFunction
Function removePerk(Perk perkToGive)
	Actor playerActor = Game.GetPlayer()
	if (playerActor.HasPerk(perkToGive))
		playerActor.RemovePerk(perkToGive)
	endIf
endFunction
Perk Property Gunslinger01 Auto Const
Perk Property Gunslinger02 Auto Const
Perk Property Gunslinger03 Auto Const
Perk Property Gunslinger04 Auto Const
Perk Property Gunslinger05 Auto Const
Perk Property Commando01 Auto Const
Perk Property Commando02 Auto Const
Perk Property Commando03 Auto Const
Perk Property Commando04 Auto Const
Perk Property Commando05 Auto Const
Perk Property Sneak01 Auto Const
Perk Property Sneak02 Auto Const
Perk Property Sneak03 Auto Const
Perk Property Sneak04 Auto Const
Perk Property Sneak05 Auto Const
Perk Property MisterSandman01 Auto Const
Perk Property MisterSandman02 Auto Const
Perk Property MisterSandman03 Auto Const
Perk Property ActionBoy01 Auto Const
Perk Property ActionBoy02 Auto Const
Perk Property ActionGirl01 Auto Const
Perk Property ActionGirl02 Auto Const
Perk Property MovingTarget01 Auto Const
Perk Property MovingTarget02 Auto Const
Perk Property MovingTarget03 Auto Const
Perk Property Ninja01 Auto Const
Perk Property Ninja02 Auto Const
Perk Property Ninja03 Auto Const
Perk Property QuickHands01 Auto Const
Perk Property QuickHands02 Auto Const
Perk Property Blitz01 Auto Const
Perk Property Blitz02 Auto Const
Perk Property GunFu01 Auto Const
Perk Property GunFu02 Auto Const
Perk Property GunFu03 Auto Const
