;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_NukaW_01001F67 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004A470, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0003DA27, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00044F09, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

GlobalVariable Property CheatTerminal_MiscToGive Auto Const
