;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Crafting_01007AB3 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Acid_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Adhesive_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Aluminum_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Antiseptic_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Asbestos_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_AntiBallisticFiber_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Bone_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Ceramic_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Glass_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Circuitry_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Cloth_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Concrete_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Copper_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Cork_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Crystal_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Fertilizer_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_FiberOptics_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Fiberglass_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Gears_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Gold_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Lead_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_22
Function Fragment_Terminal_22(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Leather_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_23
Function Fragment_Terminal_23(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_NuclearMaterial_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_24
Function Fragment_Terminal_24(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Oil_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_25
Function Fragment_Terminal_25(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Plastic_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_26
Function Fragment_Terminal_26(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Rubber_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_27
Function Fragment_Terminal_27(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Screws_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_28
Function Fragment_Terminal_28(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Silver_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_29
Function Fragment_Terminal_29(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Springs_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_30
Function Fragment_Terminal_30(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Steel_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_31
Function Fragment_Terminal_31(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Wood_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_32
Function Fragment_Terminal_32(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Acid_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Adhesive_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Aluminum_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Antiseptic_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Asbestos_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_AntiBallisticFiber_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Bone_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Ceramic_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Circuitry_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Cloth_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Concrete_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Copper_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Cork_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Crystal_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Fertilizer_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_FiberOptics_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Fiberglass_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Gears_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Glass_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Gold_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Lead_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Leather_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_NuclearMaterial_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Oil_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Plastic_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Rubber_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Screws_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Silver_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Springs_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Steel_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Wood_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_33
Function Fragment_Terminal_33(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(c_Acid_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Adhesive_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Aluminum_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Antiseptic_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Asbestos_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_AntiBallisticFiber_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Bone_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Ceramic_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Circuitry_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Cloth_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Concrete_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Copper_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Cork_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Crystal_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Fertilizer_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_FiberOptics_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Fiberglass_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Gears_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Glass_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Gold_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Lead_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Leather_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_NuclearMaterial_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Oil_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Plastic_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Rubber_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Screws_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Silver_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Springs_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Steel_scrap, CheatTerminal_CraftToGive.GetValueInt())
Game.GetPlayer().AddItem(c_Wood_scrap, CheatTerminal_CraftToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
MiscObject Property c_Acid_scrap Auto Const
MiscObject Property c_Adhesive_scrap Auto Const
MiscObject Property c_Aluminum_scrap Auto Const
MiscObject Property c_Antiseptic_scrap Auto Const
MiscObject Property c_Asbestos_scrap Auto Const
MiscObject Property c_AntiBallisticFiber_scrap Auto Const
MiscObject Property c_Bone_scrap Auto Const
MiscObject Property c_Ceramic_scrap Auto Const
MiscObject Property c_Circuitry_scrap Auto Const
MiscObject Property c_Cloth_scrap Auto Const
MiscObject Property c_Concrete_scrap Auto Const
MiscObject Property c_Copper_scrap Auto Const
MiscObject Property c_Cork_scrap Auto Const
MiscObject Property c_Crystal_scrap Auto Const
MiscObject Property c_Fertilizer_scrap Auto Const
MiscObject Property c_FiberOptics_scrap Auto Const
MiscObject Property c_Fiberglass_scrap Auto Const
MiscObject Property c_Gears_scrap Auto Const
MiscObject Property c_Glass_scrap Auto Const
MiscObject Property c_Gold_scrap Auto Const
MiscObject Property c_Lead_scrap Auto Const
MiscObject Property c_Leather_scrap Auto Const
MiscObject Property c_NuclearMaterial_scrap Auto Const
MiscObject Property c_Oil_scrap Auto Const
MiscObject Property c_Plastic_scrap Auto Const
MiscObject Property c_Rubber_scrap Auto Const
MiscObject Property c_Screws_scrap Auto Const
MiscObject Property c_Silver_scrap Auto Const
MiscObject Property c_Springs_scrap Auto Const
MiscObject Property c_Steel_scrap Auto Const
MiscObject Property c_Wood_scrap Auto Const
GlobalVariable Property CheatTerminal_CraftToGive Auto Const