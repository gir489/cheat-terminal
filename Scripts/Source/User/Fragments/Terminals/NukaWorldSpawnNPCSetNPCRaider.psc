;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:NukaWorldSpawnNPCSetNPCRaider Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActorFromDLC(0x0000DBD9, "DLCNukaWorld.esm")
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActorFromDLC(0x0000DBDB, "DLCNukaWorld.esm")
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActorFromDLC(0x0000DBDC, "DLCNukaWorld.esm")
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
