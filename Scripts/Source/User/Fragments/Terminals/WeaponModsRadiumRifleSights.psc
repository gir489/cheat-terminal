;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:WeaponModsRadiumRifleSights Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x0002602A,"DLCCoast.esm")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x0003EC61,"DLCCoast.esm")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00025FCE,"DLCCoast.esm")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x0003EC62,"DLCCoast.esm")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00025FCC,"DLCCoast.esm")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x0003EC60,"DLCCoast.esm")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00025FCF,"DLCCoast.esm")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x0003EC5F,"DLCCoast.esm")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x000407F4,"DLCCoast.esm")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00040801,"DLCCoast.esm")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x000407F2,"DLCCoast.esm")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00040803,"DLCCoast.esm")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x000407F7,"DLCCoast.esm")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x000407FE,"DLCCoast.esm")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x000407F5,"DLCCoast.esm")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00040800,"DLCCoast.esm")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x000407F3,"DLCCoast.esm")
if ( modToApply )
	Form miscMod = Game.GetFormFromFile(0x00040802,"DLCCoast.esm")
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	elseif ( miscMod ) 
		Game.GetPlayer().AddItem(miscMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
