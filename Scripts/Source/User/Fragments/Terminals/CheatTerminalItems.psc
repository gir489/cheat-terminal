;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:CheatTerminalItems Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
KeyStorageRef.Reset()
CheatTerminalGlobalFuncs.DoPotion(KeyPotion)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
if ( Game.GetPlayer().IsInPowerArmor() )
	CheatTerminal_ExitPowerarmor.Show()
else
	if ( CheatTerminal_ConfirmDeleteAllItems.Show() == 0 )
		Game.GetPlayer().AddItem(tenMM, abSilent=True)
		Game.GetPlayer().EquipItem(tenMM, abSilent  = True)
		Game.GetPlayer().UnequipItem(tenMM, abSilent  = True)
		ObjectReference portableTerminal = CheatTerminalPortable.GetRef()
		portableTerminal.Drop(true)
		Game.GetPlayer().RemoveAllItems()
		Game.GetPlayer().AddItem(Pipboy, abSilent=True)
		Game.GetPlayer().EquipItem(Pipboy, abSilent=True)
		Game.GetPlayer().AddItem(portableTerminal, abSilent = true)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
Potion Property KeyPotion Auto Const
ObjectReference Property KeyStorageRef Auto Const
Armor Property Pipboy Auto Const
ReferenceAlias Property CheatTerminalPortable Auto Const
Message Property CheatTerminal_ConfirmDeleteAllItems Auto Const
Message Property CheatTerminal_ExitPowerarmor Auto Const
Weapon Property tenmm Auto Const
