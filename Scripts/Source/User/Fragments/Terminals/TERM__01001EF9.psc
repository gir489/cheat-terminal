;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01001EF9 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GaussRifle_Muzzle_Compensator)
else
	Game.GetPlayer().AddItem(miscmod_mod_GaussRifle_Muzzle_Compensator)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GaussRifle_Muzzle_Suppressor)
else
	Game.GetPlayer().AddItem(miscmod_mod_GaussRifle_Muzzle_GaussRifleSuppressor)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_GaussRifle_Muzzle_Compensator Auto Const
ObjectMod Property mod_GaussRifle_Muzzle_Suppressor Auto Const
MiscObject Property miscmod_mod_GaussRifle_Muzzle_Compensator Auto Const
MiscObject Property miscmod_mod_GaussRifle_Muzzle_GaussRifleSuppressor Auto Const
