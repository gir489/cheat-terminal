;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:NukaWorldPerks Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000296C0, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(VANS) == false )
		playerActor.AddPerk(VANS)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_NukaWorld_HasVansPerk.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000296C0, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_NukaWorld_HasVansPerk.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00028A23, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(SteadyAim01) == false )
		playerActor.AddPerk(SteadyAim01)
	endIf
	if ( playerActor.HasPerk(SteadyAim02) == false )
		playerActor.AddPerk(SteadyAim02)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_NukaWorld_HasSteadyAimPerk.SetValue(1.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00028A23, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_NukaWorld_HasSteadyAimPerk.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000296BE, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(QuickHands01) == false )
		playerActor.AddPerk(QuickHands01)
	endIf
	if ( playerActor.HasPerk(QuickHands02) == false )
		playerActor.AddPerk(QuickHands02)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_NukaWorld_HasQuickHandsPerk.SetValue(1.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000296BE, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_NukaWorld_HasQuickHandsPerk.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00009C8B, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_NukaWorld_HasAceOperatorPerk.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00009C8B, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_NukaWorld_HasAceOperatorPerk.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00029B25, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(AttackDog01) == false )
		playerActor.AddPerk(AttackDog01)
	endIf
	if ( playerActor.HasPerk(AttackDog02) == false )
		playerActor.AddPerk(AttackDog02)
	endIf
	if ( playerActor.HasPerk(AttackDog03) == false )
		playerActor.AddPerk(AttackDog03)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_NukaWorld_HasAttackDog.SetValue(1.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00029B25, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_NukaWorld_HasAttackDog.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000277D0, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_NukaWorld_HasWW1.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000277D1, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_NukaWorld_HasWW2.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000277D2, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_NukaWorld_HasWW3.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000277D0, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	playerActor.RemovePerk(Game.GetFormFromFile(0x000277D1, "DLCNukaWorld.esm") as Perk)
	playerActor.RemovePerk(Game.GetFormFromFile(0x000277D2, "DLCNukaWorld.esm") as Perk)
	CheatTerminal_NukaWorld_HasWW1.SetValue(0)
	CheatTerminal_NukaWorld_HasWW2.SetValue(0)
	CheatTerminal_NukaWorld_HasWW3.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00028A22, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(Awareness) == false )
		playerActor.AddPerk(Awareness)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_NukaWorld_HasAwarenessPerk.SetValue(1.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00028A22, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_NukaWorld_HasAwarenessPerk.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00028A25, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(Ghoulish01) == false )
		playerActor.AddPerk(Ghoulish01)
	endIf
	if ( playerActor.HasPerk(Ghoulish02) == false )
		playerActor.AddPerk(Ghoulish02)
	endIf
	if ( playerActor.HasPerk(Ghoulish03) == false )
		playerActor.AddPerk(Ghoulish03)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_NukaWorld_HasGhoulish.SetValue(1.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00028A25, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_NukaWorld_HasGhoulish.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00029B26, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(MysteriousStranger01) == false )
		playerActor.AddPerk(MysteriousStranger01)
	endIf
	if ( playerActor.HasPerk(MysteriousStranger02) == false )
		playerActor.AddPerk(MysteriousStranger02)
	endIf
	if ( playerActor.HasPerk(MysteriousStranger03) == false )
		playerActor.AddPerk(MysteriousStranger03)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_NukaWorld_HasMysterious.SetValue(1.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00029B26, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_NukaWorld_HasMysterious.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00009C8A, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_NukaWorld_HasPackAlpha.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_22
Function Fragment_Terminal_22(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00009C8A, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_NukaWorld_HasPackAlpha.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_23
Function Fragment_Terminal_23(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000296C0, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(VANS) == false )
		playerActor.AddPerk(VANS)
	endIf
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasVansPerk.SetValue(1)
	if ( playerActor.HasPerk(SteadyAim01) == false )
		playerActor.AddPerk(SteadyAim01)
	endIf
	if ( playerActor.HasPerk(SteadyAim02) == false )
		playerActor.AddPerk(SteadyAim02)
	endIf
	perkFromForm = Game.GetFormFromFile(0x00028A23, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasSteadyAimPerk.SetValue(1.0)
	if ( playerActor.HasPerk(QuickHands01) == false )
		playerActor.AddPerk(QuickHands01)
	endIf
	if ( playerActor.HasPerk(QuickHands02) == false )
		playerActor.AddPerk(QuickHands02)
	endIf
	perkFromForm = Game.GetFormFromFile(0x000296BE, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasQuickHandsPerk.SetValue(1.0)
	if ( playerActor.HasPerk(AttackDog01) == false )
		playerActor.AddPerk(AttackDog01)
	endIf
	if ( playerActor.HasPerk(AttackDog02) == false )
		playerActor.AddPerk(AttackDog02)
	endIf
	if ( playerActor.HasPerk(AttackDog03) == false )
		playerActor.AddPerk(AttackDog03)
	endIf
	perkFromForm = Game.GetFormFromFile(0x00029B25, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasAttackDog.SetValue(1.0)
	perkFromForm = Game.GetFormFromFile(0x000277D0, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasWW1.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x000277D1, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasWW2.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x000277D2, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasWW3.SetValue(1)
	if ( playerActor.HasPerk(Awareness) == false )
		playerActor.AddPerk(Awareness)
	endIf
	perkFromForm = Game.GetFormFromFile(0x00028A22, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasAwarenessPerk.SetValue(1.0)
	if ( playerActor.HasPerk(Ghoulish01) == false )
		playerActor.AddPerk(Ghoulish01)
	endIf
	if ( playerActor.HasPerk(Ghoulish02) == false )
		playerActor.AddPerk(Ghoulish02)
	endIf
	if ( playerActor.HasPerk(Ghoulish03) == false )
		playerActor.AddPerk(Ghoulish03)
	endIf
	perkFromForm = Game.GetFormFromFile(0x00028A25, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_NukaWorld_HasGhoulish.SetValue(1.0)
	if ( playerActor.HasPerk(MysteriousStranger01) == false )
		playerActor.AddPerk(MysteriousStranger01)
	endIf
	if ( playerActor.HasPerk(MysteriousStranger02) == false )
		playerActor.AddPerk(MysteriousStranger02)
	endIf
	if ( playerActor.HasPerk(MysteriousStranger03) == false )
		playerActor.AddPerk(MysteriousStranger03)
	endIf
	perkFromForm = Game.GetFormFromFile(0x00029B26, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasMysterious.SetValue(1.0)
	perkFromForm = Game.GetFormFromFile(0x00009C8B, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasAceOperatorPerk.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x00009C8A, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasPackAlpha.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x00009C89, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasChosenPerk.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x000479EF, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm) == false )
		playerActor.AddPerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasLessonsInBloodPerk.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_24
Function Fragment_Terminal_24(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000296C0, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(VANS))
		playerActor.RemovePerk(VANS)
	endIf
	if ( playerActor.HasPerk(perkFromForm))
		playerActor.RemovePerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasVansPerk.SetValue(0)
	if ( playerActor.HasPerk(SteadyAim01))
		playerActor.RemovePerk(SteadyAim01)
	endIf
	if ( playerActor.HasPerk(SteadyAim02))
		playerActor.RemovePerk(SteadyAim02)
	endIf
	perkFromForm = Game.GetFormFromFile(0x00028A23, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm))
		playerActor.RemovePerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasSteadyAimPerk.SetValue(0)
	if ( playerActor.HasPerk(QuickHands01))
		playerActor.RemovePerk(QuickHands01)
	endIf
	if ( playerActor.HasPerk(QuickHands02))
		playerActor.RemovePerk(QuickHands02)
	endIf
	perkFromForm = Game.GetFormFromFile(0x000296BE, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm))
		playerActor.RemovePerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasQuickHandsPerk.SetValue(0)
	if ( playerActor.HasPerk(AttackDog01))
		playerActor.RemovePerk(AttackDog01)
	endIf
	if ( playerActor.HasPerk(AttackDog02))
		playerActor.RemovePerk(AttackDog02)
	endIf
	if ( playerActor.HasPerk(AttackDog03))
		playerActor.RemovePerk(AttackDog03)
	endIf
	perkFromForm = Game.GetFormFromFile(0x00029B25, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm))
		playerActor.RemovePerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasAttackDog.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x000277D0, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm))
		playerActor.RemovePerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasWW1.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x000277D1, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm))
		playerActor.RemovePerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasWW2.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x000277D2, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm))
		playerActor.RemovePerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasWW3.SetValue(0)
	if ( playerActor.HasPerk(Awareness))
		playerActor.RemovePerk(Awareness)
	endIf
	perkFromForm = Game.GetFormFromFile(0x00028A22, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm))
		playerActor.RemovePerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasAwarenessPerk.SetValue(0)
	if ( playerActor.HasPerk(Ghoulish01))
		playerActor.RemovePerk(Ghoulish01)
	endIf
	if ( playerActor.HasPerk(Ghoulish02))
		playerActor.RemovePerk(Ghoulish02)
	endIf
	if ( playerActor.HasPerk(Ghoulish03))
		playerActor.RemovePerk(Ghoulish03)
	endIf
	perkFromForm = Game.GetFormFromFile(0x00028A25, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm))
		playerActor.RemovePerk(perkFromForm)
	endIf
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_NukaWorld_HasGhoulish.SetValue(0)
	if ( playerActor.HasPerk(MysteriousStranger01))
		playerActor.RemovePerk(MysteriousStranger01)
	endIf
	if ( playerActor.HasPerk(MysteriousStranger02))
		playerActor.RemovePerk(MysteriousStranger02)
	endIf
	if ( playerActor.HasPerk(MysteriousStranger03))
		playerActor.RemovePerk(MysteriousStranger03)
	endIf
	perkFromForm = Game.GetFormFromFile(0x00029B26, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm))
		playerActor.RemovePerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasMysterious.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x00009C8B, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm))
		playerActor.RemovePerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasAceOperatorPerk.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x00009C8A, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm))
		playerActor.RemovePerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasPackAlpha.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x00009C89, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm))
		playerActor.RemovePerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasChosenPerk.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x000479EF, "DLCNukaWorld.esm") as Perk
	if ( playerActor.HasPerk(perkFromForm))
		playerActor.RemovePerk(perkFromForm)
	endIf
	CheatTerminal_NukaWorld_HasLessonsInBloodPerk.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_25
Function Fragment_Terminal_25(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00009C89, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_NukaWorld_HasChosenPerk.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_26
Function Fragment_Terminal_26(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00009C89, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_NukaWorld_HasChosenPerk.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_27
Function Fragment_Terminal_27(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000479EF, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_NukaWorld_HasLessonsInBloodPerk.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_28
Function Fragment_Terminal_28(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000479EF, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_NukaWorld_HasLessonsInBloodPerk.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

GlobalVariable Property CheatTerminal_NukaWorld_HasVansPerk Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasSteadyAimPerk Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasQuickHandsPerk Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasAceOperatorPerk Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasAttackDog Auto Const
Perk Property VANS Auto Const
Perk Property SteadyAim01 Auto Const
Perk Property SteadyAim02 Auto Const
Perk Property QuickHands01 Auto Const
Perk Property QuickHands02 Auto Const
Perk Property AttackDog01 Auto Const
Perk Property AttackDog02 Auto Const
Perk Property AttackDog03 Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasWW1 Auto Const

GlobalVariable Property CheatTerminal_NukaWorld_HasWW2 Auto Const

GlobalVariable Property CheatTerminal_NukaWorld_HasWW3 Auto Const

GlobalVariable Property CheatTerminal_NukaWorld_HasAwarenessPerk Auto Const

Perk Property Awareness Auto Const

GlobalVariable Property CheatTerminal_NukaWorld_HasGhoulish Auto Const

Perk Property Ghoulish01 Auto Const

Perk Property Ghoulish02 Auto Const

Perk Property Ghoulish03 Auto Const

Perk Property MysteriousStranger01 Auto Const

Perk Property MysteriousStranger02 Auto Const

Perk Property MysteriousStranger03 Auto Const

GlobalVariable Property CheatTerminal_NukaWorld_HasMysterious Auto Const

GlobalVariable Property CheatTerminal_NukaWorld_HasPackAlpha Auto Const

GlobalVariable Property CheatTerminal_NukaWorld_HasChosenPerk Auto Const

GlobalVariable Property CheatTerminal_NukaWorld_HasLessonsInBloodPerk Auto Const
