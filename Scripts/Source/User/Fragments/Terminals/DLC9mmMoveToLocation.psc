;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:DLC9mmMoveToLocation Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x0022B2D4, "Fallout4.esm") as ObjectReference, 0.029268, 197.342168, -216.82, 0.0, LinnocenceLookMarkerRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x000E7F4A, "Fallout4.esm") as ObjectReference, 1185.2421, -272.36777, -873.0661, 0.0, MareLookLocationRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x000ADD8F, "Fallout4.esm") as ObjectReference, -2428.4019, -1229.9903, -157.0532, 0.0, MariaLookLocationRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x0014AC72, "Fallout4.esm") as ObjectReference, -59.8336, 11.5799, 581.8663, 0.0, BloodyMarryLookAtRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property MariaLookLocationRef Auto Const
ObjectReference Property MareLookLocationRef Auto Const
ObjectReference Property LinnocenceLookMarkerRef Auto Const
ObjectReference Property BloodyMarryLookAtRef Auto Const
