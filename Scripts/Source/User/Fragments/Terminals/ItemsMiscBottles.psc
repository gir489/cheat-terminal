;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:ItemsMiscBottles Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BottleEmptyWineAmontillado, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BeerBottleEmpty01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(PrewarBottleBourbon01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BottleEmptyBourbon01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BottleEmpty02, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BottleEmpty04, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MilkBottleEmpty, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BeerGwinnettAleEmpty01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BeerGwinnettBrewEmpty01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BeerGwinnettLagerEmpty01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BeerGwinnettPaleEmpty01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BeerGwinnettPilsEmpty01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BeerGwinnettStoutEmpty01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BabyBottleDirty02, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BottleEmpty01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(NukaColaEmpty, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BottleEmptyRum01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BabyBottleDirty01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(PrewarBottleVodka01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BottleEmptyVodka01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(PrewarBottleWhiskeyEmpty01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_22
Function Fragment_Terminal_22(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BottleEmptyWhiskey01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_23
Function Fragment_Terminal_23(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BottleEmpty03, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_24
Function Fragment_Terminal_24(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BottleEmptyWine01, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
GlobalVariable Property CheatTerminal_MiscToGive Auto Const
MiscObject Property BottleEmptyWineAmontillado Auto Const
MiscObject Property BeerBottleEmpty01 Auto Const
MiscObject Property PrewarBottleBourbon01 Auto Const
MiscObject Property BottleEmptyBourbon01 Auto Const
MiscObject Property BottleEmpty02 Auto Const
MiscObject Property BottleEmpty04 Auto Const
MiscObject Property MilkBottleEmpty Auto Const
MiscObject Property BeerGwinnettAleEmpty01 Auto Const
MiscObject Property BeerGwinnettBrewEmpty01 Auto Const
MiscObject Property BeerGwinnettLagerEmpty01 Auto Const
MiscObject Property BeerGwinnettPaleEmpty01 Auto Const
MiscObject Property BeerGwinnettPilsEmpty01 Auto Const
MiscObject Property BeerGwinnettStoutEmpty01 Auto Const
MiscObject Property BabyBottleDirty02 Auto Const
MiscObject Property BottleEmpty01 Auto Const
MiscObject Property NukaColaEmpty Auto Const
MiscObject Property BottleEmptyRum01 Auto Const
MiscObject Property BabyBottleDirty01 Auto Const
MiscObject Property PrewarBottleVodka01 Auto Const
MiscObject Property BottleEmptyVodka01 Auto Const
MiscObject Property PrewarBottleWhiskeyEmpty01 Auto Const
MiscObject Property BottleEmptyWhiskey01 Auto Const
MiscObject Property BottleEmpty03 Auto Const
MiscObject Property BottleEmptyWine01 Auto Const
