;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_FarHa_01004521 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x000570DA, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x00048234, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0000914E, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x000570D3, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0000914F, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x000570D4, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x000570D9, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x00046027, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0004E698, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0004B9B1, "DLCCoast.esm")
if objectToSpawn
	Actor playerActor = Game.GetPlayer()
	ObjectReference objRef = playerActor.PlaceAtMe(objectToSpawn)
	objRef.AttachMod(mod_Legendary_Armor_LessDMGBugs)
	Game.GetPlayer().AddItem(objRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x000247C8, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x000391E6, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0000EA36, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0003A556, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x00043331, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ObjectMod Property mod_Legendary_Armor_LessDMGBugs Auto Const
