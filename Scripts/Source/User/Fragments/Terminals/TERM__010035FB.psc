;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__010035FB Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference objRef = Game.GetPlayer().PlaceAtMe(WorkbenchChemistryB)
objRef.SetAngle(0,0,0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference objRef = Game.GetPlayer().PlaceAtMe(WorkbenchCookingGroundPot)
objRef.SetAngle(0,0,0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference objRef = Game.GetPlayer().PlaceAtMe(workbenchWeaponsA, abDeleteWhenAble = true)
objRef.SetAngle(0,0,0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference objRef = Game.GetPlayer().PlaceAtMe(WorkbenchArmorA)
objRef.SetAngle(0,0,0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference objRef = Game.GetPlayer().PlaceAtMe(WorkbenchPowerArmorSmall)
objRef.SetAngle(0,0,0)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Furniture Property WorkbenchPowerArmorSmall Auto Const
Furniture Property WorkbenchChemistryB Auto Const
Furniture Property WorkbenchArmorA Auto Const
Furniture Property WorkbenchCookingGroundPot Auto Const
Furniture Property workbenchWeaponsA Auto Const
