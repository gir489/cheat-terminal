;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:NukaWorld Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000296C0, "DLCNukaWorld.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_NukaWorld_HasVansPerk, perkFromForm)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_NukaWorld_HasSteadyAimPerk, Game.GetFormFromFile(0x00028A23, "DLCNukaWorld.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_NukaWorld_HasQuickHandsPerk, Game.GetFormFromFile(0x000296BE, "DLCNukaWorld.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_NukaWorld_HasAttackDog, Game.GetFormFromFile(0x00029B25, "DLCNukaWorld.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_NukaWorld_HasWW1, Game.GetFormFromFile(0x000277D0, "DLCNukaWorld.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_NukaWorld_HasWW2, Game.GetFormFromFile(0x000277D1, "DLCNukaWorld.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_NukaWorld_HasWW3, Game.GetFormFromFile(0x000277D2, "DLCNukaWorld.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_NukaWorld_HasAwarenessPerk, Game.GetFormFromFile(0x00028A22, "DLCNukaWorld.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_NukaWorld_HasGhoulish, Game.GetFormFromFile(0x00028A25, "DLCNukaWorld.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_NukaWorld_HasMysterious, Game.GetFormFromFile(0x00029B26, "DLCNukaWorld.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_NukaWorld_HasChosenPerk, Game.GetFormFromFile(0x00009C89, "DLCNukaWorld.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_NukaWorld_HasPackAlpha, Game.GetFormFromFile(0x00009C8A, "DLCNukaWorld.esm") as Perk)
	CheatTerminalGlobalFuncs.DoPerkCheckForGlobal(CheatTerminal_NukaWorld_HasLessonsInBloodPerk, Game.GetFormFromFile(0x000479EF, "DLCNukaWorld.esm") as Perk)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasVansPerk Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasSteadyAimPerk Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasQuickHandsPerk Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasAttackDog Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasWW1 Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasWW2 Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasWW3 Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasAwarenessPerk Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasGhoulish Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasMysterious Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasChosenPerk Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasPackAlpha Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_HasLessonsInBloodPerk Auto Const
