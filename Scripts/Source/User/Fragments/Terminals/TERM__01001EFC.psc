;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01001EFC Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_InstituteLaserGun_Grip_StockFull)
else
	Game.GetPlayer().AddItem(miscmod_mod_InstituteLaserGun_Grip_Stock)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_InstituteLaserGun_Grip_Standard)
else
	Game.GetPlayer().AddItem(miscmod_mod_InstituteLaserGun_Grip_Standard)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_InstituteLaserGun_Grip_Standard Auto Const
ObjectMod Property mod_InstituteLaserGun_Grip_StockFull Auto Const
MiscObject Property miscmod_mod_InstituteLaserGun_Grip_Standard Auto Const
MiscObject Property miscmod_mod_InstituteLaserGun_Grip_Stock Auto Const
