;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:NukaWorldWeaponsCustom Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form leveledItemForm = Game.GetFormFromFile(0x00036C24, "DLCNukaWorld.esm")
if leveledItemForm 
	LeveledItem levelItemObj = leveledItemForm as LeveledItem 
	FormList modList = Game.GetFormFromFile(0x00036C25, "DLCNukaWorld.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(levelItemObj, modList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form leveledItemForm = Game.GetFormFromFile(0x00043416, "DLCNukaWorld.esm")
if leveledItemForm 
	LeveledItem levelItemObj = leveledItemForm as LeveledItem 
	FormList modList = Game.GetFormFromFile(0x00043417, "DLCNukaWorld.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(levelItemObj, modList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form leveledItemForm = Game.GetFormFromFile(0x0004EC35, "DLCNukaWorld.esm")
if leveledItemForm 
	LeveledItem levelItemObj = leveledItemForm as LeveledItem 
	FormList modList = Game.GetFormFromFile(0x0004EC36, "DLCNukaWorld.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(levelItemObj, modList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form leveledItemForm = Game.GetFormFromFile(0x0004F084, "DLCNukaWorld.esm")
if leveledItemForm 
	LeveledItem levelItemObj = leveledItemForm as LeveledItem 
	FormList modList = Game.GetFormFromFile(0x0004F085, "DLCNukaWorld.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(levelItemObj, modList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form leveledItemForm = Game.GetFormFromFile(0x0005178F, "DLCNukaWorld.esm")
if leveledItemForm 
	LeveledItem levelItemObj = leveledItemForm as LeveledItem 
	FormList modList = Game.GetFormFromFile(0x00051790, "DLCNukaWorld.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(levelItemObj, modList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form leveledItemForm = Game.GetFormFromFile(0x000520A6, "DLCNukaWorld.esm")
if leveledItemForm 
	LeveledItem levelItemObj = leveledItemForm as LeveledItem 
	FormList modList = Game.GetFormFromFile(0x000520A7, "DLCNukaWorld.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(levelItemObj, modList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form leveledItemForm = Game.GetFormFromFile(0x000520A8, "DLCNukaWorld.esm")
if leveledItemForm 
	LeveledItem levelItemObj = leveledItemForm as LeveledItem 
	FormList modList = Game.GetFormFromFile(0x000520A9, "DLCNukaWorld.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(levelItemObj, modList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form leveledItemForm = Game.GetFormFromFile(0x000520AA, "DLCNukaWorld.esm")
if leveledItemForm 
	LeveledItem levelItemObj = leveledItemForm as LeveledItem 
	FormList modList = Game.GetFormFromFile(0x000520AB, "DLCNukaWorld.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(levelItemObj, modList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form leveledItemForm = Game.GetFormFromFile(0x00050DD3, "DLCNukaWorld.esm")
if leveledItemForm 
	LeveledItem levelItemObj = leveledItemForm as LeveledItem 
	FormList modList = Game.GetFormFromFile(0x00056BEC, "DLCNukaWorld.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(levelItemObj, modList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
CheatTerminal_NukaWorld_ChineseAssaultRifle.Revert()
CheatTerminal_NukaWorld_ChineseAssaultRifle.AddForm(Game.GetFormFromFile(0x00037893, "DLCNukaWorld.esm"), 1, 1)
ObjectReference weaponRef = playerActor.PlaceAtMe(CheatTerminal_NukaWorld_ChineseAssaultRifle)
weaponRef.AttachMod(Game.GetFormFromFile(0x00033B63, "DLCNukaWorld.esm") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0004D941, "DLCNukaWorld.esm") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00033B7F, "DLCNukaWorld.esm") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00033B70, "DLCNukaWorld.esm") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x000415B2, "DLCNukaWorld.esm") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00037887, "DLCNukaWorld.esm") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00033B7A, "DLCNukaWorld.esm") as ObjectMod)
weaponRef.AttachMod(CheatTerminal_NoRecoil)
weaponRef.AttachMod(mod_Legendary_Weapon_DamageLimbRecoil)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(Game.GetFormFromFile(0x0004E422, "DLCNukaWorld.esm"))
weaponRef.AttachMod(Game.GetFormFromFile(0x0001EB9D, "DLCNukaWorld.esm") as ObjectMod)
weaponRef.AttachMod(mod_Legendary_Weapon_Bleed)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(Game.GetFormFromFile(0x0004E424, "DLCNukaWorld.esm"))
weaponRef.AttachMod(Game.GetFormFromFile(0x00037887, "DLCNukaWorld.esm") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00033B65, "DLCNukaWorld.esm") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x0003788D, "DLCNukaWorld.esm") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00033B7B, "DLCNukaWorld.esm") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00033B74, "DLCNukaWorld.esm") as ObjectMod)
weaponRef.AttachMod(mod_Legendary_Weapon_DamageConsecutiveHits)
weaponRef.AttachMod(Game.GetFormFromFile(0x0004E423, "DLCNukaWorld.esm") as ObjectMod)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
LeveledItem Property CheatTerminal_NukaWorld_ChineseAssaultRifle Auto Const
ObjectMod Property CheatTerminal_NoRecoil Auto Const
ObjectMod Property mod_Legendary_Weapon_DamageLimbRecoil Auto Const

ObjectMod Property mod_Legendary_Weapon_Bleed Auto Const

ObjectMod Property mod_Legendary_Weapon_DamageConsecutiveHits Auto Const
