;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Weap_0100A90C Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Minigun_BarrelMinigun_short)
else
	Game.GetPlayer().AddItem(miscmod_mod_Minigun_BarrelMinigun_short)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Minigun_BarrelMinigun_Long)
else
	Game.GetPlayer().AddItem(miscmod_mod_Minigun_BarrelMinigun)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Minigun_BarrelMinigun_TriBarrel)
else
	Game.GetPlayer().AddItem(miscmod_mod_Minigun_BarrelMinigun_TriBarrel)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_Minigun_BarrelMinigun_Long Auto Const
ObjectMod Property mod_Minigun_BarrelMinigun_short Auto Const
ObjectMod Property mod_Minigun_BarrelMinigun_TriBarrel Auto Const
MiscObject Property miscmod_mod_Minigun_BarrelMinigun Auto Const
MiscObject Property miscmod_mod_Minigun_BarrelMinigun_short Auto Const
MiscObject Property miscmod_mod_Minigun_BarrelMinigun_TriBarrel Auto Const
