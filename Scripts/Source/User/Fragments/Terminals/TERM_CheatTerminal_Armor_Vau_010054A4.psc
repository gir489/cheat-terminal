;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Armor_Vau_010054A4 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_Armor_VaultTecSecurity111_Clean)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_Armor_VaultTecSecurity111_Dirty)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_VaultTecSecurityHelmet)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_Armor_VaultTecSecurityCovenant)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_VaultTecSecurityHelmetCovenant)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_VaultTecSecurityHelmet_Clean)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_Armor_VaultTecSecurity81)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Armor Property Armor_VaultTecSecurity111_Clean Auto Const

LeveledItem Property CheatTerminal_Armor_VaultTecSecurity111_Clean Auto Const

Armor Property Armor_VaultTecSecurityHelmet_Clean Auto Const

Armor Property Armor_VaultTecSecurityHelmetCovenant Auto Const

LeveledItem Property CheatTerminal_Armor_VaultTecSecurityCovenant Auto Const

LeveledItem Property CheatTerminal_Armor_VaultTecSecurity111_Dirty Auto Const

LeveledItem Property CheatTerminal_Armor_VaultTecSecurity81 Auto Const

Armor Property Armor_VaultTecSecurityHelmet Auto Const
