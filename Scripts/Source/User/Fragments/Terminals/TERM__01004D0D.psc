;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01004D0D Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagHotRod01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagHotRod02).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagHotRod03).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagLaCoiffe01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagLaCoiffe05).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagPicketFences01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagPicketFences02).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagPicketFences03).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagPicketFences04).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagPicketFences05).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagRobcoFun01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagRobcoFun02).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagRobcoFun04).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagRobcoFun05).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagTattoo01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagTattoo02).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagTattoo03).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagTattoo04).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagTattoo05).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PerkMagTotalHack01).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagTotalHack02).Activate(playerActor)
playerActor.PlaceAtMe(PerkMagTotalHack03).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
playerActor.PlaceAtMe(PlayerHouse_Ruin_BabyBook01).Activate(playerActor)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Book Property PerkMagHotRod01 Auto Const
Book Property PerkMagHotRod02 Auto Const
Book Property PerkMagHotRod03 Auto Const

Book Property PerkMagLaCoiffe01 Auto Const
Book Property PerkMagLaCoiffe05 Auto Const

Book Property PerkMagPicketFences01 Auto Const
Book Property PerkMagPicketFences02 Auto Const
Book Property PerkMagPicketFences03 Auto Const
Book Property PerkMagPicketFences04 Auto Const
Book Property PerkMagPicketFences05 Auto Const

Book Property PerkMagRobcoFun01 Auto Const
Book Property PerkMagRobcoFun02 Auto Const
Book Property PerkMagRobcoFun04 Auto Const
Book Property PerkMagRobcoFun05 Auto Const

Book Property PerkMagTattoo01 Auto Const
Book Property PerkMagTattoo02 Auto Const
Book Property PerkMagTattoo03 Auto Const
Book Property PerkMagTattoo04 Auto Const
Book Property PerkMagTattoo05 Auto Const

Book Property PerkMagTotalHack01 Auto Const
Book Property PerkMagTotalHack02 Auto Const
Book Property PerkMagTotalHack03 Auto Const

MiscObject Property PlayerHouse_Ruin_BabyBook01 Auto Const
