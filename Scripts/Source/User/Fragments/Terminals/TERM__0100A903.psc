;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__0100A903 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Grip_StockShort)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Grip_StockShort)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Grip_Standard)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Grip_Standard)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Grip_Better1)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Grip_Better1)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Grip_Better2)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Grip_Better2)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Grip_StockFull)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Grip_Stock)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Grip_StockMarksmans)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Grip_StockMarksmans)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Grip_StockRecoil)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Grip_StockRecoil)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_LaserGun_Grip_Standard Auto Const
ObjectMod Property mod_LaserGun_Grip_Better1 Auto Const
ObjectMod Property mod_LaserGun_Grip_StockShort Auto Const
ObjectMod Property mod_LaserGun_Grip_Better2 Auto Const
ObjectMod Property mod_LaserGun_Grip_StockFull Auto Const
ObjectMod Property mod_LaserGun_Grip_StockMarksmans Auto Const
ObjectMod Property mod_LaserGun_Grip_StockRecoil Auto Const
MiscObject Property miscmod_mod_LaserGun_Grip_Standard Auto Const
MiscObject Property miscmod_mod_LaserGun_Grip_Better1 Auto Const
MiscObject Property miscmod_mod_LaserGun_Grip_StockShort Auto Const
MiscObject Property miscmod_mod_LaserGun_Grip_Better2 Auto Const
MiscObject Property miscmod_mod_LaserGun_Grip_Stock Auto Const
MiscObject Property miscmod_mod_LaserGun_Grip_StockMarksmans Auto Const
MiscObject Property miscmod_mod_LaserGun_Grip_StockRecoil Auto Const
