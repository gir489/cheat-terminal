;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:LaserMusketReceiver Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserMusket_Receiver_1)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserMusket_Receiver_1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserMusket_Receiver_2)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserMusket_Receiver_2)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserMusket_Receiver_3)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserMusket_Receiver_3)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserMusket_Receiver_4)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserMusket_Receiver_4)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserMusket_Receiver_5)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserMusket_Receiver_5)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_LaserMusket_Receiver_1 Auto Const
ObjectMod Property mod_LaserMusket_Receiver_2 Auto Const
ObjectMod Property mod_LaserMusket_Receiver_3 Auto Const
ObjectMod Property mod_LaserMusket_Receiver_4 Auto Const
ObjectMod Property mod_LaserMusket_Receiver_5 Auto Const
MiscObject Property miscmod_mod_LaserMusket_Receiver_1 Auto Const
MiscObject Property miscmod_mod_LaserMusket_Receiver_2 Auto Const
MiscObject Property miscmod_mod_LaserMusket_Receiver_3 Auto Const
MiscObject Property miscmod_mod_LaserMusket_Receiver_4 Auto Const
MiscObject Property miscmod_mod_LaserMusket_Receiver_5 Auto Const
