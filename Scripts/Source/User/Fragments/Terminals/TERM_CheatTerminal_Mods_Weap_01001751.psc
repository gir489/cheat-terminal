;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Weap_01001751 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Broadsider_Barrel_Long)
else
	Game.GetPlayer().AddItem(miscmod_mod_Broadsider_Barrel_Long)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Broadsider_Barrel_LightLong)
else
	Game.GetPlayer().AddItem(miscmod_mod_Broadsider_Barrel_LightLong)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_Broadsider_Barrel_Long Auto Const
ObjectMod Property mod_Broadsider_Barrel_LightLong Auto Const
MiscObject Property miscmod_mod_Broadsider_Barrel_Long Auto Const
MiscObject Property miscmod_mod_Broadsider_Barrel_LightLong Auto Const
