;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:FarHarborSpawnNPCFighterGrenades Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_SpawnNPC_FarHarbor_Angler, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_SpawnNPC_FarHarbor_FogCrawler, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_SpawnNPC_FarHarbor_Gulper, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_SpawnNPC_FarHarbor_HermitCrab, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
GlobalVariable Property CheatTerminal_ThrowablesToGive Auto Const
Weapon Property CheatTerminal_SpawnNPC_FarHarbor_Angler Auto Const
Weapon Property CheatTerminal_SpawnNPC_FarHarbor_FogCrawler Auto Const
Weapon Property CheatTerminal_SpawnNPC_FarHarbor_Gulper Auto Const
Weapon Property CheatTerminal_SpawnNPC_FarHarbor_HermitCrab Auto Const
