;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Mele_0100A8ED Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_BaseballBat_Material_StainedMedium)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_BaseballBat_Material_StainedMedium)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_BaseballBat_Material_Aluminium)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_BaseballBat_Material_Aluminium)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_BaseballBat_Material_StainedNatural)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_BaseballBat_Material_StainedNatural)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_BaseballBat_Material_StainedLight)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_BaseballBat_Material_StainedLight)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_BaseballBat_Material_StainedDark)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_BaseballBat_Material_StainedDark)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_BaseballBat_Material_PaintedRed)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_BaseballBat_Material_PaintedRed)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_BaseballBat_Material_PaintedGrey)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_BaseballBat_Material_PaintedGrey)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_BaseballBat_Material_PaintedBlue)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_BaseballBat_Material_PaintedBlue)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_BaseballBat_Material_PaintedBlack)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_BaseballBat_Material_PaintedBlack)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_melee_BaseballBat_Material_Aluminium Auto Const
ObjectMod Property mod_melee_BaseballBat_Material_StainedNatural Auto Const
ObjectMod Property mod_melee_BaseballBat_Material_StainedMedium Auto Const
ObjectMod Property mod_melee_BaseballBat_Material_StainedLight Auto Const
ObjectMod Property mod_melee_BaseballBat_Material_StainedDark Auto Const
ObjectMod Property mod_melee_BaseballBat_Material_PaintedRed Auto Const
ObjectMod Property mod_melee_BaseballBat_Material_PaintedGrey Auto Const
ObjectMod Property mod_melee_BaseballBat_Material_PaintedBlue Auto Const
ObjectMod Property mod_melee_BaseballBat_Material_PaintedBlack Auto Const
MiscObject Property miscmod_mod_melee_BaseballBat_Material_Aluminium Auto Const
MiscObject Property miscmod_mod_melee_BaseballBat_Material_StainedNatural Auto Const
MiscObject Property miscmod_mod_melee_BaseballBat_Material_StainedMedium Auto Const
MiscObject Property miscmod_mod_melee_BaseballBat_Material_StainedLight Auto Const
MiscObject Property miscmod_mod_melee_BaseballBat_Material_StainedDark Auto Const
MiscObject Property miscmod_mod_melee_BaseballBat_Material_PaintedRed Auto Const
MiscObject Property miscmod_mod_melee_BaseballBat_Material_PaintedGrey Auto Const
MiscObject Property miscmod_mod_melee_BaseballBat_Material_PaintedBlue Auto Const
MiscObject Property miscmod_mod_melee_BaseballBat_Material_PaintedBlack Auto Const
