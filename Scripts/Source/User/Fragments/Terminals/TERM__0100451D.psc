;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__0100451D Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00010B91, "DLCCoast.esm")
if objectToGive
	Actor playerActor = Game.GetPlayer()
	LL_EpicChance_Standard.SetValue(0.0)
	playerActor.AddItem(objectToGive)
	playerActor.AddItem(Game.GetFormFromFile(0x00010B96, "DLCCoast.esm"))
	playerActor.AddItem(Game.GetFormFromFile(0x00010B9B, "DLCCoast.esm"))
	playerActor.AddItem(Game.GetFormFromFile(0x00010BA0, "DLCCoast.esm"))
	playerActor.AddItem(Game.GetFormFromFile(0x00010BA5, "DLCCoast.esm"))
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00010B93, "DLCCoast.esm")
if objectToGive
	Actor playerActor = Game.GetPlayer()
	LL_EpicChance_Standard.SetValue(0.0)
	playerActor.AddItem(objectToGive)
	playerActor.AddItem(Game.GetFormFromFile(0x00010B98, "DLCCoast.esm"))
	playerActor.AddItem(Game.GetFormFromFile(0x00010B9D, "DLCCoast.esm"))
	playerActor.AddItem(Game.GetFormFromFile(0x00010BA2, "DLCCoast.esm"))
	playerActor.AddItem(Game.GetFormFromFile(0x00010BA7, "DLCCoast.esm"))
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ObjectMod Property mod_Legendary_Armor_Agility_and_Perception Auto Const
ObjectMod Property mod_Legendary_Armor_LessDamageStandStill Auto Const
ObjectMod Property mod_Legendary_Armor_Speed Auto Const
ObjectMod Property mod_Legendary_Armor_LowHealthSlowTime Auto Const

GlobalVariable Property LL_EpicChance_Standard Auto Const
