;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:ItemsMiscQuestMP Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MS17TedCompoundMap, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(DN084_ExecutiveIDCard, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(RRR05MILAObject, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(InstM01Seeds, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MS11PowerCables, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MS11GuidanceChip, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(DN070_PictureFrameDesktop01dirty, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(DN015_ArmorPrototype, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(MS11PoseidonRadar, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(COMMacCreadyPreventSample, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(DN070_BiometricScanner, CheatTerminal_MiscToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
GlobalVariable Property CheatTerminal_MiscToGive Auto Const
MiscObject Property MS17TedCompoundMap Auto Const
MiscObject Property DN084_ExecutiveIDCard Auto Const
MiscObject Property RRR05MILAObject Auto Const
MiscObject Property InstM01Seeds Auto Const
MiscObject Property MS11PowerCables Auto Const
MiscObject Property MS11GuidanceChip Auto Const
MiscObject Property DN070_PictureFrameDesktop01dirty Auto Const
MiscObject Property DN015_ArmorPrototype Auto Const
MiscObject Property MS11PoseidonRadar Auto Const
MiscObject Property COMMacCreadyPreventSample Auto Const
MiscObject Property DN070_BiometricScanner Auto Const
