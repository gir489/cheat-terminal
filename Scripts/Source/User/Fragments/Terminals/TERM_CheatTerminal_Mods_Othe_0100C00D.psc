;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Othe_0100C00D Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Fatman_Barrel_Standard)
else
	Game.GetPlayer().AddItem(miscmod_mod_Fatman_Barrel_Standard)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Fatman_Barrel_MIRV)
else
	Game.GetPlayer().AddItem(miscmod_mod_Fatman_Barrel_MIRV)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_Fatman_Barrel_Standard Auto Const
ObjectMod Property mod_Fatman_Barrel_MIRV Auto Const
MiscObject Property miscmod_mod_Fatman_Barrel_Standard Auto Const
MiscObject Property miscmod_mod_Fatman_Barrel_MIRV Auto Const
