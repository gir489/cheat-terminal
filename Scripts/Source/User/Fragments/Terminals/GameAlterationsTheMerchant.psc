;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsTheMerchant Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
While(TheMerchantContainer.GetItemCount(Caps001) > 0)
	TheMerchantContainer.RemoveItem(Caps001,65535)
EndWhile
ThemerchantContainer.AddItem(Caps001,65535)
While(TheMerchant.GetItemCount(Caps001) > 0)
	TheMerchant.RemoveItem(Caps001,65535)
EndWhile
TheMerchant.AddItem(Caps001,65535)
CheatTerminalGlobalFuncs.DoPotion(CheatTerminal_TheMerchantPotion)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CheatTerminal_MerchantResetQuestion.Show() == 0 )
	TheMerchantContainer.Reset()
	CheatTerminal_MerchantReset.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
TheMerchant.Reset()
if ( CheatTerminal_TheMerchant_Weapons.GetValue() == 1 )
	AddWeapons()
endIf
if ( CheatTerminal_TheMerchant_Throwables.GetValue() == 1 )
	AddThrowables()
endIf
if ( CheatTerminal_TheMerchant_Armor.GetValue() == 1 )
	AddArmor()
endIf
if ( CheatTerminal_TheMerchant_Clothing.GetValue() == 1 )
	AddClothing()
endIf
If ( CheatTerminal_TheMerchant_PowerArmor.GetValue() == 1 )
	AddPowerArmorPieces()
endIf
if ( CheatTerminal_TheMerchant_Aid.GetValue() == 1 )
	AddAid()
endIf
if ( CheatTerminal_TheMerchant_Ammo.GetValue() == 1 )
	AddAmmo()
endIf
if ( CheatTerminal_TheMerchant_Crafting.GetValue() == 1 )
	AddCraftingIngredients()
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Function AddClothing()
	TheMerchant.AddItem(MS10ClothesRewardF, 2)
	TheMerchant.AddItem(Clothes_BoS_CaptainKells_Hat, 2)
	TheMerchant.AddItem(ClothesFatiguesPreWar, 2)
	TheMerchant.AddItem(ClothesGreaserJacketAtomCats, 2)
	TheMerchant.AddItem(BabyBundledRight, 2)
	TheMerchant.AddItem(ClothesBaseballHat, 2)
	TheMerchant.AddItem(ClothesBaseballUniform, 2)
	TheMerchant.AddItem(ClothesBathrobe, 2)
	TheMerchant.AddItem(ClothesResident3Hat, 2)
	TheMerchant.AddItem(ClothesMommaMurphy, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_blacksleeves, 2)
	TheMerchant.AddItem(ClothesMobster01, 2)
	TheMerchant.AddItem(ClothesDeaconMobster, 2)
	TheMerchant.AddItem(ClothesBlackRimGlasses, 2)
	TheMerchant.AddItem(ClothesDog_BandanaBlue, 2)
	TheMerchant.AddItem(Armor_Batting_HelmetBlue, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Blue, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_bluesleeves, 2)
	TheMerchant.AddItem(Armor_BoS_Knight_Lancer, 2)
	TheMerchant.AddItem(Armor_BoS_Knight_UnderarmorHelmet, 2)
	TheMerchant.AddItem(Armor_BoS_Officer_Underarmor, 2)
	TheMerchant.AddItem(Armor_BoS_Knight_Underarmor, 2)
	TheMerchant.AddItem(Armor_BoS_Soldier_Underarmor, 2)
	TheMerchant.AddItem(ClothesBowlerHat, 2)
	TheMerchant.AddItem(Armor_BoS_Soldier, 2)
	TheMerchant.AddItem(Armor_BoS_Cade, 2)
	TheMerchant.AddItem(Armor_BoS_Kells, 2)
	TheMerchant.AddItem(Armor_RR303BoSDisguise, 2)
	TheMerchant.AddItem(Armor_FlightHelmetBrown, 2)
	TheMerchant.AddItem(ClothesScientistCabot, 2)
	TheMerchant.AddItem(Armor_Wastelander_Heavy, 2)
	TheMerchant.AddItem(ClothesCaitBandolier, 2)
	TheMerchant.AddItem(MS11IronsidesHat, 2)
	TheMerchant.AddItem(ClothesPrewarWomensCasual, 2)
	TheMerchant.AddItem(ClothesPrewarShortSleevesAndSlacks, 2)
	TheMerchant.AddItem(ClothesChefsHat, 2)
	TheMerchant.AddItem(ClothesChildofAtom, 2)
	TheMerchant.AddItem(ClothesChildofAtomBrown2, 2)
	TheMerchant.AddItem(ClothesChildofAtomGreen2, 2)
	TheMerchant.AddItem(ClothesChildofAtomBrown, 2)
	TheMerchant.AddItem(ClothesChildofAtomGreen, 2)
	TheMerchant.AddItem(ClothesChildofAtomBrown3, 2)
	TheMerchant.AddItem(ClothesChildofAtomGreen3, 2)
	TheMerchant.AddItem(ClothesChildofAtom3, 2)
	TheMerchant.AddItem(ClothesChildofAtom2, 2)
	TheMerchant.AddItem(Clothes_InstituteChildUniform, 2)
	TheMerchant.AddItem(ClothesSuitClean_Black, 2)
	TheMerchant.AddItem(NewsmanSuitA, 2)
	TheMerchant.AddItem(ClothesSuitClean_Blue, 2)
	TheMerchant.AddItem(ClothesSuitClean_Grey, 2)
	TheMerchant.AddItem(ClothesSuitClean_Striped, 2)
	TheMerchant.AddItem(ClothesSuitClean_Tan, 2)
	TheMerchant.AddItem(Clothes_InstituteWorker, 2)
	TheMerchant.AddItem(ClothesCoastguardHat, 2)
	TheMerchant.AddItem(ArmorPrestonGarveyOutfit, 2)
	TheMerchant.AddItem(ClothesCait, 2)
	TheMerchant.AddItem(ClothesCourserJacket, 2)
	TheMerchant.AddItem(Armor_VaultTecSecurityCovenant, 2)
	TheMerchant.AddItem(RR_PrewarSweaterVest_01, 2)
	TheMerchant.AddItem(ClothesMayorHat, 2)
	TheMerchant.AddItem(Armor_HazmatSuitDamaged, 2)
	TheMerchant.AddItem(DBTech_Armor_Highschool_Jacket, 2)
	TheMerchant.AddItem(SkinMirelurkDeepKing, 2)
	TheMerchant.AddItem(SkinMirelurkGlowingKing, 2)
	TheMerchant.AddItem(ClothesFatiguesPostWar, 2)
	TheMerchant.AddItem(Armor_Army_Helmet_postwar, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_blacksleeves_dirty, 2)
	TheMerchant.AddItem(ClothesSuitDirty_Black, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Dirty_Blue, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_bluesleeves_dirty, 2)
	TheMerchant.AddItem(ClothesSuitDirty_Blue, 2)
	TheMerchant.AddItem(ClothesVaultTecSalesmanHatPW, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Dirty_Green, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_greensleeves_dirty, 2)
	TheMerchant.AddItem(ClothesSuitDirty_Grey, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Dirty, 2)
	TheMerchant.AddItem(Clothes_InstituteUniform_Dirty, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_Dirty, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Dirty_Orange, 2)
	TheMerchant.AddItem(ClothesPostmanHatPostwar, 2)
	TheMerchant.AddItem(ClothesPostmanPostwar, 2)
	TheMerchant.AddItem(ClothesSuitDirty_Striped, 2)
	TheMerchant.AddItem(ClothesSuitDirty_Tan, 2)
	TheMerchant.AddItem(ClothesVaultTecSalesmanCoatPW, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Dirty_Yellow, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_yellowsleeves_dirty, 2)
	TheMerchant.AddItem(ClothesDog_ArmorMedium, 2)
	TheMerchant.AddItem(ClothesDog_DogCollar, 2)
	TheMerchant.AddItem(ClothesDog_DogHeadHelmet, 2)
	TheMerchant.AddItem(ClothesDog_DogCollarDouble, 2)
	TheMerchant.AddItem(Armor_Wastelander_Light, 2)
	TheMerchant.AddItem(Armor_BoS_Engineer_Scribe, 2)
	TheMerchant.AddItem(Armor_Wastelander_02_Hood_GlovesB, 2)
	TheMerchant.AddItem(ClothesEyeGlasses, 2)
	TheMerchant.AddItem(ClothesValentineCoat, 2)
	TheMerchant.AddItem(ClothesVisor, 2)
	TheMerchant.AddItem(ClothesResident3, 2)
	TheMerchant.AddItem(ClothesDeaconResident3, 2)
	TheMerchant.AddItem(ClothesFancyGlasses, 2)
	TheMerchant.AddItem(ClothesFather, 2)
	TheMerchant.AddItem(ClothesDesdemona, 2)
	TheMerchant.AddItem(ClothesIrmaDress, 2)
	TheMerchant.AddItem(Armor_BoS_Field_Scribe, 2)
	TheMerchant.AddItem(Armor_BoS_Field_Scribe_Helmet, 2)
	TheMerchant.AddItem(Armor_Rustic_Underarmor, 2)
	TheMerchant.AddItem(Armor_DeaconRustic_Underarmor, 2)
	TheMerchant.AddItem(Armor_BoS_Knight_Lancer_Helmet, 2)
	TheMerchant.AddItem(ClothesPrewarHouseDress01NonPlayable, 2)
	TheMerchant.AddItem(ClothesPrewarHouseDress01NonPlayableA, 2)
	TheMerchant.AddItem(ClothesPrewarHouseDress01NonPlayableB, 2)
	TheMerchant.AddItem(ClothesTuxedoHat, 2)
	TheMerchant.AddItem(Armor_Raider_Gasmask, 2)
	TheMerchant.AddItem(Armor_Gasmask, 2)
	TheMerchant.AddItem(ClothesPrewarWomensCasual2, 2)
	TheMerchant.AddItem(FFBK01GrandpaHat, 2)
	TheMerchant.AddItem(ClothesGrayKnitCap, 2)
	TheMerchant.AddItem(ClothesGreaserJacket, 2)
	TheMerchant.AddItem(Armor_Raider_GreenHood, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Green, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_greensleeves, 2)
	TheMerchant.AddItem(ClothesResident5Hat, 2)
	TheMerchant.AddItem(ClothesWastelandCommon, 2)
	TheMerchant.AddItem(DN160_ClothesGrognakCostume, 2)
	TheMerchant.AddItem(Armor_GunnerRustic_Underarmor, 2)
	TheMerchant.AddItem(Armor_GunnerGuard_UnderArmor, 2)
	TheMerchant.AddItem(Armor_GunnerRaider_Underwear2, 2)
	TheMerchant.AddItem(Armor_GunnerRaiderMod_Underarmor, 2)
	TheMerchant.AddItem(ClothesDog_BandanaGunnerCamo, 2)
	TheMerchant.AddItem(ClothesDog_BandanaGunnerGreen, 2)
	TheMerchant.AddItem(Armor_HardHat, 2)
	TheMerchant.AddItem(Armor_Raider_Underwear2, 2)
	TheMerchant.AddItem(Armor_HazmatSuit, 2)
	TheMerchant.AddItem(ClothesDog_ArmorHeavy, 2)
	TheMerchant.AddItem(Armor_Wastelander_Heavy_Helmet, 2)
	TheMerchant.AddItem(Armor_Wastelander_Medium_Helmet, 2)
	TheMerchant.AddItem(Clothes_InstituteWorkerwithHelmet, 2)
	TheMerchant.AddItem(ClothesResident6, 2)
	TheMerchant.AddItem(Armor_BoS_Ingram, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead, 2)
	TheMerchant.AddItem(Clothes_InstituteUniform, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat, 2)
	TheMerchant.AddItem(ClothesDog_BandanaMonkey, 2)
	TheMerchant.AddItem(ArmorKellogg, 2)
	TheMerchant.AddItem(ClothesScientist_VariantWasteland, 2)
	TheMerchant.AddItem(ClothesDeaconScientist, 2)
	TheMerchant.AddItem(ClothesPrewarHouseDress, 2)
	TheMerchant.AddItem(ClothesPrewarHouseDressA, 2)
	TheMerchant.AddItem(ClothesPreWarDressBlue, 2)
	TheMerchant.AddItem(ClothesPreWarDress, 2)
	TheMerchant.AddItem(ClothesDeaconPrewarShortSleeves, 2)
	TheMerchant.AddItem(ClothesPrewarShortSleeves, 2)
	TheMerchant.AddItem(ClothesPrewarHouseDressB, 2)
	TheMerchant.AddItem(ClothesPreWarDressPink, 2)
	TheMerchant.AddItem(ClothesDog_BandanaLeopard, 2)
	TheMerchant.AddItem(Armor_Highschool_JacketRed, 2)
	TheMerchant.AddItem(InstM02LiamsGlasses, 2)
	TheMerchant.AddItem(MS11LieutenantsHat, 2)
	TheMerchant.AddItem(ClothesDog_ArmorLight, 2)
	TheMerchant.AddItem(Armor_Raider_Underwear, 2)
	TheMerchant.AddItem(ClothesLongshoreman, 2)
	TheMerchant.AddItem(CheatTerminal_LorenzosCrown, 2)
	TheMerchant.AddItem(ClothesLorenzo, 2)
	TheMerchant.AddItem(ClothesMacCready, 2)
	TheMerchant.AddItem(ClothesMacCreadyHat, 2)
	TheMerchant.AddItem(DN028_Armor_Bear_Mascot, 2)
	TheMerchant.AddItem(ClothesMaxsonCoat, 2)
	TheMerchant.AddItem(ClothesRRJumpsuit, 2)
	TheMerchant.AddItem(ClothesGenericJumpsuit, 2)
	TheMerchant.AddItem(Armor_BoS_Science_Scribe_Helmet, 2)
	TheMerchant.AddItem(ClothesMilitaryCap, 2)
	TheMerchant.AddItem(ClothesMilitaryFatigues, 2)
	TheMerchant.AddItem(ClothesMinutemanHat, 2)
	TheMerchant.AddItem(ClothesDeaconMinutemanHat, 2)
	TheMerchant.AddItem(Armor_MiningHelmet, 2)
	TheMerchant.AddItem(ArmorPrestonGarveyHat, 2)
	TheMerchant.AddItem(ClothesMinutemanOutfit, 2)
	TheMerchant.AddItem(ArmorMinutemanGeneralHat, 2)
	TheMerchant.AddItem(ArmorMinutemanGeneral, 2)
	TheMerchant.AddItem(SkinMirelurkHunter, 2)
	TheMerchant.AddItem(SkinMirelurkHunterDirt, 2)
	TheMerchant.AddItem(SkinMirelurkHunterCaveMud, 2)
	TheMerchant.AddItem(SkinMirelurkHunterNestMud, 2)
	TheMerchant.AddItem(SkinMirelurkHunterSandRock, 2)
	TheMerchant.AddItem(SkinMirelurkHunterWater, 2)
	TheMerchant.AddItem(SkinMirelurkHunterGlow, 2)
	TheMerchant.AddItem(SkinMirelurkKingDirt, 2)
	TheMerchant.AddItem(SkinMirelurkKingWater, 2)
	TheMerchant.AddItem(ClothesNat, 2)
	TheMerchant.AddItem(ClothesResident7Hat, 2)
	TheMerchant.AddItem(ClothesDeaconResident7Hat, 2)
	TheMerchant.AddItem(DN015_Armor_Power_Torso, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Orange, 2)
	TheMerchant.AddItem(ClothesResident7, 2)
	TheMerchant.AddItem(ClothesDeaconResident7, 2)
	TheMerchant.AddItem(ClothesPastor, 2)
	TheMerchant.AddItem(ClothesMayor, 2)
	TheMerchant.AddItem(ClothesResident4, 2)
	TheMerchant.AddItem(ClothesPoliceGlasses, 2)
	TheMerchant.AddItem(ClothesDeaconWig, 2)
	TheMerchant.AddItem(ClothesWig, 2)
	TheMerchant.AddItem(ClothesPostmanHat, 2)
	TheMerchant.AddItem(ClothesPostman, 2)
	TheMerchant.AddItem(ClothesPiperCap, 2)
	TheMerchant.AddItem(Armor_BoS_Quinlan, 2)
	TheMerchant.AddItem(Armor_Wastelander_01_GlovesA, 2)
	TheMerchant.AddItem(ClothesWastelandDress, 2)
	TheMerchant.AddItem(ClothesDog_Bandana, 2)
	TheMerchant.AddItem(ClothesSlinkyDress, 2)
	TheMerchant.AddItem(Armor_FlightHelmetRed, 2)
	TheMerchant.AddItem(ClothesHancocksOutfit, 2)
	TheMerchant.AddItem(ClothesPiper, 2)
	TheMerchant.AddItem(MS10ClothesRewardM, 2)
	TheMerchant.AddItem(ClothesDog_DogCollarReinforced, 2)
	TheMerchant.AddItem(MS10ClothesRex, 2)
	TheMerchant.AddItem(ClothesPiperGoggles, 2)
	TheMerchant.AddItem(Armor_MacCready_Raider_Underarmor, 2)
	TheMerchant.AddItem(Armor_Raider_Underarmor, 2)
	TheMerchant.AddItem(Armor_Piper_Underarmor, 2)
	TheMerchant.AddItem(Armor_DeaconRaider_Underarmor, 2)
	TheMerchant.AddItem(Clothes_RaiderMod_Hood1, 2)
	TheMerchant.AddItem(Clothes_RaiderMod_Hood2, 2)
	TheMerchant.AddItem(Clothes_RaiderMod_Hood3, 2)
	TheMerchant.AddItem(Armor_Wastelander_02_NoHood_GlovesB, 2)
	TheMerchant.AddItem(Armor_BoS_Science_Scribe, 2)
	TheMerchant.AddItem(ClothesCaptainsHat, 2)
	TheMerchant.AddItem(Armor_VaultTecSecurityHelmetCovenant, 2)
	TheMerchant.AddItem(ClothesSequinDress, 2)
	TheMerchant.AddItem(MS04_SilverShroudArmor_15, 2)
	TheMerchant.AddItem(MS04_SilverShroudArmor_25, 2)
	TheMerchant.AddItem(MS04_SilverShroudArmor_35, 2)
	TheMerchant.AddItem(MS04_SilverShroudArmor_45, 2)
	TheMerchant.AddItem(MS04_SilverShroudCostume, 2)
	TheMerchant.AddItem(ClothesSilverShroud, 2)
	TheMerchant.AddItem(MS04_SilverShroudHat, 2)
	TheMerchant.AddItem(ClothesDog_BandanaSkull, 2)
	TheMerchant.AddItem(Armor_Wastelander_Medium, 2)
	TheMerchant.AddItem(ClothesDog_DogCollarSpiked, 2)
	TheMerchant.AddItem(ClothesDog_DogHeadMuzzle, 2)
	TheMerchant.AddItem(ClothesSquire, 2)
	TheMerchant.AddItem(ClothesDog_BandanaStarsNStripes, 2)
	TheMerchant.AddItem(ClothesDog_BandanaStripes, 2)
	TheMerchant.AddItem(MS02_ClothesSubmarineCrewHat, 2)
	TheMerchant.AddItem(MS02_ClothesSubmarineCrew, 2)
	TheMerchant.AddItem(ClothesSummerShorts, 2)
	TheMerchant.AddItem(ClothesDeaconSunGlasses, 2)
	TheMerchant.AddItem(ClothesSunGlasses, 2)
	TheMerchant.AddItem(Clothes_Raider_SurgicalMask, 2)
	TheMerchant.AddItem(ClothesResident5, 2)
	TheMerchant.AddItem(ClothesMobster02, 2)
	TheMerchant.AddItem(ClothesPrewarSweaterVest, 2)
	TheMerchant.AddItem(ClothesPrewarTshirtSlacks, 2)
	TheMerchant.AddItem(Armor_Raider_Suit_01_GlovesB, 2)
	TheMerchant.AddItem(ClothesTattered, 2)
	TheMerchant.AddItem(Armor_BoS_Teagan, 2)
	TheMerchant.AddItem(Armor_TinkerHeadgear, 2)
	TheMerchant.AddItem(Armor_Raider_Suit_02B_GlovesC, 2)
	TheMerchant.AddItem(ClothesHancocksHat, 2)
	TheMerchant.AddItem(ClothesMobsterhat, 2)
	TheMerchant.AddItem(ClothesResident4Hat, 2)
	TheMerchant.AddItem(ClothesTuxedo, 2)
	TheMerchant.AddItem(Armor_Highschool_UnderArmor, 2)
	TheMerchant.AddItem(Armor_DeaconHighschool_UnderArmor, 2)
	TheMerchant.AddItem(ClothesWinterHat, 2)
	TheMerchant.AddItem(ClothesSturgesBeltAddons, 2)
	TheMerchant.AddItem(ClothesDeaconMechanic, 2)
	TheMerchant.AddItem(Armor_Vault101_Underwear, 2)
	TheMerchant.AddItem(Armor_Vault101_UnderwearClean, 2)
	TheMerchant.AddItem(Armor_Vault111_Underwear, 2)
	TheMerchant.AddItem(Armor_Vault111_Underwear_NoLoot, 2)
	TheMerchant.AddItem(Armor_Vault111Clean_Underwear, 2)
	TheMerchant.AddItem(Armor_Vault114_Underwear, 2)
	TheMerchant.AddItem(Armor_Vault114_UnderwearClean, 2)
	TheMerchant.AddItem(Armor_Vault75_Underwear, 2)
	TheMerchant.AddItem(Armor_Vault75_UnderwearClean, 2)
	TheMerchant.AddItem(Armor_Vault81_Underwear, 2)
	TheMerchant.AddItem(Armor_Vault81_UnderwearChild, 2)
	TheMerchant.AddItem(Armor_DeaconVault81_Underwear, 2)
	TheMerchant.AddItem(Armor_Vault81_UnderwearClean, 2)
	TheMerchant.AddItem(Armor_VaultTecSecurity81, 2)
	TheMerchant.AddItem(Armor_Vault95_Underwear, 2)
	TheMerchant.AddItem(Armor_Vault95_Underwear_Clean, 2)
	TheMerchant.AddItem(ClothesVaultTecScientist, 2)
	TheMerchant.AddItem(Armor_VaultTecSecurity111, 2)
	TheMerchant.AddItem(Armor_VaultTecSecurity111_Clean, 2)
	TheMerchant.AddItem(Armor_VaultTecSecurityHelmet, 2)
	TheMerchant.AddItem(Armor_VaultTecSecurityHelmet_Clean, 2)
	TheMerchant.AddItem(SupermutantBlindfold, 2)
	TheMerchant.AddItem(ClothesDeaconMinutemanOutfit, 2)
	TheMerchant.AddItem(Armor_SpouseWeddingRing, 2)
	TheMerchant.AddItem(Armor_WeddingRing, 2)
	TheMerchant.AddItem(Clothes_Raider_Goggles, 2)
	TheMerchant.AddItem(Armor_BoS_Engineer_Scribe_Helmet, 2)
	TheMerchant.AddItem(Armor_Rustic_Jacket, 2)
	TheMerchant.AddItem(ClothesValentineHat, 2)
	TheMerchant.AddItem(ClothesDeaconMobsterHat, 2)
	TheMerchant.AddItem(Clothes_Metal_Shades, 2)
	TheMerchant.AddItem(Clothes_Raider_Hood, 2)
	TheMerchant.AddItem(ClothesVaultTecSalesmanHat, 2)
	TheMerchant.AddItem(Armor_FlightHelmet, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Yellow, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_yellowsleeves, 2)
	TheMerchant.AddItem(ClothesLongshoremanHat, 2)
	TheMerchant.AddItem(ClothesVaultTecSalesmanCoat, 2)
	TheMerchant.AddItem(DN054ZekesJacket, 2)
	TheMerchant.AddItem(Clothes_BoS_CaptainKells_Hat, 2)
	TheMerchant.AddItem(ClothesFatiguesPreWar, 2)
	TheMerchant.AddItem(ClothesGreaserJacketAtomCats, 2)
	TheMerchant.AddItem(BabyBundledRight, 2)
	TheMerchant.AddItem(ClothesBaseballHat, 2)
	TheMerchant.AddItem(ClothesBaseballUniform, 2)
	TheMerchant.AddItem(ClothesBathrobe, 2)
	TheMerchant.AddItem(ClothesResident3Hat, 2)
	TheMerchant.AddItem(ClothesMommaMurphy, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_blacksleeves, 2)
	TheMerchant.AddItem(ClothesMobster01, 2)
	TheMerchant.AddItem(ClothesDeaconMobster, 2)
	TheMerchant.AddItem(ClothesBlackRimGlasses, 2)
	TheMerchant.AddItem(ClothesDog_BandanaBlue, 2)
	TheMerchant.AddItem(Armor_Batting_HelmetBlue, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Blue, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_bluesleeves, 2)
	TheMerchant.AddItem(Armor_BoS_Knight_Lancer, 2)
	TheMerchant.AddItem(Armor_BoS_Knight_UnderarmorHelmet, 2)
	TheMerchant.AddItem(Armor_BoS_Officer_Underarmor, 2)
	TheMerchant.AddItem(Armor_BoS_Knight_Underarmor, 2)
	TheMerchant.AddItem(Armor_BoS_Soldier_Underarmor, 2)
	TheMerchant.AddItem(ClothesBowlerHat, 2)
	TheMerchant.AddItem(Armor_BoS_Soldier, 2)
	TheMerchant.AddItem(Armor_BoS_Cade, 2)
	TheMerchant.AddItem(Armor_BoS_Kells, 2)
	TheMerchant.AddItem(Armor_RR303BoSDisguise, 2)
	TheMerchant.AddItem(Armor_FlightHelmetBrown, 2)
	TheMerchant.AddItem(ClothesScientistCabot, 2)
	TheMerchant.AddItem(Armor_Wastelander_Heavy, 2)
	TheMerchant.AddItem(ClothesCaitBandolier, 2)
	TheMerchant.AddItem(MS11IronsidesHat, 2)
	TheMerchant.AddItem(ClothesPrewarWomensCasual, 2)
	TheMerchant.AddItem(ClothesPrewarShortSleevesAndSlacks, 2)
	TheMerchant.AddItem(ClothesChefsHat, 2)
	TheMerchant.AddItem(ClothesChildofAtom, 2)
	TheMerchant.AddItem(ClothesChildofAtomBrown2, 2)
	TheMerchant.AddItem(ClothesChildofAtomGreen2, 2)
	TheMerchant.AddItem(ClothesChildofAtomBrown, 2)
	TheMerchant.AddItem(ClothesChildofAtomGreen, 2)
	TheMerchant.AddItem(ClothesChildofAtomBrown3, 2)
	TheMerchant.AddItem(ClothesChildofAtomGreen3, 2)
	TheMerchant.AddItem(ClothesChildofAtom3, 2)
	TheMerchant.AddItem(ClothesChildofAtom2, 2)
	TheMerchant.AddItem(Clothes_InstituteChildUniform, 2)
	TheMerchant.AddItem(ClothesSuitClean_Black, 2)
	TheMerchant.AddItem(NewsmanSuitA, 2)
	TheMerchant.AddItem(ClothesSuitClean_Blue, 2)
	TheMerchant.AddItem(ClothesSuitClean_Grey, 2)
	TheMerchant.AddItem(ClothesSuitClean_Striped, 2)
	TheMerchant.AddItem(ClothesSuitClean_Tan, 2)
	TheMerchant.AddItem(Clothes_InstituteWorker, 2)
	TheMerchant.AddItem(ClothesCoastguardHat, 2)
	TheMerchant.AddItem(ArmorPrestonGarveyOutfit, 2)
	TheMerchant.AddItem(ClothesCait, 2)
	TheMerchant.AddItem(ClothesCourserJacket, 2)
	TheMerchant.AddItem(Armor_VaultTecSecurityCovenant, 2)
	TheMerchant.AddItem(RR_PrewarSweaterVest_01, 2)
	TheMerchant.AddItem(ClothesMayorHat, 2)
	TheMerchant.AddItem(Armor_HazmatSuitDamaged, 2)
	TheMerchant.AddItem(DBTech_Armor_Highschool_Jacket, 2)
	TheMerchant.AddItem(SkinMirelurkDeepKing, 2)
	TheMerchant.AddItem(SkinMirelurkGlowingKing, 2)
	TheMerchant.AddItem(ClothesFatiguesPostWar, 2)
	TheMerchant.AddItem(Armor_Army_Helmet_postwar, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_blacksleeves_dirty, 2)
	TheMerchant.AddItem(ClothesSuitDirty_Black, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Dirty_Blue, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_bluesleeves_dirty, 2)
	TheMerchant.AddItem(ClothesSuitDirty_Blue, 2)
	TheMerchant.AddItem(ClothesVaultTecSalesmanHatPW, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Dirty_Green, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_greensleeves_dirty, 2)
	TheMerchant.AddItem(ClothesSuitDirty_Grey, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Dirty, 2)
	TheMerchant.AddItem(Clothes_InstituteUniform_Dirty, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_Dirty, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Dirty_Orange, 2)
	TheMerchant.AddItem(ClothesPostmanHatPostwar, 2)
	TheMerchant.AddItem(ClothesPostmanPostwar, 2)
	TheMerchant.AddItem(ClothesSuitDirty_Striped, 2)
	TheMerchant.AddItem(ClothesSuitDirty_Tan, 2)
	TheMerchant.AddItem(ClothesVaultTecSalesmanCoatPW, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Dirty_Yellow, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_yellowsleeves_dirty, 2)
	TheMerchant.AddItem(ClothesDog_ArmorMedium, 2)
	TheMerchant.AddItem(ClothesDog_DogCollar, 2)
	TheMerchant.AddItem(ClothesDog_DogHeadHelmet, 2)
	TheMerchant.AddItem(ClothesDog_DogCollarDouble, 2)
	TheMerchant.AddItem(Armor_Wastelander_Light, 2)
	TheMerchant.AddItem(Armor_BoS_Engineer_Scribe, 2)
	TheMerchant.AddItem(Armor_Wastelander_02_Hood_GlovesB, 2)
	TheMerchant.AddItem(ClothesEyeGlasses, 2)
	TheMerchant.AddItem(ClothesValentineCoat, 2)
	TheMerchant.AddItem(ClothesVisor, 2)
	TheMerchant.AddItem(ClothesResident3, 2)
	TheMerchant.AddItem(ClothesDeaconResident3, 2)
	TheMerchant.AddItem(ClothesFancyGlasses, 2)
	TheMerchant.AddItem(ClothesFather, 2)
	TheMerchant.AddItem(ClothesDesdemona, 2)
	TheMerchant.AddItem(ClothesIrmaDress, 2)
	TheMerchant.AddItem(Armor_BoS_Field_Scribe, 2)
	TheMerchant.AddItem(Armor_BoS_Field_Scribe_Helmet, 2)
	TheMerchant.AddItem(Armor_Rustic_Underarmor, 2)
	TheMerchant.AddItem(Armor_DeaconRustic_Underarmor, 2)
	TheMerchant.AddItem(Armor_BoS_Knight_Lancer_Helmet, 2)
	TheMerchant.AddItem(ClothesPrewarHouseDress01NonPlayable, 2)
	TheMerchant.AddItem(ClothesPrewarHouseDress01NonPlayableA, 2)
	TheMerchant.AddItem(ClothesPrewarHouseDress01NonPlayableB, 2)
	TheMerchant.AddItem(ClothesTuxedoHat, 2)
	TheMerchant.AddItem(Armor_Raider_Gasmask, 2)
	TheMerchant.AddItem(Armor_Gasmask, 2)
	TheMerchant.AddItem(ClothesPrewarWomensCasual2, 2)
	TheMerchant.AddItem(FFBK01GrandpaHat, 2)
	TheMerchant.AddItem(ClothesGrayKnitCap, 2)
	TheMerchant.AddItem(ClothesGreaserJacket, 2)
	TheMerchant.AddItem(Armor_Raider_GreenHood, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Green, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_greensleeves, 2)
	TheMerchant.AddItem(ClothesResident5Hat, 2)
	TheMerchant.AddItem(ClothesWastelandCommon, 2)
	TheMerchant.AddItem(DN160_ClothesGrognakCostume, 2)
	TheMerchant.AddItem(Armor_GunnerRustic_Underarmor, 2)
	TheMerchant.AddItem(Armor_GunnerGuard_UnderArmor, 2)
	TheMerchant.AddItem(Armor_GunnerRaider_Underwear2, 2)
	TheMerchant.AddItem(Armor_GunnerRaiderMod_Underarmor, 2)
	TheMerchant.AddItem(ClothesDog_BandanaGunnerCamo, 2)
	TheMerchant.AddItem(ClothesDog_BandanaGunnerGreen, 2)
	TheMerchant.AddItem(Armor_HardHat, 2)
	TheMerchant.AddItem(Armor_Raider_Underwear2, 2)
	TheMerchant.AddItem(Armor_HazmatSuit, 2)
	TheMerchant.AddItem(ClothesDog_ArmorHeavy, 2)
	TheMerchant.AddItem(Armor_Wastelander_Heavy_Helmet, 2)
	TheMerchant.AddItem(Armor_Wastelander_Medium_Helmet, 2)
	TheMerchant.AddItem(Clothes_InstituteWorkerwithHelmet, 2)
	TheMerchant.AddItem(ClothesResident6, 2)
	TheMerchant.AddItem(Armor_BoS_Ingram, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead, 2)
	TheMerchant.AddItem(Clothes_InstituteUniform, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat, 2)
	TheMerchant.AddItem(ClothesDog_BandanaMonkey, 2)
	TheMerchant.AddItem(ArmorKellogg, 2)
	TheMerchant.AddItem(ClothesScientist_VariantWasteland, 2)
	TheMerchant.AddItem(ClothesDeaconScientist, 2)
	TheMerchant.AddItem(ClothesPrewarHouseDress, 2)
	TheMerchant.AddItem(ClothesPrewarHouseDressA, 2)
	TheMerchant.AddItem(ClothesPreWarDressBlue, 2)
	TheMerchant.AddItem(ClothesPreWarDress, 2)
	TheMerchant.AddItem(ClothesDeaconPrewarShortSleeves, 2)
	TheMerchant.AddItem(ClothesPrewarShortSleeves, 2)
	TheMerchant.AddItem(ClothesPrewarHouseDressB, 2)
	TheMerchant.AddItem(ClothesPreWarDressPink, 2)
	TheMerchant.AddItem(ClothesDog_BandanaLeopard, 2)
	TheMerchant.AddItem(Armor_Highschool_JacketRed, 2)
	TheMerchant.AddItem(InstM02LiamsGlasses, 2)
	TheMerchant.AddItem(MS11LieutenantsHat, 2)
	TheMerchant.AddItem(ClothesDog_ArmorLight, 2)
	TheMerchant.AddItem(Armor_Raider_Underwear, 2)
	TheMerchant.AddItem(ClothesLongshoreman, 2)
	TheMerchant.AddItem(CheatTerminal_LorenzosCrown, 2)
	TheMerchant.AddItem(ClothesLorenzo, 2)
	TheMerchant.AddItem(ClothesMacCready, 2)
	TheMerchant.AddItem(ClothesMacCreadyHat, 2)
	TheMerchant.AddItem(DN028_Armor_Bear_Mascot, 2)
	TheMerchant.AddItem(ClothesMaxsonCoat, 2)
	TheMerchant.AddItem(ClothesRRJumpsuit, 2)
	TheMerchant.AddItem(ClothesGenericJumpsuit, 2)
	TheMerchant.AddItem(Armor_BoS_Science_Scribe_Helmet, 2)
	TheMerchant.AddItem(ClothesMilitaryCap, 2)
	TheMerchant.AddItem(ClothesMilitaryFatigues, 2)
	TheMerchant.AddItem(ClothesMinutemanHat, 2)
	TheMerchant.AddItem(ClothesDeaconMinutemanHat, 2)
	TheMerchant.AddItem(Armor_MiningHelmet, 2)
	TheMerchant.AddItem(ArmorPrestonGarveyHat, 2)
	TheMerchant.AddItem(ClothesMinutemanOutfit, 2)
	TheMerchant.AddItem(ArmorMinutemanGeneralHat, 2)
	TheMerchant.AddItem(ArmorMinutemanGeneral, 2)
	TheMerchant.AddItem(SkinMirelurkHunter, 2)
	TheMerchant.AddItem(SkinMirelurkHunterDirt, 2)
	TheMerchant.AddItem(SkinMirelurkHunterCaveMud, 2)
	TheMerchant.AddItem(SkinMirelurkHunterNestMud, 2)
	TheMerchant.AddItem(SkinMirelurkHunterSandRock, 2)
	TheMerchant.AddItem(SkinMirelurkHunterWater, 2)
	TheMerchant.AddItem(SkinMirelurkHunterGlow, 2)
	TheMerchant.AddItem(SkinMirelurkKingDirt, 2)
	TheMerchant.AddItem(SkinMirelurkKingWater, 2)
	TheMerchant.AddItem(ClothesNat, 2)
	TheMerchant.AddItem(ClothesResident7Hat, 2)
	TheMerchant.AddItem(ClothesDeaconResident7Hat, 2)
	TheMerchant.AddItem(DN015_Armor_Power_Torso, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Orange, 2)
	TheMerchant.AddItem(ClothesResident7, 2)
	TheMerchant.AddItem(ClothesDeaconResident7, 2)
	TheMerchant.AddItem(ClothesPastor, 2)
	TheMerchant.AddItem(ClothesMayor, 2)
	TheMerchant.AddItem(ClothesResident4, 2)
	TheMerchant.AddItem(ClothesPoliceGlasses, 2)
	TheMerchant.AddItem(ClothesDeaconWig, 2)
	TheMerchant.AddItem(ClothesWig, 2)
	TheMerchant.AddItem(ClothesPostmanHat, 2)
	TheMerchant.AddItem(ClothesPostman, 2)
	TheMerchant.AddItem(ClothesPiperCap, 2)
	TheMerchant.AddItem(Armor_BoS_Quinlan, 2)
	TheMerchant.AddItem(Armor_Wastelander_01_GlovesA, 2)
	TheMerchant.AddItem(ClothesWastelandDress, 2)
	TheMerchant.AddItem(ClothesDog_Bandana, 2)
	TheMerchant.AddItem(ClothesSlinkyDress, 2)
	TheMerchant.AddItem(Armor_FlightHelmetRed, 2)
	TheMerchant.AddItem(ClothesHancocksOutfit, 2)
	TheMerchant.AddItem(ClothesPiper, 2)
	TheMerchant.AddItem(MS10ClothesRewardM, 2)
	TheMerchant.AddItem(ClothesDog_DogCollarReinforced, 2)
	TheMerchant.AddItem(MS10ClothesRex, 2)
	TheMerchant.AddItem(ClothesPiperGoggles, 2)
	TheMerchant.AddItem(Armor_MacCready_Raider_Underarmor, 2)
	TheMerchant.AddItem(Armor_Raider_Underarmor, 2)
	TheMerchant.AddItem(Armor_Piper_Underarmor, 2)
	TheMerchant.AddItem(Armor_DeaconRaider_Underarmor, 2)
	TheMerchant.AddItem(Clothes_RaiderMod_Hood1, 2)
	TheMerchant.AddItem(Clothes_RaiderMod_Hood2, 2)
	TheMerchant.AddItem(Clothes_RaiderMod_Hood3, 2)
	TheMerchant.AddItem(Armor_Wastelander_02_NoHood_GlovesB, 2)
	TheMerchant.AddItem(Armor_BoS_Science_Scribe, 2)
	TheMerchant.AddItem(ClothesCaptainsHat, 2)
	TheMerchant.AddItem(Armor_VaultTecSecurityHelmetCovenant, 2)
	TheMerchant.AddItem(ClothesSequinDress, 2)
	TheMerchant.AddItem(MS04_SilverShroudArmor_15, 2)
	TheMerchant.AddItem(MS04_SilverShroudArmor_25, 2)
	TheMerchant.AddItem(MS04_SilverShroudArmor_35, 2)
	TheMerchant.AddItem(MS04_SilverShroudArmor_45, 2)
	TheMerchant.AddItem(MS04_SilverShroudCostume, 2)
	TheMerchant.AddItem(ClothesSilverShroud, 2)
	TheMerchant.AddItem(MS04_SilverShroudHat, 2)
	TheMerchant.AddItem(ClothesDog_BandanaSkull, 2)
	TheMerchant.AddItem(Armor_Wastelander_Medium, 2)
	TheMerchant.AddItem(ClothesDog_DogCollarSpiked, 2)
	TheMerchant.AddItem(ClothesDog_DogHeadMuzzle, 2)
	TheMerchant.AddItem(ClothesSquire, 2)
	TheMerchant.AddItem(ClothesDog_BandanaStarsNStripes, 2)
	TheMerchant.AddItem(ClothesDog_BandanaStripes, 2)
	TheMerchant.AddItem(MS02_ClothesSubmarineCrewHat, 2)
	TheMerchant.AddItem(MS02_ClothesSubmarineCrew, 2)
	TheMerchant.AddItem(ClothesSummerShorts, 2)
	TheMerchant.AddItem(ClothesDeaconSunGlasses, 2)
	TheMerchant.AddItem(ClothesSunGlasses, 2)
	TheMerchant.AddItem(Clothes_Raider_SurgicalMask, 2)
	TheMerchant.AddItem(ClothesResident5, 2)
	TheMerchant.AddItem(ClothesMobster02, 2)
	TheMerchant.AddItem(ClothesPrewarSweaterVest, 2)
	TheMerchant.AddItem(ClothesPrewarTshirtSlacks, 2)
	TheMerchant.AddItem(Armor_Raider_Suit_01_GlovesB, 2)
	TheMerchant.AddItem(ClothesTattered, 2)
	TheMerchant.AddItem(Armor_BoS_Teagan, 2)
	TheMerchant.AddItem(Armor_TinkerHeadgear, 2)
	TheMerchant.AddItem(Armor_Raider_Suit_02B_GlovesC, 2)
	TheMerchant.AddItem(ClothesHancocksHat, 2)
	TheMerchant.AddItem(ClothesMobsterhat, 2)
	TheMerchant.AddItem(ClothesResident4Hat, 2)
	TheMerchant.AddItem(ClothesTuxedo, 2)
	TheMerchant.AddItem(Armor_Highschool_UnderArmor, 2)
	TheMerchant.AddItem(Armor_DeaconHighschool_UnderArmor, 2)
	TheMerchant.AddItem(ClothesWinterHat, 2)
	TheMerchant.AddItem(ClothesSturgesBeltAddons, 2)
	TheMerchant.AddItem(ClothesDeaconMechanic, 2)
	TheMerchant.AddItem(Armor_Vault101_Underwear, 2)
	TheMerchant.AddItem(Armor_Vault101_UnderwearClean, 2)
	TheMerchant.AddItem(Armor_Vault111_Underwear, 2)
	TheMerchant.AddItem(Armor_Vault111_Underwear_NoLoot, 2)
	TheMerchant.AddItem(Armor_Vault111Clean_Underwear, 2)
	TheMerchant.AddItem(Armor_Vault114_Underwear, 2)
	TheMerchant.AddItem(Armor_Vault114_UnderwearClean, 2)
	TheMerchant.AddItem(Armor_Vault75_Underwear, 2)
	TheMerchant.AddItem(Armor_Vault75_UnderwearClean, 2)
	TheMerchant.AddItem(Armor_Vault81_Underwear, 2)
	TheMerchant.AddItem(Armor_Vault81_UnderwearChild, 2)
	TheMerchant.AddItem(Armor_DeaconVault81_Underwear, 2)
	TheMerchant.AddItem(Armor_Vault81_UnderwearClean, 2)
	TheMerchant.AddItem(Armor_VaultTecSecurity81, 2)
	TheMerchant.AddItem(Armor_Vault95_Underwear, 2)
	TheMerchant.AddItem(Armor_Vault95_Underwear_Clean, 2)
	TheMerchant.AddItem(ClothesVaultTecScientist, 2)
	TheMerchant.AddItem(Armor_VaultTecSecurity111, 2)
	TheMerchant.AddItem(Armor_VaultTecSecurity111_Clean, 2)
	TheMerchant.AddItem(Armor_VaultTecSecurityHelmet, 2)
	TheMerchant.AddItem(Armor_VaultTecSecurityHelmet_Clean, 2)
	TheMerchant.AddItem(SupermutantBlindfold, 2)
	TheMerchant.AddItem(ClothesDeaconMinutemanOutfit, 2)
	TheMerchant.AddItem(Armor_SpouseWeddingRing, 2)
	TheMerchant.AddItem(Armor_WeddingRing, 2)
	TheMerchant.AddItem(Clothes_Raider_Goggles, 2)
	TheMerchant.AddItem(Armor_BoS_Engineer_Scribe_Helmet, 2)
	TheMerchant.AddItem(Armor_Rustic_Jacket, 2)
	TheMerchant.AddItem(ClothesValentineHat, 2)
	TheMerchant.AddItem(ClothesDeaconMobsterHat, 2)
	TheMerchant.AddItem(Clothes_Metal_Shades, 2)
	TheMerchant.AddItem(Clothes_Raider_Hood, 2)
	TheMerchant.AddItem(ClothesVaultTecSalesmanHat, 2)
	TheMerchant.AddItem(Armor_FlightHelmet, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoatDivisionHead_Yellow, 2)
	TheMerchant.AddItem(Clothes_InstituteLabCoat_yellowsleeves, 2)
	TheMerchant.AddItem(ClothesLongshoremanHat, 2)
	TheMerchant.AddItem(ClothesVaultTecSalesmanCoat, 2)
	TheMerchant.AddItem(DN054ZekesJacket, 2)
	Form formFromMod = Game.GetFormFromFile(0x000570D3, "DLCCoast.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000305BE, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000570DA, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00048234, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004885A, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000914B, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000914E, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000914F, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000570D4, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000570D9, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000391E8, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000391E6, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000540FC, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00046027, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004E698, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000247C5, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004B9B1, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0003A556, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00046024, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004262B, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000247C8, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002E026, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004B9B3, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0005159B, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00043331, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000914C, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
	endIf
	formFromMod = Game.GetFormFromFile(0x00042323, "DLCNukaWorld.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000417D0, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00044DA5, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00044DA6, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00029C0F, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00029C10, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000424A1, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00044DA8, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00029C12, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004E283, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004E284, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004E285, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004E282, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004E281, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004E27F, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027422, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004E280, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00042322, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00029C0B, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00028745, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00028747, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00028746, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00028744, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000520CF, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027423, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00029C06, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00029C0C, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00038517, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0003407C, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004A6A1, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00012342, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00029C0D, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00044DA4, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00044DA3, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00044DA7, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0003578E, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00029C11, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00029C0A, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0003407D, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027425, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027426, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004231F, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027424, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000D2AE, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000296B8, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BA9, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BA7, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00042325, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00029C08, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00029C09, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000392AE, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BAA, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BA8, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004E26F, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
	endIf
EndFunction
Function AddPowerArmorPieces()
	TheMerchant.AddItem(Armor_Power_X01_Helm, 2)
	TheMerchant.AddItem(Armor_Power_X01_ArmLeft, 2)
	TheMerchant.AddItem(Armor_Power_X01_LegLeft, 2)
	TheMerchant.AddItem(Armor_Power_X01_ArmRight, 2)
	TheMerchant.AddItem(Armor_Power_X01_LegRight, 2)
	TheMerchant.AddItem(Armor_Power_X01_Torso, 2)
	TheMerchant.AddItem(Armor_Power_T45_Torso, 2)
	TheMerchant.AddItem(Armor_Power_T45_Helm, 2)
	TheMerchant.AddItem(Armor_Power_T45_ArmLeft, 2)
	TheMerchant.AddItem(Armor_Power_T45_LegLeft, 2)
	TheMerchant.AddItem(Armor_Power_T45_ArmRight, 2)
	TheMerchant.AddItem(Armor_Power_T45_LegRight, 2)
	TheMerchant.AddItem(Armor_Power_T51_Torso, 2)
	TheMerchant.AddItem(Armor_Power_T51_Helmet, 2)
	TheMerchant.AddItem(Armor_Power_T51_ArmLeft, 2)
	TheMerchant.AddItem(Armor_Power_T51_LegLeft, 2)
	TheMerchant.AddItem(Armor_Power_T51_ArmRight, 2)
	TheMerchant.AddItem(Armor_Power_T51_LegRight, 2)
	TheMerchant.AddItem(Armor_Power_T60_Torso, 2)
	TheMerchant.AddItem(Armor_Power_T60_Helm, 2)
	TheMerchant.AddItem(Armor_Power_T60_ArmLeft, 2)
	TheMerchant.AddItem(Armor_Power_T60_LegLeft, 2)
	TheMerchant.AddItem(Armor_Power_T60_ArmRight, 2)
	TheMerchant.AddItem(Armor_Power_T60_LegRight, 2)
	Form formFromMod = Game.GetFormFromFile(0x0000C57B, "DLCRobot.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000C57A, "DLCRobot.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000C576, "DLCRobot.esm")
		TheMerchant.AddItem(formFromMod, 2)
	endIf
	formFromMod = Game.GetFormFromFile(0x00043C79, "DLCCoast.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00043C7F, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00043C76, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00043C7C, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00043C74, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00043C7A, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00043C77, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00043C7D, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00043C75, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00043C7B, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00043C78, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00043C7E, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
	endIf
	formFromMod = Game.GetFormFromFile(0x0001151B, "DLCNukaWorld.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00011515, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00011511, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00011517, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00011513, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00011519, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00040CFC, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00040CFA, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00040CFD, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00040CFB, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00040CFE, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00040CFF, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000DC2B, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000DC29, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000DC27, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000DC2C, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000DC28, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000DC2A, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
	endIf
EndFunction
Function AddArmor()
	TheMerchant.AddItem(RailroadArmoredCoat, 2)
	TheMerchant.AddItem(Armor_Railroad01, 2)
	TheMerchant.AddItem(Armor_Railroad02, 2)
	TheMerchant.AddItem(Armor_Railroad03, 2)
	TheMerchant.AddItem(Armor_Railroad04, 2)
	TheMerchant.AddItem(Armor_Railroad05, 2)
	TheMerchant.AddItem(Armor_Synth_Torso, 2)
	TheMerchant.AddItem(Armor_Synth_Helmet_Closed, 2)
	TheMerchant.AddItem(Armor_Synth_Helmet_Open, 2)
	TheMerchant.AddItem(Armor_Synth_ArmLeft, 2)
	TheMerchant.AddItem(Armor_Synth_LegLeft, 2)
	TheMerchant.AddItem(Armor_Synth_ArmRight, 2)
	TheMerchant.AddItem(Armor_Synth_LegRight, 2)
	TheMerchant.AddItem(Armor_Synth_Underarmor, 2)
	TheMerchant.AddItem(Armor_Synth_Underarmor_forSynths, 2)
	TheMerchant.AddItem(Armor_DCGuard_Helmet02, 2)
	TheMerchant.AddItem(Armor_DCGuard_Helmet01, 2)
	TheMerchant.AddItem(Armor_DCGuard_LArm, 2)
	TheMerchant.AddItem(Armor_DeaconDCGuard_LArm, 2)
	TheMerchant.AddItem(Armor_DCGuard_LArmLower, 2)
	TheMerchant.AddItem(Armor_DCGuard_LArmUpper, 2)
	TheMerchant.AddItem(Armor_DCGuard_RArm, 2)
	TheMerchant.AddItem(Armor_DeaconDCGuard_RArm, 2)
	TheMerchant.AddItem(Armor_DCGuard_RArmLower, 2)
	TheMerchant.AddItem(Armor_DCGuard_RArmUpper, 2)
	TheMerchant.AddItem(Armor_DCGuard_TorsoArmor, 2)
	TheMerchant.AddItem(Armor_DeaconDCGuard_TorsoArmor, 2)
	TheMerchant.AddItem(Armor_Combat_Torso, 2)
	TheMerchant.AddItem(Armor_Combat_Helmet, 2)
	TheMerchant.AddItem(Armor_Combat_ArmLeft, 2)
	TheMerchant.AddItem(Armor_Combat_LegLeft, 2)
	TheMerchant.AddItem(Armor_Combat_ArmRight, 2)
	TheMerchant.AddItem(Armor_Combat_LegRight, 2)
	TheMerchant.AddItem(Armor_Leather_Torso, 2)
	TheMerchant.AddItem(Armor_Leather_TorsoE3, 2)
	TheMerchant.AddItem(Armor_Raider_Suit_02A_GlovesC, 2)
	TheMerchant.AddItem(Armor_Leather_ArmLeft, 2)
	TheMerchant.AddItem(Armor_Leather_LegLeft, 2)
	TheMerchant.AddItem(Armor_Leather_ArmRight, 2)
	TheMerchant.AddItem(Armor_Leather_ArmRightE3, 2)
	TheMerchant.AddItem(Armor_Leather_LegRight, 2)
	TheMerchant.AddItem(Armor_DeaconLeather_LegRight, 2)
	TheMerchant.AddItem(Armor_Metal_Torso, 2)
	TheMerchant.AddItem(Armor_Metal_Helmet, 2)
	TheMerchant.AddItem(Armor_Metal_ArmLeft, 2)
	TheMerchant.AddItem(Armor_Metal_LegLeft, 2)
	TheMerchant.AddItem(Armor_Metal_ArmRight, 2)
	TheMerchant.AddItem(Armor_Metal_LegRight, 2)
	TheMerchant.AddItem(Armor_RaiderMod_Torso, 2)
	TheMerchant.AddItem(Armor_RaiderMod_Underarmor, 2)
	TheMerchant.AddItem(Armor_RaiderMod_ArmLeft, 2)
	TheMerchant.AddItem(Armor_RaiderMod_LegLeft, 2)
	TheMerchant.AddItem(Armor_Power_Raider_Torso, 2)
	TheMerchant.AddItem(Armor_Power_Raider_Helm, 2)
	TheMerchant.AddItem(Armor_Power_Raider_ArmLeft, 2)
	TheMerchant.AddItem(Armor_Power_Raider_LegLeft, 2)
	TheMerchant.AddItem(Armor_Power_Raider_ArmRight, 2)
	TheMerchant.AddItem(Armor_Power_Raider_LegRight, 2)
	TheMerchant.AddItem(Armor_RaiderMod_ArmRight, 2)
	TheMerchant.AddItem(Armor_RaiderMod_LegRight,2)
	Form formFromMod = Game.GetFormFromFile(0x0000864A, "DLCRobot.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000864C, "DLCRobot.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00008BC1, "DLCRobot.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00008BC3, "DLCRobot.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000863F, "DLCRobot.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00008644, "DLCRobot.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00008648, "DLCRobot.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00008642, "DLCRobot.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00008646, "DLCRobot.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000864E, "DLCRobot.esm")
		TheMerchant.AddItem(formFromMod, 2)
	endIf
	formFromMod = Game.GetFormFromFile(0x0000F04E, "DLCCoast.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000F04A, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000F04C, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000F04B, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000F04D, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004FA89, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00009E5B, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00009E58, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00009E56, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00009E59, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00009E57, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00009E5A, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0003A557, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000EE79, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000EA36, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000EE75, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000EE77, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000EE76, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000EE78, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004FA88, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
	endIf
	formFromMod = Game.GetFormFromFile(0x0002770D, "DLCNukaWorld.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002770B, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027410, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027707, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027709, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027414, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002770A, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027708, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027705, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0003B8A9, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027419, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002770E, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027706, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002740B, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027415, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002770C, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002740F, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002741F, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002740D, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027412, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027417, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00027421, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002741E, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002741A, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002741C, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00039568, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00039562, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00028743, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0003B8AE, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002873E, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002873D, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00028738, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00028731, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00028730, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00028735, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00028734, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00028733, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00028732, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00028737, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00028736, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0003B8AA, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00028739, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002873F, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0003B8AB, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002873B, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00028741, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0003B554, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BB7, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BB5, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BB0, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BBD, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0003B557, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BB8, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BC1, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BBC, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BBF, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BBA, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BB6, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BC0, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BBB, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BBE, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BB9, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BAB, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0003B559, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0003B556, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0003B558, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0003B555, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BB4, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BAF, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BB2, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00026BAD, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
	endIf
EndFunction
Function AddWeapons()
	TheMerchant.AddItem(magnum, 2)
	TheMerchant.AddItem(tenmm, 2)
	TheMerchant.AddItem(AlienBlaster, 2)
	TheMerchant.AddItem(ArtillerySmokeFlare, 2)
	TheMerchant.AddItem(AssaultRifle, 2)
	TheMerchant.AddItem(BaseballBat, 2)
	TheMerchant.AddItem(Baton, 2)
	TheMerchant.AddItem(Board, 2)
	TheMerchant.AddItem(BoxingGlove, 2)
	TheMerchant.AddItem(Broadsider, 2)
	TheMerchant.AddItem(ChineseOfficerSword, 2)
	TheMerchant.AddItem(Knife, 2)
	TheMerchant.AddItem(CombatRifle, 2)
	TheMerchant.AddItem(CombatShotgun, 2)
	TheMerchant.AddItem(Cryolator, 2)
	TheMerchant.AddItem(DeathclawGauntlet, 2)
	TheMerchant.AddItem(Deliverer, 2)
	TheMerchant.AddItem(DoubleBarrelShotgun, 2)
	TheMerchant.AddItem(Fatman, 2)
	TheMerchant.AddItem(Flamer, 2)
	TheMerchant.AddItem(FlareGun, 2)
	TheMerchant.AddItem(FusionCoreWeapon, 2)
	TheMerchant.AddItem(GammaGun, 2)
	TheMerchant.AddItem(GatlingLaser, 2)
	TheMerchant.AddItem(GaussRifle, 2)
	TheMerchant.AddItem(HuntingRifle, 2)
	TheMerchant.AddItem(InstituteLaserGun, 2)
	TheMerchant.AddItem(JunkJet, 2)
	TheMerchant.AddItem(Knuckles, 2)
	TheMerchant.AddItem(LaserGun, 2)
	TheMerchant.AddItem(LaserMusket, 2)
	TheMerchant.AddItem(LeadPipe, 2)
	TheMerchant.AddItem(Machete, 2)
	TheMerchant.AddItem(Minigun, 2)
	TheMerchant.AddItem(MissileLauncher, 2)
	TheMerchant.AddItem(MolotovCocktail, 2)
	TheMerchant.AddItem(PipeGun, 2)
	TheMerchant.AddItem(PipeBoltAction, 2)
	TheMerchant.AddItem(PipeRevolver, 2)
	TheMerchant.AddItem(PipeWrench, 2)
	TheMerchant.AddItem(PlasmaGun, 2)
	TheMerchant.AddItem(PoolCue, 2)
	TheMerchant.AddItem(PowerFist, 2)
	TheMerchant.AddItem(RailwayRifle, 2)
	TheMerchant.AddItem(RevolutionarySword, 2)
	TheMerchant.AddItem(Ripper, 2)
	TheMerchant.AddItem(RollingPin, 2)
	TheMerchant.AddItem(Shishkebab, 2)
	TheMerchant.AddItem(Sledgehammer, 2)
	TheMerchant.AddItem(SubmachineGun, 2)
	TheMerchant.AddItem(SuperSledge, 2)
	TheMerchant.AddItem(Switchblade, 2)
	TheMerchant.AddItem(PipeSyringer, 2)
	TheMerchant.AddItem(TireIron, 2)
	Form formFromMod = Game.GetFormFromFile(0x00000F99, "9mmpistol.esp")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 2)
	endIf
	formFromMod = Game.GetFormFromFile(0x00008B8E, "DLCRobot.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000020F8, "DLCRobot.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000100B7, "DLCRobot.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00003E07, "DLCRobot.esm")
		TheMerchant.AddItem(formFromMod, 2)
	endIf
	formFromMod = Game.GetFormFromFile(0x00010B81, "DLCCoast.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0002C8B0, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004B5F4, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000540ED, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00009582, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00025FC4, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 2)
	endIf
	formFromMod = Game.GetFormFromFile(0x000415B3, "DLCNukaWorld.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00007BC8, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00046A29, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0000A6C0, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00033B60, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0001AABC, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00052926, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00052215, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000396A7, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00007BC6, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0005221E, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
	endIf
EndFunction
Function AddThrowables()
	TheMerchant.AddItem(BaseballGrenade, 100)
	TheMerchant.AddItem(BottlecapMine, 100)
	TheMerchant.AddItem(CryoMine, 100)
	TheMerchant.AddItem(CryoGrenade, 100)
	TheMerchant.AddItem(fragGrenade, 100)
	TheMerchant.AddItem(fragMine, 100)
	TheMerchant.AddItem(InstituteBeaconGrenade, 100)
	TheMerchant.AddItem(PulseGrenadeInstitute, 100)
	TheMerchant.AddItem(MolotovCocktail, 100)
	TheMerchant.AddItem(nukaGrenade, 100)
	TheMerchant.AddItem(NukeMine, 100)
	TheMerchant.AddItem(PlasmaGrenade, 100)
	TheMerchant.AddItem(PlasmaMine, 100)
	TheMerchant.AddItem(PulseGrenade, 100)
	TheMerchant.AddItem(PulseMine, 100)
	TheMerchant.AddItem(SynthTeleportGrenade, 100)
	Form formFromMod = Game.GetFormFromFile(0x0000211C, "DLCRobot.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 100)
		formFromMod = Game.GetFormFromFile(0x00002120, "DLCRobot.esm")
		TheMerchant.AddItem(formFromMod, 100)
	endIf
	formFromMod = Game.GetFormFromFile(0x00054072, "DLCCoast.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 100)
		formFromMod = Game.GetFormFromFile(0x000540F7, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 100)
		formFromMod = Game.GetFormFromFile(0x0005406E, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 100)
	endIf
	formFromMod = Game.GetFormFromFile(0x0002618B, "DLCNukaWorld.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00033905, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x0004A619, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x000346FC, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00025B0A, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00023E60, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00040CDE, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00040CDD, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
		formFromMod = Game.GetFormFromFile(0x00035E75, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 2)
	endIf
EndFunction
Function AddAid()
	if ( HC_Vendor_Antiboitic_ChanceNone.GetValue() == 0 )
		TheMerchant.AddItem(HC_Antibiotics, 200)
		TheMerchant.AddItem(HC_Herbal_Anodyne, 200)
		TheMerchant.AddItem(HC_Herbal_Antimicrobial, 200)
		TheMerchant.AddItem(HC_Herbal_Stimulant, 200)
		TheMerchant.AddItem(HC_SippableWater, 200)
		TheMerchant.AddItem(HC_SippableDirtyWater, 200)
	else
		TheMerchant.AddItem(WaterPurified, 200)
		TheMerchant.AddItem(WaterDirty, 200)
	endIf
	TheMerchant.AddItem(Addictol, 200)
	TheMerchant.AddItem(MelonWildGS, 200)
	TheMerchant.AddItem(SteakBloatfly, 200)
	TheMerchant.AddItem(BeerBottleStandard01, 200)
	TheMerchant.AddItem(BerryMentats, 200)
	TheMerchant.AddItem(Berserk, 200)
	TheMerchant.AddItem(BlamcoMacAndCheese, 200)
	TheMerchant.AddItem(BlamcoMacAndCheese_PreWar, 200)
	TheMerchant.AddItem(bleedOut, 200)
	TheMerchant.AddItem(BloatflyLarva, 200)
	TheMerchant.AddItem(MeatBloatfly, 200)
	TheMerchant.AddItem(Bloodpack, 200)
	TheMerchant.AddItem(MeatBloodbug, 200)
	TheMerchant.AddItem(SteakBloodbug, 200)
	TheMerchant.AddItem(BloodLeaf, 200)
	TheMerchant.AddItem(MoonshineBobrov, 200)
	TheMerchant.AddItem(bourbon, 200)
	TheMerchant.AddItem(MeatBrahmin, 200)
	TheMerchant.AddItem(BrainFungus, 200)
	TheMerchant.AddItem(Bubblegum, 200)
	TheMerchant.AddItem(Buffjet, 200)
	TheMerchant.AddItem(Buffout, 200)
	TheMerchant.AddItem(Bufftats, 200)
	TheMerchant.AddItem(Calmex, 200)
	TheMerchant.AddItem(Dogfood, 200)
	TheMerchant.AddItem(Carrot, 200)
	TheMerchant.AddItem(CarrotWild, 200)
	TheMerchant.AddItem(MeatCat, 200)
	TheMerchant.AddItem(CaveFungus, 200)
	TheMerchant.AddItem(steakMirelurkSoftshell, 200)
	TheMerchant.AddItem(Corn, 200)
	TheMerchant.AddItem(Cram, 200)
	TheMerchant.AddItem(SquirrelBitsCrispy, 200)
	TheMerchant.AddItem(CurieHealthpak, 200)
	TheMerchant.AddItem(DaddyO, 200)
	TheMerchant.AddItem(DandyBoyApples, 200)
	TheMerchant.AddItem(DayTripper, 200)
	TheMerchant.AddItem(EggDeathclaw, 200)
	TheMerchant.AddItem(OmeletteEggDeathclaw, 200)
	TheMerchant.AddItem(MeatDeathclaw, 200)
	TheMerchant.AddItem(SteakDeathclaw, 200)
	TheMerchant.AddItem(SouffleDeathclaw, 200)
	TheMerchant.AddItem(MS17DeezerLemonade, 200)
	TheMerchant.AddItem(DirtyWastelander, 200)
	TheMerchant.AddItem(DN102_DruggedWater, 200)
	TheMerchant.AddItem(Endangerol, 200)
	TheMerchant.AddItem(BoSExperiment, 200)
	TheMerchant.AddItem(FancyLadSnackCakes, 200)
	TheMerchant.AddItem(FancyLadSnackCakes_PreWar, 200)
	TheMerchant.AddItem(DN062FoodPaste, 200)
	TheMerchant.AddItem(CarrotV81, 200)
	TheMerchant.AddItem(CornV81, 200)
	TheMerchant.AddItem(MelonV81, 200)
	TheMerchant.AddItem(MutfruitV81, 200)
	TheMerchant.AddItem(Fury, 200)
	TheMerchant.AddItem(DN079_MeatGhoul, 200)
	TheMerchant.AddItem(BloodpackGlowing, 200)
	TheMerchant.AddItem(GlowingFungus, 200)
	TheMerchant.AddItem(Gourd, 200)
	TheMerchant.AddItem(GourdInstitute, 200)
	TheMerchant.AddItem(GourdWild, 200)
	TheMerchant.AddItem(GrapeMentats, 200)
	TheMerchant.AddItem(SteakRadroach, 200)
	TheMerchant.AddItem(steakRadStag, 200)
	TheMerchant.AddItem(GroundMolerat, 200)
	TheMerchant.AddItem(Gumdrops, 200)
	TheMerchant.AddItem(BeerGwinnettAle, 200)
	TheMerchant.AddItem(BeerGwinnettBrew, 200)
	TheMerchant.AddItem(BeerGwinnettLager, 200)
	TheMerchant.AddItem(BeerGwinnettPale, 200)
	TheMerchant.AddItem(BeerGwinnettPils, 200)
	TheMerchant.AddItem(BeerGwinnettStout, 200)
	TheMerchant.AddItem(SweetRollBirthday, 200)
	TheMerchant.AddItem(HubFlower, 200)
	TheMerchant.AddItem(BeerBottleStandard01_Cold, 200)
	TheMerchant.AddItem(BeerGwinnettAle_Cold, 200)
	TheMerchant.AddItem(BeerGwinnettBrew_Cold, 200)
	TheMerchant.AddItem(BeerGwinnettLager_Cold, 200)
	TheMerchant.AddItem(BeerGwinnettPale_Cold, 200)
	TheMerchant.AddItem(BeerGwinnettPils_Cold, 200)
	TheMerchant.AddItem(BeerGwinnettStout_Cold, 200)
	TheMerchant.AddItem(NukaColaCherry_Cold, 200)
	TheMerchant.AddItem(NukaCola_Cold, 200)
	TheMerchant.AddItem(NukaColaQuantum_Cold, 200)
	TheMerchant.AddItem(IguanaBits, 200)
	TheMerchant.AddItem(IguanaStick, 200)
	TheMerchant.AddItem(IguanaSoup, 200)
	TheMerchant.AddItem(InstaMash, 200)
	TheMerchant.AddItem(WaterInstitute, 200)
	TheMerchant.AddItem(FoodPackInstitute, 200)
	TheMerchant.AddItem(GlowingOneBlood, 200)
	TheMerchant.AddItem(Jet, 200)
	TheMerchant.AddItem(JetFuel, 200)
	TheMerchant.AddItem(lockJoint, 200)
	TheMerchant.AddItem(MedX, 200)
	TheMerchant.AddItem(Melon, 200)
	TheMerchant.AddItem(MelonWarwick, 200)
	TheMerchant.AddItem(MelonWild, 200)
	TheMerchant.AddItem(Mentats, 200)
	TheMerchant.AddItem(mindCloud, 200)
	TheMerchant.AddItem(MirelurkCake, 200)
	TheMerchant.AddItem(MirelurkEgg, 200)
	TheMerchant.AddItem(OmeletteEggMirelurk, 200)
	TheMerchant.AddItem(MeatMirelurk, 200)
	TheMerchant.AddItem(SteakMirelurkQueen, 200)
	TheMerchant.AddItem(MoldyFood01, 200)
	TheMerchant.AddItem(steakMolerat, 200)
	TheMerchant.AddItem(MeatMolerat, 200)
	TheMerchant.AddItem(MS19MoleratPoison, 200)
	TheMerchant.AddItem(Meatdog, 200)
	TheMerchant.AddItem(steakMutantHound, 200)
	TheMerchant.AddItem(MeatMutanthound, 200)
	TheMerchant.AddItem(FernFlower, 200)
	TheMerchant.AddItem(Mutfruit, 200)
	TheMerchant.AddItem(steakDog, 200)
	TheMerchant.AddItem(MS09LorenzoSerum, 200)
	TheMerchant.AddItem(NoodleCup, 200)
	TheMerchant.AddItem(NukaColaCherry, 200)
	TheMerchant.AddItem(NukaCola, 200)
	TheMerchant.AddItem(NukaColaQuantum, 200)
	TheMerchant.AddItem(OrangeMentats, 200)
	TheMerchant.AddItem(Overdrive, 200)
	TheMerchant.AddItem(Pax, 200)
	TheMerchant.AddItem(PreservedPie01, 200)
	TheMerchant.AddItem(DNBostonCommon_BoylstonWine, 200)
	TheMerchant.AddItem(PorknBeans, 200)
	TheMerchant.AddItem(PotatoCrisps, 200)
	TheMerchant.AddItem(LukowskisPottedMeat, 200)
	TheMerchant.AddItem(InstaMash_PreWar, 200)
	TheMerchant.AddItem(MS05BEggDeathclaw, 200)
	TheMerchant.AddItem(Psycho, 200)
	TheMerchant.AddItem(PsychoJet, 200)
	TheMerchant.AddItem(Psychobuff, 200)
	TheMerchant.AddItem(Psychotats, 200)
	TheMerchant.AddItem(MeatMirelurkQueen, 200)
	TheMerchant.AddItem(MeatRadroach, 200)
	TheMerchant.AddItem(EggRadscorpion, 200)
	TheMerchant.AddItem(OmeletteEggRadscorpion, 200)
	TheMerchant.AddItem(MeatRadscorpion, 200)
	TheMerchant.AddItem(SteakRadscorpion, 200)
	TheMerchant.AddItem(radscorpionVenom, 200)
	TheMerchant.AddItem(MeatRadstag, 200)
	TheMerchant.AddItem(stewRadStag, 200)
	TheMerchant.AddItem(RRStealthBoy, 200)
	TheMerchant.AddItem(Razorgrain, 200)
	TheMerchant.AddItem(RefreshingBeverage, 200)
	TheMerchant.AddItem(steakBrahmin, 200)
	TheMerchant.AddItem(steakMirelurk, 200)
	TheMerchant.AddItem(Rum, 200)
	TheMerchant.AddItem(SalisburySteak, 200)
	TheMerchant.AddItem(SalisburySteak_PreWar, 200)
	TheMerchant.AddItem(SiltBean, 200)
	TheMerchant.AddItem(SkeetoSpit, 200)
	TheMerchant.AddItem(DN124_BuzzBites, 200)
	TheMerchant.AddItem(MeatMirelurkSoftshell, 200)
	TheMerchant.AddItem(SquirrelBits, 200)
	TheMerchant.AddItem(SquirrelStick, 200)
	TheMerchant.AddItem(SquirrelStew, 200)
	TheMerchant.AddItem(StealthBoy, 200)
	TheMerchant.AddItem(steakStingwing, 200)
	TheMerchant.AddItem(MeatStingwing, 200)
	TheMerchant.AddItem(SugarBombs, 200)
	TheMerchant.AddItem(SugarBombs_PreWar, 200)
	TheMerchant.AddItem(SweetRoll, 200)
	TheMerchant.AddItem(MeatGorilla, 200)
	TheMerchant.AddItem(Tarberry, 200)
	TheMerchant.AddItem(OmeletteEggDeathclawPristine, 200)
	TheMerchant.AddItem(Tato, 200)
	TheMerchant.AddItem(TatoWild, 200)
	TheMerchant.AddItem(Thistle, 200)
	TheMerchant.AddItem(UltraJet, 200)
	TheMerchant.AddItem(MS19Cure, 200)
	TheMerchant.AddItem(VegetableSoup, 200)
	TheMerchant.AddItem(Vodka, 200)
	TheMerchant.AddItem(Whiskey, 200)
	TheMerchant.AddItem(CornWild, 200)
	TheMerchant.AddItem(MutfruitWild, 200)
	TheMerchant.AddItem(RazorgrainWild, 200)
	TheMerchant.AddItem(TarberryWild, 200)
	TheMerchant.AddItem(Wine, 200)
	TheMerchant.AddItem(DN133_WineAmontillado, 200)
	TheMerchant.AddItem(BoSX111Compound, 200)
	TheMerchant.AddItem(XCell, 200)
	TheMerchant.AddItem(MeatYaoGuai, 200)
	TheMerchant.AddItem(steakYaoGuai, 200)
	TheMerchant.AddItem(RoastYaoGuai, 200)
	TheMerchant.AddItem(yellowBelly, 200)
	TheMerchant.AddItem(YumYumDeviledEggs, 200)
	Form formFromMod = Game.GetFormFromFile(0x00004F12, "DLCRobot.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 200)
	endIf
	formFromMod = Game.GetFormFromFile(0x00008833, "DLCCoast.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000088A6, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000088A8, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000088AA, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0000F11D, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00013384, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000140F9, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0001B189, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0001C710, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0001C714, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0001C717, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0002B5D4, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00032C2E, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00044042, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00044043, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00048400, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00048401, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00048BD4, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004B4B5, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004B4BB, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004B5FA, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004BA11, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004BA12, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004D2E7, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004D2E8, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004E773, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000540FF, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00054281, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00054282, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00054284, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00054286, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00054452, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00056A49, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00056A4A, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00057158, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0005715B, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 200)
	endIf
	formFromMod = Game.GetFormFromFile(0x000138EE, "DLCNukaWorld.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000138C3, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000418D1, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00026189, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000138C4, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0002689A, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004CBBD, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0001CD24, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000138C5, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0002689B, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004AA17, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004AA1C, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004B419, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004CBB9, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000518D3, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004AA1D, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004AA24, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502AF, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502A9, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502AC, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502B2, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502B4, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0002EDA9, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0002EDAD, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0002EDAF, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0002EDB0, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0002EDB1, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502D5, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502B7, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502BA, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502BD, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0002EDAB, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502C0, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502C3, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502C6, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502C9, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502CC, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502CF, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502D2, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502D8, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502DB, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502DE, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000502E1, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00042CA6, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00042CA1, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00030EFF, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00030EFD, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0003B8B4, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00030F02, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00030EF6, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00024536, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0002453A, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00024538, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00024544, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0002453D, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00030EFA, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00030F01, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00042C39, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0003B8B3, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00024534, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00042C3D, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00042C41, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00030EFC, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00030EFE, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00030EF7, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0003B8B2, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00030EF8, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00042C43, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00030F00, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00030EFB, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x00030EF9, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004AA1E, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004AA23, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x000138F7, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004C0E3, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004CBBC, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
		formFromMod = Game.GetFormFromFile(0x0004CBBF, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 200)
	endIf
endFunction
Function AddAmmo()
	TheMerchant.AddItem(Ammo308Caliber, 1000)
	TheMerchant.AddItem(Ammo38Caliber, 1000)
	TheMerchant.AddItem(Ammo44, 1000)
	TheMerchant.AddItem(Ammo45Caliber, 1000)
	TheMerchant.AddItem(Ammo50Caliber, 1000)
	TheMerchant.AddItem(Ammo10mm, 1000)
	TheMerchant.AddItem(Ammo2mmEC, 1000)
	TheMerchant.AddItem(Ammo556, 1000)
	TheMerchant.AddItem(Ammo5mm, 1000)
	TheMerchant.AddItem(AmmoAlienBlaster, 1000)
	TheMerchant.AddItem(AmmoCannonBall, 1000)
	TheMerchant.AddItem(AmmoCryoCell, 1000)
	TheMerchant.AddItem(AmmoFlamerFuel, 1000)
	TheMerchant.AddItem(AmmoFlareGun, 1000)
	TheMerchant.AddItem(AmmoFusionCell, 1000)
	TheMerchant.AddItem(AmmoFusionCore, 1000)
	TheMerchant.AddItem(AmmoGammaCell, 1000)
	TheMerchant.AddItem(AmmoSyringer, 1000)
	TheMerchant.AddItem(AmmoJunkJet, 1000)
	TheMerchant.AddItem(AmmoFatManMiniNuke, 1000)
	TheMerchant.AddItem(AmmoMirelurkQueenSpawn, 1000)
	TheMerchant.AddItem(AmmoMissile, 1000)
	TheMerchant.AddItem(AmmoPlasmaCartridge, 1000)
	TheMerchant.AddItem(AmmoRRSpike, 1000)
	TheMerchant.AddItem(AmmoShotgunShell, 1000)
	Form formFromMod = Game.GetFormFromFile(0x00000FA8, "9mmpistol.esp")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 1000)
	endIf
	formFromMod = Game.GetFormFromFile(0x00010B80, "DLCCoast.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 1000)
		formFromMod = Game.GetFormFromFile(0x0002740E, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 1000)
		formFromMod = Game.GetFormFromFile(0x0002C8B1, "DLCCoast.esm")
		TheMerchant.AddItem(formFromMod, 1000)
	endIf
	formFromMod = Game.GetFormFromFile(0x0000A6C2, "DLCNukaWorld.esm")
	if ( formFromMod != None )
		TheMerchant.AddItem(formFromMod, 1000)
		formFromMod = Game.GetFormFromFile(0x0000A6C6, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 1000)
		formFromMod = Game.GetFormFromFile(0x0000A6C9, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 1000)
		formFromMod = Game.GetFormFromFile(0x0001B039, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 1000)
		formFromMod = Game.GetFormFromFile(0x0002BDC2, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 1000)
		formFromMod = Game.GetFormFromFile(0x00030076, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 1000)
		formFromMod = Game.GetFormFromFile(0x00032F5E, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 1000)
		formFromMod = Game.GetFormFromFile(0x00032F5F, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 1000)
		formFromMod = Game.GetFormFromFile(0x00037897, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 1000)
		formFromMod = Game.GetFormFromFile(0x0004C0E4, "DLCNukaWorld.esm")
		TheMerchant.AddItem(formFromMod, 1000)
	endIf
endFunction
Function AddCraftingIngredients()
	TheMerchant.AddItem(c_Acid_scrap, 5000)
	TheMerchant.AddItem(c_Adhesive_scrap, 5000)
	TheMerchant.AddItem(c_Aluminum_scrap, 5000)
	TheMerchant.AddItem(c_Antiseptic_scrap, 5000)
	TheMerchant.AddItem(c_Asbestos_scrap, 5000)
	TheMerchant.AddItem(c_AntiBallisticFiber_scrap, 5000)
	TheMerchant.AddItem(c_Bone_scrap, 5000)
	TheMerchant.AddItem(c_Ceramic_scrap, 5000)
	TheMerchant.AddItem(c_Circuitry_scrap, 5000)
	TheMerchant.AddItem(c_Cloth_scrap, 5000)
	TheMerchant.AddItem(c_Concrete_scrap, 5000)
	TheMerchant.AddItem(c_Copper_scrap, 5000)
	TheMerchant.AddItem(c_Cork_scrap, 5000)
	TheMerchant.AddItem(c_Crystal_scrap, 5000)
	TheMerchant.AddItem(c_Fertilizer_scrap, 5000)
	TheMerchant.AddItem(c_FiberOptics_scrap, 5000)
	TheMerchant.AddItem(c_Fiberglass_scrap, 5000)
	TheMerchant.AddItem(c_Gears_scrap, 5000)
	TheMerchant.AddItem(c_Glass_scrap, 5000)
	TheMerchant.AddItem(c_Gold_scrap, 5000)
	TheMerchant.AddItem(c_Lead_scrap, 5000)
	TheMerchant.AddItem(c_Leather_scrap, 5000)
	TheMerchant.AddItem(c_NuclearMaterial_scrap, 5000)
	TheMerchant.AddItem(c_Oil_scrap, 5000)
	TheMerchant.AddItem(c_Plastic_scrap, 5000)
	TheMerchant.AddItem(c_Rubber_scrap, 5000)
	TheMerchant.AddItem(c_Screws_scrap, 5000)
	TheMerchant.AddItem(c_Silver_scrap, 5000)
	TheMerchant.AddItem(c_Springs_scrap, 5000)
	TheMerchant.AddItem(c_Steel_scrap, 5000)
	TheMerchant.AddItem(c_Wood_scrap, 5000)
endFunction
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
Potion Property CheatTerminal_TheMerchantPotion Auto Const
Actor Property TheMerchant Auto Const
ObjectReference Property TheMerchantContainer Auto Const
MiscObject Property Caps001 Auto Const
Weapon Property magnum Auto Const
Weapon Property tenmm Auto Const
Weapon Property AlienBlaster Auto Const
Weapon Property ArtillerySmokeFlare Auto Const
Weapon Property AssaultRifle Auto Const
Weapon Property BaseballBat Auto Const
Weapon Property BaseballGrenade Auto Const
Weapon Property Baton Auto Const
Weapon Property Board Auto Const
Weapon Property BottlecapMine Auto Const
Weapon Property BoxingGlove Auto Const
Weapon Property Broadsider Auto Const
Weapon Property ChineseOfficerSword Auto Const
Weapon Property Knife Auto Const
Weapon Property CombatRifle Auto Const
Weapon Property CombatShotgun Auto Const
Weapon Property CryoMine Auto Const
Weapon Property CryoGrenade Auto Const
Weapon Property Cryolator Auto Const
Weapon Property DeathclawGauntlet Auto Const
Weapon Property Deliverer Auto Const
Weapon Property DoubleBarrelShotgun Auto Const
Weapon Property Fatman Auto Const
Weapon Property Flamer Auto Const
Weapon Property FlareGun Auto Const
Weapon Property fragGrenade Auto Const
Weapon Property fragMine Auto Const
Weapon Property FusionCoreWeapon Auto Const
Weapon Property GammaGun Auto Const
Weapon Property GatlingLaser Auto Const
Weapon Property GaussRifle Auto Const
Weapon Property HuntingRifle Auto Const
Weapon Property InstituteLaserGun Auto Const
Weapon Property InstituteBeaconGrenade Auto Const
Weapon Property PulseGrenadeInstitute Auto Const
Weapon Property JunkJet Auto Const
Weapon Property Knuckles Auto Const
Weapon Property LaserGun Auto Const
Weapon Property LaserMusket Auto Const
Weapon Property LeadPipe Auto Const
Weapon Property Machete Auto Const
Weapon Property Minigun Auto Const
Weapon Property MissileLauncher Auto Const
Weapon Property MolotovCocktail Auto Const
Weapon Property nukaGrenade Auto Const
Weapon Property NukeMine Auto Const
Weapon Property PipeGun Auto Const
Weapon Property PipeBoltAction Auto Const
Weapon Property PipeRevolver Auto Const
Weapon Property PipeWrench Auto Const
Weapon Property PlasmaGun Auto Const
Weapon Property PlasmaGrenade Auto Const
Weapon Property PlasmaMine Auto Const
Weapon Property PoolCue Auto Const
Weapon Property PowerFist Auto Const
Weapon Property PulseGrenade Auto Const
Weapon Property PulseMine Auto Const
Weapon Property RailwayRifle Auto Const
Weapon Property RevolutionarySword Auto Const
Weapon Property Ripper Auto Const
Weapon Property RollingPin Auto Const
Weapon Property Shishkebab Auto Const
Weapon Property Sledgehammer Auto Const
Weapon Property SubmachineGun Auto Const
Weapon Property SuperSledge Auto Const
Weapon Property Switchblade Auto Const
Weapon Property SynthTeleportGrenade Auto Const
Weapon Property PipeSyringer Auto Const
Weapon Property TireIron Auto Const
Message Property CheatTerminal_MerchantReset Auto Const
Message Property CheatTerminal_MerchantResetQuestion Auto Const
GlobalVariable Property CheatTerminal_TheMerchant_Weapons Auto Const
GlobalVariable Property CheatTerminal_TheMerchant_Throwables Auto Const
Armor Property MS10ClothesRewardF Auto Const
Armor Property Clothes_BoS_CaptainKells_Hat Auto Const
Armor Property ClothesFatiguesPreWar Auto Const
Armor Property ClothesGreaserJacketAtomCats Auto Const
Armor Property BabyBundledRight Auto Const
Armor Property ClothesBaseballHat Auto Const
Armor Property ClothesBaseballUniform Auto Const
Armor Property ClothesBathrobe Auto Const
Armor Property ClothesResident3Hat Auto Const
Armor Property ClothesMommaMurphy Auto Const
Armor Property Clothes_InstituteLabCoat_blacksleeves Auto Const
Armor Property ClothesMobster01 Auto Const
Armor Property ClothesDeaconMobster Auto Const
Armor Property ClothesBlackRimGlasses Auto Const
Armor Property ClothesDog_BandanaBlue Auto Const
Armor Property Armor_Batting_HelmetBlue Auto Const
Armor Property Clothes_InstituteLabCoatDivisionHead_Blue Auto Const
Armor Property Clothes_InstituteLabCoat_bluesleeves Auto Const
Armor Property Armor_BoS_Knight_Lancer Auto Const
Armor Property Armor_BoS_Knight_UnderarmorHelmet Auto Const
Armor Property Armor_BoS_Officer_Underarmor Auto Const
Armor Property Armor_BoS_Knight_Underarmor Auto Const
Armor Property Armor_BoS_Soldier_Underarmor Auto Const
Armor Property ClothesBowlerHat Auto Const
Armor Property Armor_BoS_Soldier Auto Const
Armor Property Armor_BoS_Cade Auto Const
Armor Property Armor_BoS_Kells Auto Const
Armor Property Armor_RR303BoSDisguise Auto Const
Armor Property Armor_FlightHelmetBrown Auto Const
Armor Property ClothesScientistCabot Auto Const
Armor Property Armor_Wastelander_Heavy Auto Const
Armor Property ClothesCaitBandolier Auto Const
Armor Property MS11IronsidesHat Auto Const
Armor Property ClothesPrewarWomensCasual Auto Const
Armor Property ClothesPrewarShortSleevesAndSlacks Auto Const
Armor Property ClothesChefsHat Auto Const
Armor Property ClothesChildofAtom Auto Const
Armor Property ClothesChildofAtomBrown2 Auto Const
Armor Property ClothesChildofAtomGreen2 Auto Const
Armor Property ClothesChildofAtomBrown Auto Const
Armor Property ClothesChildofAtomGreen Auto Const
Armor Property ClothesChildofAtomBrown3 Auto Const
Armor Property ClothesChildofAtomGreen3 Auto Const
Armor Property ClothesChildofAtom3 Auto Const
Armor Property ClothesChildofAtom2 Auto Const
Armor Property Clothes_InstituteChildUniform Auto Const
Armor Property ClothesSuitClean_Black Auto Const
Armor Property NewsmanSuitA Auto Const
Armor Property ClothesSuitClean_Blue Auto Const
Armor Property ClothesSuitClean_Grey Auto Const
Armor Property ClothesSuitClean_Striped Auto Const
Armor Property ClothesSuitClean_Tan Auto Const
Armor Property Clothes_InstituteWorker Auto Const
Armor Property ClothesCoastguardHat Auto Const
Armor Property ArmorPrestonGarveyOutfit Auto Const
Armor Property ClothesCait Auto Const
Armor Property ClothesCourserJacket Auto Const
Armor Property Armor_VaultTecSecurityCovenant Auto Const
Armor Property RR_PrewarSweaterVest_01 Auto Const
Armor Property ClothesMayorHat Auto Const
Armor Property Armor_HazmatSuitDamaged Auto Const
Armor Property DBTech_Armor_Highschool_Jacket Auto Const
Armor Property SkinMirelurkDeepKing Auto Const
Armor Property SkinMirelurkGlowingKing Auto Const
Armor Property ClothesFatiguesPostWar Auto Const
Armor Property Armor_Army_Helmet_postwar Auto Const
Armor Property Clothes_InstituteLabCoat_blacksleeves_dirty Auto Const
Armor Property ClothesSuitDirty_Black Auto Const
Armor Property Clothes_InstituteLabCoatDivisionHead_Dirty_Blue Auto Const
Armor Property Clothes_InstituteLabCoat_bluesleeves_dirty Auto Const
Armor Property ClothesSuitDirty_Blue Auto Const
Armor Property ClothesVaultTecSalesmanHatPW Auto Const
Armor Property Clothes_InstituteLabCoatDivisionHead_Dirty_Green Auto Const
Armor Property Clothes_InstituteLabCoat_greensleeves_dirty Auto Const
Armor Property ClothesSuitDirty_Grey Auto Const
Armor Property Clothes_InstituteLabCoatDivisionHead_Dirty Auto Const
Armor Property Clothes_InstituteUniform_Dirty Auto Const
Armor Property Clothes_InstituteLabCoat_Dirty Auto Const
Armor Property Clothes_InstituteLabCoatDivisionHead_Dirty_Orange Auto Const
Armor Property ClothesPostmanHatPostwar Auto Const
Armor Property ClothesPostmanPostwar Auto Const
Armor Property ClothesSuitDirty_Striped Auto Const
Armor Property ClothesSuitDirty_Tan Auto Const
Armor Property ClothesVaultTecSalesmanCoatPW Auto Const
Armor Property Clothes_InstituteLabCoatDivisionHead_Dirty_Yellow Auto Const
Armor Property Clothes_InstituteLabCoat_yellowsleeves_dirty Auto Const
Armor Property ClothesDog_ArmorMedium Auto Const
Armor Property ClothesDog_DogCollar Auto Const
Armor Property ClothesDog_DogHeadHelmet Auto Const
Armor Property ClothesDog_DogCollarDouble Auto Const
Armor Property Armor_Wastelander_Light Auto Const
Armor Property Armor_BoS_Engineer_Scribe Auto Const
Armor Property Armor_Wastelander_02_Hood_GlovesB Auto Const
Armor Property ClothesEyeGlasses Auto Const
Armor Property ClothesValentineCoat Auto Const
Armor Property ClothesVisor Auto Const
Armor Property ClothesResident3 Auto Const
Armor Property ClothesDeaconResident3 Auto Const
Armor Property ClothesFancyGlasses Auto Const
Armor Property ClothesFather Auto Const
Armor Property ClothesDesdemona Auto Const
Armor Property ClothesIrmaDress Auto Const
Armor Property Armor_BoS_Field_Scribe Auto Const
Armor Property Armor_BoS_Field_Scribe_Helmet Auto Const
Armor Property Armor_Rustic_Underarmor Auto Const
Armor Property Armor_DeaconRustic_Underarmor Auto Const
Armor Property Armor_BoS_Knight_Lancer_Helmet Auto Const
Armor Property ClothesPrewarHouseDress01NonPlayable Auto Const
Armor Property ClothesPrewarHouseDress01NonPlayableA Auto Const
Armor Property ClothesPrewarHouseDress01NonPlayableB Auto Const
Armor Property ClothesTuxedoHat Auto Const
Armor Property Armor_Raider_Gasmask Auto Const
Armor Property Armor_Gasmask Auto Const
Armor Property ClothesPrewarWomensCasual2 Auto Const
Armor Property FFBK01GrandpaHat Auto Const
Armor Property ClothesGrayKnitCap Auto Const
Armor Property ClothesGreaserJacket Auto Const
Armor Property Armor_Raider_GreenHood Auto Const
Armor Property Clothes_InstituteLabCoatDivisionHead_Green Auto Const
Armor Property Clothes_InstituteLabCoat_greensleeves Auto Const
Armor Property ClothesResident5Hat Auto Const
Armor Property ClothesWastelandCommon Auto Const
Armor Property DN160_ClothesGrognakCostume Auto Const
Armor Property Armor_GunnerRustic_Underarmor Auto Const
Armor Property Armor_GunnerGuard_UnderArmor Auto Const
Armor Property Armor_GunnerRaider_Underwear2 Auto Const
Armor Property Armor_GunnerRaiderMod_Underarmor Auto Const
Armor Property ClothesDog_BandanaGunnerCamo Auto Const
Armor Property ClothesDog_BandanaGunnerGreen Auto Const
Armor Property Armor_HardHat Auto Const
Armor Property Armor_Raider_Underwear2 Auto Const
Armor Property Armor_HazmatSuit Auto Const
Armor Property ClothesDog_ArmorHeavy Auto Const
Armor Property Armor_Wastelander_Heavy_Helmet Auto Const
Armor Property Armor_Wastelander_Medium_Helmet Auto Const
Armor Property Clothes_InstituteWorkerwithHelmet Auto Const
Armor Property ClothesResident6 Auto Const
Armor Property Armor_BoS_Ingram Auto Const
Armor Property Clothes_InstituteLabCoatDivisionHead Auto Const
Armor Property Clothes_InstituteUniform Auto Const
Armor Property Clothes_InstituteLabCoat Auto Const
Armor Property ClothesDog_BandanaMonkey Auto Const
Armor Property ArmorKellogg Auto Const
Armor Property ClothesScientist_VariantWasteland Auto Const
Armor Property ClothesDeaconScientist Auto Const
Armor Property ClothesPrewarHouseDress Auto Const
Armor Property ClothesPrewarHouseDressA Auto Const
Armor Property ClothesPreWarDressBlue Auto Const
Armor Property ClothesPreWarDress Auto Const
Armor Property ClothesDeaconPrewarShortSleeves Auto Const
Armor Property ClothesPrewarShortSleeves Auto Const
Armor Property ClothesPrewarHouseDressB Auto Const
Armor Property ClothesPreWarDressPink Auto Const
Armor Property ClothesDog_BandanaLeopard Auto Const
Armor Property Armor_Highschool_JacketRed Auto Const
Armor Property InstM02LiamsGlasses Auto Const
Armor Property MS11LieutenantsHat Auto Const
Armor Property ClothesDog_ArmorLight Auto Const
Armor Property Armor_Raider_Underwear Auto Const
Armor Property ClothesLongshoreman Auto Const
Armor Property CheatTerminal_LorenzosCrown Auto Const
Armor Property ClothesLorenzo Auto Const
Armor Property ClothesMacCready Auto Const
Armor Property ClothesMacCreadyHat Auto Const
Armor Property DN028_Armor_Bear_Mascot Auto Const
Armor Property ClothesMaxsonCoat Auto Const
Armor Property ClothesRRJumpsuit Auto Const
Armor Property ClothesGenericJumpsuit Auto Const
Armor Property Armor_BoS_Science_Scribe_Helmet Auto Const
Armor Property ClothesMilitaryCap Auto Const
Armor Property ClothesMilitaryFatigues Auto Const
Armor Property ClothesMinutemanHat Auto Const
Armor Property ClothesDeaconMinutemanHat Auto Const
Armor Property Armor_MiningHelmet Auto Const
Armor Property ArmorPrestonGarveyHat Auto Const
Armor Property ClothesMinutemanOutfit Auto Const
Armor Property ArmorMinutemanGeneralHat Auto Const
Armor Property ArmorMinutemanGeneral Auto Const
Armor Property SkinMirelurkHunter Auto Const
Armor Property SkinMirelurkHunterDirt Auto Const
Armor Property SkinMirelurkHunterCaveMud Auto Const
Armor Property SkinMirelurkHunterNestMud Auto Const
Armor Property SkinMirelurkHunterSandRock Auto Const
Armor Property SkinMirelurkHunterWater Auto Const
Armor Property SkinMirelurkHunterGlow Auto Const
Armor Property SkinMirelurkKingDirt Auto Const
Armor Property SkinMirelurkKingWater Auto Const
Armor Property ClothesNat Auto Const
Armor Property ClothesResident7Hat Auto Const
Armor Property ClothesDeaconResident7Hat Auto Const
Armor Property DN015_Armor_Power_Torso Auto Const
Armor Property Clothes_InstituteLabCoatDivisionHead_Orange Auto Const
Armor Property ClothesResident7 Auto Const
Armor Property ClothesDeaconResident7 Auto Const
Armor Property ClothesPastor Auto Const
Armor Property ClothesMayor Auto Const
Armor Property ClothesResident4 Auto Const
Armor Property ClothesPoliceGlasses Auto Const
Armor Property ClothesDeaconWig Auto Const
Armor Property ClothesWig Auto Const
Armor Property ClothesPostmanHat Auto Const
Armor Property ClothesPostman Auto Const
Armor Property ClothesPiperCap Auto Const
Armor Property Armor_BoS_Quinlan Auto Const
Armor Property Armor_Wastelander_01_GlovesA Auto Const
Armor Property ClothesWastelandDress Auto Const
Armor Property ClothesDog_Bandana Auto Const
Armor Property ClothesSlinkyDress Auto Const
Armor Property Armor_FlightHelmetRed Auto Const
Armor Property ClothesHancocksOutfit Auto Const
Armor Property ClothesPiper Auto Const
Armor Property MS10ClothesRewardM Auto Const
Armor Property ClothesDog_DogCollarReinforced Auto Const
Armor Property MS10ClothesRex Auto Const
Armor Property ClothesPiperGoggles Auto Const
Armor Property Armor_MacCready_Raider_Underarmor Auto Const
Armor Property Armor_Raider_Underarmor Auto Const
Armor Property Armor_Piper_Underarmor Auto Const
Armor Property Armor_DeaconRaider_Underarmor Auto Const
Armor Property Clothes_RaiderMod_Hood1 Auto Const
Armor Property Clothes_RaiderMod_Hood2 Auto Const
Armor Property Clothes_RaiderMod_Hood3 Auto Const
Armor Property Armor_Wastelander_02_NoHood_GlovesB Auto Const
Armor Property Armor_BoS_Science_Scribe Auto Const
Armor Property ClothesCaptainsHat Auto Const
Armor Property Armor_VaultTecSecurityHelmetCovenant Auto Const
Armor Property ClothesSequinDress Auto Const
Armor Property MS04_SilverShroudArmor_15 Auto Const
Armor Property MS04_SilverShroudArmor_25 Auto Const
Armor Property MS04_SilverShroudArmor_35 Auto Const
Armor Property MS04_SilverShroudArmor_45 Auto Const
Armor Property MS04_SilverShroudCostume Auto Const
Armor Property ClothesSilverShroud Auto Const
Armor Property MS04_SilverShroudHat Auto Const
Armor Property ClothesDog_BandanaSkull Auto Const
Armor Property Armor_Wastelander_Medium Auto Const
Armor Property ClothesDog_DogCollarSpiked Auto Const
Armor Property ClothesDog_DogHeadMuzzle Auto Const
Armor Property ClothesSquire Auto Const
Armor Property ClothesDog_BandanaStarsNStripes Auto Const
Armor Property ClothesDog_BandanaStripes Auto Const
Armor Property MS02_ClothesSubmarineCrewHat Auto Const
Armor Property MS02_ClothesSubmarineCrew Auto Const
Armor Property ClothesSummerShorts Auto Const
Armor Property ClothesDeaconSunGlasses Auto Const
Armor Property ClothesSunGlasses Auto Const
Armor Property Clothes_Raider_SurgicalMask Auto Const
Armor Property ClothesResident5 Auto Const
Armor Property ClothesMobster02 Auto Const
Armor Property ClothesPrewarSweaterVest Auto Const
Armor Property ClothesPrewarTshirtSlacks Auto Const
Armor Property Armor_Raider_Suit_01_GlovesB Auto Const
Armor Property ClothesTattered Auto Const
Armor Property Armor_BoS_Teagan Auto Const
Armor Property Armor_TinkerHeadgear Auto Const
Armor Property Armor_Raider_Suit_02B_GlovesC Auto Const
Armor Property ClothesHancocksHat Auto Const
Armor Property ClothesMobsterhat Auto Const
Armor Property ClothesResident4Hat Auto Const
Armor Property ClothesTuxedo Auto Const
Armor Property Armor_Highschool_UnderArmor Auto Const
Armor Property Armor_DeaconHighschool_UnderArmor Auto Const
Armor Property ClothesWinterHat Auto Const
Armor Property ClothesSturgesBeltAddons Auto Const
Armor Property ClothesDeaconMechanic Auto Const
Armor Property Armor_Vault101_Underwear Auto Const
Armor Property Armor_Vault101_UnderwearClean Auto Const
Armor Property Armor_Vault111_Underwear Auto Const
Armor Property Armor_Vault111_Underwear_NoLoot Auto Const
Armor Property Armor_Vault111Clean_Underwear Auto Const
Armor Property Armor_Vault114_Underwear Auto Const
Armor Property Armor_Vault114_UnderwearClean Auto Const
Armor Property Armor_Vault75_Underwear Auto Const
Armor Property Armor_Vault75_UnderwearClean Auto Const
Armor Property Armor_Vault81_Underwear Auto Const
Armor Property Armor_Vault81_UnderwearChild Auto Const
Armor Property Armor_DeaconVault81_Underwear Auto Const
Armor Property Armor_Vault81_UnderwearClean Auto Const
Armor Property Armor_VaultTecSecurity81 Auto Const
Armor Property Armor_Vault95_Underwear Auto Const
Armor Property Armor_Vault95_Underwear_Clean Auto Const
Armor Property ClothesVaultTecScientist Auto Const
Armor Property Armor_VaultTecSecurity111 Auto Const
Armor Property Armor_VaultTecSecurity111_Clean Auto Const
Armor Property Armor_VaultTecSecurityHelmet Auto Const
Armor Property Armor_VaultTecSecurityHelmet_Clean Auto Const
Armor Property SupermutantBlindfold Auto Const
Armor Property ClothesDeaconMinutemanOutfit Auto Const
Armor Property Armor_SpouseWeddingRing Auto Const
Armor Property Armor_WeddingRing Auto Const
Armor Property Clothes_Raider_Goggles Auto Const
Armor Property Armor_BoS_Engineer_Scribe_Helmet Auto Const
Armor Property Armor_Rustic_Jacket Auto Const
Armor Property ClothesValentineHat Auto Const
Armor Property ClothesDeaconMobsterHat Auto Const
Armor Property Clothes_Metal_Shades Auto Const
Armor Property Clothes_Raider_Hood Auto Const
Armor Property ClothesVaultTecSalesmanHat Auto Const
Armor Property Armor_FlightHelmet Auto Const
Armor Property Clothes_InstituteLabCoatDivisionHead_Yellow Auto Const
Armor Property Clothes_InstituteLabCoat_yellowsleeves Auto Const
Armor Property ClothesLongshoremanHat Auto Const
Armor Property ClothesVaultTecSalesmanCoat Auto Const
Armor Property DN054ZekesJacket Auto Const
Armor Property Armor_Power_X01_Helm Auto Const
Armor Property Armor_Power_X01_ArmLeft Auto Const
Armor Property Armor_Power_X01_LegLeft Auto Const
Armor Property Armor_Power_X01_ArmRight Auto Const
Armor Property Armor_Power_X01_LegRight Auto Const
Armor Property Armor_Power_X01_Torso Auto Const
Armor Property Armor_Power_T45_Torso Auto Const
Armor Property Armor_Power_T45_Helm Auto Const
Armor Property Armor_Power_T45_ArmLeft Auto Const
Armor Property Armor_Power_T45_LegLeft Auto Const
Armor Property Armor_Power_T45_ArmRight Auto Const
Armor Property Armor_Power_T45_LegRight Auto Const
Armor Property Armor_Power_T51_Torso Auto Const
Armor Property Armor_Power_T51_Helmet Auto Const
Armor Property Armor_Power_T51_ArmLeft Auto Const
Armor Property Armor_Power_T51_LegLeft Auto Const
Armor Property Armor_Power_T51_ArmRight Auto Const
Armor Property Armor_Power_T51_LegRight Auto Const
Armor Property Armor_Power_T60_Torso Auto Const
Armor Property Armor_Power_T60_Helm Auto Const
Armor Property Armor_Power_T60_ArmLeft Auto Const
Armor Property Armor_Power_T60_LegLeft Auto Const
Armor Property Armor_Power_T60_ArmRight Auto Const
Armor Property Armor_Power_T60_LegRight Auto Const
Armor Property RailroadArmoredCoat Auto Const
Armor Property Armor_Railroad01 Auto Const
Armor Property Armor_Railroad02 Auto Const
Armor Property Armor_Railroad03 Auto Const
Armor Property Armor_Railroad04 Auto Const
Armor Property Armor_Railroad05 Auto Const
Armor Property Armor_Synth_Torso Auto Const
Armor Property Armor_Synth_Helmet_Closed Auto Const
Armor Property Armor_Synth_Helmet_Open Auto Const
Armor Property Armor_Synth_ArmLeft Auto Const
Armor Property Armor_Synth_LegLeft Auto Const
Armor Property Armor_Synth_ArmRight Auto Const
Armor Property Armor_Synth_LegRight Auto Const
Armor Property Armor_Synth_Underarmor Auto Const
Armor Property Armor_Synth_Underarmor_forSynths Auto Const
Armor Property Armor_DCGuard_Helmet02 Auto Const
Armor Property Armor_DCGuard_Helmet01 Auto Const
Armor Property Armor_DCGuard_LArm Auto Const
Armor Property Armor_DeaconDCGuard_LArm Auto Const
Armor Property Armor_DCGuard_LArmLower Auto Const
Armor Property Armor_DCGuard_LArmUpper Auto Const
Armor Property Armor_DCGuard_RArm Auto Const
Armor Property Armor_DeaconDCGuard_RArm Auto Const
Armor Property Armor_DCGuard_RArmLower Auto Const
Armor Property Armor_DCGuard_RArmUpper Auto Const
Armor Property Armor_DCGuard_TorsoArmor Auto Const
Armor Property Armor_DeaconDCGuard_TorsoArmor Auto Const
Armor Property Armor_Combat_Torso Auto Const
Armor Property Armor_Combat_Helmet Auto Const
Armor Property Armor_Combat_ArmLeft Auto Const
Armor Property Armor_Combat_LegLeft Auto Const
Armor Property Armor_Combat_ArmRight Auto Const
Armor Property Armor_Combat_LegRight Auto Const
Armor Property Armor_Leather_Torso Auto Const
Armor Property Armor_Leather_TorsoE3 Auto Const
Armor Property Armor_Raider_Suit_02A_GlovesC Auto Const
Armor Property Armor_Leather_ArmLeft Auto Const
Armor Property Armor_Leather_LegLeft Auto Const
Armor Property Armor_Leather_ArmRight Auto Const
Armor Property Armor_Leather_ArmRightE3 Auto Const
Armor Property Armor_Leather_LegRight Auto Const
Armor Property Armor_DeaconLeather_LegRight Auto Const
Armor Property Armor_Metal_Torso Auto Const
Armor Property Armor_Metal_Helmet Auto Const
Armor Property Armor_Metal_ArmLeft Auto Const
Armor Property Armor_Metal_LegLeft Auto Const
Armor Property Armor_Metal_ArmRight Auto Const
Armor Property Armor_Metal_LegRight Auto Const
Armor Property Armor_RaiderMod_Torso Auto Const
Armor Property Armor_RaiderMod_Underarmor Auto Const
Armor Property Armor_RaiderMod_ArmLeft Auto Const
Armor Property Armor_RaiderMod_LegLeft Auto Const
Armor Property Armor_Power_Raider_Torso Auto Const
Armor Property Armor_Power_Raider_Helm Auto Const
Armor Property Armor_Power_Raider_ArmLeft Auto Const
Armor Property Armor_Power_Raider_LegLeft Auto Const
Armor Property Armor_Power_Raider_ArmRight Auto Const
Armor Property Armor_Power_Raider_LegRight Auto Const
Armor Property Armor_RaiderMod_ArmRight Auto Const
Armor Property Armor_RaiderMod_LegRight Auto Const
GlobalVariable Property CheatTerminal_TheMerchant_PowerArmor Auto Const

GlobalVariable Property CheatTerminal_TheMerchant_Clothing Auto Const

GlobalVariable Property CheatTerminal_TheMerchant_Armor Auto Const

Potion Property HC_Antibiotics Auto Const
Potion Property HC_Herbal_Anodyne Auto Const
Potion Property HC_Herbal_Antimicrobial Auto Const
Potion Property HC_Herbal_Stimulant Auto Const
Potion Property HC_SippableWater Auto Const
Potion Property HC_SippableDirtyWater Auto Const
Potion Property WaterPurified Auto Const
Potion Property WaterDirty Auto Const
Potion Property Addictol Auto Const
Potion Property MelonWildGS Auto Const
Potion Property SteakBloatfly Auto Const
Potion Property BeerBottleStandard01 Auto Const
Potion Property BerryMentats Auto Const
Potion Property Berserk Auto Const
Potion Property BlamcoMacAndCheese Auto Const
Potion Property BlamcoMacAndCheese_PreWar Auto Const
Potion Property bleedOut Auto Const
Potion Property BloatflyLarva Auto Const
Potion Property MeatBloatfly Auto Const
Potion Property Bloodpack Auto Const
Potion Property MeatBloodbug Auto Const
Potion Property SteakBloodbug Auto Const
Potion Property BloodLeaf Auto Const
Potion Property MoonshineBobrov Auto Const
Potion Property bourbon Auto Const
Potion Property MeatBrahmin Auto Const
Potion Property BrainFungus Auto Const
Potion Property Bubblegum Auto Const
Potion Property Buffjet Auto Const
Potion Property Buffout Auto Const
Potion Property Bufftats Auto Const
Potion Property Calmex Auto Const
Potion Property Dogfood Auto Const
Potion Property Carrot Auto Const
Potion Property CarrotWild Auto Const
Potion Property MeatCat Auto Const
Potion Property CaveFungus Auto Const
Potion Property steakMirelurkSoftshell Auto Const
Potion Property Corn Auto Const
Potion Property Cram Auto Const
Potion Property SquirrelBitsCrispy Auto Const
Potion Property CurieHealthpak Auto Const
Potion Property DaddyO Auto Const
Potion Property DandyBoyApples Auto Const
Potion Property DayTripper Auto Const
Potion Property EggDeathclaw Auto Const
Potion Property OmeletteEggDeathclaw Auto Const
Potion Property MeatDeathclaw Auto Const
Potion Property SteakDeathclaw Auto Const
Potion Property SouffleDeathclaw Auto Const
Potion Property MS17DeezerLemonade Auto Const
Potion Property DirtyWastelander Auto Const
Potion Property DN102_DruggedWater Auto Const
Potion Property Endangerol Auto Const
Potion Property BoSExperiment Auto Const
Potion Property FancyLadSnackCakes Auto Const
Potion Property FancyLadSnackCakes_PreWar Auto Const
Potion Property DN062FoodPaste Auto Const
Potion Property CarrotV81 Auto Const
Potion Property CornV81 Auto Const
Potion Property MelonV81 Auto Const
Potion Property MutfruitV81 Auto Const
Potion Property Fury Auto Const
Potion Property DN079_MeatGhoul Auto Const
Potion Property BloodpackGlowing Auto Const
Potion Property GlowingFungus Auto Const
Potion Property Gourd Auto Const
Potion Property GourdInstitute Auto Const
Potion Property GourdWild Auto Const
Potion Property GrapeMentats Auto Const
Potion Property SteakRadroach Auto Const
Potion Property steakRadStag Auto Const
Potion Property GroundMolerat Auto Const
Potion Property Gumdrops Auto Const
Potion Property BeerGwinnettAle Auto Const
Potion Property BeerGwinnettBrew Auto Const
Potion Property BeerGwinnettLager Auto Const
Potion Property BeerGwinnettPale Auto Const
Potion Property BeerGwinnettPils Auto Const
Potion Property BeerGwinnettStout Auto Const
Potion Property SweetRollBirthday Auto Const
Potion Property HubFlower Auto Const
Potion Property BeerBottleStandard01_Cold Auto Const
Potion Property BeerGwinnettAle_Cold Auto Const
Potion Property BeerGwinnettBrew_Cold Auto Const
Potion Property BeerGwinnettLager_Cold Auto Const
Potion Property BeerGwinnettPale_Cold Auto Const
Potion Property BeerGwinnettPils_Cold Auto Const
Potion Property BeerGwinnettStout_Cold Auto Const
Potion Property NukaColaCherry_Cold Auto Const
Potion Property NukaCola_Cold Auto Const
Potion Property NukaColaQuantum_Cold Auto Const
Potion Property IguanaBits Auto Const
Potion Property IguanaStick Auto Const
Potion Property IguanaSoup Auto Const
Potion Property InstaMash Auto Const
Potion Property WaterInstitute Auto Const
Potion Property FoodPackInstitute Auto Const
Potion Property GlowingOneBlood Auto Const
Potion Property Jet Auto Const
Potion Property JetFuel Auto Const
Potion Property lockJoint Auto Const
Potion Property MedX Auto Const
Potion Property Melon Auto Const
Potion Property MelonWarwick Auto Const
Potion Property MelonWild Auto Const
Potion Property Mentats Auto Const
Potion Property mindCloud Auto Const
Potion Property MirelurkCake Auto Const
Potion Property MirelurkEgg Auto Const
Potion Property OmeletteEggMirelurk Auto Const
Potion Property MeatMirelurk Auto Const
Potion Property SteakMirelurkQueen Auto Const
Potion Property MoldyFood01 Auto Const
Potion Property steakMolerat Auto Const
Potion Property MeatMolerat Auto Const
Potion Property MS19MoleratPoison Auto Const
Potion Property Meatdog Auto Const
Potion Property steakMutantHound Auto Const
Potion Property MeatMutanthound Auto Const
Potion Property FernFlower Auto Const
Potion Property Mutfruit Auto Const
Potion Property steakDog Auto Const
Potion Property MS09LorenzoSerum Auto Const
Potion Property NoodleCup Auto Const
Potion Property NukaColaCherry Auto Const
Potion Property NukaCola Auto Const
Potion Property NukaColaQuantum Auto Const
Potion Property OrangeMentats Auto Const
Potion Property Overdrive Auto Const
Potion Property Pax Auto Const
Potion Property PreservedPie01 Auto Const
Potion Property DNBostonCommon_BoylstonWine Auto Const
Potion Property PorknBeans Auto Const
Potion Property PotatoCrisps Auto Const
Potion Property LukowskisPottedMeat Auto Const
Potion Property InstaMash_PreWar Auto Const
Potion Property MS05BEggDeathclaw Auto Const
Potion Property Psycho Auto Const
Potion Property PsychoJet Auto Const
Potion Property Psychobuff Auto Const
Potion Property Psychotats Auto Const
Potion Property MeatMirelurkQueen Auto Const
Potion Property MeatRadroach Auto Const
Potion Property EggRadscorpion Auto Const
Potion Property OmeletteEggRadscorpion Auto Const
Potion Property MeatRadscorpion Auto Const
Potion Property SteakRadscorpion Auto Const
Potion Property radscorpionVenom Auto Const
Potion Property MeatRadstag Auto Const
Potion Property stewRadStag Auto Const
Potion Property RRStealthBoy Auto Const
Potion Property Razorgrain Auto Const
Potion Property RefreshingBeverage Auto Const
Potion Property steakBrahmin Auto Const
Potion Property steakMirelurk Auto Const
Potion Property Rum Auto Const
Potion Property SalisburySteak Auto Const
Potion Property SalisburySteak_PreWar Auto Const
Potion Property SiltBean Auto Const
Potion Property SkeetoSpit Auto Const
Potion Property DN124_BuzzBites Auto Const
Potion Property MeatMirelurkSoftshell Auto Const
Potion Property SquirrelBits Auto Const
Potion Property SquirrelStick Auto Const
Potion Property SquirrelStew Auto Const
Potion Property StealthBoy Auto Const
Potion Property steakStingwing Auto Const
Potion Property MeatStingwing Auto Const
Potion Property SugarBombs Auto Const
Potion Property SugarBombs_PreWar Auto Const
Potion Property SweetRoll Auto Const
Potion Property MeatGorilla Auto Const
Potion Property Tarberry Auto Const
Potion Property OmeletteEggDeathclawPristine Auto Const
Potion Property Tato Auto Const
Potion Property TatoWild Auto Const
Potion Property Thistle Auto Const
Potion Property UltraJet Auto Const
Potion Property MS19Cure Auto Const
Potion Property VegetableSoup Auto Const
Potion Property Vodka Auto Const
Potion Property Whiskey Auto Const
Potion Property CornWild Auto Const
Potion Property MutfruitWild Auto Const
Potion Property RazorgrainWild Auto Const
Potion Property TarberryWild Auto Const
Potion Property Wine Auto Const
Potion Property DN133_WineAmontillado Auto Const
Potion Property BoSX111Compound Auto Const
Potion Property XCell Auto Const
Potion Property MeatYaoGuai Auto Const
Potion Property steakYaoGuai Auto Const
Potion Property RoastYaoGuai Auto Const
Potion Property yellowBelly Auto Const
Potion Property YumYumDeviledEggs Auto Const
GlobalVariable Property HC_Vendor_Antiboitic_ChanceNone Auto Const
GlobalVariable Property CheatTerminal_TheMerchant_Aid Auto Const
Ammo Property Ammo308Caliber Auto Const
Ammo Property Ammo38Caliber Auto Const
Ammo Property Ammo44 Auto Const
Ammo Property Ammo45Caliber Auto Const
Ammo Property Ammo50Caliber Auto Const
Ammo Property Ammo10mm Auto Const
Ammo Property Ammo2mmEC Auto Const
Ammo Property Ammo556 Auto Const
Ammo Property Ammo5mm Auto Const
Ammo Property AmmoAlienBlaster Auto Const
Ammo Property AmmoCannonBall Auto Const
Ammo Property AmmoCryoCell Auto Const
Ammo Property AmmoFlamerFuel Auto Const
Ammo Property AmmoFlareGun Auto Const
Ammo Property AmmoFusionCell Auto Const
Ammo Property AmmoFusionCore Auto Const
Ammo Property AmmoGammaCell Auto Const
Ammo Property AmmoSyringer Auto Const
Ammo Property AmmoJunkJet Auto Const
Ammo Property AmmoFatManMiniNuke Auto Const
Ammo Property AmmoMirelurkQueenSpawn Auto Const
Ammo Property AmmoMissile Auto Const
Ammo Property AmmoPlasmaCartridge Auto Const
Ammo Property AmmoRRSpike Auto Const
Ammo Property AmmoShotgunShell Auto Const
GlobalVariable Property CheatTerminal_TheMerchant_Ammo Auto Const
MiscObject Property c_Acid_scrap Auto Const
MiscObject Property c_Adhesive_scrap Auto Const
MiscObject Property c_Aluminum_scrap Auto Const
MiscObject Property c_Antiseptic_scrap Auto Const
MiscObject Property c_Asbestos_scrap Auto Const
MiscObject Property c_AntiBallisticFiber_scrap Auto Const
MiscObject Property c_Bone_scrap Auto Const
MiscObject Property c_Ceramic_scrap Auto Const
MiscObject Property c_Circuitry_scrap Auto Const
MiscObject Property c_Cloth_scrap Auto Const
MiscObject Property c_Concrete_scrap Auto Const
MiscObject Property c_Copper_scrap Auto Const
MiscObject Property c_Cork_scrap Auto Const
MiscObject Property c_Crystal_scrap Auto Const
MiscObject Property c_Fertilizer_scrap Auto Const
MiscObject Property c_FiberOptics_scrap Auto Const
MiscObject Property c_Fiberglass_scrap Auto Const
MiscObject Property c_Gears_scrap Auto Const
MiscObject Property c_Glass_scrap Auto Const
MiscObject Property c_Gold_scrap Auto Const
MiscObject Property c_Lead_scrap Auto Const
MiscObject Property c_Leather_scrap Auto Const
MiscObject Property c_NuclearMaterial_scrap Auto Const
MiscObject Property c_Oil_scrap Auto Const
MiscObject Property c_Plastic_scrap Auto Const
MiscObject Property c_Rubber_scrap Auto Const
MiscObject Property c_Screws_scrap Auto Const
MiscObject Property c_Silver_scrap Auto Const
MiscObject Property c_Springs_scrap Auto Const
MiscObject Property c_Steel_scrap Auto Const
MiscObject Property c_Wood_scrap Auto Const
GlobalVariable Property CheatTerminal_TheMerchant_Crafting Auto Const
