;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:T60RightLegMaterialBOS Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(Armor_Power_T60_LegRight) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(Armor_Power_T60_LegRight) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(Armor_Power_T60_LegRight, PA_T60_Material_BOSKnight)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(Armor_Power_T60_LegRight) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(Armor_Power_T60_LegRight) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(Armor_Power_T60_LegRight, PA_T60_Material_BOSPaladin)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(Armor_Power_T60_LegRight) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(Armor_Power_T60_LegRight) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(Armor_Power_T60_LegRight, PA_T60_Material_BOSSentinel)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(Armor_Power_T60_LegRight) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(Armor_Power_T60_LegRight) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(Armor_Power_T60_LegRight, PA_T60_Material_BOSStandard)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(Armor_Power_T60_LegRight) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(Armor_Power_T60_LegRight) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(Armor_Power_T60_LegRight, PA_T60_Material_BOSOfficer)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(Armor_Power_T60_LegRight) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(Armor_Power_T60_LegRight) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(Armor_Power_T60_LegRight, PA_T60_Material_BOSInitiate)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(Armor_Power_T60_LegRight) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(Armor_Power_T60_LegRight) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(Armor_Power_T60_LegRight, PA_T60_Material_BOSKnightSgt)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(Armor_Power_T60_LegRight) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(Armor_Power_T60_LegRight) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(Armor_Power_T60_LegRight, PA_T60_Material_BOSKnightCaptain)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(Armor_Power_T60_LegRight) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(Armor_Power_T60_LegRight) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(Armor_Power_T60_LegRight, PA_T60_Material_BOSElder)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Message Property CheatTerminal_PowerArmorModMessage Auto Const
Message Property CheatTerminal_PowerArmorTooMany Auto Const
Message Property CheatTerminal_PowerArmorNotEnough Auto Const
Armor Property Armor_Power_T60_LegRight Auto Const
ObjectMod Property PA_T60_Material_BOSElder Auto Const
ObjectMod Property PA_T60_Material_BOSInitiate Auto Const
ObjectMod Property PA_T60_Material_BOSKnight Auto Const
ObjectMod Property PA_T60_Material_BOSKnightCaptain Auto Const
ObjectMod Property PA_T60_Material_BOSKnightSgt Auto Const
ObjectMod Property PA_T60_Material_BOSOfficer Auto Const
ObjectMod Property PA_T60_Material_BOSPaladin Auto Const
ObjectMod Property PA_T60_Material_BOSSentinel Auto Const
ObjectMod Property PA_T60_Material_BOSStandard Auto Const
