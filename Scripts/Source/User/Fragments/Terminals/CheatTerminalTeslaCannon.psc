;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:CheatTerminalTeslaCannon Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00000F99, "ccbgsfo4046-tescan.esl")
if objectToGive
	LL_EpicChance_Standard.SetValue(0.0)
	Game.GetPlayer().AddItem(objectToGive)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

GlobalVariable Property LL_EpicChance_Standard Auto Const
