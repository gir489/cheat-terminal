;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:HalloweenArmor Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000006CA,"ccFSVFO4007-Halloween.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000233,"ccFSVFO4007-Halloween.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0000029B,"ccFSVFO4007-Halloween.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0000070B,"ccFSVFO4007-Halloween.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
