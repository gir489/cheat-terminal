;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsPlayerManipulation Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
if ( Game.GetPlayer().IsInPowerArmor() )
	PowerArmorMessage.Show()
else
	CheatTerminalGlobalFuncs.DoPotion(RacePotion)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CheatTerminal_ChangeNameQuestion.Show() == 0 )
	CheatTerminalGlobalFuncs.DoPotion(NamePotion)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
float flRads = Game.GetPlayer().GetValue(Rads) * -1
Game.GetPlayer().ModValue(Rads, flRads)
Game.GetPlayer().EquipItem(Addictol_SILENT, false, true)
Game.GetPlayer().EquipItem(HC_Antibiotics_SILENT_SCRIPT_ONLY, false, true)
Game.GetPlayer().ResetHealthAndLimbs()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddKeyword(PowerArmorPreventArmorDamageKeyword)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemoveKeyword(PowerArmorPreventArmorDamageKeyword)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Potion Property RacePotion Auto Const
Message Property PowerArmorMessage Auto Const
Message Property CheatTerminal_ChangeNameQuestion Auto Const
Potion Property NamePotion Auto Const
ActorValue Property Rads Auto Const
Potion Property HC_Antibiotics_SILENT_SCRIPT_ONLY Auto Const
Potion Property Addictol_SILENT Auto Const
Keyword Property PowerArmorPreventArmorDamageKeyword Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
