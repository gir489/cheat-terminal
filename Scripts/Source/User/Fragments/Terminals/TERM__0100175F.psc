;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__0100175F Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureMaxson, abForcePersist = true, abDeleteWhenAble = false)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureGavil, abForcePersist = true, abDeleteWhenAble = false)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureBrandis, abForcePersist = true, abDeleteWhenAble = false)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(PowerArmorFurnitureT60_AtomCat, abForcePersist = true, abDeleteWhenAble = false)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(BoSPowerArmorFurnitureKnightPlayer, abForcePersist = true, abDeleteWhenAble = false)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(BoSPowerArmorFurniturePaladin, abForcePersist = true, abDeleteWhenAble = false)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(BoSPowerArmorFurnitureElder, abForcePersist = true, abDeleteWhenAble = false)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(BoSPowerArmorFurnitureSentinel, abForcePersist = true, abDeleteWhenAble = false)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(BoSPowerArmorFurnitureKnightSgt, abForcePersist = true, abDeleteWhenAble = false)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference spawnedObjectedRef = Game.GetPlayer().PlaceAtMe(BoSPowerArmorFurnitureKnightCaptain, abForcePersist = true, abDeleteWhenAble = false)
spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Furniture Property PowerArmorFurnitureT45 Auto Const

Furniture Property PowerArmorFurnitureT51 Auto Const
Furniture Property PowerArmorFurnitureT60 Auto Const
Furniture Property PowerArmorFurnitureT60_AtomCat Auto Const
Furniture Property PowerArmorFurnitureT60_Military Auto Const
Furniture Property PowerArmorFurnitureJetpack Auto Const
Furniture Property BoSPowerArmorFurnitureKnightPlayer Auto Const
Furniture Property BoSPowerArmorFurnitureKnightSgt Auto Const
Furniture Property BoSPowerArmorFurnitureKnightCaptain Auto Const
Furniture Property BoSPowerArmorFurniturePaladin Auto Const
Furniture Property BoSPowerArmorFurnitureSentinel Auto Const
Furniture Property BoSPowerArmorFurnitureElder Auto Const
Furniture Property PowerArmorFurnitureGavil Auto Const
Furniture Property PowerArmorFurnitureBrandis Auto Const
Furniture Property PowerArmorFurnitureMaxson Auto Const

GlobalVariable Property LL_EpicChance_Standard Auto Const

Furniture Property PowerArmorFurnitureRaider Auto Const

Furniture Property PowerArmorFurnitureX01 Auto Const
