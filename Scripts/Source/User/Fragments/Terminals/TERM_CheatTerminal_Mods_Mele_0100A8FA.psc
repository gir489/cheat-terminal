;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Mele_0100A8FA Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_RevolutionarySword_Shock)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_RevolutionarySword_Shock)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_RevolutionarySword_Null)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_RevolutionarySword_Standard)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_RevolutionarySword_Serrated)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_RevolutionarySword_Serrated)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_melee_RevolutionarySword_ShockAndStun)
else
	Game.GetPlayer().AddItem(miscmod_mod_melee_RevolutionarySword_ShockAndStun)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_melee_RevolutionarySword_Null Auto Const
ObjectMod Property mod_melee_RevolutionarySword_Serrated Auto Const
ObjectMod Property mod_melee_RevolutionarySword_Shock Auto Const
ObjectMod Property mod_melee_RevolutionarySword_ShockAndStun Auto Const
MiscObject Property miscmod_mod_melee_RevolutionarySword_Standard Auto Const
MiscObject Property miscmod_mod_melee_RevolutionarySword_Serrated Auto Const
MiscObject Property miscmod_mod_melee_RevolutionarySword_Shock Auto Const
MiscObject Property miscmod_mod_melee_RevolutionarySword_ShockAndStun Auto Const
