;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:CheatTerminalItemsArmorUnique Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_BoS_PowerArmorBlackReward01, CustomItemMods_BoS_PowerArmorBlackReward01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_BoS_PowerArmorBlackReward02, CustomItemMods_BoS_PowerArmorBlackReward02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_DN109_TessasFist, CustomItemMods_DN109_TessasFist)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_BoSM01KellsRewardArmor, CustomItemMods_BoSM01KellsArmorKnight)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_DN015_PolymerLabsRewardArmor, CustomItemMods_DN015_PolymerLabsRewardArmor)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_BoSM02_KellsRewardArmorKnight, CustomItemMods_BoSM02_KellsRewardArmorKnight)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_BoSM02_KellsRewardArmorPaladin, CustomItemMods_BoSM02_KellsRewardArmorPaladin)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_BoSM02_KellsRewardArmorSentinel, CustomItemMods_BoSM02_KellsRewardArmorSentinel)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_BoSM04_KellsRewardArmorKnight, CustomItemMods_BoSM04_KellsRewardArmorKnight)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_BoSM04_KellsRewardArmorPaladin, CustomItemMods_BoSM04_KellsRewardArmorPaladin)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_BoSM04_KellsRewardArmorSentinel, CustomItemMods_BoSM04_KellsRewardArmorSentinel)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_BoS_PowerArmorBlackReward01 Auto Const
FormList Property CustomItemMods_BoS_PowerArmorBlackReward01 Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_BoS_PowerArmorBlackReward02 Auto Const
FormList Property CustomItemMods_BoS_PowerArmorBlackReward02 Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_DN109_TessasFist Auto Const
FormList Property CustomItemMods_DN109_TessasFist Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_BoSM01KellsRewardArmor Auto Const
FormList Property CustomItemMods_BoSM01KellsArmorKnight Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_DN015_PolymerLabsRewardArmor Auto Const
FormList Property CustomItemMods_DN015_PolymerLabsRewardArmor Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_BoSM02_KellsRewardArmorKnight Auto Const
FormList Property CustomItemMods_BoSM02_KellsRewardArmorKnight Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_BoSM02_KellsRewardArmorPaladin Auto Const
FormList Property CustomItemMods_BoSM02_KellsRewardArmorPaladin Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_BoSM02_KellsRewardArmorSentinel Auto Const
FormList Property CustomItemMods_BoSM02_KellsRewardArmorSentinel Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_BoSM04_KellsRewardArmorKnight Auto Const
FormList Property CustomItemMods_BoSM04_KellsRewardArmorKnight Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_BoSM04_KellsRewardArmorPaladin Auto Const
FormList Property CustomItemMods_BoSM04_KellsRewardArmorPaladin Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_BoSM04_KellsRewardArmorSentinel Auto Const
FormList Property CustomItemMods_BoSM04_KellsRewardArmorSentinel Auto Const
