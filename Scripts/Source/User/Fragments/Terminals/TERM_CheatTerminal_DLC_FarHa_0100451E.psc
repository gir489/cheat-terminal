;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_FarHa_0100451E Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0004FA88, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0004885A, "DLCCoast.esm")
if objectToSpawn
	Game.GetPlayer().AddItem(objectToSpawn)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x0004FA7D, "DLCCoast.esm")
if objectToSpawn
	Actor playerActor = Game.GetPlayer()
	ObjectReference objRef = playerActor.PlaceAtMe(objectToSpawn)
	objRef.AttachMod(Game.GetFormFromFile(0x0005D371, "DLCCoast.esm") as ObjectMod)
	Game.GetPlayer().AddItem(objRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00045678, "DLCCoast.esm")
if ( formFromMod)
	LeveledItem modLeveledItem = formFromMod as LeveledItem
	FormList modFormList = Game.GetFormFromFile(0x00045679, "DLCCoast.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(modLeveledItem, modFormList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x00045677, "DLCCoast.esm")
if objectToSpawn
	Actor playerActor = Game.GetPlayer()
	ObjectReference objRef = playerActor.PlaceAtMe(objectToSpawn)
	objRef.AttachMod(Game.GetFormFromFile(0x00010BB5, "DLCCoast.esm") as ObjectMod)
	Game.GetPlayer().AddItem(objRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00057055, "DLCCoast.esm")
if ( formFromMod)
	LeveledItem modLeveledItem = formFromMod as LeveledItem
	FormList modFormList = Game.GetFormFromFile(0x00057056, "DLCCoast.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(modLeveledItem, modFormList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
