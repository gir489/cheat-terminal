;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Weap_01007A86 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Deliverer_Scope_SightsIron)
else
	Game.GetPlayer().AddItem(miscmod_mod_Deliverer_Scope_SightsIron)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Deliverer_Scope_SightsGlow)
else
	Game.GetPlayer().AddItem(miscmod_mod_Deliverer_Scope_SightsGlow)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_Deliverer_Scope_SightsIron Auto Const
ObjectMod Property mod_Deliverer_Scope_SightsGlow Auto Const
MiscObject Property miscmod_mod_Deliverer_Scope_SightsIron Auto Const
MiscObject Property miscmod_mod_Deliverer_Scope_SightsGlow Auto Const
