;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__0100B0B7 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference objRef = Game.GetPlayer().PlaceAtMe(mod_CombatRifle_Scope_SightsImprovedIron)
Game.GetPlayer().AddItem(objRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ObjectMod Property mod_CombatRifle_Scope_SightsImprovedGlowIron Auto Const

ObjectMod Property mod_CombatRifle_Scope_SightsImprovedIron Auto Const
