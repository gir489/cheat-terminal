;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:FarHarborGameAlterations Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference mapMarker = Game.GetFormFromFile(0x0005689A, "DLCCoast.esm") as ObjectReference
If ( mapMarker != None )
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0000AFE3, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x00016E3A, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0000ECE5, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0001F97F, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004F8E2, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x00006128, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x00016E37, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004FE1B, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004F140, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004FE1D, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0001FD17, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0002F40D, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x00016E34, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x000567C0, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x00038EB0, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004855F, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0001536C, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x000049FB, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0001FC9A, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x00005C7A, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x00011A81, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0005691B, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x000258E3, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004FD24, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x000347D1, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x00049D0F, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x000247BF, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x00009351, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0001046E, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x00024F71, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004FFA6, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004FFA8, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x000258E7, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x00037CE9, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x00056918, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004D6B8, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0005422B, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0005689F, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0005422A, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x000399CA, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0000448D, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x00016E3D, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x000567BE, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0002F40B, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x00054254, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(true)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
GlobalVariable glov = Game.GetFormFromFile(0x00032C30, "DLCCoast.esm") as GlobalVariable
if ( glov != None )
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00031709, "DLCCoast.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x0005D0C3, "DLCCoast.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00054264, "DLCCoast.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00050B3C, "DLCCoast.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x0005426B, "DLCCoast.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00013124, "DLCCoast.esm") as GlobalVariable
	glov.SetValue(1)
	int response = CheatTerminal_FarHarborUnlockQuestion.Show()
	if ( response == 0 )
		Quest qst = Game.GetFormFromFile(0x00004C8E, "DLCCoast.esm") as Quest
		qst.SetStage(117)
		qst = Game.GetFormFromFile(0x0000A991, "DLCCoast.esm") as Quest
		qst.SetStage(500)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00014602, "DLCCoast.esm")
if ( formFromMod )
	ObjectReference longRef = formFromMod as ObjectReference
	CheatTerminalGlobalFuncs.DoCompanionTeleport(akTerminalRef, longRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference mapMarker = Game.GetFormFromFile(0x0005689A, "DLCCoast.esm") as ObjectReference
If ( mapMarker != None )
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0000AFE3, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x00016E3A, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0000ECE5, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0001F97F, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004F8E2, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x00006128, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x00016E37, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004FE1B, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004F140, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004FE1D, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0001FD17, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0002F40D, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x00016E34, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x000567C0, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x00038EB0, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004855F, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0001536C, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x000049FB, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0001FC9A, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x00005C7A, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x00011A81, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0005691B, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x000258E3, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004FD24, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x000347D1, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x00049D0F, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x000247BF, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x00009351, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0001046E, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x00024F71, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004FFA6, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004FFA8, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x000258E7, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x00037CE9, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x00056918, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004D6B8, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0005422B, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0005689F, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0005422A, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x000399CA, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0000448D, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x00016E3D, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x000567BE, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0002F40B, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x00054254, "DLCCoast.esm") as ObjectReference
	mapMarker.AddToMap(false)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
Message Property CheatTerminal_FarHarborUnlockQuestion Auto Const
