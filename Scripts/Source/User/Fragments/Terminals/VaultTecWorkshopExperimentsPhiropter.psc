;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:VaultTecWorkshopExperimentsPhiropter Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000053B7, "DLCworkshop03.esm")
if ( formFromMod )
	GlobalVariable glov = formFromMod as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x000053B9, "DLCworkshop03.esm") as GlobalVariable ;DLC06RecipeUnlockPhoropter
	glov.SetValue(3)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000053B7, "DLCworkshop03.esm")
if ( formFromMod )
	GlobalVariable glov = formFromMod as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x000053B9, "DLCworkshop03.esm") as GlobalVariable ;DLC06RecipeUnlockPhoropter
	glov.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000053B7, "DLCworkshop03.esm")
if ( formFromMod )
	GlobalVariable glov = formFromMod as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x000053B9, "DLCworkshop03.esm") as GlobalVariable ;DLC06RecipeUnlockPhoropter
	glov.SetValue(2)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000053B7, "DLCworkshop03.esm")
if ( formFromMod )
	GlobalVariable glov = formFromMod as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x000053B9, "DLCworkshop03.esm") as GlobalVariable ;DLC06RecipeUnlockPhoropter
	glov.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
