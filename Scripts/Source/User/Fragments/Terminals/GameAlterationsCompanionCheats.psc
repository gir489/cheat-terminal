;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsCompanionCheats Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CompanionRef.GetRef() != None)
	Actor followerActor = CompanionRef.GetRef() as Actor
	followerActor.AddPerk(CheatTerminal_Companion_InfiniteCarryWeight, false)
	CheatTerminal_DoesCompanionHaveInfiniteCarryWeight.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CompanionRef.GetRef() != None)
	Actor followerActor = CompanionRef.GetRef() as Actor
	followerActor.RemovePerk(CheatTerminal_Companion_InfiniteCarryWeight)
	CheatTerminal_DoesCompanionHaveInfiniteCarryWeight.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CompanionRef.GetRef() != None)
	Actor followerActor = CompanionRef.GetRef() as Actor
	followerActor.AddKeyword(PowerArmorPreventArmorDamageKeyword)
	CheatTerminal_DoesCompanionHavePowerArmorPerk.SetValue(1.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CompanionRef.GetRef() != None)
	Actor followerActor = CompanionRef.GetRef() as Actor
	followerActor.RemoveKeyword(PowerArmorPreventArmorDamageKeyword)
	CheatTerminal_DoesCompanionHavePowerArmorPerk.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CompanionRef.GetRef() != None)
	Actor followerActor = CompanionRef.GetRef() as Actor
	followerActor.SetEssential(true)
	CheatTerminal_DoesCompanionHaveEssential.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CompanionRef.GetRef() != None)
	Actor followerActor = CompanionRef.GetRef() as Actor
	followerActor.SetEssential(false)
	CheatTerminal_DoesCompanionHaveEssential.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CompanionRef.GetRef() != None)
	Actor followerActor = CompanionRef.GetRef() as Actor
	followerActor.AddKeyword(TeammateDontUseAmmoKeyword)
	CheatTerminal_DoesCompanionHaveInfiniteAmmo.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CompanionRef.GetRef() != None)
	Actor followerActor = CompanionRef.GetRef() as Actor
	followerActor.RemoveKeyword(TeammateDontUseAmmoKeyword)
	CheatTerminal_DoesCompanionHaveInfiniteAmmo.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ReferenceAlias Property CompanionRef Auto Const
Keyword Property TeammateDontUseAmmoKeyword Auto Const
GlobalVariable Property CheatTerminal_DoesCompanionHaveInfiniteAmmo Auto Const
Perk Property CheatTerminal_Companion_InfiniteCarryWeight Auto Const
GlobalVariable Property CheatTerminal_DoesCompanionHaveInfiniteCarryWeight Auto Const
Keyword Property PowerArmorPreventArmorDamageKeyword Auto Const
GlobalVariable Property CheatTerminal_DoesCompanionHavePowerArmorPerk Auto Const
GlobalVariable Property CheatTerminal_DoesCompanionHaveEssential Auto Const
