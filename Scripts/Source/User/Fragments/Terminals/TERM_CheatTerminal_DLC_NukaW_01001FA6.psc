;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_NukaW_01001FA6 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x0003787D, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037BA7, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00037882, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B39, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00037883, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B38, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00037885, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B36, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00037881, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B3A, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x0003788B, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B30, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x0003788A, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B31, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00037884, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B37, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00037880, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B3B, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00037889, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B32, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00037886, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B34, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x0003787F, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B3C, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00037888, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B33, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x0003787E, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B3D, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00037887, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B35, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
