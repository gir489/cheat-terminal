;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Weapons_H_01002E08 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(LL_Flamer)
weaponRef.AttachMod(mod_Legendary_Weapon_Bleed)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Broadsider)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Flamer)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(JunkJet)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Minigun)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(MS16_LL_Minigun_Fahrenheit)
weaponRef.AttachMod(mod_Legendary_Weapon_IncendiaryBullets)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
LeveledItem Property LL_Flamer Auto Const
ObjectMod Property mod_Legendary_Weapon_Bleed Auto Const
Weapon Property JunkJet Auto Const
LeveledItem Property LL_Minigun Auto Const
LeveledItem Property MS16_LL_Minigun_Fahrenheit Auto Const
ObjectMod Property mod_Legendary_Weapon_IncendiaryBullets Auto Const
GlobalVariable Property LL_EpicChance_Standard Auto Const
LeveledItem Property LL_Broadsider Auto Const
