;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__010054A4 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Armor_Synth_ArmLeft_Light)
Game.GetPlayer().AddItem(LL_Armor_Synth_ArmRight_Light)
Game.GetPlayer().AddItem(LL_Armor_Synth_LegLeft_Light)
Game.GetPlayer().AddItem(LL_Armor_Synth_LegRight_Light)
Game.GetPlayer().AddItem(LL_Armor_Synth_Torso_Light)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Armor_Synth_ArmLeft_Medium)
Game.GetPlayer().AddItem(LL_Armor_Synth_ArmRight_Medium)
Game.GetPlayer().AddItem(LL_Armor_Synth_LegLeft_Medium)
Game.GetPlayer().AddItem(LL_Armor_Synth_LegRight_Medium)
Game.GetPlayer().AddItem(LL_Armor_Synth_Torso_Medium)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Armor_Synth_ArmLeft_Heavy)
Game.GetPlayer().AddItem(LL_Armor_Synth_ArmRight_Heavy)
Game.GetPlayer().AddItem(LL_Armor_Synth_LegLeft_Heavy)
Game.GetPlayer().AddItem(LL_Armor_Synth_LegRight_Heavy)
Game.GetPlayer().AddItem(LL_Armor_Synth_Torso_Heavy)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Armor_Synth_ArmLeft_Light_Dirty)
Game.GetPlayer().AddItem(LL_Armor_Synth_ArmRight_Light_Dirty)
Game.GetPlayer().AddItem(LL_Armor_Synth_LegLeft_Light_Dirty)
Game.GetPlayer().AddItem(LL_Armor_Synth_LegRight_Light_Dirty)
Game.GetPlayer().AddItem(LL_Armor_Synth_Torso_Light_Dirty)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Armor_Synth_ArmLeft_Medium_Dirty)
Game.GetPlayer().AddItem(LL_Armor_Synth_ArmRight_Medium_Dirty)
Game.GetPlayer().AddItem(LL_Armor_Synth_LegLeft_Medium_Dirty)
Game.GetPlayer().AddItem(LL_Armor_Synth_LegRight_Medium_Dirty)
Game.GetPlayer().AddItem(LL_Armor_Synth_Torso_Medium_Dirty)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Armor_Synth_ArmLeft_Heavy_Dirty)
Game.GetPlayer().AddItem(LL_Armor_Synth_ArmRight_Heavy_Dirty)
Game.GetPlayer().AddItem(LL_Armor_Synth_LegLeft_Heavy_Dirty)
Game.GetPlayer().AddItem(LL_Armor_Synth_LegRight_Heavy_Dirty)
Game.GetPlayer().AddItem(LL_Armor_Synth_Torso_Heavy_Dirty)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_SynthFieldHelmet_Clean)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_SynthHelmet_Clean)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_SynthHelmet_Dirty)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_SynthFieldHelmet_Dirty)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

LeveledItem Property LL_Armor_Synth_ArmLeft_Light Auto Const
LeveledItem Property LL_Armor_Synth_ArmRight_Light Auto Const
LeveledItem Property LL_Armor_Synth_LegLeft_Light Auto Const
LeveledItem Property LL_Armor_Synth_LegRight_Light Auto Const
LeveledItem Property LL_Armor_Synth_Torso_Light Auto Const

LeveledItem Property LL_Armor_Synth_ArmLeft_Light_Dirty Auto Const
LeveledItem Property LL_Armor_Synth_ArmRight_Light_Dirty Auto Const
LeveledItem Property LL_Armor_Synth_LegLeft_Light_Dirty Auto Const
LeveledItem Property LL_Armor_Synth_LegRight_Light_Dirty Auto Const
LeveledItem Property LL_Armor_Synth_Torso_Light_Dirty Auto Const

LeveledItem Property LL_Armor_Synth_ArmLeft_Medium Auto Const
LeveledItem Property LL_Armor_Synth_ArmRight_Medium Auto Const
LeveledItem Property LL_Armor_Synth_LegLeft_Medium Auto Const
LeveledItem Property LL_Armor_Synth_LegRight_Medium Auto Const
LeveledItem Property LL_Armor_Synth_Torso_Medium Auto Const

LeveledItem Property LL_Armor_Synth_ArmLeft_Medium_Dirty Auto Const
LeveledItem Property LL_Armor_Synth_ArmRight_Medium_Dirty Auto Const
LeveledItem Property LL_Armor_Synth_LegLeft_Medium_Dirty Auto Const
LeveledItem Property LL_Armor_Synth_LegRight_Medium_Dirty Auto Const
LeveledItem Property LL_Armor_Synth_Torso_Medium_Dirty Auto Const

LeveledItem Property LL_Armor_Synth_ArmLeft_Heavy Auto Const
LeveledItem Property LL_Armor_Synth_ArmRight_Heavy Auto Const
LeveledItem Property LL_Armor_Synth_LegLeft_Heavy Auto Const
LeveledItem Property LL_Armor_Synth_LegRight_Heavy Auto Const
LeveledItem Property LL_Armor_Synth_Torso_Heavy Auto Const

LeveledItem Property LL_Armor_Synth_ArmLeft_Heavy_Dirty Auto Const
LeveledItem Property LL_Armor_Synth_ArmRight_Heavy_Dirty Auto Const
LeveledItem Property LL_Armor_Synth_LegLeft_Heavy_Dirty Auto Const
LeveledItem Property LL_Armor_Synth_LegRight_Heavy_Dirty Auto Const
LeveledItem Property LL_Armor_Synth_Torso_Heavy_Dirty Auto Const

LeveledItem Property CheatTerminal_SynthFieldHelmet_Dirty Auto Const
LeveledItem Property CheatTerminal_SynthFieldHelmet_Clean Auto Const
LeveledItem Property CheatTerminal_SynthHelmet_Dirty Auto Const
LeveledItem Property CheatTerminal_SynthHelmet_Clean Auto Const

GlobalVariable Property LL_EpicChance_Standard Auto Const
