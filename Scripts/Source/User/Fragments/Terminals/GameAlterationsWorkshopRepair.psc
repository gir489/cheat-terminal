;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsWorkshopRepair Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
WorkshopScript workshopRef = CheatTerminalGlobalFuncs.FindClosestWorkshop()
if ( workshopRef )
	CheatTerminal_GameAlterations_RepairedWorkshopObjects.Show(RepairAllObjectsInWorkshop(workshopRef))
else
	CheatTerminal_GameAlterations_MissingWorkshop.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
int count = 0
int index = 0
while index < WorkshopParent.WorkshopsCollection.GetCount()
	WorkshopScript workshopRef = WorkshopParent.WorkshopsCollection.GetAt(index) as WorkshopScript
	if ( workshopRef )
		count += RepairAllObjectsInWorkshop(workshopRef)
	endIf
	index += 1
endWhile
CheatTerminal_GameAlterations_RepairedWorkshopObjects.Show(count)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
int Function RepairAllObjectsInWorkshop(WorkshopScript workshopRef)
	ObjectReference[] resourceObjectsDamaged = WorkshopParent.GetResourceObjects(workshopRef, NONE, 1)
	int i = 0
	While ( i < resourceObjectsDamaged.Length)
		WorkshopObjectActorScript damagedObject = resourceObjectsDamaged[i] as WorkshopObjectActorScript
		damagedObject.HandleRepair()
		i += 1
	EndWhile
	return i
endFunction
WorkshopParentScript Property WorkshopParent Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
Message Property CheatTerminal_GameAlterations_MissingWorkshop Auto Const
Message Property CheatTerminal_GameAlterations_RepairedWorkshopObjects Auto Const
