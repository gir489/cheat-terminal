;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:SpecialsAndPerksPerksLuck Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(FortuneFinder01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(FortuneFinder02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(FortuneFinder03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(FortuneFinder04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(FortuneFinder01)
Game.GetPlayer().RemovePerk(FortuneFinder02)
Game.GetPlayer().RemovePerk(FortuneFinder03)
Game.GetPlayer().RemovePerk(FortuneFinder04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Scrounger01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Scrounger02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Scrounger03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Scrounger04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Scrounger01)
Game.GetPlayer().RemovePerk(Scrounger02)
Game.GetPlayer().RemovePerk(Scrounger03)
Game.GetPlayer().RemovePerk(Scrounger04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(BloodyMessPerk01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(BloodyMessPerk02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(BloodyMessPerk03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemoveSpell(BloodyMessNearbySpell)
Game.GetPlayer().RemovePerk(BloodyMessPerk01)
Game.GetPlayer().RemovePerk(BloodyMessPerk02)
Game.GetPlayer().RemovePerk(BloodyMessPerk03)
Game.GetPlayer().RemovePerk(BloodyMessPerk04)
Game.GetPlayer().SetValue(BloodyMess, 0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(MysteriousStranger01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(MysteriousStranger02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(MysteriousStranger03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(MysteriousStranger01)
Game.GetPlayer().RemovePerk(MysteriousStranger02)
Game.GetPlayer().RemovePerk(MysteriousStranger03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(IdiotSavant01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(IdiotSavant02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(IdiotSavant03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_22
Function Fragment_Terminal_22(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(IdiotSavant01)
Game.GetPlayer().RemovePerk(IdiotSavant02)
Game.GetPlayer().RemovePerk(IdiotSavant03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_23
Function Fragment_Terminal_23(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(BetterCriticals01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_24
Function Fragment_Terminal_24(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(BetterCriticals02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_25
Function Fragment_Terminal_25(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(BetterCriticals03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_26
Function Fragment_Terminal_26(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(BetterCriticals01)
Game.GetPlayer().RemovePerk(BetterCriticals02)
Game.GetPlayer().RemovePerk(BetterCriticals03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_27
Function Fragment_Terminal_27(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(CriticalBanker01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_28
Function Fragment_Terminal_28(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(CriticalBanker02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_29
Function Fragment_Terminal_29(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(CriticalBanker03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_30
Function Fragment_Terminal_30(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(CriticalBanker01)
Game.GetPlayer().RemovePerk(CriticalBanker02)
Game.GetPlayer().RemovePerk(CriticalBanker03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_31
Function Fragment_Terminal_31(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(GrimReaperSprint01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_32
Function Fragment_Terminal_32(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(GrimReaperSprint02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_33
Function Fragment_Terminal_33(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(GrimReaperSprint03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_34
Function Fragment_Terminal_34(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(GrimReaperSprint01)
Game.GetPlayer().RemovePerk(GrimReaperSprint02)
Game.GetPlayer().RemovePerk(GrimReaperSprint03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_35
Function Fragment_Terminal_35(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(FourLeafClover01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_36
Function Fragment_Terminal_36(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(FourLeafClover02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_37
Function Fragment_Terminal_37(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(FourLeafClover03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_38
Function Fragment_Terminal_38(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(FourLeafClover04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_39
Function Fragment_Terminal_39(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(FourLeafClover01)
Game.GetPlayer().RemovePerk(FourLeafClover02)
Game.GetPlayer().RemovePerk(FourLeafClover03)
Game.GetPlayer().RemovePerk(FourLeafClover04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_40
Function Fragment_Terminal_40(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Ricochet01)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_41
Function Fragment_Terminal_41(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Ricochet02)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_42
Function Fragment_Terminal_42(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(Ricochet03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_43
Function Fragment_Terminal_43(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().RemovePerk(Ricochet01)
Game.GetPlayer().RemovePerk(Ricochet02)
Game.GetPlayer().RemovePerk(Ricochet03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_44
Function Fragment_Terminal_44(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddPerk(BloodyMessPerk04)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_45
Function Fragment_Terminal_45(ObjectReference akTerminalRef)
;BEGIN CODE
givePerk(FortuneFinder01)
givePerk(FortuneFinder02)
givePerk(FortuneFinder03)
givePerk(FortuneFinder04)
givePerk(Scrounger01)
givePerk(Scrounger02)
givePerk(Scrounger03)
givePerk(Scrounger04)
givePerk(BloodyMessPerk01)
givePerk(BloodyMessPerk02)
givePerk(BloodyMessPerk03)
givePerk(BloodyMessPerk04)
givePerk(MysteriousStranger01)
givePerk(MysteriousStranger02)
givePerk(MysteriousStranger03)
givePerk(IdiotSavant01)
givePerk(IdiotSavant02)
givePerk(IdiotSavant03)
givePerk(BetterCriticals01)
givePerk(BetterCriticals02)
givePerk(BetterCriticals03)
givePerk(CriticalBanker01)
givePerk(CriticalBanker02)
givePerk(CriticalBanker03)
givePerk(GrimReaperSprint01)
givePerk(GrimReaperSprint02)
givePerk(GrimReaperSprint03)
givePerk(FourLeafClover01)
givePerk(FourLeafClover02)
givePerk(FourLeafClover03)
givePerk(FourLeafClover04)
givePerk(Ricochet01)
givePerk(Ricochet02)
givePerk(Ricochet03)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_46
Function Fragment_Terminal_46(ObjectReference akTerminalRef)
;BEGIN CODE
removePerk(FortuneFinder01)
removePerk(FortuneFinder02)
removePerk(FortuneFinder03)
removePerk(FortuneFinder04)
removePerk(Scrounger01)
removePerk(Scrounger02)
removePerk(Scrounger03)
removePerk(Scrounger04)
Game.GetPlayer().RemoveSpell(BloodyMessNearbySpell)
removePerk(BloodyMessPerk01)
removePerk(BloodyMessPerk02)
removePerk(BloodyMessPerk03)
removePerk(BloodyMessPerk04)
Game.GetPlayer().SetValue(BloodyMess, 0)
removePerk(MysteriousStranger01)
removePerk(MysteriousStranger02)
removePerk(MysteriousStranger03)
removePerk(IdiotSavant01)
removePerk(IdiotSavant02)
removePerk(IdiotSavant03)
removePerk(BetterCriticals01)
removePerk(BetterCriticals02)
removePerk(BetterCriticals03)
removePerk(CriticalBanker01)
removePerk(CriticalBanker02)
removePerk(CriticalBanker03)
removePerk(GrimReaperSprint01)
removePerk(GrimReaperSprint02)
removePerk(GrimReaperSprint03)
removePerk(FourLeafClover01)
removePerk(FourLeafClover02)
removePerk(FourLeafClover03)
removePerk(FourLeafClover04)
removePerk(Ricochet01)
removePerk(Ricochet02)
removePerk(Ricochet03)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Function givePerk(Perk perkToGive)
	Actor playerActor = Game.GetPlayer()
	if (playerActor.HasPerk(perkToGive) == false)
		playerActor.AddPerk(perkToGive)
	endIf
endFunction
Function removePerk(Perk perkToGive)
	Actor playerActor = Game.GetPlayer()
	if (playerActor.HasPerk(perkToGive))
		playerActor.RemovePerk(perkToGive)
	endIf
endFunction
Perk Property FortuneFinder01 Auto Const
Perk Property FortuneFinder02 Auto Const
Perk Property FortuneFinder03 Auto Const
Perk Property FortuneFinder04 Auto Const
Perk Property Scrounger01 Auto Const
Perk Property Scrounger02 Auto Const
Perk Property Scrounger03 Auto Const
Perk Property Scrounger04 Auto Const
Perk Property BloodyMessPerk01 Auto Const
Perk Property BloodyMessPerk02 Auto Const
Perk Property BloodyMessPerk03 Auto Const
Perk Property BloodyMessPerk04 Auto Const
Perk Property MysteriousStranger01 Auto Const
Perk Property MysteriousStranger02 Auto Const
Perk Property MysteriousStranger03 Auto Const
Perk Property IdiotSavant01 Auto Const
Perk Property IdiotSavant02 Auto Const
Perk Property IdiotSavant03 Auto Const
Perk Property BetterCriticals01 Auto Const
Perk Property BetterCriticals02 Auto Const
Perk Property BetterCriticals03 Auto Const
Perk Property CriticalBanker01 Auto Const
Perk Property CriticalBanker02 Auto Const
Perk Property CriticalBanker03 Auto Const
Perk Property GrimReaperSprint01 Auto Const
Perk Property GrimReaperSprint02 Auto Const
Perk Property GrimReaperSprint03 Auto Const
Perk Property FourLeafClover01 Auto Const
Perk Property FourLeafClover02 Auto Const
Perk Property FourLeafClover03 Auto Const
Perk Property FourLeafClover04 Auto Const
Perk Property Ricochet01 Auto Const
Perk Property Ricochet02 Auto Const
Perk Property Ricochet03 Auto Const
SPELL Property BloodyMessNearbySpell Auto Const
ActorValue Property BloodyMess Auto Const
