;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Clothing__010091EB Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_Vault75_Underwear)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_Vault101_Underwear)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_Vault101_UnderwearClean)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_Vault111_Underwear)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_Vault111Clean_Underwear)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_Vault114_Underwear)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_Vault114_UnderwearClean)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_Vault75_UnderwearClean)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_Vault81_Underwear)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_Vault95_Underwear)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_Vault81_UnderwearClean)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Armor_Vault95_Underwear_Clean)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(ClothesVaultTecSalesmanCoat)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(ClothesVaultTecSalesmanHat)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Armor Property Armor_Vault101_Underwear Auto Const
Armor Property Armor_Vault101_UnderwearClean Auto Const
Armor Property Armor_Vault111_Underwear Auto Const
Armor Property Armor_Vault111Clean_Underwear Auto Const
Armor Property Armor_Vault114_Underwear Auto Const
Armor Property Armor_Vault114_UnderwearClean Auto Const
Armor Property Armor_Vault75_Underwear Auto Const
Armor Property Armor_Vault75_UnderwearClean Auto Const
Armor Property Armor_Vault81_Underwear Auto Const
Armor Property Armor_Vault81_UnderwearClean Auto Const
Armor Property Armor_Vault95_Underwear Auto Const
Armor Property Armor_Vault95_Underwear_Clean Auto Const

Armor Property ClothesVaultTecSalesmanCoat Auto Const

Armor Property ClothesVaultTecSalesmanHat Auto Const
