;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:SpawnNPCFighters Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_SpawnNPC_Deathclaw, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_SpawnNPC_LibertyPrime, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_SpawnNPC_MrGutsy, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_SpawnNPC_SentryBot, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_SpawnNPC_SuperMutantBehemoth, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_SpawnNPC_Synth, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_SpawnNPC_LaCucaracha, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(CheatTerminal_SpawnNPC_PowerArmorSolider, CheatTerminal_ThrowablesToGive.GetValueInt())
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
GlobalVariable Property CheatTerminal_ThrowablesToGive Auto Const
Weapon Property CheatTerminal_SpawnNPC_Deathclaw Auto Const
Weapon Property CheatTerminal_SpawnNPC_LibertyPrime Auto Const
Weapon Property CheatTerminal_SpawnNPC_MrGutsy Auto Const
Weapon Property CheatTerminal_SpawnNPC_SentryBot Auto Const
Weapon Property CheatTerminal_SpawnNPC_SuperMutantBehemoth Auto Const
Weapon Property CheatTerminal_SpawnNPC_Synth Auto Const
Weapon Property CheatTerminal_SpawnNPC_LaCucaracha Auto Const
Weapon Property CheatTerminal_SpawnNPC_PowerArmorSolider Auto Const