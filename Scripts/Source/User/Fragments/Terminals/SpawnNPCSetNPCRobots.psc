;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:SpawnNPCSetNPCRobots Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActor(CheatTerminal_LvlAssaultron)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActor(CheatTerminal_LvlEyeBot)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActor(CheatTerminal_EncLibertyPrimeFollower)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActor(CheatTerminal_LvlMrGutsy)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActor(CheatTerminal_LvlMrHandy_Random)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActor(CheatTerminal_LvlProtectron)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SetNPCSpawnActor(CheatTerminal_LvlSentryBot)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ActorBase Property CheatTerminal_LvlAssaultron Auto Const
ActorBase Property CheatTerminal_LvlEyeBot Auto Const
ActorBase Property CheatTerminal_EncLibertyPrimeFollower Auto Const
ActorBase Property CheatTerminal_LvlMrGutsy Auto Const
ActorBase Property CheatTerminal_LvlMrHandy_Random Auto Const
ActorBase Property CheatTerminal_LvlProtectron Auto Const
ActorBase Property CheatTerminal_LvlSentryBot Auto Const
