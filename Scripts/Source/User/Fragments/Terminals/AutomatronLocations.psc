;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:AutomatronLocations Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x000008B6, "DLCRobot.esm")
if markerForm
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerForm as ObjectReference, -880, 4832, 1600)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00004315, "DLCRobot.esm")
if markerForm
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerForm as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0000284E, "DLCRobot.esm")
if markerForm
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerForm as ObjectReference, -738.4425, -276.3154, 522.7972)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x0021590C, "Fallout4.esm")
if markerForm
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerForm as ObjectReference, 3941.0075, 1994.1816, -71.9141)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
