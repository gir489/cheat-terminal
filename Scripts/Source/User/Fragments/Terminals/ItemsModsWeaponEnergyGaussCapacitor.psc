;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:ItemsModsWeaponEnergyGaussCapacitor Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GaussRifle_GaussMag_Large_and_MoreDamage1)
else
	Game.GetPlayer().AddItem(miscmod_mod_GaussRifle_GaussMag_Large_and_MoreDamage1)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GaussRifle_GaussMag_Medium)
else
	Game.GetPlayer().AddItem(miscmod_mod_GaussRifle_GaussMag_Medium)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_GaussRifle_GaussMag_ExtraLarge_and_MoreDamage2)
else
	Game.GetPlayer().AddItem(miscmod_mod_GaussRifle_GaussMag_ExtraLarge_and_MoreDamage2)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_GaussRifle_GaussMag_Medium Auto Const
ObjectMod Property mod_GaussRifle_GaussMag_Large_and_MoreDamage1 Auto Const
ObjectMod Property mod_GaussRifle_GaussMag_ExtraLarge_and_MoreDamage2 Auto Const
MiscObject Property miscmod_mod_GaussRifle_GaussMag_Medium Auto Const
MiscObject Property miscmod_mod_GaussRifle_GaussMag_Large_and_MoreDamage1 Auto Const
MiscObject Property miscmod_mod_GaussRifle_GaussMag_ExtraLarge_and_MoreDamage2 Auto Const
