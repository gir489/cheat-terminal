;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_MoveToLoc_01001F09 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, FederalSurveillanceMapMarkerRef001)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, GlowingSeaPOIJS02MapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, GlowingSeaPOIJB04aMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, CaveMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, GlowingSeaCraterMapMarkerREF)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, GlowingSeaPOIRJ03MapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, GlowingSeaPOIRJ01MapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, GlowingSeaEdgeMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, GlowingSeaPOIJS01MapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, GlowingSeaPOIJB05MapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, CaveBottomMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property FederalSurveillanceMapMarkerRef001 Auto Const
ObjectReference Property GlowingSeaPOIJS02MapMarker Auto Const
ObjectReference Property GlowingSeaPOIJB04aMapMarker Auto Const
ObjectReference Property CaveMapMarker Auto Const
ObjectReference Property CaveBottomMapMarker Auto Const
ObjectReference Property GlowingSeaCraterMapMarkerREF Auto Const
ObjectReference Property GlowingSeaPOIRJ03MapMarker Auto Const
ObjectReference Property GlowingSeaPOIRJ01MapMarker Auto Const
ObjectReference Property GlowingSeaEdgeMapMarker Auto Const
ObjectReference Property GlowingSeaPOIJS01MapMarker Auto Const
ObjectReference Property GlowingSeaPOIJB05MapMarker Auto Const
