;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:FarHarborPerks Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00034E7F, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(ActionBoy01) == false )
		playerActor.AddPerk(ActionBoy01)
	endIf
	if ( playerActor.HasPerk(ActionBoy02) == false )
		playerActor.AddPerk(ActionBoy02)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasActionBoy03.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00034E7F, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasActionBoy03.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00034E80, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(ActionGirl01) == false )
		playerActor.AddPerk(ActionGirl01)
	endIf
	if ( playerActor.HasPerk(ActionGirl02) == false )
		playerActor.AddPerk(ActionGirl02)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasActionGirl03.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00034E80, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasActionGirl03.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00034E81, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(CritricalBanker01) == false )
		playerActor.AddPerk(CritricalBanker01)
	endIf
	if ( playerActor.HasPerk(CritricalBanker02) == false )
		playerActor.AddPerk(CritricalBanker02)
	endIf
	if ( playerActor.HasPerk(CritricalBanker03) == false )
		playerActor.AddPerk(CritricalBanker03)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasCriticalBanker04.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00034E81, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasCriticalBanker04.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004FA84, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasFortifyXPBonusPerk.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004FA84, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasFortifyXPBonusPerk.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000365F8, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(LoneWanderer01) == false )
		playerActor.AddPerk(LoneWanderer01)
	endIf
	if ( playerActor.HasPerk(LoneWanderer02) == false )
		playerActor.AddPerk(LoneWanderer02)
	endIf
	if ( playerActor.HasPerk(LoneWanderer03) == false )
		playerActor.AddPerk(LoneWanderer03)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasLoneWanderer04.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000365F8, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasLoneWanderer04.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00043222, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(NightPerson01) == false )
		playerActor.AddPerk(NightPerson01)
	endIf
	if ( playerActor.HasPerk(NightPerson02) == false )
		playerActor.AddPerk(NightPerson02)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasNightPerson03.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002C9B3, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasProtectorOfArcadia.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002C9B3, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasProtectorOfArcadia.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00043222, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasNightPerson03.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000423A4, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(RadResistant01) == false )
		playerActor.AddPerk(RadResistant01)
	endIf
	if ( playerActor.HasPerk(RadResistant02) == false )
		playerActor.AddPerk(RadResistant02)
	endIf
	if ( playerActor.HasPerk(RadResistant03) == false )
		playerActor.AddPerk(RadResistant03)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasRadResistant04.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000423A4, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasRadResistant04.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000423A5, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(Scrapper01) == false )
		playerActor.AddPerk(Scrapper01)
	endIf
	if ( playerActor.HasPerk(Scrapper02) == false )
		playerActor.AddPerk(Scrapper02)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasScrapper03.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000423A5, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasScrapper03.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000423A3, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(StrongBack01) == false )
		playerActor.AddPerk(StrongBack01)
	endIf
	if ( playerActor.HasPerk(StrongBack02) == false )
		playerActor.AddPerk(StrongBack02)
	endIf
	if ( playerActor.HasPerk(StrongBack03) == false )
		playerActor.AddPerk(StrongBack03)
	endIf
	if ( playerActor.HasPerk(StrongBack04) == false )
		playerActor.AddPerk(StrongBack04)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasStrongBack05.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000423A3, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasStrongBack05.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00023B36, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasCrusaderOfAtomPerk.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_22
Function Fragment_Terminal_22(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00023B36, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasCrusaderOfAtomPerk.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_23
Function Fragment_Terminal_23(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00034E7F, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(ActionBoy01) == false )
		playerActor.AddPerk(ActionBoy01)
	endIf
	if ( playerActor.HasPerk(ActionBoy02) == false )
		playerActor.AddPerk(ActionBoy02)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasActionBoy03.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x00034E80, "DLCCoast.esm") as Perk
	if ( playerActor.HasPerk(ActionGirl01) == false )
		playerActor.AddPerk(ActionGirl01)
	endIf
	if ( playerActor.HasPerk(ActionGirl02) == false )
		playerActor.AddPerk(ActionGirl02)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasActionGirl03.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x00034E81, "DLCCoast.esm") as Perk
	if ( playerActor.HasPerk(CritricalBanker01) == false )
		playerActor.AddPerk(CritricalBanker01)
	endIf
	if ( playerActor.HasPerk(CritricalBanker02) == false )
		playerActor.AddPerk(CritricalBanker02)
	endIf
	if ( playerActor.HasPerk(CritricalBanker03) == false )
		playerActor.AddPerk(CritricalBanker03)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasCriticalBanker04.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x000365F8, "DLCCoast.esm") as Perk
	if ( playerActor.HasPerk(LoneWanderer01) == false )
		playerActor.AddPerk(LoneWanderer01)
	endIf
	if ( playerActor.HasPerk(LoneWanderer02) == false )
		playerActor.AddPerk(LoneWanderer02)
	endIf
	if ( playerActor.HasPerk(LoneWanderer03) == false )
		playerActor.AddPerk(LoneWanderer03)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasLoneWanderer04.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x00043222, "DLCCoast.esm") as Perk
	if ( playerActor.HasPerk(NightPerson01) == false )
		playerActor.AddPerk(NightPerson01)
	endIf
	if ( playerActor.HasPerk(NightPerson02) == false )
		playerActor.AddPerk(NightPerson02)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasNightPerson03.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x000423A4, "DLCCoast.esm") as Perk
	if ( playerActor.HasPerk(RadResistant01) == false )
		playerActor.AddPerk(RadResistant01)
	endIf
	if ( playerActor.HasPerk(RadResistant02) == false )
		playerActor.AddPerk(RadResistant02)
	endIf
	if ( playerActor.HasPerk(RadResistant03) == false )
		playerActor.AddPerk(RadResistant03)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasRadResistant04.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x000423A5, "DLCCoast.esm") as Perk
	if ( playerActor.HasPerk(Scrapper01) == false )
		playerActor.AddPerk(Scrapper01)
	endIf
	if ( playerActor.HasPerk(Scrapper02) == false )
		playerActor.AddPerk(Scrapper02)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasScrapper03.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x000423A3, "DLCCoast.esm") as Perk
	if ( playerActor.HasPerk(StrongBack01) == false )
		playerActor.AddPerk(StrongBack01)
	endIf
	if ( playerActor.HasPerk(StrongBack02) == false )
		playerActor.AddPerk(StrongBack02)
	endIf
	if ( playerActor.HasPerk(StrongBack03) == false )
		playerActor.AddPerk(StrongBack03)
	endIf
	if ( playerActor.HasPerk(StrongBack04) == false )
		playerActor.AddPerk(StrongBack04)
	endIf
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasStrongBack05.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x0004FA84, "DLCCoast.esm") as Perk
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasFortifyXPBonusPerk.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x00023B36, "DLCCoast.esm") as Perk
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasCrusaderOfAtomPerk.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x0002C9B4, "DLCCoast.esm") as Perk
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasDestroyerAcadiaPerk.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x0002C9B5, "DLCCoast.esm") as Perk
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasFarHarborSurvivalistPerk.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x00018621, "DLCCoast.esm") as Perk
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasHuntersWisdom.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x0002C9B2, "DLCCoast.esm") as Perk
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasInquisitorOfAtomPerk.SetValue(1)
	perkFromForm = Game.GetFormFromFile(0x0002C9B3, "DLCCoast.esm") as Perk
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasProtectorOfArcadia.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_24
Function Fragment_Terminal_24(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00018621, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasHuntersWisdom.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_25
Function Fragment_Terminal_25(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002C9B4, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasDestroyerAcadiaPerk.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_26
Function Fragment_Terminal_26(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002C9B4, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasDestroyerAcadiaPerk.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_27
Function Fragment_Terminal_27(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002C9B5, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasFarHarborSurvivalistPerk.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_28
Function Fragment_Terminal_28(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002C9B5, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasFarHarborSurvivalistPerk.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_29
Function Fragment_Terminal_29(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00018621, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasHuntersWisdom.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_30
Function Fragment_Terminal_30(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002C9B2, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.AddPerk(perkFromForm)
	CheatTerminal_FarHarbor_HasInquisitorOfAtomPerk.SetValue(1)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_31
Function Fragment_Terminal_31(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0002C9B2, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasInquisitorOfAtomPerk.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_32
Function Fragment_Terminal_32(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00034E7F, "DLCCoast.esm")
if ( formFromMod )
	Perk perkFromForm = formFromMod as Perk
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(ActionBoy01))
		playerActor.RemovePerk(ActionBoy01)
	endIf
	if ( playerActor.HasPerk(ActionBoy02))
		playerActor.RemovePerk(ActionBoy02)
	endIf
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasActionBoy03.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x00034E80, "DLCCoast.esm") as Perk
	if ( playerActor.HasPerk(ActionGirl01))
		playerActor.RemovePerk(ActionGirl01)
	endIf
	if ( playerActor.HasPerk(ActionGirl02))
		playerActor.RemovePerk(ActionGirl02)
	endIf
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasActionGirl03.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x00034E81, "DLCCoast.esm") as Perk
	if ( playerActor.HasPerk(CritricalBanker01))
		playerActor.RemovePerk(CritricalBanker01)
	endIf
	if ( playerActor.HasPerk(CritricalBanker02))
		playerActor.RemovePerk(CritricalBanker02)
	endIf
	if ( playerActor.HasPerk(CritricalBanker03))
		playerActor.RemovePerk(CritricalBanker03)
	endIf
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasCriticalBanker04.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x000365F8, "DLCCoast.esm") as Perk
	if ( playerActor.HasPerk(LoneWanderer01))
		playerActor.RemovePerk(LoneWanderer01)
	endIf
	if ( playerActor.HasPerk(LoneWanderer02))
		playerActor.RemovePerk(LoneWanderer02)
	endIf
	if ( playerActor.HasPerk(LoneWanderer03))
		playerActor.RemovePerk(LoneWanderer03)
	endIf
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasLoneWanderer04.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x00043222, "DLCCoast.esm") as Perk
	if ( playerActor.HasPerk(NightPerson01))
		playerActor.RemovePerk(NightPerson01)
	endIf
	if ( playerActor.HasPerk(NightPerson02))
		playerActor.RemovePerk(NightPerson02)
	endIf
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasNightPerson03.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x000423A4, "DLCCoast.esm") as Perk
	if ( playerActor.HasPerk(RadResistant01))
		playerActor.RemovePerk(RadResistant01)
	endIf
	if ( playerActor.HasPerk(RadResistant02))
		playerActor.RemovePerk(RadResistant02)
	endIf
	if ( playerActor.HasPerk(RadResistant03))
		playerActor.RemovePerk(RadResistant03)
	endIf
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasRadResistant04.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x000423A5, "DLCCoast.esm") as Perk
	if ( playerActor.HasPerk(Scrapper01))
		playerActor.RemovePerk(Scrapper01)
	endIf
	if ( playerActor.HasPerk(Scrapper02))
		playerActor.RemovePerk(Scrapper02)
	endIf
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasScrapper03.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x000423A3, "DLCCoast.esm") as Perk
	if ( playerActor.HasPerk(StrongBack01))
		playerActor.RemovePerk(StrongBack01)
	endIf
	if ( playerActor.HasPerk(StrongBack02))
		playerActor.RemovePerk(StrongBack02)
	endIf
	if ( playerActor.HasPerk(StrongBack03))
		playerActor.RemovePerk(StrongBack03)
	endIf
	if ( playerActor.HasPerk(StrongBack04))
		playerActor.RemovePerk(StrongBack04)
	endIf
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasStrongBack05.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x0004FA84, "DLCCoast.esm") as Perk
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasFortifyXPBonusPerk.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x00023B36, "DLCCoast.esm") as Perk
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasCrusaderOfAtomPerk.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x0002C9B4, "DLCCoast.esm") as Perk
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasDestroyerAcadiaPerk.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x0002C9B5, "DLCCoast.esm") as Perk
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasFarHarborSurvivalistPerk.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x00018621, "DLCCoast.esm") as Perk
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasHuntersWisdom.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x0002C9B2, "DLCCoast.esm") as Perk
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasInquisitorOfAtomPerk.SetValue(0)
	perkFromForm = Game.GetFormFromFile(0x0002C9B3, "DLCCoast.esm") as Perk
	playerActor.RemovePerk(perkFromForm)
	CheatTerminal_FarHarbor_HasProtectorOfArcadia.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Perk Property ActionBoy01 Auto Const
Perk Property ActionBoy02 Auto Const
Perk Property ActionGirl01 Auto Const
Perk Property ActionGirl02 Auto Const
Perk Property CritricalBanker01 Auto Const
Perk Property CritricalBanker02 Auto Const
Perk Property CritricalBanker03 Auto Const
Perk Property LoneWanderer01 Auto Const
Perk Property LoneWanderer02 Auto Const
Perk Property LoneWanderer03 Auto Const
Perk Property NightPerson01 Auto Const
Perk Property NightPerson02 Auto Const
Perk Property RadResistant01 Auto Const
Perk Property RadResistant02 Auto Const
Perk Property RadResistant03 Auto Const
Perk Property Scrapper01 Auto Const
Perk Property Scrapper02 Auto Const
Perk Property StrongBack01 Auto Const
Perk Property StrongBack02 Auto Const
Perk Property StrongBack03 Auto Const
Perk Property StrongBack04 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasActionBoy03 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasActionGirl03 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasFortifyXPBonusPerk Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasCriticalBanker04 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasCrusaderOfAtomPerk Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasDestroyerAcadiaPerk Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasFarHarborSurvivalistPerk Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasHuntersWisdom Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasInquisitorOfAtomPerk Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasLoneWanderer04 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasNightPerson03 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasRadResistant04 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasScrapper03 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasStrongBack05 Auto Const
GlobalVariable Property CheatTerminal_FarHarbor_HasProtectorOfArcadia Auto Const
