;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsCompanionHeight Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CompanionRef.GetRef().SetScale(1)
CheatTerminal_Companion_Height.SetValue(1)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CompanionRef.GetRef().SetScale(0.85)
CheatTerminal_Companion_Height.SetValue(0.85)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CompanionRef.GetRef().SetScale(0.90)
CheatTerminal_Companion_Height.SetValue(0.90)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CompanionRef.GetRef().SetScale(0.95)
CheatTerminal_Companion_Height.SetValue(0.95)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
CompanionRef.GetRef().SetScale(1.05)
CheatTerminal_Companion_Height.SetValue(1.05)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CompanionRef.GetRef().SetScale(1.10)
CheatTerminal_Companion_Height.SetValue(1.10)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
CompanionRef.GetRef().SetScale(1.15)
CheatTerminal_Companion_Height.SetValue(1.15)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
CompanionRef.GetRef().SetScale(0.10)
CheatTerminal_Companion_Height.SetValue(0.10)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
CompanionRef.GetRef().SetScale(0.50)
CheatTerminal_Companion_Height.SetValue(0.50)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
CompanionRef.GetRef().SetScale(1.5)
CheatTerminal_Companion_Height.SetValue(1.5)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
CompanionRef.GetRef().SetScale(3)
CheatTerminal_Companion_Height.SetValue(3)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ReferenceAlias Property CompanionRef Auto Const

GlobalVariable Property CheatTerminal_Companion_Height Auto Const
