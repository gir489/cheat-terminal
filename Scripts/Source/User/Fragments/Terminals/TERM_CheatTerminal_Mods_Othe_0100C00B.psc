;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Othe_0100C00B Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_MissileLauncher_Scope_SightsIron)
else
	Game.GetPlayer().AddItem(miscmod_mod_MissileLauncher_Scope_SightsIron)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_MissileLauncher_Scope_ScopeLong_NV)
else
	Game.GetPlayer().AddItem(miscmod_mod_MissileLauncher_Scope_ScopeLong_NV)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_MissileLauncher_Scope_TargetingBox)
else
	Game.GetPlayer().AddItem(miscmod_mod_MissileLauncher_Scope_TargetingBox)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_MissileLauncher_Scope_ScopeLong)
else
	Game.GetPlayer().AddItem(miscmod_mod_MissileLauncher_Scope_ScopeLong)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_MissileLauncher_Scope_SightsIron Auto Const
ObjectMod Property mod_MissileLauncher_Scope_ScopeLong Auto Const
ObjectMod Property mod_MissileLauncher_Scope_ScopeLong_NV Auto Const
ObjectMod Property mod_MissileLauncher_Scope_TargetingBox Auto Const
MiscObject Property miscmod_mod_MissileLauncher_Scope_SightsIron Auto Const
MiscObject Property miscmod_mod_MissileLauncher_Scope_ScopeLong Auto Const
MiscObject Property miscmod_mod_MissileLauncher_Scope_ScopeLong_NV Auto Const
MiscObject Property miscmod_mod_MissileLauncher_Scope_TargetingBox Auto Const
