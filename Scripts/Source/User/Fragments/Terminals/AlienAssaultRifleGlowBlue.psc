;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:AlienAssaultRifleGlowBlue Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00007AB8,"alienassaultrifle.esp")
if ( modToApply )
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00007AC4,"alienassaultrifle.esp")
if ( modToApply )
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00007AC7,"alienassaultrifle.esp")
if ( modToApply )
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00007AC6,"alienassaultrifle.esp")
if ( modToApply )
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00007AC5,"alienassaultrifle.esp")
if ( modToApply )
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
