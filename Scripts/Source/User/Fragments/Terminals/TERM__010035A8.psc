;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__010035A8 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_InstituteLaserGun_Rifle_Auto)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_InstituteLaserGun_Pistol_Auto)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_InstituteLaserGun_SimpleRifle)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_InstituteLaserGun_SimplePistol)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_InstituteLaserGun_Rifle_SuperSniper)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_InstituteLaserGun_RifleShort_SemiAuto)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(LL_InstituteLaserGun_Shotgun_Rifle_SemiAuto)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
LeveledItem Property LL_InstituteLaserGun_SimplePistol Auto Const

LeveledItem Property LL_InstituteLaserGun_SimpleRifle Auto Const

LeveledItem Property LL_InstituteLaserGun_Rifle_Auto Auto Const

LeveledItem Property LL_InstituteLaserGun_Pistol_Auto Auto Const

LeveledItem Property LL_InstituteLaserGun_Rifle_SuperSniper Auto Const

LeveledItem Property LL_InstituteLaserGun_RifleShort_SemiAuto Auto Const

LeveledItem Property LL_InstituteLaserGun_Shotgun_Rifle_SemiAuto Auto Const Mandatory
