;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:CheatTerminalVaultTecWorkshop Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000049FD, "DLCworkshop03.esm") ;DLC06AllowVaultAnywhere
if ( formFromMod )
	GlobalVariable glov = formFromMod as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x000053B7, "DLCworkshop03.esm") as GlobalVariable ;DLC06RecipeUnlockPhoropter
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x000049FF, "DLCworkshop03.esm") as GlobalVariable ;DLC06RecipeUnlockPowerbike
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x000053C5, "DLCworkshop03.esm") as GlobalVariable ;DLC06RecipeUnlockSlotMachine
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00004BF5, "DLCworkshop03.esm") as GlobalVariable ;DLC06RecipeUnlockSodaStation
	glov.SetValue(1)
;	glov = Game.GetFormFromFile(0x000053B9, "DLCworkshop03.esm") as GlobalVariable ;DLC06RecipeUnlockPhoropterExperimental
;	glov.SetValue(0)
;	glov = Game.GetFormFromFile(0x00004BF7, "DLCworkshop03.esm") as GlobalVariable ;DLC06RecipeUnlockPowerbikeExperimental
;	glov.SetValue(0)
;	glov = Game.GetFormFromFile(0x000053C6, "DLCworkshop03.esm") as GlobalVariable ;DLC06RecipeUnlockSlotMachineExperimental
;	glov.SetValue(0)
;	glov = Game.GetFormFromFile(0x00004BF8, "DLCworkshop03.esm") as GlobalVariable ;DLC06RecipeUnlockSodaStationExperimental
;	glov.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000043FB, "DLCworkshop03.esm")
If ( formFromMod != None )
	ObjectReference mapMarker = formFromMod as ObjectReference
	mapMarker.Enable()
	mapMarker.AddToMap(true)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00004AC7, "DLCworkshop03.esm")
if formFromMod
	CheatTerminalGlobalFuncs.SpawnSettler(formFromMod as ActorBase)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x000043FB, "DLCworkshop03.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
WorkshopParentScript Property WorkshopParent Auto Const
Message Property CheatTerminal_GameAlterations_MissingWorkshop Auto Const
Message Property CheatTerminal_GameAlterations_SpawnedSettler Auto Const
