;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsWorkshopSize Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
WorkshopScript workshopRef = CheatTerminalGlobalFuncs.FindClosestWorkshop()
if ( workshopRef )
	workshopRef.SetValue(WorkshopMaxTriangles, workshopRef.GetBaseValue(WorkshopMaxTriangles) * 0.5)
	workshopRef.SetValue(WorkshopMaxDraws, workshopRef.GetBaseValue(WorkshopMaxDraws) * 0.5)
else
	CheatTerminal_GameAlterations_MissingWorkshop.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
WorkshopScript workshopRef = CheatTerminalGlobalFuncs.FindClosestWorkshop()
if ( workshopRef )
	workshopRef.SetValue(WorkshopMaxTriangles, workshopRef.GetBaseValue(WorkshopMaxTriangles) * 2)
	workshopRef.SetValue(WorkshopMaxDraws, workshopRef.GetBaseValue(WorkshopMaxDraws) * 2)
else
	CheatTerminal_GameAlterations_MissingWorkshop.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ActorValue Property WorkshopMaxTriangles Auto Const
ActorValue Property WorkshopMaxDraws Auto Const
Message Property CheatTerminal_GameAlterations_MissingWorkshop Auto Const
