;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Weap_010072E1 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Receiver_Standard)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Receiver_Standard)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Receiver_Lighter)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Receiver_Lighter)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Receiver_Heavier)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Receiver_Heavier)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Receiver_BetterCriticals)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Receiver_BetterCriticals)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Receiver_MoreDamage1)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Receiver_MoreDamage1)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Receiver_Automatic1)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Receiver_Automatic1)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Receiver_Automatic1_and_ArmorPiercing)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Receiver_Automatic1_and_ArmorPiercing)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Receiver_FastTrigger)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Receiver_FastTrigger)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Receiver_MoreDamage2)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Receiver_MoreDamage2)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Receiver_Automatic1_and_MoreDamage1)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Receiver_Automatic1_and_MoreDamage1)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Receiver_Automatic2)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Receiver_Automatic2)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Receiver_MoreDamage2_and_BetterCriticals)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Receiver_MoreDamage2_and_BetterCriticals)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Receiver_Automatic1_and_MoreDamage1_and_ArmorPiercing)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Receiver_Automatic1_and_MoreDamage1_and_ArmorPiercing)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Receiver_MoreDamage3)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Receiver_MoreDamage3)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Receiver_Automatic1_and_MoreDamage2)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Receiver_Automatic2_and_MoreDamage2)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ObjectMod Property mod_10mm_Receiver_Standard Auto Const
ObjectMod Property mod_10mm_Receiver_Lighter Auto Const
ObjectMod Property mod_10mm_Receiver_Heavier Auto Const
ObjectMod Property mod_10mm_Receiver_BetterCriticals Auto Const
ObjectMod Property mod_10mm_Receiver_MoreDamage1 Auto Const
ObjectMod Property mod_10mm_Receiver_Automatic1 Auto Const
ObjectMod Property mod_10mm_Receiver_Automatic1_and_ArmorPiercing Auto Const
ObjectMod Property mod_10mm_Receiver_FastTrigger Auto Const
ObjectMod Property mod_10mm_Receiver_MoreDamage2 Auto Const
ObjectMod Property mod_10mm_Receiver_Automatic1_and_MoreDamage1 Auto Const
ObjectMod Property mod_10mm_Receiver_Automatic2 Auto Const
ObjectMod Property mod_10mm_Receiver_MoreDamage2_and_BetterCriticals Auto Const
ObjectMod Property mod_10mm_Receiver_Automatic1_and_MoreDamage1_and_ArmorPiercing Auto Const
ObjectMod Property mod_10mm_Receiver_MoreDamage3 Auto Const
ObjectMod Property mod_10mm_Receiver_Automatic1_and_MoreDamage2 Auto Const

MiscObject Property miscmod_mod_10mm_Receiver_Standard Auto Const
MiscObject Property miscmod_mod_10mm_Receiver_Lighter Auto Const
MiscObject Property miscmod_mod_10mm_Receiver_Heavier Auto Const
MiscObject Property miscmod_mod_10mm_Receiver_BetterCriticals Auto Const
MiscObject Property miscmod_mod_10mm_Receiver_MoreDamage1 Auto Const
MiscObject Property miscmod_mod_10mm_Receiver_Automatic1 Auto Const
MiscObject Property miscmod_mod_10mm_Receiver_Automatic1_and_ArmorPiercing Auto Const
MiscObject Property miscmod_mod_10mm_Receiver_FastTrigger Auto Const
MiscObject Property miscmod_mod_10mm_Receiver_MoreDamage2 Auto Const
MiscObject Property miscmod_mod_10mm_Receiver_Automatic1_and_MoreDamage1 Auto Const
MiscObject Property miscmod_mod_10mm_Receiver_Automatic2 Auto Const
MiscObject Property miscmod_mod_10mm_Receiver_MoreDamage2_and_BetterCriticals Auto Const
MiscObject Property miscmod_mod_10mm_Receiver_Automatic1_and_MoreDamage1_and_ArmorPiercing Auto Const
MiscObject Property miscmod_mod_10mm_Receiver_MoreDamage3 Auto Const
MiscObject Property miscmod_mod_10mm_Receiver_Automatic2_and_MoreDamage2 Auto Const
