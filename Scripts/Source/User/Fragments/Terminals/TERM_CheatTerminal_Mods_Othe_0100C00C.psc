;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Othe_0100C00C Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_MissileLauncher_TubeMuzzle_Stabilizer)
else
	Game.GetPlayer().AddItem(miscmod_mod_MissileLauncher_TubeMuzzle_Stabilizer)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_MissileLauncher_TubeMuzzle_Bayonet)
else
	Game.GetPlayer().AddItem(miscmod_mod_MissileLauncher_TubeMuzzle_Bayonet)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )

	grabbedRef.AttachMod(mod_Null_Muzzle as ObjectMod)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_MissileLauncher_TubeMuzzle_Bayonet Auto Const
ObjectMod Property mod_MissileLauncher_TubeMuzzle_Stabilizer Auto Const
MiscObject Property miscmod_mod_MissileLauncher_TubeMuzzle_Bayonet Auto Const
MiscObject Property miscmod_mod_MissileLauncher_TubeMuzzle_Stabilizer Auto Const

ObjectMod Property mod_Null_Muzzle Auto Const
