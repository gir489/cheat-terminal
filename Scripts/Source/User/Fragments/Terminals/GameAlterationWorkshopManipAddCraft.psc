;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationWorkshopManipAddCraft Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
WorkshopScript workshopRef = CheatTerminalGlobalFuncs.FindClosestWorkshop()
if ( workshopRef )
	AddItemsToWorkshop(workshopRef)
else
	CheatTerminal_GameAlterations_MissingWorkshop.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
int index = 0
while index < WorkshopParent.WorkshopsCollection.GetCount()
	WorkshopScript workshopRef = WorkshopParent.WorkshopsCollection.GetAt(index) as WorkshopScript
	if ( workshopRef )
		AddItemsToWorkshop(workshopRef)
	endIf
	index += 1
endWhile
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Function AddItemsToWorkshop(ObjectReference workshopRef)
	workshopRef.AddItem(c_Acid_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Adhesive_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Aluminum_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Antiseptic_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Asbestos_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_AntiBallisticFiber_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Bone_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Ceramic_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Circuitry_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Cloth_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Concrete_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Copper_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Cork_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Crystal_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Fertilizer_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_FiberOptics_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Fiberglass_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Gears_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Glass_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Gold_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Lead_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Leather_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_NuclearMaterial_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Oil_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Plastic_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Rubber_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Screws_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Silver_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Springs_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Steel_scrap, CheatTerminal_CraftToGive.GetValueInt())
	workshopRef.AddItem(c_Wood_scrap, CheatTerminal_CraftToGive.GetValueInt())
EndFunction 
WorkshopParentScript Property WorkshopParent Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
MiscObject Property c_Acid_scrap Auto Const
MiscObject Property c_Adhesive_scrap Auto Const
MiscObject Property c_Aluminum_scrap Auto Const
MiscObject Property c_Antiseptic_scrap Auto Const
MiscObject Property c_Asbestos_scrap Auto Const
MiscObject Property c_AntiBallisticFiber_scrap Auto Const
MiscObject Property c_Bone_scrap Auto Const
MiscObject Property c_Ceramic_scrap Auto Const
MiscObject Property c_Circuitry_scrap Auto Const
MiscObject Property c_Cloth_scrap Auto Const
MiscObject Property c_Concrete_scrap Auto Const
MiscObject Property c_Copper_scrap Auto Const
MiscObject Property c_Cork_scrap Auto Const
MiscObject Property c_Crystal_scrap Auto Const
MiscObject Property c_Fertilizer_scrap Auto Const
MiscObject Property c_FiberOptics_scrap Auto Const
MiscObject Property c_Fiberglass_scrap Auto Const
MiscObject Property c_Gears_scrap Auto Const
MiscObject Property c_Glass_scrap Auto Const
MiscObject Property c_Gold_scrap Auto Const
MiscObject Property c_Lead_scrap Auto Const
MiscObject Property c_Leather_scrap Auto Const
MiscObject Property c_NuclearMaterial_scrap Auto Const
MiscObject Property c_Oil_scrap Auto Const
MiscObject Property c_Plastic_scrap Auto Const
MiscObject Property c_Rubber_scrap Auto Const
MiscObject Property c_Screws_scrap Auto Const
MiscObject Property c_Silver_scrap Auto Const
MiscObject Property c_Springs_scrap Auto Const
MiscObject Property c_Steel_scrap Auto Const
MiscObject Property c_Wood_scrap Auto Const
GlobalVariable Property CheatTerminal_CraftToGive Auto Const
Message Property CheatTerminal_GameAlterations_MissingWorkshop Auto Const
