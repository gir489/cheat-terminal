;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Clothing__010091F2 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Clothes_InstituteLabCoat_Dirty)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Clothes_InstituteLabCoat_blacksleeves_dirty)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Clothes_InstituteLabCoatDivisionHead_Dirty_Blue)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Clothes_InstituteLabCoat_bluesleeves_dirty)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Clothes_InstituteLabCoatDivisionHead_Dirty_Green)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Clothes_InstituteLabCoat_greensleeves_dirty)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Clothes_InstituteLabCoatDivisionHead_Dirty)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Clothes_InstituteLabCoatDivisionHead_Dirty_Orange)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Clothes_InstituteLabCoatDivisionHead_Dirty_Yellow)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Clothes_InstituteUniform_Dirty)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Clothes_InstituteLabCoat_yellowsleeves_dirty)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

Armor Property Clothes_InstituteLabCoat_blacksleeves_dirty Auto Const
Armor Property Clothes_InstituteLabCoatDivisionHead_Dirty_Blue Auto Const
Armor Property Clothes_InstituteLabCoat_bluesleeves_dirty Auto Const
Armor Property Clothes_InstituteLabCoatDivisionHead_Dirty_Green Auto Const
Armor Property Clothes_InstituteLabCoat_greensleeves_dirty Auto Const
Armor Property Clothes_InstituteLabCoatDivisionHead_Dirty Auto Const
Armor Property Clothes_InstituteLabCoat_Dirty Auto Const
Armor Property Clothes_InstituteLabCoatDivisionHead_Dirty_Orange Auto Const
Armor Property Clothes_InstituteLabCoatDivisionHead_Dirty_Yellow Auto Const
Armor Property Clothes_InstituteLabCoat_yellowsleeves_dirty Auto Const
Armor Property Clothes_InstituteUniform_Dirty Auto Const
