;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Othe_0100A911 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AlienBlaster_AlienMag_AmmoStandard)
else
	Game.GetPlayer().AddItem(miscmod_mod_AlienBlaster_AlienMag_AmmoStandard)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_AlienBlaster_AlienMag_AmmoConversion)
else
	Game.GetPlayer().AddItem(miscmod_mod_AlienBlaster_AlienMag_AmmoConversion)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_AlienBlaster_AlienMag_AmmoStandard Auto Const
ObjectMod Property mod_AlienBlaster_AlienMag_AmmoConversion Auto Const
MiscObject Property miscmod_mod_AlienBlaster_AlienMag_AmmoStandard Auto Const
MiscObject Property miscmod_mod_AlienBlaster_AlienMag_AmmoConversion Auto Const
