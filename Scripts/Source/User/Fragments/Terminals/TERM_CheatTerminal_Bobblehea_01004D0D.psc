;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Bobblehea_01004D0D Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Agility)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Barter)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_BigGuns)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Charisma)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Endurance)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_EnergyWeapons)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Explosives)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Intelligence)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Lockpicking)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Luck)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Medicine)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Melee)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Perception)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Repair)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Science)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_SmallGuns)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Sneak)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_18
Function Fragment_Terminal_18(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Speech)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_19
Function Fragment_Terminal_19(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Strength)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_20
Function Fragment_Terminal_20(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Unarmed)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_21
Function Fragment_Terminal_21(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(BobbleHead_Agility)
Game.GetPlayer().AddItem(BobbleHead_Barter)
Game.GetPlayer().AddItem(BobbleHead_BigGuns)
Game.GetPlayer().AddItem(BobbleHead_Charisma)
Game.GetPlayer().AddItem(BobbleHead_Endurance)
Game.GetPlayer().AddItem(BobbleHead_EnergyWeapons)
Game.GetPlayer().AddItem(BobbleHead_Explosives)
Game.GetPlayer().AddItem(BobbleHead_Intelligence)
Game.GetPlayer().AddItem(BobbleHead_Lockpicking)
Game.GetPlayer().AddItem(BobbleHead_Luck)
Game.GetPlayer().AddItem(BobbleHead_Medicine)
Game.GetPlayer().AddItem(BobbleHead_Melee)
Game.GetPlayer().AddItem(BobbleHead_Perception)
Game.GetPlayer().AddItem(BobbleHead_Repair)
Game.GetPlayer().AddItem(BobbleHead_Science)
Game.GetPlayer().AddItem(BobbleHead_SmallGuns)
Game.GetPlayer().AddItem(BobbleHead_Sneak)
Game.GetPlayer().AddItem(BobbleHead_Speech)
Game.GetPlayer().AddItem(BobbleHead_Strength)
Game.GetPlayer().AddItem(BobbleHead_Unarmed)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ObjectReference Property BobbleHead_Strength Auto Const
ObjectReference Property BobbleHead_Perception Auto Const
ObjectReference Property BobbleHead_Endurance Auto Const
ObjectReference Property BobbleHead_Charisma Auto Const
ObjectReference Property BobbleHead_Intelligence Auto Const
ObjectReference Property BobbleHead_Agility Auto Const
ObjectReference Property BobbleHead_Luck Auto Const
ObjectReference Property BobbleHead_Repair Auto Const
ObjectReference Property BobbleHead_EnergyWeapons Auto Const
ObjectReference Property BobbleHead_LockPicking Auto Const
ObjectReference Property BobbleHead_BigGuns Auto Const
ObjectReference Property BobbleHead_Speech Auto Const
ObjectReference Property BobbleHead_Melee Auto Const
ObjectReference Property BobbleHead_Explosives Auto Const
ObjectReference Property BobbleHead_Unarmed Auto Const
ObjectReference Property BobbleHead_Barter Auto Const
ObjectReference Property BobbleHead_Science Auto Const
ObjectReference Property BobbleHead_Sneak Auto Const
ObjectReference Property BobbleHead_Medicine Auto Const
ObjectReference Property BobbleHead_SmallGuns Auto Const
