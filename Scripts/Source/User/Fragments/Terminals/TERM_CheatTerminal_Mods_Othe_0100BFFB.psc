;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Othe_0100BFFB Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Flamer_Receiver_TankStandard)
else
	Game.GetPlayer().AddItem(miscmod_mod_Flamer_Receiver_TankStandard)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_Flamer_Receiver_TankNapalm)
else
	Game.GetPlayer().AddItem(miscmod_mod_Flamer_Receiver_TankNapalm)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_Flamer_Receiver_TankStandard Auto Const
ObjectMod Property mod_Flamer_Receiver_TankNapalm Auto Const
MiscObject Property miscmod_mod_Flamer_Receiver_TankStandard Auto Const
MiscObject Property miscmod_mod_Flamer_Receiver_TankNapalm Auto Const
