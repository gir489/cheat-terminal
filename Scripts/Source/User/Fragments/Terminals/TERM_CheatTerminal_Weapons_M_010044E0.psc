;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Weapons_M_010044E0 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_BaseballBat_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(DN070WorldSeriesBaseballBat)
weaponRef.AttachMod(mod_Custom_DN070WorldSeriesBat)
weaponRef.AttachMod(mod_melee_BaseballBat_Material_StainedMedium)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
ObjectReference weaponRef = playerActor.PlaceAtMe(Aspiration_Weapon_DC_Bat)
weaponRef.AttachMod(mod_Legendary_Weapon_VATSMelee)
weaponRef.AttachMod(mod_melee_BaseballBat_Spikes)
weaponRef.AttachMod(mod_Aspiration_Weapon_IncreasedCost_10)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Board_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_LeadPipe_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_PipeWrench_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.SpawnCustomItem(CustomItem_DoNotPlaceDirectly_BigJim, CustomItemMods_BigJim)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_PoolCue_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_RollingPin_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Baton_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Sledgehammer_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_SuperSledge_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_BaseballBat_Swatter)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_TireIron_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_WalkingCane_Simple)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
LL_EpicChance_Standard.SetValue(0.0)
Game.GetPlayer().AddItem(LL_Baton_Shock)
LL_EpicChance_Standard.SetValue(10.0)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
LeveledItem Property LL_BaseballBat_Simple Auto Const
LeveledItem Property DN070WorldSeriesBaseballBat Auto Const
ObjectMod Property mod_Custom_DN070WorldSeriesBat Auto Const
ObjectMod Property mod_melee_BaseballBat_Material_StainedMedium Auto Const
LeveledItem Property Aspiration_Weapon_DC_Bat Auto Const
ObjectMod Property mod_Legendary_Weapon_VATSMelee Auto Const
ObjectMod Property mod_melee_BaseballBat_Spikes Auto Const
ObjectMod Property mod_Aspiration_Weapon_IncreasedCost_10 Auto Const
LeveledItem Property LL_Board_Simple Auto Const
LeveledItem Property LL_LeadPipe_Simple Auto Const
LeveledItem Property LL_PipeWrench_Simple Auto Const
LeveledItem Property CustomItem_DoNotPlaceDirectly_BigJim Auto Const
FormList Property CustomItemMods_BigJim Auto Const
LeveledItem Property LL_PoolCue_Simple Auto Const
LeveledItem Property LL_RollingPin_Simple Auto Const
LeveledItem Property LL_Baton_Simple Auto Const
LeveledItem Property LL_Baton_Shock Auto Const
LeveledItem Property LL_Sledgehammer_Simple Auto Const
LeveledItem Property LL_SuperSledge_Simple Auto Const
LeveledItem Property LL_BaseballBat_Swatter Auto Const
LeveledItem Property LL_TireIron_Simple Auto Const
LeveledItem Property LL_WalkingCane_Simple Auto Const
GlobalVariable Property LL_EpicChance_Standard Auto Const
