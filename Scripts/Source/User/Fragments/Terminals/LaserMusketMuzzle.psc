;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:LaserMusketMuzzle Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserMusket_Muzzle_Camera_A)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserMusket_Muzzle_Camera_A)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserMusket_Muzzle_Focuser_A)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserMusket_Muzzle_Focuser_A)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserMusket_Muzzle_Splitter_A)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserMusket_Muzzle_Splitter_A)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_LaserMusket_Muzzle_Camera_A Auto Const
ObjectMod Property mod_LaserMusket_Muzzle_Focuser_A Auto Const
ObjectMod Property mod_LaserMusket_Muzzle_Splitter_A Auto Const
MiscObject Property miscmod_mod_LaserMusket_Muzzle_Camera_A Auto Const
MiscObject Property miscmod_mod_LaserMusket_Muzzle_Focuser_A Auto Const
MiscObject Property miscmod_mod_LaserMusket_Muzzle_Splitter_A Auto Const
