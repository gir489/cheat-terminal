;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__0100A904 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Scope_SightsIron)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Scope_SightsIron)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Scope_SightReflex)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Scope_SightReflex)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Scope_SightsImprovedIron)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Scope_SightsIronImproved)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Scope_ScopeMedium)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Scope_ScopeMedium)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Scope_ScopeLong)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Scope_ScopeLong)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Scope_ScopeShort_NV)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Scope_ScopeShort_NV)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Scope_ScopeMedium_NV)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Scope_ScopeMedium_NV)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Scope_ScopeLong_NV)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Scope_ScopeLong_NV)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Scope_ScopeShort_Recon)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Scope_ScopeShort_Recon)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Scope_ScopeLong_Recon)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Scope_ScopeLong_Recon)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_LaserGun_Scope_ScopeShort)
else
	Game.GetPlayer().AddItem(miscmod_mod_LaserGun_Scope_ScopeShort)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_LaserGun_Scope_SightsIron Auto Const
ObjectMod Property mod_LaserGun_Scope_SightsImprovedIron Auto Const
ObjectMod Property mod_LaserGun_Scope_ScopeShort Auto Const
ObjectMod Property mod_LaserGun_Scope_SightReflex Auto Const
ObjectMod Property mod_LaserGun_Scope_ScopeMedium Auto Const
ObjectMod Property mod_LaserGun_Scope_ScopeLong Auto Const
ObjectMod Property mod_LaserGun_Scope_ScopeShort_NV Auto Const
ObjectMod Property mod_LaserGun_Scope_ScopeMedium_NV Auto Const
ObjectMod Property mod_LaserGun_Scope_ScopeLong_NV Auto Const
ObjectMod Property mod_LaserGun_Scope_ScopeShort_Recon Auto Const
ObjectMod Property mod_LaserGun_Scope_ScopeLong_Recon Auto Const
MiscObject Property miscmod_mod_LaserGun_Scope_SightsIron Auto Const
MiscObject Property miscmod_mod_LaserGun_Scope_SightsIronImproved Auto Const
MiscObject Property miscmod_mod_LaserGun_Scope_ScopeShort Auto Const
MiscObject Property miscmod_mod_LaserGun_Scope_SightReflex Auto Const
MiscObject Property miscmod_mod_LaserGun_Scope_ScopeMedium Auto Const
MiscObject Property miscmod_mod_LaserGun_Scope_ScopeLong Auto Const
MiscObject Property miscmod_mod_LaserGun_Scope_ScopeShort_NV Auto Const
MiscObject Property miscmod_mod_LaserGun_Scope_ScopeMedium_NV Auto Const
MiscObject Property miscmod_mod_LaserGun_Scope_ScopeLong_NV Auto Const
MiscObject Property miscmod_mod_LaserGun_Scope_ScopeShort_Recon Auto Const
MiscObject Property miscmod_mod_LaserGun_Scope_ScopeLong_Recon Auto Const
