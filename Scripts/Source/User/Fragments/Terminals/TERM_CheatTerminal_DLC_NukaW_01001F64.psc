;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_NukaW_01001F64 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004430D, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004430F, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00044311, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00044313, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00044315, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00044317, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00044319, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004431B, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004431D, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004431F, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00044322, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00044321, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00044325, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00044327, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00044329, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004430D, "DLCNukaWorld.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod as ObjectReference)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x0004430F, "DLCNukaWorld.esm") as ObjectReference)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00044311, "DLCNukaWorld.esm") as ObjectReference)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00044313, "DLCNukaWorld.esm") as ObjectReference)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00044315, "DLCNukaWorld.esm") as ObjectReference)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00044317, "DLCNukaWorld.esm") as ObjectReference)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00044319, "DLCNukaWorld.esm") as ObjectReference)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x0004431B, "DLCNukaWorld.esm") as ObjectReference)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x0004431D, "DLCNukaWorld.esm") as ObjectReference)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x0004431F, "DLCNukaWorld.esm") as ObjectReference)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00044321, "DLCNukaWorld.esm") as ObjectReference)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00044322, "DLCNukaWorld.esm") as ObjectReference)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00044325, "DLCNukaWorld.esm") as ObjectReference)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00044327, "DLCNukaWorld.esm") as ObjectReference)
	Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00044329, "DLCNukaWorld.esm") as ObjectReference)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
