;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:AssaultCarbineWeapons Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00002EB6, "DPAssaultCarbine.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00000FA2, "DPAssaultCarbine.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00002EB3, "DPAssaultCarbine.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00002EB4, "DPAssaultCarbine.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00002EB5, "DPAssaultCarbine.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00002EB8, "DPAssaultCarbine.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00006BD6, "DPAssaultCarbine.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00001F5B, "DPAssaultCarbine.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().AddItem(Game.GetFormFromFile(0x00002F67, "DPAssaultCarbine.esp"))
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
CheatTerminal_AssaultCarbine_gir489sArmyM16A4.Revert()
CheatTerminal_AssaultCarbine_gir489sArmyM16A4.AddForm(Game.GetFormFromFile(0x00000FA2, "DPAssaultCarbine.esp"), 1, 1)
ObjectReference weaponRef = playerActor.PlaceAtMe(CheatTerminal_AssaultCarbine_gir489sArmyM16A4)
weaponRef.AttachMod(Game.GetFormFromFile(0x00000FA6,"DPAssaultCarbine.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00000FAE,"DPAssaultCarbine.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00000FB4,"DPAssaultCarbine.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00007B0D,"DPAssaultCarbine.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00006BD1,"DPAssaultCarbine.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00000FE5,"DPAssaultCarbine.esp") as ObjectMod)
weaponRef.AttachMod(Game.GetFormFromFile(0x00000FCB,"DPAssaultCarbine.esp") as ObjectMod)
weaponRef.AttachMod(CheatTerminal_NoRecoil)
weaponRef.AttachMod(mod_Legendary_Weapon_DamageLimbRecoil)
playerActor.AddItem(weaponRef)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

LeveledItem Property CheatTerminal_AssaultCarbine_gir489sArmyM16A4 Auto Const
ObjectMod Property CheatTerminal_NoRecoil Auto Const
ObjectMod Property mod_Legendary_Weapon_DamageLimbRecoil Auto Const
