;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01007AF9 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(MurkyCabinMapMarker.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(GraygardenMapMarker.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(GreentopMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(GNNMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(HubCityAutoWreckersMapMarker.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(HugosHoleMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(HydeParkMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(IrishPrideIndustriesShipyardMapMarker.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(JalbertBrothersDisposalMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(JamaicaPlainMapMarkerRef.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Game.GetPlayer().MoveTo(KingsportLighthouseMapMarker.GetLinkedRef())
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ObjectReference Property MurkyCabinMapMarker Auto Const

ObjectReference Property GraygardenMapMarker Auto Const

ObjectReference Property GreentopMapMarkerRef Auto Const

ObjectReference Property GNNMapMarkerRef Auto Const

ObjectReference Property HubCityAutoWreckersMapMarker Auto Const

ObjectReference Property HugosHoleMapMarkerRef Auto Const

ObjectReference Property HydeParkMapMarkerRef Auto Const

ObjectReference Property IrishPrideIndustriesShipyardMapMarker Auto Const

ObjectReference Property JalbertBrothersDisposalMapMarkerRef Auto Const

ObjectReference Property JamaicaPlainMapMarkerRef Auto Const

ObjectReference Property KingsportLighthouseMapMarker Auto Const

ObjectReference Property LakeCochituateMapMarkerRef Auto Const
