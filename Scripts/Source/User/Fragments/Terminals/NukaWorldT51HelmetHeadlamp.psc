;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:NukaWorldT51HelmetHeadlamp Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(GetArmorPiece()) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(GetArmorPiece()) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(GetArmorPiece(), PA_T51_Helmet_Headlamp)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(GetArmorPiece()) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(GetArmorPiece()) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(GetArmorPiece(), PA_T51_Helmet_Headlamp_Blue)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(GetArmorPiece()) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(GetArmorPiece()) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(GetArmorPiece(), PA_T51_Helmet_Headlamp_Bright)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(GetArmorPiece()) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(GetArmorPiece()) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(GetArmorPiece(), PA_T51_Helmet_Headlamp_Purple)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(GetArmorPiece()) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(GetArmorPiece()) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(GetArmorPiece(), PA_T51_Helmet_Headlamp_RedTactical)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Actor playerActor = Game.GetPlayer()
if ( playerActor.IsInPowerArmor() )
	if ( playerActor.GetItemCount(GetArmorPiece()) > 1 )
		CheatTerminal_PowerArmorTooMany.Show()
	elseif ( playerActor.GetItemCount(GetArmorPiece()) == 0 )
		CheatTerminal_PowerArmorNotEnough.Show()
	else
		playerActor.AttachModToInventoryItem(GetArmorPiece(), PA_T51_Helmet_Headlamp_VaultBoy)
	endIf
else
	CheatTerminal_PowerArmorModMessage.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Form Function GetArmorPiece()
	return Game.GetFormFromFile(0x00011515, "DLCNukaWorld.esm")
EndFunction
Message Property CheatTerminal_PowerArmorModMessage Auto Const
Message Property CheatTerminal_PowerArmorTooMany Auto Const
Message Property CheatTerminal_PowerArmorNotEnough Auto Const
ObjectMod Property PA_T51_Helmet_Headlamp Auto Const
ObjectMod Property PA_T51_Helmet_Headlamp_Blue Auto Const
ObjectMod Property PA_T51_Helmet_Headlamp_Bright Auto Const
ObjectMod Property PA_T51_Helmet_Headlamp_Purple Auto Const
ObjectMod Property PA_T51_Helmet_Headlamp_RedTactical Auto Const
ObjectMod Property PA_T51_Helmet_Headlamp_VaultBoy Auto Const
