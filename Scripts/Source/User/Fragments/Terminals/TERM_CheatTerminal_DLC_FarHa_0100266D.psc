;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_FarHa_0100266D Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0003A388, "DLCCoast.esm")
if ( formFromMod)
	LeveledItem modLeveledItem = formFromMod as LeveledItem
	FormList modFormList = Game.GetFormFromFile(0x0003A389, "DLCCoast.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(modLeveledItem, modFormList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x00034E7A, "DLCCoast.esm")
if objectToGive
	LL_EpicChance_Standard.SetValue(0.0)
	Game.GetPlayer().AddItem(objectToGive)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0005158C, "DLCCoast.esm")
if ( formFromMod)
	LeveledItem modLeveledItem = formFromMod as LeveledItem
	FormList modFormList = Game.GetFormFromFile(0x00051596, "DLCCoast.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(modLeveledItem, modFormList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00034E7B, "DLCCoast.esm")
if ( formFromMod)
	LeveledItem modLeveledItem = formFromMod as LeveledItem
	FormList modFormList = Game.GetFormFromFile(0x00056A19, "DLCCoast.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(modLeveledItem, modFormList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToGive = Game.GetFormFromFile(0x0004FF72, "DLCCoast.esm")
if objectToGive
	LL_EpicChance_Standard.SetValue(0.0)
	Game.GetPlayer().AddItem(objectToGive)
	LL_EpicChance_Standard.SetValue(10.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0005158D, "DLCCoast.esm")
if ( formFromMod)
	LeveledItem modLeveledItem = formFromMod as LeveledItem
	FormList modFormList = Game.GetFormFromFile(0x00051597, "DLCCoast.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(modLeveledItem, modFormList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004FA7E, "DLCCoast.esm")
if ( formFromMod)
	LeveledItem modLeveledItem = formFromMod as LeveledItem
	FormList modFormList = Game.GetFormFromFile(0x0004FA7F, "DLCCoast.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(modLeveledItem, modFormList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0004923C, "DLCCoast.esm")
if ( formFromMod)
	LeveledItem modLeveledItem = formFromMod as LeveledItem
	FormList modFormList = Game.GetFormFromFile(0x0004923D, "DLCCoast.esm") as FormList
	CheatTerminalGlobalFuncs.SpawnCustomItem(modLeveledItem, modFormList)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
GlobalVariable Property LL_EpicChance_Standard Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
