;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:ContrapMiscItems Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0000092B, "DLCworkshop02.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000D20, "DLCworkshop02.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000D1F, "DLCworkshop02.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000D1E, "DLCworkshop02.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000D18, "DLCworkshop02.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000008CF, "DLCworkshop02.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000D16, "DLCworkshop02.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000A4B, "DLCworkshop02.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000D17, "DLCworkshop02.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000977, "DLCworkshop02.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000900, "DLCworkshop02.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000906, "DLCworkshop02.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000901, "DLCworkshop02.esm")
if formFromMod
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_MiscToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
GlobalVariable Property CheatTerminal_MiscToGive Auto Const