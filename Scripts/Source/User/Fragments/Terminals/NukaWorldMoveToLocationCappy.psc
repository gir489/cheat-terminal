;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:NukaWorldMoveToLocationCappy Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00015A85, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	NukaWorldCappyLookAtRef.MoveTo(markerRef, -55.132324, -10.498887, 77.900024, false)
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef, 53.277676, 47.521113, -44.659976, 0.0, NukaWorldCappyLookAtRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x000315EF, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef, 23.757815, 71.537246, -17.580557, 200.87)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00015A87, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	NukaWorldCappyLookAtRef.MoveTo(markerRef, 71.396484, 3.506836, 76, false)
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef, -74.483516, -62.033164, objectToLookAtParam = NukaWorldCappyLookAtRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Game.GetFormFromFile(0x00015A88, "DLCNukaWorld.esm") as ObjectReference, 86.187979, -44.769609, -106.04916, 302.08)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00015A8D, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	NukaWorldCappyLookAtRef.MoveTo(markerRef, 28.891055, 68.71106, 106.656738, false)
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef, -24.678945, -27.63894, 0.366738, 0.0, NukaWorldCappyLookAtRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00015A90, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	NukaWorldCappyLookAtRef.MoveTo(markerRef, -25.503906, 13.824463, 87.999939, false)
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef, 13.866094, 83.204463, 0.549939, 0.0, NukaWorldCappyLookAtRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00015A93, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	NukaWorldCappyLookAtRef.MoveTo(markerRef, -57.255737, 65.633911, 102, false)
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef, -1.815737, -22.116089, -0.1, 0.0, NukaWorldCappyLookAtRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00015A96, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	NukaWorldCappyLookAtRef.MoveTo(markerRef, 7.274727, 436.532715, 138.654663, false)
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef, 18.824727, 49.782715, -1.165337, 0.0, NukaWorldCappyLookAtRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00015A99, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	NukaWorldCappyLookAtRef.MoveTo(markerRef, -31.514707, 3.757813, 86, false)
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef, 30.655293, 7.797813, objectToLookAtParam = NukaWorldCappyLookAtRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Form markerForm = Game.GetFormFromFile(0x00015A9C, "DLCNukaWorld.esm")
if markerForm
	ObjectReference markerRef = markerForm as ObjectReference
	NukaWorldCappyLookAtRef.MoveTo(markerRef, 77.316632, 27.042969, 145.651245, false)
	CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, markerRef, -68.313368, 20.222969, -28.778755, 0.0, NukaWorldCappyLookAtRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000806, "DLCNukaWorld.esm")
if ( formFromMod )
	Quest DLC04MS01 = formFromMod as Quest
	if ( DLC04MS01.IsRunning() )
		DLC04MS01.SetStage(110)
		DLC04MS01.SetStage(115)
		DLC04MS01.SetStage(120)
		DLC04MS01.SetStage(125)
		DLC04MS01.SetStage(130)
		DLC04MS01.SetStage(135)
		DLC04MS01.SetStage(140)
		DLC04MS01.SetStage(145)
		DLC04MS01.SetStage(150)
		DLC04MS01.SetStage(155)
	endIf
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const

ObjectReference Property NukaWorldCappyLookAtRef Auto Const
