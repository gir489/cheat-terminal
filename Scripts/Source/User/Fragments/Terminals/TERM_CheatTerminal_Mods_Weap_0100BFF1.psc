;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_Mods_Weap_0100BFF1 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_DoubleBarrelShotgun_Scope_SightsIron)
else
	Game.GetPlayer().AddItem(miscmod_mod_DoubleBarrelShotgun_Scope_SightsIron)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_DoubleBarrelShotgun_Scope_SightReflex)
else
	Game.GetPlayer().AddItem(miscmod_mod_DoubleBarrelShotgun_Scope_SightReflex)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_DoubleBarrelShotgun_Scope_SightReflexCircle)
else
	Game.GetPlayer().AddItem(miscmod_mod_DoubleBarrelShotgun_Scope_SightReflexCircle)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_DoubleBarrelShotgun_Scope_SightsIronGlow)
else
	Game.GetPlayer().AddItem(miscmod_mod_DoubleBarrelShotgun_Scope_SightsIronGlow)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ObjectMod Property mod_DoubleBarrelShotgun_Scope_SightsIron Auto Const
ObjectMod Property mod_DoubleBarrelShotgun_Scope_SightsIronGlow Auto Const
ObjectMod Property mod_DoubleBarrelShotgun_Scope_SightReflex Auto Const
ObjectMod Property mod_DoubleBarrelShotgun_Scope_SightReflexCircle Auto Const
MiscObject Property miscmod_mod_DoubleBarrelShotgun_Scope_SightsIron Auto Const
MiscObject Property miscmod_mod_DoubleBarrelShotgun_Scope_SightsIronGlow Auto Const
MiscObject Property miscmod_mod_DoubleBarrelShotgun_Scope_SightReflex Auto Const
MiscObject Property miscmod_mod_DoubleBarrelShotgun_Scope_SightReflexCircle Auto Const
