;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsWorkshopDeleteItems Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
WorkshopScript workshopRef = CheatTerminalGlobalFuncs.FindClosestWorkshop()
if ( workshopRef == None )
	CheatTerminal_GameAlterations_MissingWorkshop.Show()
	return
endIf
int i = 0
While ( i < CheatTerminal_AllScrapables.GetSize() )
	FormList formListToCheck = CheatTerminal_AllScrapables.GetAt(i) as FormList
	ObjectReference objRef = Game.FindClosestReferenceOfAnyTypeInListFromRef(formListToCheck, workshopRef as ObjectReference, 4000)
	While ( objRef != None )
		if ( objRef.HasKeyword(UnscrappableObject) )
			BackupScrapItem(objRef)
			objRef = Game.FindClosestReferenceOfAnyTypeInListFromRef(formListToCheck, workshopRef as ObjectReference, 4000)
		else
			objRef.Reset(CheatTerminal_AidStorageRef) ;This is required to move the object away from the cell while the engine deletes the object.
			objRef.Delete()
			objRef = Game.FindClosestReferenceOfAnyTypeInListFromRef(formListToCheck, workshopRef as ObjectReference, 4000)
		endIf
	EndWhile
	i += 1
EndWhile
RestoreBackedUpScrapItems()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
WorkshopScript workshopRef = CheatTerminalGlobalFuncs.FindClosestWorkshop()
if ( workshopRef == None )
	CheatTerminal_GameAlterations_MissingWorkshop.Show()
	return
endIf
int i = 0
While ( i < CheatTerminal_AllWorkshopItems.GetSize() )
	FormList formListToCheck = CheatTerminal_AllWorkshopItems.GetAt(i) as FormList
	ObjectReference objRef = Game.FindClosestReferenceOfAnyTypeInListFromRef(formListToCheck, workshopRef as ObjectReference, 4000)
	While ( objRef != None )
		if ( objRef.HasKeyword(UnscrappableObject) )
			BackupWorkshopItem(objRef)
			objRef = Game.FindClosestReferenceOfAnyTypeInListFromRef(formListToCheck, workshopRef as ObjectReference, 4000)
		else
			objRef.Reset(CheatTerminal_AidStorageRef) ;This is required to move the object away from the cell while the engine deletes the object.
			workshopRef.StoreInWorkshop(objRef.GetBaseObject())
			objRef.Delete()
			objRef = Game.FindClosestReferenceOfAnyTypeInListFromRef(formListToCheck, workshopRef as ObjectReference, 4000)
		endIf
	EndWhile
	i += 1
EndWhile
RestoreBackedupWorkshopItems()
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
Function BackupScrapItem(ObjectReference objToBackup)
	CheatTerminal_ScrapObjectsToBackup.AddForm(objToBackup)
	ObjectReference marker = objToBackup.PlaceAtMe(XMarker) as ObjectReference
	CheatTerminal_ScrapObjectsToBackupXMarkers.AddForm(marker)
	objToBackup.Reset(CheatTerminal_AidStorageRef) ;This is required to move the object away from the cell while the engine deletes other objects.
EndFunction
Function BackupWorkshopItem(ObjectReference objToBackup)
	CheatTerminal_WorkshopObjectsToBackup.AddForm(objToBackup)
	ObjectReference marker = objToBackup.PlaceAtMe(XMarker) as ObjectReference
	CheatTerminal_WorkshopObjectsToBackupXMarkers.AddForm(marker)
	objToBackup.Reset(CheatTerminal_AidStorageRef) ;This is required to move the object away from the cell while the engine deletes other objects.
EndFunction
Function RestoreBackedUpScrapItems()
	int i = 0
	While ( i < CheatTerminal_ScrapObjectsToBackup.GetSize() )
		ObjectReference objFromFormList = CheatTerminal_ScrapObjectsToBackup.GetAt(i) as ObjectReference
		ObjectReference marker = CheatTerminal_ScrapObjectsToBackupXMarkers.GetAt(i) as ObjectReference
		objFromFormList.Reset(marker)
		marker.Delete()
		i += 1
	EndWhile
	CheatTerminal_ScrapObjectsToBackup.Revert()
	CheatTerminal_ScrapObjectsToBackupXMarkers.Revert()
EndFunction
Function RestoreBackedupWorkshopItems()
	int i = 0
	While ( i < CheatTerminal_WorkshopObjectsToBackup.GetSize() )
		ObjectReference objFromFormList = CheatTerminal_WorkshopObjectsToBackup.GetAt(i) as ObjectReference
		ObjectReference marker = CheatTerminal_WorkshopObjectsToBackupXMarkers.GetAt(i) as ObjectReference
		objFromFormList.Reset(marker)
		marker.Delete()
		i += 1
	EndWhile
	CheatTerminal_WorkshopObjectsToBackup.Revert()
	CheatTerminal_WorkshopObjectsToBackupXMarkers.Revert()
EndFunction
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
Message Property CheatTerminal_GameAlterations_MissingWorkshop Auto Const
GlobalVariable Property CheatTerminal_DeletePlayerOwnedWorkshopItems Auto Const
Faction Property PlayerFaction Auto Const
Keyword Property UnscrappableObject Auto Const
FormList Property CheatTerminal_AllScrapables Auto Const
FormList Property CheatTerminal_AllWorkshopItems Auto Const
ObjectReference Property CheatTerminal_AidStorageRef Auto Const
Static Property XMarker Auto Const
FormList Property CheatTerminal_WorkshopObjectsToBackup Auto Const
FormList Property CheatTerminal_WorkshopObjectsToBackupXMarkers Auto Const
FormList Property CheatTerminal_ScrapObjectsToBackup Auto Const
FormList Property CheatTerminal_ScrapObjectsToBackupXMarkers Auto Const
