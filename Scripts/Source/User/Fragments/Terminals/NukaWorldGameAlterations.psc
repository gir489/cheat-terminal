;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:NukaWorldGameAlterations Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference mapMarker = Game.GetFormFromFile(0x0004CD96, "DLCNukaWorld.esm") as ObjectReference
If ( mapMarker != None )
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004CD95, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004DF89, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004CBC7, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0000B94B, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004C5ED, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004190A, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004C5EA, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004C5EB, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004C7A5, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004B06E, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004E0EE, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004DF85, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004DF87, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004CD93, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0000E767, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004C76A, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0001CD2A, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004190C, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004CD94, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004C5E7, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004CD9B, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004CD99, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0001EFCE, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0000E76B, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0000B8E3, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x000557ED, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0002446D, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x00017647, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004C76C, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004C76E, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0000ADB6, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0000E76A, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x000260E3, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004CBC1, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004CBC2, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0000E768, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0000E775, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004CBC3, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004DF88, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0000E769, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004C2A0, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0001EFCD, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
	mapMarker = Game.GetFormFromFile(0x0004E132, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(true)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CheatTerminal_NukaWorld_BroadsiderHoopShotMessage.Show() == 0 )
	ObjectReference spawnAtMe = Game.GetPlayer().PlaceAtMe(Broadsider)
	spawnAtMe.AttachMod(mod_Legendary_Weapon_UnlimitedAmmoCapacity)
	spawnAtMe.AttachMod(CheatTerminal_NoRecoilRapidFireAutomatic)
	Game.GetPlayer().AddItem(spawnAtMe)
	Game.GetPlayer().AddItem(AmmoCannonBall, 10000)
endIf
FormList HoopshotTriggerObjects = Game.GetFormFromFile(0x00051E43, "DLCNukaWorld.esm") as FormList
HoopshotTriggerObjects.Revert()
HoopshotTriggerObjects.AddForm(ExplosionBroadsiderBase)
CheatTerminal_NukaWorld_BroadsiderHoopShot.SetValueInt(1)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0000A5B1, "DLCNukaWorld.esm")
if ( formFromMod )
	ObjectReference gageRef = formFromMod as ObjectReference
	CheatTerminalGlobalFuncs.DoCompanionTeleport(akTerminalRef, gageRef)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
FormList HoopshotTriggerObjects = Game.GetFormFromFile(0x00051E43, "DLCNukaWorld.esm") as FormList
HoopshotTriggerObjects.Revert()
CheatTerminal_NukaWorld_BroadsiderHoopShot.SetValueInt(0)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
GlobalVariable glov = Game.GetFormFromFile(0x00053884, "DLCNukaWorld.esm") as GlobalVariable
if ( glov != None )
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x00031709, "DLCNukaWorld.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x000496F5, "DLCNukaWorld.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x000496F4, "DLCNukaWorld.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x000496F6, "DLCNukaWorld.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x000496F1, "DLCNukaWorld.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x000496F8, "DLCNukaWorld.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x000496F3, "DLCNukaWorld.esm") as GlobalVariable
	glov.SetValue(1)
	glov = Game.GetFormFromFile(0x0004964E, "DLCNukaWorld.esm") as GlobalVariable
	glov.SetValue(1)
	Quest qst = Game.GetFormFromFile(0x0004430B, "DLCNukaWorld.esm") as Quest
	qst.SetStage(10)
	qst.SetStage(20)
	qst.SetStage(30)
	qst.SetStage(40)
	qst.SetStage(50)
	qst.SetStage(60)
	qst.SetStage(70)
	qst.SetStage(80)
	qst.SetStage(90)
	qst.SetStage(100)
	qst.SetStage(110)
	qst.SetStage(120)
	qst.SetStage(130)
	qst.SetStage(140)
	qst.SetStage(150)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0000D0D1, "DLCNukaWorld.esm")
if ( formFromMod)
	ObjectReference DLC04MS01RewardDoor = formFromMod as ObjectReference
	DLC04MS01RewardDoor.SetLockLevel(0)
	DLC04MS01RewardDoor.SetOpen()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Quest DLC04MQ05 = Game.GetFormFromFile(0x00000805, "DLCNukaWorld.esm") as Quest
DLC04MQ05.SetStage(5)
DLC04MQ05.SetActive()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
(Game.GetFormFromFile(0x06051A94, "DLCNukaWorld.esm") as ObjectReference).Reset()
(Game.GetFormFromFile(0x06051A74, "DLCNukaWorld.esm") as ObjectReference).Reset()
(Game.GetFormFromFile(0x06051A45, "DLCNukaWorld.esm") as ObjectReference).Reset()
(Game.GetFormFromFile(0x06051A44, "DLCNukaWorld.esm") as ObjectReference).Reset()
(Game.GetFormFromFile(0x06051A3E, "DLCNukaWorld.esm") as ObjectReference).Reset()
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference mapMarker = Game.GetFormFromFile(0x0004CD96, "DLCNukaWorld.esm") as ObjectReference
If ( mapMarker != None )
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004CD95, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004DF89, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004CBC7, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0000B94B, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004C5ED, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004190A, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004C5EA, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004C5EB, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004C7A5, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004B06E, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004E0EE, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004DF85, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004DF87, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004CD93, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0000E767, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004C76A, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0001CD2A, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004190C, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004CD94, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004C5E7, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004CD9B, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004CD99, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0001EFCE, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0000E76B, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0000B8E3, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x000557ED, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0002446D, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x00017647, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004C76C, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004C76E, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0000ADB6, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0000E76A, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x000260E3, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004CBC1, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004CBC2, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0000E768, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0000E775, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004CBC3, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004DF88, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0000E769, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004C2A0, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0001EFCD, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
	mapMarker = Game.GetFormFromFile(0x0004E132, "DLCNukaWorld.esm") as ObjectReference
	mapMarker.AddToMap(false)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
GlobalVariable glov = Game.GetFormFromFile(0x0004964E, "DLCNukaWorld.esm") as GlobalVariable
glov.SetValue(1)
glov = Game.GetFormFromFile(0x000496F1, "DLCNukaWorld.esm") as GlobalVariable
glov.SetValue(1)
glov = Game.GetFormFromFile(0x000496F8, "DLCNukaWorld.esm") as GlobalVariable
glov.SetValue(1)
glov = Game.GetFormFromFile(0x000496F3, "DLCNukaWorld.esm") as GlobalVariable
glov.SetValue(1)
glov = Game.GetFormFromFile(0x000496F4, "DLCNukaWorld.esm") as GlobalVariable
glov.SetValue(1)
glov = Game.GetFormFromFile(0x000496F5, "DLCNukaWorld.esm") as GlobalVariable
glov.SetValue(1)
glov = Game.GetFormFromFile(0x000496F6, "DLCNukaWorld.esm") as GlobalVariable
glov.SetValue(1)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.DoMerchantTeleport(akTerminalRef, Game.GetFormFromFile(0x0001E309, "DLCNukaWorld.esm") as ObjectReference)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
GlobalVariable Property CheatTerminal_WorkbenchPersist Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
GlobalVariable Property CheatTerminal_NukaWorld_BroadsiderHoopShot Auto Const
Message Property CheatTerminal_NukaWorld_BroadsiderHoopShotMessage Auto Const
Explosion Property ExplosionBroadsiderBase Auto Const
Weapon Property Broadsider Auto Const
ObjectMod Property mod_Legendary_Weapon_UnlimitedAmmoCapacity Auto Const
ObjectMod Property CheatTerminal_NoRecoilRapidFireAutomatic Auto Const
Ammo Property AmmoCannonBall Auto Const
