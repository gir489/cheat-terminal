;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:HalloweenMain Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form objectToSpawn = Game.GetFormFromFile(0x00000723, "ccFSVFO4007-Halloween.esl")
if objectToSpawn
	Actor playerActor = Game.GetPlayer()
	ObjectReference spawnedObjectedRef = playerActor.PlaceAtMe(objectToSpawn, abForcePersist = true, abDeleteWhenAble = false)
	spawnedObjectedRef.SetAngle(0.0, 0.0, 0.0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
