;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_NukaW_01001FA5 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B6E, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B1F, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B6A, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B19, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B66, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B1C, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B6C, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B1D, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B68, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B17, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B64, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B1A, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B6D, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B22, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B69, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B20, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B65, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B21, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B6B, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B1E, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B67, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B18, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B63, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B1B, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
