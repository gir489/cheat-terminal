;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsCompanionSPECIAL Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CompanionRef.GetRef() != None)
	CheatTerminal_Companion_SPECIALHolder.SetValue(CompanionRef.GetRef().GetBaseValue(Game.GetStrengthAV()))
else
	CheatTerminal_Companion_SPECIALHolder.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CompanionRef.GetRef() != None)
	CheatTerminal_Companion_SPECIALHolder.SetValue(CompanionRef.GetRef().GetBaseValue(Game.GetPerceptionAV()))
else
	CheatTerminal_Companion_SPECIALHolder.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CompanionRef.GetRef() != None)
	CheatTerminal_Companion_SPECIALHolder.SetValue(CompanionRef.GetRef().GetBaseValue(Game.GetEnduranceAV()))
else
	CheatTerminal_Companion_SPECIALHolder.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CompanionRef.GetRef() != None)
	CheatTerminal_Companion_SPECIALHolder.SetValue(CompanionRef.GetRef().GetBaseValue(Game.GetCharismaAV()))
else
	CheatTerminal_Companion_SPECIALHolder.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CompanionRef.GetRef() != None)
	CheatTerminal_Companion_SPECIALHolder.SetValue(CompanionRef.GetRef().GetBaseValue(Game.GetIntelligenceAV()))
else
	CheatTerminal_Companion_SPECIALHolder.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CompanionRef.GetRef() != None)
	CheatTerminal_Companion_SPECIALHolder.SetValue(CompanionRef.GetRef().GetBaseValue(Game.GetAgilityAV()))
else
	CheatTerminal_Companion_SPECIALHolder.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
if ( CompanionRef.GetRef() != None)
	CheatTerminal_Companion_SPECIALHolder.SetValue(CompanionRef.GetRef().GetBaseValue(Game.GetLuckAV()))
else
	CheatTerminal_Companion_SPECIALHolder.SetValue(0)
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
ReferenceAlias Property CompanionRef Auto Const
GlobalVariable Property CheatTerminal_Companion_SPECIALHolder Auto Const
