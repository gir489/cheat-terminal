;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM__01007A8E Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Grip_Standard)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Grip_Standard)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Grip_Better1)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Grip_Better1)
endif
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
if ( grabbedRef )
	grabbedRef.AttachMod(mod_10mm_Grip_Better2)
else
	Game.GetPlayer().AddItem(miscmod_mod_10mm_Grip_Better2)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ObjectMod Property mod_10mm_Grip_Standard Auto Const
ObjectMod Property mod_10mm_Grip_Better1 Auto Const
ObjectMod Property mod_10mm_Grip_Better2 Auto Const
MiscObject Property miscmod_mod_10mm_Grip_Standard Auto Const
MiscObject Property miscmod_mod_10mm_Grip_Better1 Auto Const
MiscObject Property miscmod_mod_10mm_Grip_Better2 Auto Const
