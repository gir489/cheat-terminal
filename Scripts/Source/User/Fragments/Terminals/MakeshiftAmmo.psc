;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:MakeshiftAmmo Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000001CC,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000021,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000023,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000F9E,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000000C3,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000020,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000022,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_08
Function Fragment_Terminal_08(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0000001F,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_09
Function Fragment_Terminal_09(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0000001E,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000026,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_11
Function Fragment_Terminal_11(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000025,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_12
Function Fragment_Terminal_12(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000024,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_13
Function Fragment_Terminal_13(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x0000018F,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_14
Function Fragment_Terminal_14(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000190,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_15
Function Fragment_Terminal_15(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x000000F6,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_16
Function Fragment_Terminal_16(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000159,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_17
Function Fragment_Terminal_17(ObjectReference akTerminalRef)
;BEGIN CODE
Form formFromMod = Game.GetFormFromFile(0x00000205,"ccSBJFO4003-Grenade.esl")
if (formFromMod)
	Game.GetPlayer().AddItem(formFromMod, CheatTerminal_AmmoToGive.GetValueInt())
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
GlobalVariable Property CheatTerminal_AmmoToGive Auto Const
