;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_DLC_NukaW_01001FA7 Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B82, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B26, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B81, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B27, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B83, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B23, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B80, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B24, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
Form modToApply = Game.GetFormFromFile(0x00033B7F, "DLCNukaWorld.esm")
Form miscMod = Game.GetFormFromFile(0x00037B25, "DLCNukaWorld.esm")
if modToApply
	ObjectReference grabbedRef = Game.GetPlayerGrabbedRef()
	if ( grabbedRef )
		grabbedRef.AttachMod(modToApply as ObjectMod)
	else
		Game.GetPlayer().AddItem(miscMod)
	endif
endIf
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
