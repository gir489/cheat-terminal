;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:TERM_CheatTerminal_MoveToLoc_01001F0B Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, GlowingSeaPOIDB02MapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, GlowingSeaPOIJB03MapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_03
Function Fragment_Terminal_03(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, RelayTower06ExtMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_04
Function Fragment_Terminal_04(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, VirgilsLabExtMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_05
Function Fragment_Terminal_05(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, GlowingSeaSentinelSiteExtMapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_06
Function Fragment_Terminal_06(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, GlowingSeaPOIDB01MapMarker)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_07
Function Fragment_Terminal_07(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, Vault95MapMarkerRef)
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_10
Function Fragment_Terminal_10(ObjectReference akTerminalRef)
;BEGIN CODE
CheatTerminalGlobalFuncs.TeleportPlayer(akTerminalRef, GlowingSeaPOIJB06MapMarker)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ObjectReference Property GlowingSeaPOIDB02MapMarker Auto Const
ObjectReference Property GlowingSeaPOIJB03MapMarker Auto Const
ObjectReference Property RelayTower06ExtMapMarker Auto Const
ObjectReference Property VirgilsLabExtMapMarker Auto Const
ObjectReference Property GlowingSeaSentinelSiteExtMapMarker Auto Const
ObjectReference Property GlowingSeaPOIDB01MapMarker Auto Const
ObjectReference Property Vault95MapMarkerRef Auto Const
ObjectReference Property GlowingSeaPOIJB06MapMarker Auto Const
