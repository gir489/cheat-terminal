;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Terminals:GameAlterationsWorkshopManipHappiness Extends Terminal Hidden Const

;BEGIN FRAGMENT Fragment_Terminal_01
Function Fragment_Terminal_01(ObjectReference akTerminalRef)
;BEGIN CODE
WorkshopScript workshopRef = CheatTerminalGlobalFuncs.FindClosestWorkshop()
if ( workshopRef )
	workshopRef.SetValue(WorkshopRatingHappinessTarget, 100)
	workshopRef.SetValue(WorkshopRatingBonusHappiness, 100)
	workshopRef.SetValue(WorkshopRatingHappiness, 100)
	workshopRef.SetValue(WorkshopRatingTotalBonusHappiness, 100)
	workshopRef.SetValue(WorkshopRatingHappinessModifier, 100)
	workshopRef.SetValue(WorkshopRatingDamageCurrent, 0)
	workshopRef.SetValue(WorkshopRatingDamageMax, 0)
	workshopRef.SetValue(WorkshopRatingLastAttackDaysSince, 99)
	workshopRef.SetValue(WorkshopRatingDamageFood, 0)
	workshopRef.SetValue(WorkshopRatingDamageWater, 0)
	workshopRef.SetValue(WorkshopRatingDamageSafety, 0)
	workshopRef.SetValue(WorkshopRatingDamagePower, 0)
	workshopRef.SetValue(WorkshopRatingDamagePopulation, 0)
	workshopRef.SetValue(WorkshopRatingMissingFood, 0)
	workshopRef.SetValue(WorkshopRatingMissingWater, 0)
	workshopRef.SetValue(WorkshopRatingMissingBeds, 0)
	workshopRef.SetValue(WorkshopRatingMissingSafety, 0)
else
	CheatTerminal_GameAlterations_MissingWorkshop.Show()
endIf
;END CODE
EndFunction
;END FRAGMENT

;BEGIN FRAGMENT Fragment_Terminal_02
Function Fragment_Terminal_02(ObjectReference akTerminalRef)
;BEGIN CODE
int index = 0
while index < WorkshopParent.WorkshopsCollection.GetCount()
	WorkshopScript workshopRef = WorkshopParent.WorkshopsCollection.GetAt(index) as WorkshopScript
	if ( workshopRef )
		workshopRef.SetValue(WorkshopRatingHappinessTarget, 100)
		workshopRef.SetValue(WorkshopRatingBonusHappiness, 100)
		workshopRef.SetValue(WorkshopRatingHappiness, 100)
		workshopRef.SetValue(WorkshopRatingTotalBonusHappiness, 100)
		workshopRef.SetValue(WorkshopRatingHappinessModifier, 100)
		workshopRef.SetValue(WorkshopRatingDamageCurrent, 0)
		workshopRef.SetValue(WorkshopRatingDamageMax, 0)
		workshopRef.SetValue(WorkshopRatingLastAttackDaysSince, 99)
		workshopRef.SetValue(WorkshopRatingDamageFood, 0)
		workshopRef.SetValue(WorkshopRatingDamageWater, 0)
		workshopRef.SetValue(WorkshopRatingDamageSafety, 0)
		workshopRef.SetValue(WorkshopRatingDamagePower, 0)
		workshopRef.SetValue(WorkshopRatingDamagePopulation, 0)
		workshopRef.SetValue(WorkshopRatingMissingFood, 0)
		workshopRef.SetValue(WorkshopRatingMissingWater, 0)
		workshopRef.SetValue(WorkshopRatingMissingBeds, 0)
		workshopRef.SetValue(WorkshopRatingMissingSafety, 0)
	endIf
	index += 1
endWhile
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment
WorkshopParentScript Property WorkshopParent Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const
ActorValue Property WorkshopRatingHappiness Auto Const
ActorValue Property WorkshopRatingBonusHappiness Auto Const
ActorValue Property WorkshopRatingDamageCurrent Auto Const
ActorValue Property WorkshopRatingDamageMax Auto Const
ActorValue Property WorkshopRatingLastAttackDaysSince Auto Const
ActorValue Property WorkshopRatingDamageFood Auto Const
ActorValue Property WorkshopRatingDamageWater Auto Const
ActorValue Property WorkshopRatingDamageSafety Auto Const
ActorValue Property WorkshopRatingDamagePower Auto Const
ActorValue Property WorkshopRatingDamagePopulation Auto Const
ActorValue Property WorkshopRatingHappinessModifier Auto Const
ActorValue Property WorkshopRatingHappinessTarget Auto Const
ActorValue Property WorkshopRatingMissingFood Auto Const
ActorValue Property WorkshopRatingMissingWater Auto Const
ActorValue Property WorkshopRatingMissingBeds Auto Const
ActorValue Property WorkshopRatingMissingSafety Auto Const
ActorValue Property WorkshopRatingTotalBonusHappiness Auto Const
Message Property CheatTerminal_GameAlterations_MissingWorkshop Auto Const
