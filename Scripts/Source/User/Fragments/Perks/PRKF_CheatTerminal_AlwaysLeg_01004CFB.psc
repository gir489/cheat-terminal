;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Perks:PRKF_CheatTerminal_AlwaysLeg_01004CFB Extends Perk Hidden Const

;BEGIN FRAGMENT Fragment_Entry_01
Function Fragment_Entry_01(ObjectReference akTargetRef, Actor akActor)
;BEGIN CODE
if (LL_EpicChance_Standard.GetValue() != 100.0 || ChanceLegendary.GetValue() != 100.0 )
	ChanceLegendary.SetValue(100.0)
	LL_ChanceNone_PowerArmorMod.SetValue(0.0)
	LL_EpicChance_Armor_Boss.SetValue(100.0)
	LL_EpicChance_Standard.SetValue(100.0)
	LL_EpicChance_VendorBase.SetValue(100.0)
endif
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

GlobalVariable Property ChanceLegendary Auto Const

GlobalVariable Property LL_ChanceNone_PowerArmorMod Auto Const

GlobalVariable Property LL_EpicChance_Armor_Boss Auto Const

GlobalVariable Property LL_EpicChance_Standard Auto Const

GlobalVariable Property LL_EpicChance_VendorBase Auto Const
