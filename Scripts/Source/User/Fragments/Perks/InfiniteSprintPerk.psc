;BEGIN FRAGMENT CODE - Do not edit anything between this and the end comment
Scriptname Fragments:Perks:InfiniteSprintPerk Extends Perk Hidden Const

;BEGIN FRAGMENT Fragment_Entry_04
Function Fragment_Entry_04(ObjectReference akTargetRef, Actor akActor)
;BEGIN CODE
playerActor.RestoreValue(ActorValueToRestore, 180)
;END CODE
EndFunction
;END FRAGMENT

;END FRAGMENT CODE - Do not edit anything between this and the begin comment

ActorValue Property ActorValueToRestore Auto Const

Actor Property playerActor Auto Const
