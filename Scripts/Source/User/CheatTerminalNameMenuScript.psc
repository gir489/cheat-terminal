Scriptname CheatTerminalNameMenuScript extends ActiveMagicEffect

Event OnEffectStart(Actor akTarget, Actor akCaster)
	While (Utility.IsInMenuMode())
		Utility.Wait(0.5)
	EndWhile
	Utility.Wait(0.5)
	Game.ShowSPECIALMenu()
EndEvent
