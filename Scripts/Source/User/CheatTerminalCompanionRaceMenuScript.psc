Scriptname CheatTerminalCompanionRaceMenuScript extends ActiveMagicEffect

Event OnEffectStart(Actor akTarget, Actor akCaster)
	While (Utility.IsInMenuMode())
		Utility.Wait(0.5)
	EndWhile
	Game.ForceFirstPerson()
	Utility.Wait(0.5)
	CheatTerminal_ShowHelpRaceMenu.Show()
	Actor companionActor = FollowerAlias.GetRef() as Actor
	if ( companionActor.IsWeaponDrawn() )
		Weapon curWeapon = companionActor.GetEquippedWeapon()
		companionActor.PlayIdle(RaiderSheath)
	endIf
	companionActor.PlayIdle(CheatTerminal_ElevatorFaceCamera)
	Utility.Wait(3.0)
	Game.ShowRaceMenu(companionActor)
EndEvent

Message Property CheatTerminal_ShowHelpRaceMenu Auto Const
Idle Property RaiderSheath Auto Const
Idle Property CheatTerminal_ElevatorFaceCamera Auto Const
Keyword Property WeaponTypeMelee1H Auto Const
Keyword Property WeaponTypeMelee2H Auto Const
ReferenceAlias Property FollowerAlias Auto Const