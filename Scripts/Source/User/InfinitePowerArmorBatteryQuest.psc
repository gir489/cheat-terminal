Scriptname InfinitePowerArmorBatteryQuest extends Quest

Event Actor.OnSit(Actor akActor, ObjectReference akFurniture)
	Utility.Wait(0.1)
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.IsInPowerArmor() )
		playerActor.SetValue(PowerArmorBattery, 16777217)
	endIf
EndEvent

event ObjectReference.OnItemRemoved(ObjectReference akSource, Form akBaseItem, int aiItemCount, ObjectReference akItemReference, ObjectReference akSourceContainer)
	Utility.Wait(0.1)
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.IsInPowerArmor() )
		playerActor.SetValue(PowerArmorBattery, 16777217)
		if ( playerActor.HasPerk(CheatTerminal_InfiniteAmmo) == false && !Utility.IsInMenuMode() )
			playerActor.AddItem(AmmoFusionCore)
		endIf
	endIf
endEvent

Event OnQuestInit()
	AddInventoryEventFilter(AmmoFusionCore)
	RegisterForRemoteEvent(Game.GetPlayer(), "OnItemRemoved")
	RegisterForRemoteEvent(Game.GetPlayer(), "OnSit")
EndEvent

Event OnQuestShutdown()
	UnregisterForAllRemoteEvents()
EndEvent

Ammo Property AmmoFusionCore Auto Const
ActorValue Property PowerArmorBattery Auto Const
Perk Property CheatTerminal_InfiniteAmmo Auto Const
Keyword Property FurnitureTypePowerArmor Auto Const