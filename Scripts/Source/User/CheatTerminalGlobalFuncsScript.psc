Scriptname CheatTerminalGlobalFuncsScript extends Quest Conditional
;This is a script that the Cheat Terminal uses to reduce code duplication.
;======================================================================================================================================================
;Members
float deltaX
float deltaY
float deltaZ
float yaw
ObjectReference objectToLookAt
Faction factionToManipulate
ActorBase npcSpawnActor
ActorBase tempSpawnActor
;======================================================================================================================================================
;Functions
Function SetNPCSpawnActor(ActorBase akSetActorToSpawnTo)
	npcSpawnActor = akSetActorToSpawnTo
EndFunction
ActorBase Function GetNPCSpawnActor()
	return npcSpawnActor
EndFunction
Function SetTempSpawnActor(ActorBase akTempSpawnActor)
	tempSpawnActor = akTempSpawnActor
EndFunction
Function SetNPCSpawnActorFromDLC(int formId, String dlcName)
	npcSpawnActor = Game.GetFormFromFile(formId, dlcName) as ActorBase
EndFunction
ActorBase Function GetTempSpawnActor()
	return tempSpawnActor
EndFunction
Function SpawnWorkshopItem(Form objectToSpawn, bool allowNoWorkshop = false)
	WorkshopScript workshopRef = FindClosestWorkshop()
	if ( workshopRef )
		ObjectReference objRef = Game.GetPlayer().PlaceAtMe(objectToSpawn)
		objRef.SetAngle(0, 0, objRef.GetAngleZ())
		WorkshopParent.BuildObjectPUBLIC(objRef, workshopRef)
	elseif ( allowNoWorkshop == true )
		bool delete = CheatTerminal_WorkbenchPersist.GetValue() == 0
		ObjectReference objRef = Game.GetPlayer().PlaceAtMe(objectToSpawn, abDeleteWhenAble = delete)
		objRef.SetAngle(0, 0, objRef.GetAngleZ())
	else
		CheatTerminal_GameAlterations_MissingWorkshop.Show()
	endIf
EndFunction
Function DoPotion(Potion akPotion)
	Actor playerActor = Game.GetPlayer()
	if ( CheatTerminal_PersonalStorageMethod.GetValue() == 0 )
		CheatTerminal_ClosePipboy.Show()
	else 
		playerActor.MoveTo(playerActor)
	endIf
	playerActor.EquipItem(akPotion, false, true)
EndFunction
Function SpawnSettler(ActorBase actorToSpawn, bool autoAssign = true)
	WorkshopScript workshopRef = FindClosestWorkshop()
	if ( workshopRef )
		Actor newWorkshopActor = Game.GetPlayer().PlaceActorAtMe(actorToSpawn)
		newWorkshopActor.SetEssential(false)
		newWorkshopActor.SetAngle(0, 0, newWorkshopActor.GetAngleZ())
		WorkshopParent.AddActorToWorkshop(newWorkshopActor as WorkshopNPCScript, workshopRef)
		if ( autoAssign ) 
			WorkshopParent.TryToAutoAssignActor(workshopRef, newWorkshopActor as WorkshopNPCScript)
		endIf
		CheatTerminal_GameAlterations_SpawnedSettler.Show()
	else
		CheatTerminal_GameAlterations_MissingWorkshop.Show()
	endIf
EndFunction
Function DoPerkCheckForGlobal(GlobalVariable globalVar, Perk perkToCheck)
	Actor playerActor = Game.GetPlayer()
	if ( playerActor.HasPerk(perkToCheck) )
		globalVar.SetValue(1.0)
	else
		globalVar.SetValue(0)
	endIf
EndFunction
Function TeleportPlayerToDebug(ObjectReference akTerminalRef, ObjectReference mapMarker, float xDelta = 0.0, float yDelta = 0.0, float zDelta = 0.0, float yawParam = 0.0, ObjectReference objectToLookAtParam = None)
	if ( CheatTerminal_DebugMarkerRef.HasKeyword(CheatTerminal_MarkerActive) == false )
		CheatTerminal_DebugMarkerRef.MoveTo(Game.GetPlayer())
		CheatTerminal_DebugMarkerRef.AddKeyword(CheatTerminal_MarkerActive)
	endif
	TeleportPlayer(akTerminalRef, mapMarker, xDelta, yDelta, zDelta, yawParam, objectToLookAtParam, true)
EndFunction
Function ReturnPlayerFromDebug(ObjectReference akTerminalRef)
	TeleportPlayer(akTerminalRef, CheatTerminal_DebugMarkerRef)
	CheatTerminal_DebugMarkerRef.RemoveKeyword(CheatTerminal_MarkerActive)
EndFunction
Function TeleportPlayer(ObjectReference akTerminalRef, ObjectReference mapMarker, float xDelta = 0.0, float yDelta = 0.0, float zDelta = 0.0, float yawParam = 0.0, ObjectReference objectToLookAtParam = None, bool isDebugCalling = false)
	ObjectReference objToTeleportTo
	deltaX = xDelta
	deltaY = yDelta
	deltaZ = zDelta
	yaw = yawParam
	objectToLookAt = objectToLookAtParam
	if ( isDebugCalling == false )
		CheatTerminal_DebugMarkerRef.RemoveKeyword(CheatTerminal_MarkerActive)
	endIf
	if ( mapMarker.GetLinkedRef() == None )
		objToTeleportTo = mapMarker
	else
		objToTeleportTo = mapMarker.GetLinkedRef()
	endIf
	if ( akTerminalRef != None )
		bool matchRotation = (yaw == 0.0 && objectToLookAt == None )
		Game.GetPlayer().MoveTo(objToTeleportTo, deltaX, deltaY, deltaZ, matchRotation)
		if ( matchRotation == false && yaw != 0.0 )
			Game.GetPlayer().SetAngle(0.0, 0.0, yaw)
		elseif (objectToLookAt != None)
			Game.StartDialogueCameraOrCenterOnTarget(objectToLookAt)
		endIf
		if ( FollowerAlias.GetRef() != None)
			Actor followerActor = FollowerAlias.GetRef() as Actor
			followerActor.MoveTo(Game.GetPlayer())
		endIf
	else
		if ( CheatTerminal_MoveToMethod.GetValue() == 0 )
			if ( MoveToMapMarkerHolder.GetRef() == None )
				RegisterForMenuOpenCloseEvent("PipboyMenu")
			endIf
			MoveToMapMarkerHolder.ForceRefTo(objToTeleportTo)
			if ( CheatTerminal_ReadMeEnable.GetValue() == 1.0 )
				CheatTerminal_ClosePipboy.Show()
			endIf
		else
			bool matchRotation = (yaw == 0.0 && objectToLookAt == None )
			Game.GetPlayer().MoveTo(objToTeleportTo, deltaX, deltaY, deltaZ, matchRotation)
			if ( matchRotation == false && yaw != 0.0 )
				Game.GetPlayer().SetAngle(0.0, 0.0, yaw)
			elseif (objectToLookAt != None)
				Game.StartDialogueCameraOrCenterOnTarget(objectToLookAt)
			endIf
			if ( FollowerAlias.GetRef() != None)
				Actor followerActor = FollowerAlias.GetRef() as Actor
				followerActor.MoveTo(Game.GetPlayer())
			endIf
		endIf
	endIf
EndFunction
Event OnMenuOpenCloseEvent(string asMenuName, bool abOpening)
	if ( abOpening == false )
		bool matchRotation = (yaw == 0.0 && objectToLookAt == None )
		Game.GetPlayer().MoveTo(MoveToMapMarkerHolder.GetRef(), deltaX, deltaY, deltaZ, matchRotation)
		if ( matchRotation == false && yaw != 0.0 )
			Game.GetPlayer().SetAngle(0.0, 0.0, yaw)
		elseif (objectToLookAt != None)
			Game.StartDialogueCameraOrCenterOnTarget(objectToLookAt)
		endIf
		if ( FollowerAlias.GetRef() != None)
			Actor followerActor = FollowerAlias.GetRef() as Actor
			followerActor.MoveTo(Game.GetPlayer())
		endIf
		MoveToMapMarkerHolder.Clear()
		UnRegisterForMenuOpenCloseEvent("PipboyMenu")
	endIf
EndEvent
Function DoCompanionTeleport(ObjectReference akTerminalRef, ObjectReference akCompanionRef)
	if ( akCompanionRef.IsDisabled() == false )
		if ( CheatTerminal_Config_CompanionTeleportMethod.GetValue() == 0 )
			akCompanionRef.MoveTo(Game.GetPlayer())
		else
			TeleportPlayer(akTerminalRef, akCompanionRef)
		endIf
	else
		CheatTerminal_Companion_Disabled.Show()
	endIf
EndFunction
Function DoMerchantTeleport(ObjectReference akTerminalRef, ObjectReference akMerchantRef)
	Actor actorMerchant = akMerchantRef as Actor
	if ( (actorMerchant.IsDead() || akMerchantRef.IsDisabled()) && CheatTerminal_MerchantDead.Show() == 1 )
		return
	endIf
	if ( CheatTerminal_Config_MerchantTeleportMethod.GetValue() == 0 )
		akMerchantRef.MoveTo(Game.GetPlayer())
	else
		TeleportPlayer(akTerminalRef, akMerchantRef)
	endIf
EndFunction
Function SpawnCustomItem(LeveledItem LeveledListToSpawnFrom, Formlist ModsFormlist)
	Actor playerActor = Game.GetPlayer()
	ObjectReference spawnedItem = playerActor.PlaceAtMe(LeveledListToSpawnFrom)
	int i = 0
	While (i < ModsFormlist.GetSize())
		spawnedItem.AttachMod(ModsFormlist.GetAt(i) as ObjectMod)
		i += 1
	EndWhile
	playerActor.AddItem(spawnedItem)
EndFunction
Function SetFaction(Faction factionToSet)
	factionToManipulate = factionToSet
	if ( factionToManipulate.IsPlayerExpelled() || factionToManipulate.IsPlayerEnemy() )
		CheatTerminal_GameAlterations_IsPlayerEnemy.SetValue(1)
	else
		CheatTerminal_GameAlterations_IsPlayerEnemy.SetValue(factionToManipulate.GetFactionReaction(Game.GetPlayer()))
	endIf
EndFunction
Function SetFactionLevel(int level)
	if ( level == 0 )
		factionToManipulate.SetEnemy(PlayerFaction, true, true)
		factionToManipulate.SetPlayerEnemy(false)
		factionToManipulate.SetPlayerExpelled(false)
	elseif ( level == 1 )
		factionToManipulate.SetEnemy(PlayerFaction)
		factionToManipulate.SetPlayerEnemy(true)
		factionToManipulate.SetPlayerExpelled(true)
	elseif ( level == 2 )
		factionToManipulate.SetAlly(PlayerFaction)
		factionToManipulate.SetPlayerEnemy(false)
		factionToManipulate.SetPlayerExpelled(false)
	elseif ( level == 3 )
		factionToManipulate.SetAlly(PlayerFaction, true, true)
		factionToManipulate.SetPlayerEnemy(false)
		factionToManipulate.SetPlayerExpelled(false)
	endIf
	CheatTerminal_GameAlterations_IsPlayerEnemy.SetValue(factionToManipulate.GetFactionReaction(Game.GetPlayer()))
EndFunction
Function SpawnCustomCompanion(ActorBase actorToAdd)
	if ( FollowerAlias.GetRef() == None)
		Actor NPC = Game.GetPlayer().PlaceActorAtMe(actorToAdd, 3)
		NPC.SetAngle(0, 0, NPC.GetAngleZ())
		NPC.SetRelationshipRank(Game.GetPlayer(), 4)
		NPC.SetPlayerTeammate(true, true, false)
		NPC.AllowCompanion(true, true)
		NPC.AddKeyword(CheatTerminal_MarkCompanion)
		NPC.FollowerFollow()
		NPC.SetEssential(true)
		PlayerHasActiveCompanion.SetValue(1)
		CheatTerminal_SpawnedCompanion.Show()
	else
		CheatTerminal_AlreadyHaveCompanion.show()
	endIf
EndFunction
Function DismissCustomCompanion()
	if ( FollowerAlias.GetRef() != None)
		Actor followerActor = FollowerAlias.GetRef() as Actor
		if ( followerActor.HasKeyword(CheatTerminal_MarkCompanion) )
			followerActor.DisallowCompanion(true)
			FollowerAlias.Clear()
			followerActor.Delete()
			PlayerHasActiveCompanion.SetValue(0)
		endIf
	endif
EndFunction
WorkshopScript Function FindClosestWorkshop()
	WorkshopScript workshopFromLocation = WorkshopParent.GetWorkshopFromLocation(Game.GetPlayer().GetCurrentLocation())
	if ( workshopFromLocation != None )
		return workshopFromLocation
	else
		if(CheatTerminal_HasDLC_FarHarbor.GetValue() && !CheatTerminal_AllWorkbenches.HasForm(Game.GetFormFromFile(0x00023A96, "DLCCoast.esm")))
			CheatTerminal_AllWorkbenches.AddForm(Game.GetFormFromFile(0x00023A96, "DLCCoast.esm"))
		endIf
		return Game.FindClosestReferenceOfAnyTypeInListFromRef(CheatTerminal_AllWorkbenches, Game.GetPlayer(), 5000.0) as WorkshopScript
	endIf
EndFunction
Function CheckDLC()
	CheatTerminal_HasDLC_9mm.SetValue((Game.GetFormFromFile(0x00000F99, "9mmpistol.esp") != None) as Float)
	CheatTerminal_HasDLC_Automatron.SetValue((Game.GetFormFromFile(0x0000096E, "DLCRobot.esm") != None) as Float)
	CheatTerminal_HasDLC_Contraptions.SetValue((Game.GetFormFromFile(0x00000A96, "DLCworkshop02.esm") != None) as Float)
	CheatTerminal_HasDLC_FarHarbor.SetValue((Game.GetFormFromFile(0x00000800, "DLCCoast.esm") != None) as Float)
	CheatTerminal_HasDLC_NukaWorld.SetValue((Game.GetFormFromFile(0x00000800, "DLCNukaWorld.esm") != None) as Float)
	CheatTerminal_HasDLC_VaultTecWorkshop.SetValue((Game.GetFormFromFile(0x00000FF0, "DLCworkshop03.esm") != None) as Float)
	CheatTerminal_HasDLC_WastelandWorkshop.SetValue((Game.GetFormFromFile(0x00000919, "DLCworkshop01.esm") != None) as Float)
	bool has_hellfire = Game.GetFormFromFile(0x00000806, "ccbgsfo4044-hellfirepowerarmor.esl") != None
	bool has_X02 = Game.GetFormFromFile(0x00000970, "ccbgsfo4115-x02.esl") != None
	bool has_tesla = Game.GetFormFromFile(0x00000F99, "ccbgsfo4046-tescan.esl") != None
	bool has_heavy = Game.GetFormFromFile(0x00000809, "ccbgsfo4116-heavyflamer.esl") != None
	bool has_makeshift = Game.GetFormFromFile(0x00000F99, "ccSBJFO4003-Grenade.esl") != None
	bool has_enclave = Game.GetFormFromFile(0x000001FC, "ccOTMFO4001-Remnants.esl") != None
	bool has_halloween = Game.GetFormFromFile(0x0000070B, "ccFSVFO4007-Halloween.esl") != None
	CheatTerminal_HasDLC_NextGen.SetValue((has_hellfire && has_X02 && has_tesla && has_heavy && has_makeshift && has_enclave && has_halloween) as Float)
	CheatTerminal_HasDLC_AssaultCarbine.SetValue((Game.GetFormFromFile(0x00006BD1, "DPAssaultCarbine.esp") != None) as Float)
	CheatTerminal_HasDLC_AssaultCarbine_NukaWorld.SetValue((Game.GetFormFromFile(0x0000320A, "DPAssaultCarbineNukaWorld.esp") != None) as Float)
	CheatTerminal_HasDLC_Wattz.SetValue((Game.GetFormFromFile(0x00000F99, "WattzLaserGun.esp") != None) as Float)
	CheatTerminal_HasDLC_AlienAssaultRifle.SetValue((Game.GetFormFromFile(0x00000800, "alienassaultrifle.esp") != None) as Float)
	CheatTerminal_HasMod_SCARH.SetValue((Game.GetFormFromFile(0x00002E1F, "SCAR-L.esp") != None) as Float)
	CheatTerminal_HasMod_G36.SetValue((Game.GetFormFromFile(0x00000F9E, "G36Complex.esp") != None) as Float)
	CheatTerminal_HasMod_RU556.SetValue((Game.GetFormFromFile(0x00000F99, "RU556.esp") != None) As Float)
	CheatTerminal_HasMod_Factor.SetValue((Game.GetFormFromFile(0x00001EE6, "Factor.esp") != None) As Float)
	CheatTerminal_HasMod_M1Garand.SetValue((Game.GetFormFromFile(0x00000F99, "m1garand.esp") != None) As Float)
	CheatTerminal_HasMod_MG42.SetValue((Game.GetFormFromFile(0x00000FB4, "MG42.esp") != None) As Float)
	CheatTerminal_HasMod_Deagle.SetValue((Game.GetFormFromFile(0x00000F9C, "DesertEagleNV.esp") != None) As Float)
	CheatTerminal_HasMod_AUG.SetValue((Game.GetFormFromFile(0x00000FBF, "SteyrAUGnv.esp") != None) As Float)
	CheatTerminal_HasMod_PL14.SetValue((Game.GetFormFromFile(0x00000FA0, "PL-14.esp") != None) As Float)
	CheatTerminal_HasMod_M60.SetValue((Game.GetFormFromFile(0x00001F2D, "M60.esp") != None) As Float)
	CheatTerminal_HasMod_RPD.SetValue((Game.GetFormFromFile(0x00001066, "RPD.esp") != None) As Float)
	CheatTerminal_HasMod_G3Family.SetValue((Game.GetFormFromFile(0x00000F99, "G3Family.esp") != None) As Float)
	CheatTerminal_HasMod_AQUILA.SetValue((Game.GetFormFromFile(0x00000F99, "AQUILA.esp") != None) As Float)
EndFunction
Function SpawnNPCExplosion(Actor akTarget)
	if ( CheatTerminal_SpawnNPC_ExplosionType.GetValue() == 0 )
		akTarget.PlaceAtMe(TeleportOutPlayerFXExplosion)
	elseif ( CheatTerminal_SpawnNPC_ExplosionType.GetValue() == 1 )
		akTarget.PlaceAtMe(AAADebugExplosion)
	elseif ( CheatTerminal_SpawnNPC_ExplosionType.GetValue() == 2 )
		akTarget.PlaceAtMe(DN128Shieldsplosion)
	elseif ( CheatTerminal_SpawnNPC_ExplosionType.GetValue() == 3 )
		akTarget.PlaceAtMe(BottleCapMineExplosion)
	elseif ( CheatTerminal_SpawnNPC_ExplosionType.GetValue() == 4 )
		akTarget.PlaceAtMe(FireExtinguisherExplosion)
	elseif ( CheatTerminal_SpawnNPC_ExplosionType.GetValue() == 5 )
		akTarget.PlaceAtMe(DN102_LabDemo1Explosion)
	elseif ( CheatTerminal_SpawnNPC_ExplosionType.GetValue() == 6 )
		akTarget.PlaceAtMe(DN102_LabDemo5Explosion)
	elseif ( CheatTerminal_SpawnNPC_ExplosionType.GetValue() == 7 )
		akTarget.PlaceAtMe(GullExplosion)
	elseif ( CheatTerminal_SpawnNPC_ExplosionType.GetValue() == 8 )
		akTarget.PlaceAtMe(SandbagWallExplosion)
	elseif ( CheatTerminal_SpawnNPC_ExplosionType.GetValue() == 9 )
		akTarget.PlaceAtMe(whaleExplosion)
	elseif ( CheatTerminal_SpawnNPC_ExplosionType.GetValue() == 10 )
		akTarget.PlaceAtMe(Game.GetFormFromFile(0x0002522F, "DLCCoast.esm") as Explosion)
	endIf
EndFunction
Function SpawnLeaveNPCExplosion(Actor akTarget)
	if ( CheatTerminal_SpawnNPC_LeaveExplosionType.GetValue() == 0 )
		akTarget.PlaceAtMe(TeleportOutPlayerFXExplosion)
	elseif ( CheatTerminal_SpawnNPC_LeaveExplosionType.GetValue() == 1 )
		akTarget.PlaceAtMe(AAADebugExplosion)
	elseif ( CheatTerminal_SpawnNPC_LeaveExplosionType.GetValue() == 2 )
		akTarget.PlaceAtMe(DN128Shieldsplosion)
	elseif ( CheatTerminal_SpawnNPC_LeaveExplosionType.GetValue() == 3 )
		akTarget.PlaceAtMe(BottleCapMineExplosion)
	elseif ( CheatTerminal_SpawnNPC_LeaveExplosionType.GetValue() == 4 )
		akTarget.PlaceAtMe(FireExtinguisherExplosion)
	elseif ( CheatTerminal_SpawnNPC_LeaveExplosionType.GetValue() == 5 )
		akTarget.PlaceAtMe(DN102_LabDemo1Explosion)
	elseif ( CheatTerminal_SpawnNPC_LeaveExplosionType.GetValue() == 6 )
		akTarget.PlaceAtMe(DN102_LabDemo5Explosion)
	elseif ( CheatTerminal_SpawnNPC_LeaveExplosionType.GetValue() == 7 )
		akTarget.PlaceAtMe(GullExplosion)
	elseif ( CheatTerminal_SpawnNPC_LeaveExplosionType.GetValue() == 8 )
		akTarget.PlaceAtMe(SandbagWallExplosion)
	elseif ( CheatTerminal_SpawnNPC_LeaveExplosionType.GetValue() == 9 )
		akTarget.PlaceAtMe(whaleExplosion)
	elseif ( CheatTerminal_SpawnNPC_LeaveExplosionType.GetValue() == 10 )
		akTarget.PlaceAtMe(Game.GetFormFromFile(0x0002522F, "DLCCoast.esm") as Explosion)
	endIf
EndFunction
;======================================================================================================================================================
; Properties
WorkshopParentScript Property WorkshopParent Auto Const
GlobalVariable Property CheatTerminal_WorkbenchPersist Auto Const
Message Property CheatTerminal_GameAlterations_MissingWorkshop Auto Const
Message Property CheatTerminal_GameAlterations_SpawnedSettler Auto Const
ReferenceAlias Property FollowerAlias Auto Const
ReferenceAlias Property MoveToMapMarkerHolder Auto Const
Message Property CheatTerminal_ClosePipboy Auto Const
Message Property CheatTerminal_Companion_Disabled Auto Const
GlobalVariable Property CheatTerminal_ReadMeEnable Auto Const
GlobalVariable Property CheatTerminal_MoveToMethod Auto Const
GlobalVariable Property CheatTerminal_Config_CompanionTeleportMethod Auto Const
GlobalVariable Property CheatTerminal_Config_MerchantTeleportMethod Auto Const
GlobalVariable Property CheatTerminal_GameAlterations_IsPlayerEnemy Auto Const
GlobalVariable Property CheatTerminal_PersonalStorageMethod Auto Const
GlobalVariable Property PlayerHasActiveCompanion Auto Const
Faction Property PlayerFaction Auto Const
Message Property CheatTerminal_AlreadyHaveCompanion Auto Const
Message Property CheatTerminal_SpawnedCompanion Auto Const
Message Property CheatTerminal_MerchantDead Auto Const
Keyword Property CheatTerminal_MarkCompanion Auto Const
FormList Property CheatTerminal_AllWorkbenches Auto Const
ObjectReference Property CheatTerminal_DebugMarkerRef Auto Const
Keyword Property CheatTerminal_MarkerActive Auto Const
GlobalVariable Property CheatTerminal_HasDLC_9mm Auto Const
GlobalVariable Property CheatTerminal_HasDLC_Automatron Auto Const
GlobalVariable Property CheatTerminal_HasDLC_Contraptions Auto Const
GlobalVariable Property CheatTerminal_HasDLC_FarHarbor Auto Const
GlobalVariable Property CheatTerminal_HasDLC_NukaWorld Auto Const
GlobalVariable Property CheatTerminal_HasDLC_VaultTecWorkshop Auto Const
GlobalVariable Property CheatTerminal_HasDLC_WastelandWorkshop Auto Const
GlobalVariable Property CheatTerminal_HasDLC_NextGen Auto Const
GlobalVariable Property CheatTerminal_HasDLC_AssaultCarbine Auto Const
GlobalVariable Property CheatTerminal_HasDLC_AssaultCarbine_NukaWorld Auto Const
GlobalVariable Property CheatTerminal_HasDLC_Wattz Auto Const
GlobalVariable Property CheatTerminal_HasDLC_AlienAssaultRifle Auto Const
GlobalVariable Property CheatTerminal_HasMod_SCARH Auto Const
GlobalVariable Property CheatTerminal_HasMod_G36 Auto Const
GlobalVariable Property CheatTerminal_HasMod_RU556 Auto Const
GlobalVariable Property CheatTerminal_HasMod_Factor Auto Const
GlobalVariable Property CheatTerminal_HasMod_M1Garand Auto Const
GlobalVariable Property CheatTerminal_HasMod_MG42 Auto Const
GlobalVariable Property CheatTerminal_HasMod_Deagle Auto Const
GlobalVariable Property CheatTerminal_HasMod_AUG Auto Const
GlobalVariable Property CheatTerminal_HasMod_PL14 Auto Const
GlobalVariable Property CheatTerminal_HasMod_M60 Auto Const
GlobalVariable Property CheatTerminal_HasMod_RPD Auto Const
GlobalVariable Property CheatTerminal_HasMod_G3Family Auto Const
GlobalVariable Property CheatTerminal_HasMod_AQUILA Auto Const
GlobalVariable Property CheatTerminal_SpawnNPC_ExplosionType Auto Const
GlobalVariable Property CheatTerminal_SpawnNPC_LeaveExplosionType Auto Const
Explosion Property TeleportOutPlayerFXExplosion Auto Const
Explosion Property AAADebugExplosion Auto Const
Explosion Property DN128Shieldsplosion Auto Const
Explosion Property BottleCapMineExplosion Auto Const
Explosion Property FireExtinguisherExplosion Auto Const
Explosion Property DN102_LabDemo1Explosion Auto Const
Explosion Property DN102_LabDemo5Explosion Auto Const
Explosion Property GullExplosion Auto Const
Explosion Property SandbagWallExplosion Auto Const
Explosion Property whaleExplosion Auto Const