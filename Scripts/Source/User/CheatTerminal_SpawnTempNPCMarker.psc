Scriptname CheatTerminal_SpawnTempNPCMarker extends ObjectReference Conditional

Actor NPC

Event OnInit()
	NPC = Self.PlaceAtMe(ActorToSpawn, 1, false, true, false) as Actor
	CheatTerminalGlobalFuncs.SpawnNPCExplosion(NPC)
	NPC.SetRelationshipRank(Game.GetPlayer(), 4)
	NPC.SetPlayerTeammate(true, true, false)
	NPC.SetEssential(true)
	NPC.Enable(true)
	NPC.SetAngle(0, 0, NPC.GetAngleZ()) ;We need to remove the rotation, if any, so they don't moonwalk.
	NPC.MoveTo(self)
	NPC.AddKeyword(PowerArmorPreventArmorDamageKeyword)
	NPC.RemoveFromAllFactions()
	NPC.AddToFaction(PlayerFaction)
	NPC.SetValue(Health, 1000000)
	RegisterForRemoteEvent(NPC, "OnCombatStateChanged")
	StartTimer(CheatTerminal_SpawnNPC_Duration.GetValue())
EndEvent

Event Actor.OnCombatStateChanged(Actor akSender, Actor akTarget, int aeCombatState)
	if ( akSender == NPC && akTarget == Game.GetPlayer() )
		NPC.StopCombat()
		NPC.SetRelationshipRank(Game.GetPlayer(), 4)
		NPC.SetPlayerTeammate(true, true, false)
	endIf
EndEvent

Event OnTimer(int aiTimerID)
	CheatTerminalGlobalFuncs.SpawnLeaveNPCExplosion(NPC)
	NPC.Disable()
	NPC.Delete()
	self.Delete()
EndEvent

ActorBase Property ActorToSpawn Auto Const
Faction Property PlayerFaction Auto Const
ActorValue Property Health Auto Const
Keyword Property PowerArmorPreventArmorDamageKeyword Auto Const
GlobalVariable Property CheatTerminal_SpawnNPC_Duration Auto Const
CheatTerminalGlobalFuncsScript Property CheatTerminalGlobalFuncs Auto Const