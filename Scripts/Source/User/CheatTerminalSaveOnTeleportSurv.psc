Scriptname CheatTerminalSaveOnTeleportSurv extends Quest

Event OnQuestInit()
   RegisterForRemoteEvent(Game.GetPlayer(), "OnDifficultyChanged")
   RegisterForPlayerTeleport()
EndEvent

Event OnQuestShutdown()
	UnRegisterForRemoteEvent(Game.GetPlayer(), "OnDifficultyChanged")
	UnregisterForPlayerTeleport()
EndEvent

Event Actor.OnDifficultyChanged(Actor akSource, int aOldDifficulty, int aNewDifficulty)
	if (aOldDifficulty == 6) && (aNewDifficulty != 6)
		self.Stop()
	EndIf
EndEvent

Event OnPlayerTeleport()
	While (Utility.IsInMenuMode())
		Utility.Wait(0.1)
	EndWhile
	Utility.Wait(0.5)
	Game.RequestAutoSave()
EndEvent