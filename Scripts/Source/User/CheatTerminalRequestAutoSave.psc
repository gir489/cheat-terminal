Scriptname CheatTerminalRequestAutoSave extends ActiveMagicEffect

Event OnEffectStart(Actor akTarget, Actor akCaster)
	While (Utility.IsInMenuMode())
		Utility.Wait(0.1)
	EndWhile
	Utility.Wait(0.5)
	Game.RequestAutoSave()
EndEvent